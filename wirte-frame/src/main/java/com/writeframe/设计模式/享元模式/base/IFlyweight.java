package com.writeframe.设计模式.享元模式.base;

/**
 * @ClassName IFlyweight
 * @Description 享元角色，顶层接口
 */
public interface IFlyweight {
    //一般需要传入外部状态。
    void operation(String extrinsicState);
}
