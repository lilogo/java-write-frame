package com.lean.chat.mapper;


import com.lean.chat.entitiy.LoginUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Lenovo
 */
@Mapper
public interface LoginMapper {
	LoginUser getUserByName(String name);

	LoginUser getUserById(Integer id);
}
