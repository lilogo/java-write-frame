package com.writeframe.cas;

/**
 * @ClassName: WriteCas
 * @Description: 手写模拟CAS
 * CAS算法是什么？
            CAS是英文单词CompareAndSwap的缩写，中文意思是：比较并替换。CAS需要有3个操作数：内存地址V，旧的预期值A，即将要更新的目标值B。
            CAS指令执行时，当且仅当内存地址V的值与预期值A相等时，将内存地址V的值修改为B，否则就什么都不做。整个比较并替换的操作是一个原子操作。
            CAS是乐观锁技术，当多个线程尝试使用CAS同时更新同一个变量时，只有其中一个线程能更新变量的值，而其它线程都失败，失败的线程并不会被挂起，
            而是被告知这次竞争中失败，并可以再次尝试。
            只能针对单个变量的操作，不能用于多个标量实现原子操作
            循环+CAS的实现让所有的线程处于高频运行，争抢CPU的执行，长时间不成功，会带来很大的CPU资源消耗。
 * @Author: zhanghongwei
 * @Date: 2022/4/29 13:45
 */
public class WriteCas {
    //内存中的值
    private  volatile  int innerValue;

    public synchronized int getInnerValue(){
        return this.innerValue;
    }

    //比较交换值 内存地址V，旧的预期值A，即将要更新的目标值B
    public synchronized boolean CompareAndSet(int expectValue,int newValue){
        //如果预期值等于内存值，说明可以进行更改
        if(innerValue==expectValue){
            innerValue = newValue;
            return true;
        }
        return false;
    }
}
