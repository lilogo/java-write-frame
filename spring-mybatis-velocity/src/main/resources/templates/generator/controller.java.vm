package ${package.Controller};


import ${package.Entity}.${entity};
import ${package.Service}.${entity}Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import ${cfg.rPath};

import javax.servlet.http.HttpSession;
import java.util.List;

#if(${restControllerStyle})
import org.springframework.web.bind.annotation.RestController;
#else
import org.springframework.stereotype.Controller;
#end
#if(${superControllerClassPackage})
import ${superControllerClassPackage};
#end

/**
 * <p>
 * $!{table.comment} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
#if(${restControllerStyle})
@RestController
#else
@Controller
#end
@RequestMapping("${table.entityPath}")
#if(${kotlin})
class ${table.controllerName}#if(${superControllerClass}) : ${superControllerClass}()#end

#else
    #if(${superControllerClass})
public class ${table.controllerName} extends ${superControllerClass} {
    #else
public class ${table.controllerName} {
    #end

    @Autowired
    private ${entity}Service ${table.entityPath}Service;
    /**
     * 跳转列表
     */
    @RequestMapping("list")
    public ModelAndView list(){
        ModelAndView mv=new ModelAndView();
        mv.setViewName("${table.entityPath}/list");
        return mv;
    }

    /**
    * 跳转添加页面
    */
    @RequestMapping("toAdd")
    public ModelAndView toAdd(){
        ModelAndView mv=new ModelAndView();
        mv.setViewName("${table.entityPath}/add");
        return mv;
    }

    /**
    * 跳转编辑页面
    */
    @RequestMapping("toEdit")
    public ModelAndView toEdit(int id){
        ${entity} ${table.entityPath} = ${table.entityPath}Service.getById(id);
        ModelAndView mv=new ModelAndView();
        mv.addObject("${table.entityPath}",${table.entityPath});
        mv.setViewName("${table.entityPath}/edit");
        return mv;
    }


    /**
    * 编辑接口
    */
    @RequestMapping("edit")
    public R edit(${entity} ${table.entityPath}){
        ${table.entityPath}Service.updateById(${table.entityPath});
        return new R().update();
    }

    /**
    * 新增接口
    */
    @PostMapping("add")
    public R add(${entity} ${table.entityPath}){
        ${table.entityPath}Service.save(${table.entityPath});
        return new R().add();
    }

    /**
    * 删除接口
    */
    @RequestMapping("delete")
    public R deleteByIds(@RequestBody List<Integer> ids){
        ${table.entityPath}Service.removeByIds(ids);
        return new R().delete();
    }

    /**
    * 分页查询
    */
    @GetMapping("data")
    public R<List<${entity}>> data(${entity} ${table.entityPath},int page,int limit,HttpSession session){
        // 分页查询
        return ${table.entityPath}Service.queryPage(${table.entityPath},page,limit,session);
    }


}
#end