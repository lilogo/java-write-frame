package com.writeframe.baselock;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.Semaphore;
import java.util.function.Function;

/**
 * 对象池呢，指的是一次性创建出 N 个对象，之后所有的线程重复利用这 N 个对象，
 * 当然对象在被释放前，也是不允许其他线程使用的。对象池， 可以用 List 保存实例对象，
 * 这个很简单。但关键是限流器的设计， 这里的限流，指的是不允许多于 N线程同时进入临界区。
 * 那如何快速实现一个这样的限流器呢？这种场景，我立刻就想到了信号量的解决方案。
 *
 * @param <T>
 * @param <R>
 */
public class SemaphorePoolDemo<T, R> {
    private final List<T> pool;
    // 用信号量实现限流器
    private final Semaphore sem;

    // 构造函数
    SemaphorePoolDemo(int size, T t) {
        pool = new Vector<T>() {};
        for (int i = 0; i < size; i++) {
            pool.add(t);
        }
        sem = new Semaphore(size);
    }

    // 利用对象池的对象，调用 func
    R exec(Function<T, R> func) {
        T t = null;
        try {
            sem.acquire();
            t = pool.remove(0);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.add(t);
            sem.release();
        }
        return func.apply(t);
    }

    public static void main(String[] args) {
        // 创建对象池
        SemaphorePoolDemo<Long, String> pool = new SemaphorePoolDemo<Long, String>(10, 2L);
        // 通过对象池获取 t，之后执行
        pool.exec(t -> {
            System.out.println(t);
            return t.toString();
        });
    }
}
