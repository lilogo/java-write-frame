package com.writeframe.设计模式.代理模式.staticproxy;

/**
 * @ClassName ParentProxy
 * @Description 父母 代理类
 *
 * 静态代理：
 * 　　只能代理某特定的目标对象。如：房产中介，只能代理租客。
 */
public class ParentProxy implements IPerson {
    private IPerson person;

    public ParentProxy(IPerson person) {
        this.person = person;
    }

    @Override
    public void findLove() {
        System.out.println("开始帮儿子，物色对象");
        person.findLove();
        System.out.println("儿子说，ok,开始交往");
    }

    public static void main(String[] args) {
        Person person = new Person();
        ParentProxy proxy = new ParentProxy(person);
        proxy.findLove();
    }
}
