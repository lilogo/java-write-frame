package com.lean.mybatis.datascope;

import cn.hutool.extra.spring.SpringUtil;
import com.lean.mybatis.service.MenuService;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Properties;

@Intercepts({ @Signature(method = "prepare", type = StatementHandler.class, args = { Connection.class,Integer.class }) })
public class DataScopeInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        StatementHandler handler = (StatementHandler)invocation.getTarget();
        //由于mappedStatement中有我们需要的方法id,但却是protected的，所以要通过反射获取
        MetaObject statementHandler = SystemMetaObject.forObject(handler);
        MappedStatement mappedStatement = (MappedStatement) statementHandler.getValue("delegate.mappedStatement");
        //没自定义注解直接按通过算
        DataAuthSelect dataAuth = getDataAuth(mappedStatement);
        if (dataAuth == null) {
            return invocation.proceed();
        }
        //判断是否登录 TODO　
        //获取sql SELECT * FROM t_role WHERE id = ? and is_valid = 0
        BoundSql boundSql = handler.getBoundSql();
        String sql = boundSql.getSql();
        //获得方法类型 (如select,update)
        SqlCommandType sqlCommandType = mappedStatement.getSqlCommandType();
        if ("SELECT".equalsIgnoreCase(sqlCommandType.toString())) {
            //增强sql代码块  这里可通过判断用户权限为不通类型的用户拼接不同的sql
            //根据用户id和机构id 获取数据权限范围
            MenuService menuService = SpringUtil.getBean(MenuService.class);
            DataScope dataScope = menuService.getDataScope(null, null);
            // 拼接新SQL
            // SELECT * FROM t_role WHERE id = ? AND is_valid = 0 AND  (create_id=10)
            // SELECT * FROM t_role WHERE id = ? AND is_valid = 0 AND  (org_id in(2,21) or create_id=10)
            sql=getSelect(sql,dataScope);
            //将增强后的sql放回
            statementHandler.setValue("delegate.boundSql.sql",sql);
        }
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object o) {
        //生成代理对象
        return Plugin.wrap(o, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }

    /**
     * 通过反射获取mapper方法是否加了自定义注解
     */
    private DataAuthSelect getDataAuth(MappedStatement mappedStatement) throws ClassNotFoundException {
        DataAuthSelect dataAuth = null;
        String id = mappedStatement.getId();
        String className = id.substring(0, id.lastIndexOf("."));
        String methodName = id.substring(id.lastIndexOf(".") + 1);
        final Class<?> cls = Class.forName(className);
        final Method[] methods = cls.getMethods();
        for (Method method : methods) {
            if (method.getName().equals(methodName) && method.isAnnotationPresent(DataAuthSelect.class)) {
                dataAuth = method.getAnnotation(DataAuthSelect.class);
                break;
            }
        }
        return dataAuth;
    }

    private String getSelect(String buildSql, DataScope scope){
        try {
            Select select = (Select) CCJSqlParserUtil.parse(buildSql);
            PlainSelect plainSelect = (PlainSelect) select.getSelectBody();

            Expression expression = plainSelect.getWhere();
            if(expression == null){
                plainSelect.setWhere(new StringValue(scope.getSqlFilter()));
            }else{
                AndExpression andExpression =  new AndExpression(expression, new StringValue(scope.getSqlFilter()));
                plainSelect.setWhere(andExpression);
            }

            return select.toString().replaceAll("'", "");
        }catch (JSQLParserException e){
            return buildSql;
        }
    }
}
