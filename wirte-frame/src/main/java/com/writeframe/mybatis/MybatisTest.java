package com.writeframe.mybatis;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Random;

public class MybatisTest {

    public static void main(String[] args) throws Exception {
        // selectData();
        // insertData();
        // deleteData(1);
        updateUser();
        selectData();
    }

    public static void selectData() throws Exception {
        // 1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建 SqlSessionFactory 的构建者对象
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        // 3.使用构建者创建工厂对象 SqlSessionFactory
        SqlSessionFactory factory = builder.build(in);
        // 4.使用 SqlSessionFactory 生产 SqlSession 对象
        SqlSession session = factory.openSession();
        // 5.使用 SqlSession 创建 dao 接口的代理对象
        UserDao userDao = session.getMapper(UserDao.class);
        // 6.使用代理对象执行查询所有方法 List<User>
        List<User> users = userDao.findAll();
        for (User user : users) {
            System.out.println(user);
        }
        // 7.释放资源
        session.close();
        in.close();
    }

    public static void insertData() throws Exception {
        // 1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建 SqlSessionFactory 的构建者对象
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        // 3.使用构建者创建工厂对象 SqlSessionFactory
        SqlSessionFactory factory = builder.build(in);
        // 4.使用 SqlSessionFactory 生产 SqlSession 对象
        SqlSession session = factory.openSession();
        // 5.使用 SqlSession 创建 dao 接口的代理对象
        UserDao userDao = session.getMapper(UserDao.class);
        // 6.使用代理对象执行查询所有方法 List<User>
        User user = new User();
        user.setUsername("b");
        user.setBirthday("b");
        user.setSex("b");
        user.setAddress("b");
        userDao.saveUser(user);
        System.out.println(user);
        // 7.释放资源
        session.close();
        in.close();
    }

    public static void deleteData(int id) throws Exception {
        // 1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建 SqlSessionFactory 的构建者对象
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        // 3.使用构建者创建工厂对象 SqlSessionFactory
        SqlSessionFactory factory = builder.build(in);
        // 4.使用 SqlSessionFactory 生产 SqlSession 对象
        SqlSession session = factory.openSession();
        // 5.使用 SqlSession 创建 dao 接口的代理对象
        UserDao userDao = session.getMapper(UserDao.class);
        int deleteUser = userDao.deleteUser(id);
        System.out.println(deleteUser);
        // 7.释放资源
        session.close();
        in.close();
    }

    public static void updateUser() throws Exception {
        // 1.读取配置文件
        InputStream in = Resources.getResourceAsStream("mybatis-config.xml");
        // 2.创建 SqlSessionFactory 的构建者对象
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        // 3.使用构建者创建工厂对象 SqlSessionFactory
        SqlSessionFactory factory = builder.build(in);
        // 4.使用 SqlSessionFactory 生产 SqlSession 对象
        SqlSession session = factory.openSession();
        // 5.使用 SqlSession 创建 dao 接口的代理对象
        UserDao userDao = session.getMapper(UserDao.class);
        // 6.使用代理对象执行查询所有方法
        int userName = userDao.updateUserName(new Random().nextInt(10) + 1 + "_a", 2);
        System.out.println(userName);
        // 7.释放资源
        session.close();
        in.close();
    }

}

interface UserDao {
    // @Select("select * from t_users")
    List<User> findAll();

    int saveUser(User user);

    int deleteUser(Integer id);

    int updateUserName(String username, Integer id);
}

class User {

    private Integer id;
    private String username;
    private String birthday;
    private String sex;
    private String address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username='" + username + '\'' + ", birthday='" + birthday + '\'' + ", sex='" + sex + '\'' + ", address='" + address
            + '\'' + '}';
    }
}
