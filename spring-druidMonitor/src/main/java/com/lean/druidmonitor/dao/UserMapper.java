package com.lean.druidmonitor.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lean.druidmonitor.entity.User;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 *
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

    /**
     * 批量插入
     * @param list
     * @return
     */
    Integer insertBacth(List<User> list);
}
