package com.lean.mongodb;

import com.lean.mongodb.entity.Teacher;
import com.lean.mongodb.service.TeacherDaoTypeOne;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ClassName: TeacherDaoTypeOneDaoTests
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/6/22 9:36
 */
@SpringBootTest
public class TeacherDaoTypeOneDaoTests {

    @Autowired
    private TeacherDaoTypeOne teacherDaoTypeOne;

    @Test
    public void test(){
        for (int i = 0; i < 100; i++) {
            Teacher teacher=new Teacher();
            teacher.setUsername("111");
            teacherDaoTypeOne.insert(teacher);
        }

    }

}
