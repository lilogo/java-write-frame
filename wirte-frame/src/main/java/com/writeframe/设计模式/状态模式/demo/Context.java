package com.writeframe.设计模式.状态模式.demo;

/**
 * @ClassName Context
 * @Description 上下文对象
 */
public class Context {
    private static  final IState STATE_A = new ConcreteStateA();
    private static  final IState STATE_B = new ConcreteStateB();

    //默认状态
    private IState currentState = STATE_A;
    public void setState(IState state){
        currentState = state;
    }
    public void handle(){
        this.currentState.handle();
    }

}
