package com.lean.cassandra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@EnableCassandraRepositories
@SpringBootApplication
public class SpringLeanCassandraApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringLeanCassandraApplication.class, args);
    }
}
