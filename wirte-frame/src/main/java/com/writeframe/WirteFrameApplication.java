package com.writeframe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WirteFrameApplication {

    public static void main(String[] args) {
        SpringApplication.run(WirteFrameApplication.class, args);
    }

}
