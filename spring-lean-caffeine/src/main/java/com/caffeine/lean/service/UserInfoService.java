package com.caffeine.lean.service;

import com.caffeine.lean.entity.UserInfo;
import com.caffeine.lean.utils.CaffeineUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户模块接口
 */
@Service
@Slf4j
public class UserInfoService {

    //模拟数据库存储数据
    private Map<Integer, UserInfo> userInfoMap = new ConcurrentHashMap<>();

    @Autowired
    private CaffeineUtils caffeineUtils;

    public void addUserInfo(UserInfo userInfo) {
        log.info("create");
        userInfoMap.put(userInfo.getId(), userInfo);
        // 加入缓存
        caffeineUtils.putAndUpdateCache(String.valueOf(userInfo.getId()), userInfo);
    }

    public UserInfo getByName(Integer userId) {
        // 先从缓存读取
        UserInfo userInfo = caffeineUtils.getObjCacheByKey(String.valueOf(userId), UserInfo.class);
        if (userInfo != null) {
            return userInfo;
        }
        // 如果缓存中不存在，则从库中查找
        log.info("get");
        userInfo = userInfoMap.get(userId);
        // 如果用户信息不为空，则加入缓存
        if (userInfo != null) {
            caffeineUtils.putAndUpdateCache(String.valueOf(userInfo.getId()), userInfo);
        }
        return userInfo;
    }

    public UserInfo updateUserInfo(UserInfo userInfo) {
        log.info("update");
        if (!userInfoMap.containsKey(userInfo.getId())) {
            return null;
        }
        // 取旧的值
        UserInfo oldUserInfo = userInfoMap.get(userInfo.getId());
        // 替换内容
        if (StringUtils.isNotBlank(oldUserInfo.getName())) {
            oldUserInfo.setName(userInfo.getName());
        }
        if (StringUtils.isNotBlank(oldUserInfo.getSex())) {
            oldUserInfo.setSex(userInfo.getSex());
        }
        oldUserInfo.setAge(userInfo.getAge());
        // 将新的对象存储，更新旧对象信息
        userInfoMap.put(oldUserInfo.getId(), oldUserInfo);
        // 替换缓存中的值
        caffeineUtils.putAndUpdateCache(String.valueOf(oldUserInfo.getId()), oldUserInfo);
        return oldUserInfo;
    }

    public void deleteById(Integer id) {
        log.info("delete");
        userInfoMap.remove(id);
        // 从缓存中删除
        caffeineUtils.removeCacheByKey(String.valueOf(id));
    }

}
