package com.writeframe.设计模式.原型模式;

import java.lang.reflect.Field;

/**
 * 说明：
 * 　　上面的工具类，使用了反射进行设值，就是一种原型模式的处理。它的本质和set设值是一样的，不过，代码更为优雅。
 */
public class BeanUtils {
    public static Object copy(Object protoType) {
        Class<?> clazz = protoType.getClass();
        Object returnValue = null;
        try {
            returnValue = clazz.newInstance();
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                //根据原型.get,设置给set
                //通过反射调用，returnValue.setXXx(proto.getXXx())
                field.set(returnValue, field.get(protoType));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return returnValue;
    }
}
