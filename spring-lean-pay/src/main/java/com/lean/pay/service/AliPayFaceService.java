package com.lean.pay.service;

public interface AliPayFaceService {
    /**
     * 统一收单交易支付接口
     * @param productId
     * @param type
     * @return
     */
    String createPayOrder(Long productId,Integer type);

    /**
     * 统一收单交易查询
     * @param orderNo
     * @return
     */
    String queryPayOrder(String orderNo);

    /**
     * 统一收单交易退款接口
     * @param orderNo
     * @param reason
     */
    String refund(String orderNo, String reason);

    /**
     * 统一收单交易退款查询
     * @param orderNo
     * @param outRequestNo
     * @return
     */
    String queryRefund(String orderNo,String outRequestNo);

    /**
     * 统一收单交易撤销接口
     * @param orderNo
     */
    void cancelOrder(String orderNo);

    /**
     * 统一收单交易关闭接口
     * @param orderNo
     */
    void closeOrder(String orderNo);

    /**
     * 查询对账单下载地址
     * @param billDate
     * @param type
     * @return
     */
    String queryBill(String billDate, String type);
}
