package com.writeframe.设计模式.责任链模式.loginDemo;


/**
 * 说明：
 * 　　login方法中，需要进行一系列的校验。入参判空校验，用户名存在性校验，用户权限校验。。。
 * 　　这些校验可能会非常复杂，写在一个方法中，方法的职责过重，需要进行拆分。
 */
public class MemberService {
//第一种
//    public void login(String loginName, String loginPass){
//        //入参判空
//        if(StrUtil.isEmpty(loginName) || StrUtil.isEmpty(loginPass)){
//            System.out.println("用户名和密码为空");
//            return;
//        }
//        System.out.println("用户名和密码不为空，可以往下执行了");
//
//        Member member = checkExist(loginName,loginPass);
//        if(null == member){
//            System.out.println("用户不存在");
//            return;
//        }
//        //用户名存在，表示可登录
//        System.out.println("登录成功");
//        if(!"管理员".equals(member.getRoleName())){
//            System.out.println("您不是管理员，没有操作权限");
//        }
//        System.out.println("允许操作");
//    }
//
//    private Member checkExist(String loginName, String loginPass) {
//        Member member = new Member(loginName,loginPass);
//        member.setRoleName("管理员");
//        return member;
//    }



    public void login(String loginName, String loginPass){
        //通过责任链来处理
        Handler validateHandler = new ValidateHandler();
        Handler loginHandler = new LoginHandler();
        Handler authHandler = new AuthHandler();
        loginHandler.next(authHandler);
        validateHandler.next(loginHandler);
        Member member = new Member(loginName, loginPass);
        validateHandler.doHandle(member);
    }

}
