package com.lean.springsecurity.example.service.impl;

import com.lean.springsecurity.example.service.HelloService;
import org.springframework.stereotype.Service;

/**
 *
 * comment:
 */
@Service("helloService")
public class HelloServiceImpl implements HelloService {
}
