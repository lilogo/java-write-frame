package com.lean.itextpdf.example2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.itextpdf.text.pdf.draw.LineSeparator;

public class PdfPrintReport {

    // 定义全局的字体静态变量
    private static Font titlefont;
    private static Font headfont;
    private static Font keyfont;
    private static Font textfont;
    // 最大宽度
    private static int maxWidth = 520;
    // 静态代码块
    static {
        try {
            BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
            titlefont = new Font(bfChinese, 16, Font.BOLD);
            headfont = new Font(bfChinese, 14, Font.BOLD);
            keyfont = new Font(bfChinese, 10, Font.BOLD);
            textfont = new Font(bfChinese, 10, Font.NORMAL);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // main测试
    public static void main(String[] args) throws Exception {
        try {
            // 1.建立一个document对象
            Document document = new Document(PageSize.A4);
            // 2.建立一个书写器(Writer)与document对象关联
            File file = new File("./doc/" + dateNowStr() + ".pdf");
            // 判断文件是否已存在
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
                Watermark watermark = new Watermark("Roc.Y");// 水印方法-类
                writer.setPageEvent(watermark);
                // 3.打开文档
                document.open();
                document.addTitle("标题");// 标题
                document.addAuthor("作者");// 作者
                document.addSubject("Subject pdf sample");// 主题
                document.addKeywords("Keywords");// 关键字
                document.addCreator("Creator");// 创建者

                watermark.onEndPage(writer, document);// 调用水印方法

                // 4.向文档中添加内容
                new PdfPrintReport().generatePDF(document);

                // 5.关闭文档
                document.close();

                System.out.println("文件已创建");
            } else {
                System.out.println("文件已存在");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 生成PDF文件
    public void generatePDF(Document document) throws Exception {

        // 段落
        Paragraph paragraph = new Paragraph("证明", titlefont);
        // 设置文字居中 0靠左 1，居中 2，靠右
        paragraph.setAlignment(1);
        paragraph.setIndentationLeft(12); // 设置左缩进
        paragraph.setIndentationRight(12); // 设置右缩进
        paragraph.setFirstLineIndent(24); // 设置首行缩进
        paragraph.setLeading(20f); // 行间距
        paragraph.setSpacingBefore(5f); // 设置段落上空白
        paragraph.setSpacingAfter(10f); // 设置段落下空白

        // 直线
        Paragraph p1 = new Paragraph();
        p1.add(new Chunk(new LineSeparator()));

        // 点线
        Paragraph p2 = new Paragraph();
        p2.add(new Chunk(new DottedLineSeparator()));

        // 超链接
        Anchor anchor = new Anchor("baidu");
        anchor.setReference("www.baidu.com");

        // 定位
        Anchor gotoP = new Anchor("goto");
        gotoP.setReference("#top");

        // 添加图片
        Image image = Image.getInstance("./doc/aaa.jpg");
        image.setAlignment(Image.ALIGN_CENTER);
        image.scalePercent(40);

        // 表格
        PdfPTable tableData = createTable(new float[] {80, 130, 80, 130, 70, 70});
        PdfPTable table = createTable(new float[] {140, 220, 50, 50, 50, 50});

        // 二维码
        String myStr = "14785236987541";
        BarcodeQRCode qrcode = new BarcodeQRCode(myStr, 1, 1, null);
        Image qrcodeImage = qrcode.getImage();

        int minHeight = 20; // 表格高度
        // 第一行
        tableData.addCell(createCell("基本信息", headfont, 30, 6, 0));
        // 第二行
        tableData.addCell(createCell("申请单位", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight));
        tableData.addCell(createCell("申请日期", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight));
        tableData.addCell(createCellByQRCode(qrcodeImage, Element.ALIGN_CENTER, 2, 5));
        // 第三行
        tableData.addCell(createCell("原因", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight, 3, 0));
        // 第四行
        tableData.addCell(createCell("去向", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight, 3, 0));
        // 第五行
        tableData.addCell(createCell("人员姓名", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight));
        tableData.addCell(createCell("人员工号", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight));
        // 第六行
        tableData.addCell(createCell("审核人姓名", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight));
        tableData.addCell(createCell("审核人工号", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight));
        // 第八行
        tableData.addCell(createCell("单号", keyfont, minHeight, 0, 0));
        tableData.addCell(createCell("", textfont, minHeight, 5, 0));
        // 第九行 - 物资
        table.addCell(createCell("列表信息", headfont, 30, 6, 0));
        table.addCell(createCell("列1", keyfont, minHeight));
        table.addCell(createCell("列2", keyfont, minHeight));
        table.addCell(createCell("列3", keyfont, minHeight));
        table.addCell(createCell("列4", keyfont, minHeight));
        table.addCell(createCell("列5", keyfont, minHeight));
        table.addCell(createCell("列6", keyfont, minHeight));
        Integer totalQuantity = 0;
        for (int i = 0; i < 3; i++) {
            table.addCell(createCell("测试", textfont));
            table.addCell(createCell("测试", textfont));
            table.addCell(createCell("测试", textfont));
            table.addCell(createCell("测试", textfont));
            table.addCell(createCell("测试", textfont));
            table.addCell(createCell("测试", textfont));
            totalQuantity++;
        }
        table.setSpacingBefore(15);
        table.addCell(createCell("总计", keyfont));
        table.addCell(createCell("", textfont));
        table.addCell(createCell("", textfont));
        table.addCell(createCell("", textfont));
        table.addCell(createCell(String.valueOf(totalQuantity) + "件事", textfont));
        table.addCell(createCell("", textfont));

        document.add(paragraph);
        document.add(anchor);
        document.add(p2);
        document.add(gotoP);
        document.add(p1);
        document.add(tableData);
        document.add(table);
        document.add(image);
    }

    /**
     * 创建单元格(用于插入-二维码)
     */
    public PdfPCell createCellByQRCode(Image img, int align, int colspan, int rowspan) {
        // 加入到表格
        PdfPCell cell = new PdfPCell(img, true);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setFixedHeight(30);
        cell.setColspan(colspan);
        cell.setRowspan(rowspan);
        return cell;
    }

    /**
     * 创建单元格(指定字体)
     *
     * @param value
     * @param font
     * @return
     */
    public PdfPCell createCell(String value, Font font) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPhrase(new Phrase(value, font));
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平..、最小高度）
     *
     * @param value
     * @param font
     * @param minHeight
     * @return
     */
    public PdfPCell createCell(String value, Font font, int minHeight) {
        PdfPCell cell = new PdfPCell();
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPhrase(new Phrase(value, font));
        if (minHeight > 0) {
            cell.setMinimumHeight(minHeight);
        }
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平居..、最小高度、单元格跨x列合并、、单元格跨x行合并）
     *
     * @param value
     * @param font
     * @param minHeight
     * @param colspan
     * @param rowspan
     * @return
     */
    public PdfPCell createCell(String value, Font font, int minHeight, int colspan, int rowspan) {
        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase(value, font));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        if (minHeight > 0) {
            cell.setMinimumHeight(minHeight);
        }
        if (colspan > 0) {
            cell.setColspan(colspan);
        }
        if (rowspan > 0) {
            cell.setRowspan(rowspan);
        }
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平居..、单元格跨x列合并、设置单元格内边距）
     *
     * @param value
     * @param font
     * @param align
     * @param colspan
     * @param boderFlag
     * @return
     */
    public PdfPCell createCell(String value, Font font, int align, int colspan, boolean boderFlag) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setColspan(colspan);
        cell.setPhrase(new Phrase(value, font));
        cell.setPadding(3.0f);
        if (!boderFlag) {
            cell.setBorder(0);
            cell.setPaddingTop(15.0f);
            cell.setPaddingBottom(8.0f);
        } else if (boderFlag) {
            cell.setBorder(0);
            cell.setPaddingTop(0.0f);
            cell.setPaddingBottom(15.0f);
        }
        return cell;
    }

    /**
     * 创建单元格（指定字体、水平..、边框宽度：0表示无边框、内边距）
     *
     * @param value
     * @param font
     * @param align
     * @param borderWidth
     * @param paddingSize
     * @param flag
     * @return
     */
    public PdfPCell createCell(String value, Font font, int align, float[] borderWidth, float[] paddingSize, boolean flag) {
        PdfPCell cell = new PdfPCell();
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setHorizontalAlignment(align);
        cell.setPhrase(new Phrase(value, font));
        cell.setBorderWidthLeft(borderWidth[0]);
        cell.setBorderWidthRight(borderWidth[1]);
        cell.setBorderWidthTop(borderWidth[2]);
        cell.setBorderWidthBottom(borderWidth[3]);
        cell.setPaddingTop(paddingSize[0]);
        cell.setPaddingBottom(paddingSize[1]);
        if (flag) {
            cell.setColspan(2);
        }
        return cell;
    }

    /**
     * 创建默认列宽，指定列数、水平(居中、右、左)的表格
     *
     * @param colNumber
     * @param align
     * @return
     */
    public PdfPTable createTable(int colNumber, int align) {
        PdfPTable table = new PdfPTable(colNumber);
        try {
            table.setTotalWidth(maxWidth);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(align);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    /**
     * 创建指定列宽、列数的表格
     *
     * @param widths
     * @return
     */
    public PdfPTable createTable(float[] widths) {
        PdfPTable table = new PdfPTable(widths);
        try {
            table.setTotalWidth(maxWidth);
            table.setLockedWidth(true);
            table.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.getDefaultCell().setBorder(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    /**
     * 创建空白的表格
     *
     * @return
     */
    public PdfPTable createBlankTable() {
        PdfPTable table = new PdfPTable(1);
        table.getDefaultCell().setBorder(0);
        table.addCell(createCell("", keyfont));
        table.setSpacingAfter(20.0f);
        table.setSpacingBefore(20.0f);
        return table;
    }

    // 获取当前日期 - 年月日时分秒
    public static String dateNowStr() {
        Date d = new Date();
        System.out.println(d);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(d);
    }

}
