package com.lean.image.test;


import com.lean.image.util.GongGeUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

/**
 * Description 宫格图
 * ProjectName imagetool
 */

public class GongGeSample {

    public static void main(String[] args) throws Exception {

        List<BufferedImage> bufferedImages = GongGeUtil.generateNineGongGe("F:\\testfile\\testimg\\gray_source.jpeg");

        for (int i = 0; i < bufferedImages.size(); i++) {
            File file = new File("F:\\testfile\\testimg\\gongge\\"+(i+1)+".jpg");
            Thread.sleep(100);
            ImageIO.write(bufferedImages.get(i), "jpg", file);
        }
    }
}
