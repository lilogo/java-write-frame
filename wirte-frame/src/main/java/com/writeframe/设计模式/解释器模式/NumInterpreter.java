package com.writeframe.设计模式.解释器模式;

/**
 * @ClassName NumInterpreter
 * @Description 具体解释器实现类，终结表达式 解释某个类的作用
 */
public class NumInterpreter implements IArithmeticInterpreter {
    private int value;

    public NumInterpreter(int value) {
        this.value = value;
    }

    @Override
    public int interpret() {
        return this.value;
    }
}
