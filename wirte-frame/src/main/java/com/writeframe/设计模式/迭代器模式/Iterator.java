package com.writeframe.设计模式.迭代器模式;

/**
 * @ClassName Iterator
 * @Description 顶层迭代器接口
 */
public interface Iterator<E> {
    boolean hasNext();
    E next();
}
