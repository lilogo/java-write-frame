package com.lean.process.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class DemoService {

    @Autowired
    private RedisUtil redisUtil;

    @Async
    public void test(String redisKey) {

        Long userId=1L;
        // 防止并发处理
        String flagKey = "test_uuid_:" + userId;
        String strValue = redisUtil.get(flagKey);
        if (!StringUtils.isEmpty(strValue)) {
            return;
        }
        BatchOperateRet bar = new BatchOperateRet();
        int total=100;
        //模拟逻辑业务
        for (int i = 0; i < total; i++) {
            OperateRetMsg opt = new OperateRetMsg();
            if(i%2==0){
                //成功对象
                opt.setMsg("成功");
                opt.setData(null);
                bar.getSuccessList().add(opt);
            }else {
                //失败对象
                opt.setData(null);
                bar.getFailList().add(opt);
            }

            ProcessMsg pm = new ProcessMsg(0, bar.getSuccessList().size() + bar.getFailList().size(), total,bar.getSuccessList().size(), "", bar);
            redisUtil.setProcessMsg(redisKey, pm);
        }
        //执行完成
        ProcessMsg pm = new ProcessMsg(1, total, total, bar.getSuccessList().size(), "", bar);
        redisUtil.setProcessMsg(redisKey, pm);
        // 删除key
        redisUtil.del(flagKey);
    }
}
