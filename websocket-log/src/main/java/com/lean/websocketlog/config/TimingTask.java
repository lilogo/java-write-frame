package com.lean.websocketlog.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @ClassName: TimingTask
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/6/17 16:10
 */
@Component
public class TimingTask {

    private static final Logger logger = LoggerFactory.getLogger("TimingTask");

    int info = 1;

    @Scheduled(fixedRate = 1000)
    public void outputLogger() {
        logger.info("测试日志输出" + info++);
    }
}
