package com.writeframe.设计模式.建造者模式.demo1;

/**
 * @ClassName Course
 * @Description 业务类：课程类
 * 课程的组成：
 * 　　　　预习资料【提前发出】----演讲ppt【教学资料】----》视频资料【视频提前录制好】---》课后，总结出课堂笔记 ---》课后作业。
 * 　　   注意：这些过程，之间是没有先后关系的，顺序是可以自由调整的。
 */
public class Course {
    private String name;
    private String ppt;
    private String video;
    private String note;
    private String homework;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPpt() {
        return ppt;
    }

    public void setPpt(String ppt) {
        this.ppt = ppt;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    @Override
    public String toString() {
        return "Course{" +
                "name='" + name + '\'' +
                ", ppt='" + ppt + '\'' +
                ", video='" + video + '\'' +
                ", note='" + note + '\'' +
                ", homework='" + homework + '\'' +
                '}';
    }
}
