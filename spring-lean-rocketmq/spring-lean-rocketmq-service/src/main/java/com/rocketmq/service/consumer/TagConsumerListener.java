package com.rocketmq.service.consumer;

import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * consumerGroup: 消费组
 * topic：主题
 * selectorExpression： 过滤表达式： tag/SQL
 * messageModel:消息模式  集群clustering、广播broadcasting
 */
@Service
@RocketMQMessageListener(consumerGroup = "common-customer-tag-group", topic = "common_topic",selectorType = SelectorType.TAG ,selectorExpression = "tagA||tagB", messageModel = MessageModel.CLUSTERING)
public class TagConsumerListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt messageExt) {
        System.out.println(messageExt.toString());
    }
}

