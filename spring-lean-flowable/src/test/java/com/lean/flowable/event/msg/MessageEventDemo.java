package com.lean.flowable.event.msg;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.FormService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.repository.ProcessDefinition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

//配置
//消息定义定义
//
// <message id="newInvoice" name="newInvoiceMessage" />
//中间消息抛出事件定义以及消息引用
//
//<intermediateCatchEvent id="intermediatemessageevent" name="中间消息抛出事件">
//      <messageEventDefinition messageRef="newInvoice"></messageEventDefinition>
//</intermediateCatchEvent>
//边界消息事件定义以及消息引用
//    <boundaryEvent id="boundarymessage" name="边界消息事件" attachedToRef="usertask" cancelActivity="true">
//      <messageEventDefinition messageRef="newInvoice"></messageEventDefinition>
//    </boundaryEvent>
public class MessageEventDemo extends BaseConfiguation {

    /**
     * 部署
     */
    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder =
            repositoryService.createDeployment().category("messageevent").name("messageevent").addClasspathResource("/process/msg/消息事件实践.bpmn20.xml");
        Deployment deploy = deploymentBuilder.deploy();

        System.out.println("外部流程,流程ID: " + deploy.getId());
    }

    /**
     * 启动流程实例
     *
     */
    @Test
    public void startProcessInstanceByKey() {
        String processDefinitionKey = "messageevent";
        runtimeService.startProcessInstanceByKey(processDefinitionKey);
    }

    /**
     * 完成任务
     */
    @Test
    public void complete() {
        String taskId = "272508";
        Map<String, Object> variables = new HashMap<String, Object>();
        taskService.complete(taskId, variables);
    }

    /**
     * 接收消息
     */
    @Test
    public void receiveMessage() {
        String messageName = "newInvoiceMessage";
        String executionId = "280005";
        runtimeService.messageEventReceived(messageName, executionId);
    }
}
