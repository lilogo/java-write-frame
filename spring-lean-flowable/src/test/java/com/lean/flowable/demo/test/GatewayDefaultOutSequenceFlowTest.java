package com.lean.flowable.demo.test;

import java.util.HashMap;
import java.util.Map;

import com.lean.flowable.config.BaseConfiguation;
import com.lean.flowable.demo.model.Order;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;


/**
 *网关默认出线--测试
 *
 */
public class GatewayDefaultOutSequenceFlowTest extends BaseConfiguation {

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("gatewaydefaultsequence")
													.name("gatewaydefaultsequence")
													.addClasspathResource("process/网关默认出线.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *
	 */
	@Test
	public void start() {
		String processDefinitionKey = "gatewaydefaultsequence";
		Map<String, Object> variables = new HashMap<String, Object>();
		Order order = new Order(1,"苹果",1200);
		variables.put("order", order);
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "117509";
		taskService.complete(taskId);
	}

}
