package com.lean.look;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/")
@Controller
public class LookController {

    @RequestMapping("/word")
    public String lookWord() {
        return "word-look";
    }

    @RequestMapping("/pdf")
    public String lookPdf() {
        return "pdf-look";
    }

    @RequestMapping("/ppt")
    public String lookPpt() {
        return "ppt-look";
    }

    @RequestMapping("/execl")
    public String lookExecl() {
        return "execl-look";
    }

}
