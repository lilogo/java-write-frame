package com.lean.influxdb.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
@Data
@Configuration
public class InfluxDBConfig {
    @Value("${spring.influx.user}")
    public String userName;

    @Value("${spring.influx.password}")
    public String password;

    @Value("${spring.influx.url}")
    public String url;

    //数据库
    @Value("${spring.influx.database}")
    public String database;
}


