package com.lean.jsqlparser.service;/**
 * @author Lenovo
 * @date 2022-07-06
 */

import com.lean.jsqlparser.dao.UserMapper;
import com.lean.jsqlparser.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: UserService
 * @Description:
 * @Author: zhw
 * @Date: 2022/7/6 14:44
 */
@Service("UserService")
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public Integer insertBacth(List<User> list){
        Integer integer = userMapper.insertBacth(list);
        return integer;
    }

}
