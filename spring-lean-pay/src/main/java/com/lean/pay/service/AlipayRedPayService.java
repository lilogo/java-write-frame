package com.lean.pay.service;

import com.alipay.api.domain.CashCampaignInfo;

import java.util.List;
import java.util.Map;

public interface AlipayRedPayService {

    /**
     * 创建现金活动
     * @return
     */
    Map<String,Object> createRedActivity();

    /**
     * 触发现金红包活动
     * @return
     * @param crowdNo
     */
    String triggerRedActivity(String crowdNo);

    /**
     * 更改现金活动状态
     * @param crowdNo
     * @param campStatus
     */
    void modifyRedActivityStatus(String crowdNo, String campStatus);

    /**
     * 现金活动列表查询
     * @param pageSize
     * @param pageNum
     */
    List<CashCampaignInfo> redActivityList(String pageSize, String pageNum);

    /**
     * 现金活动详情查询
     * @param crowdNo
     * @return
     */
    String redActivityDetail(String crowdNo);
}
