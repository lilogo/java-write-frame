package com.lean.flowable.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class FlowProcModel implements Serializable {

    //流程实例id
    private String processDefinitionId;

    //部署ID
    private String deploymentId;

    //流程名称
    private String name;

    //模板名称
    private String deployName;

    //流程key
    private String key;

    //流程分类
    private String category;

    //配置表单名称
    private String formName;

    //配置表单id
    private Long formId;

    //版本
    private int version;

    //流程定义状态: 1:激活 , 2:中止
    private int suspensionState;

    //部署时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date deploymentTime;

}
