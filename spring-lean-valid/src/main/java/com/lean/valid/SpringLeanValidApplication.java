package com.lean.valid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanValidApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanValidApplication.class, args);
    }

}
