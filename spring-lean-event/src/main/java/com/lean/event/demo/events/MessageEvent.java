package com.lean.event.demo.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;
/**
 * 自定义事件 继承ApplicationEvent
 */
@Getter
@Setter
@ToString
public class MessageEvent extends ApplicationEvent {
    //传输的数据对象
    private String messageId;
    public MessageEvent(Object source, String messageId) {
        super(source);
        this.messageId = messageId;
    }
}
