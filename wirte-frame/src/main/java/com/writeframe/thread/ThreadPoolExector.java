package com.writeframe.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @ClassName: ThreadPoolExector
 * @Description: 手写线程池
 * @Author: zhanghongwei
 * @Date: 2022/4/29 11:28
 */
public class ThreadPoolExector implements Exector {

    private BlockingQueue<Runnable> blockingQueue;

    private List<WorkThread> workThreads = new ArrayList<>();
    /** 默认线程池中的线程的数量 */
    private static final int WORK_NUM = 5;

    /** 默认处理任务的数量 */
    private static final int TASK_NUM = 100;

    private int workNumber;//线程数量

    private int taskNumber;//任务数量

    public ThreadPoolExector(int corePoolSize, int taskNumber) {
        if (taskNumber<=0){
            taskNumber = TASK_NUM;
        }
        if (workNumber<=0){
            workNumber = WORK_NUM;
        }
        this.blockingQueue = new ArrayBlockingQueue<Runnable>(taskNumber);
        this.workNumber = workNumber;
        this.taskNumber = taskNumber;

        workThreads = new ArrayList<>();

        //工作线程准备好了
        //启动一定数量的线程数，从队列中获取任务处理
        for (int i=0;i<workNumber;i++) {
            WorkThread workThread = new WorkThread("thead_"+i);
            workThread.start();
            workThreads.add(workThread);
        }



    }


    @Override
    public void execute(Runnable runnable) {
        try {
            blockingQueue.put(runnable);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void shutdown() {
        for (WorkThread workThread : workThreads) {
            workThread.stopWorker();
            workThread = null;//help gc
        }
        workThreads.clear();
    }

    class WorkThread extends Thread {

        public WorkThread(String name){
            super();
            setName(name);
        }

        /**
         *interrupt():
         *      是用于中断线程的，调用该方法的线程的状态将被置为"中断"状态。注意：线程中断仅仅是设置线程的中断状态位，不会停止线程。isInterrupt()返回true。
         *      如果线程在wait， sleep，join的时候受阻，调用了interrupt()方法，那么其不仅会清除中断标志位，还会抛出InterruptedException异常。isInterrupt()返回false。导致
         *      线程没有被中断，这个时候需要在调用一次线程本身interrupt()方法设置中断位。
         *interrupted():
         *      第一次使用返回true，并清除中断标志位，在此之后查询中断状态isInterrupt()都会返回false
         *isInterrupted():
         *      仅仅查询中断标志位来判断是否发生中断并返回true或者false。
         */
        @Override
        public void run() {
            while (true) {
                //判断受否被中断 检验线程是否中断，如果是返回true，否则返回false。
                if (Thread.currentThread().isInterrupted()) {
                    break;
                }
                try {
                    //没有获取任务,在此阻塞。底层调用了 lock await()阻塞方法。
                    Runnable runnable =blockingQueue.take();
                    if (runnable !=null) {
                        System.out.println(getName()+" ready execute:"+runnable.toString());
                        runnable.run();//执行任务
                    }
                    runnable = null;//help gc
                } catch (InterruptedException e) {
                    //如果线程在wait， sleep，join的时候受阻，调用了interrupt()方法，那么不仅会清除中断标志位，还会抛出InterruptedException异常。
                    //为了保证数据的一致性和完整性，需要用Thread.interrupt()方法再次中断自己，置上中断标志位。
                    Thread.currentThread().interrupt();
                }
            }
        }
        public void stopWorker(){
            interrupt();
        }
    }




}
