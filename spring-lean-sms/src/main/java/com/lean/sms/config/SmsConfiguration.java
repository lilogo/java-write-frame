package com.lean.sms.config;


import com.lean.sms.enums.SmsPlatformEnum;
import com.lean.sms.properties.SmsProperties;
import com.lean.sms.strategy.*;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 短信 Context
 *
 * @author 
 */
@Configuration
@EnableConfigurationProperties(SmsProperties.class)
@ConditionalOnProperty(prefix = "sms", value = "enabled")
public class SmsConfiguration {

    @Bean
    public SmsStrategy smsStrategy(SmsProperties smsProperties) {
        if (smsProperties.getConfig().getType() == SmsPlatformEnum.ALIYUN) {
            return new AliyunSmsStrategy(smsProperties);
        } else if (smsProperties.getConfig().getType() == SmsPlatformEnum.TENCENT) {
            return new TencentSmsStrategy(smsProperties);
        } else if (smsProperties.getConfig().getType() == SmsPlatformEnum.QINIU) {
            return new QiniuSmsStrategy(smsProperties);
        } else if (smsProperties.getConfig().getType() == SmsPlatformEnum.HUAWEI) {
            return new HuaweiSmsStrategy(smsProperties);
        } else {
            throw new RuntimeException("未知的短信平台");
        }
    }
}
