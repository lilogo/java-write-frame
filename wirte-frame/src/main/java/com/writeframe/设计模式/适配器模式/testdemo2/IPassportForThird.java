package com.writeframe.设计模式.适配器模式.testdemo2;

import com.writeframe.设计模式.适配器模式.ResultMsg;

/**
 * 4.扩展需求，增加抖音帐号登录
 *
 * 由于功能扩展，原来的适配器类，可能职责过重，依赖大量openAPI.
 *
 * 因此，可以对系统进行拆分，针对每一种登录，实现一个适配器，来处理登录逻辑。
 *
 * 【1】目标类【实为接口】
 * @ClassName IPassportForThird
 * @Description 第三方登录，目标类
 */
public interface IPassportForThird {
    /**
     * qq登录
     * @param openId
     * @return
     */
    ResultMsg loginForQQ(String openId);

    /**
     * 微信号登录
     * @param openId
     * @return
     */
    ResultMsg loginForWeChat(String openId);

    /**
     * 内部员工token登录
     * @param token
     * @return
     */
    ResultMsg loginForToken(String token);

    /**
     * 手机号+验证码登录
     * @param phone
     * @param code
     * @return
     */
    ResultMsg loginForTelephone(String phone, String code);
}
