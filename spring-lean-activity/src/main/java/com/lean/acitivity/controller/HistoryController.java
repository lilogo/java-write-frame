package com.lean.acitivity.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lean.acitivity.common.Result;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.date.DateUtil;


/**
 * 历史记录
 */
@RestController
@RequestMapping("/history")
public class HistoryController {
    @Autowired
    private HistoryService historyService;

    /**
     * 实例节点查询
     *
     * @param processInstanceId 流程实例Id
     * @return
     */
    @RequestMapping("/")
    public Result demo(String processInstanceId) {
        List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId("")
                .orderByHistoricActivityInstanceStartTime()
                .asc()
                .list();

        List<Map> res = new ArrayList<>();
        for (HistoricActivityInstance activityInstance : list) {
            Map<String, Object> map = new HashMap<String, Object>(2);
            map.put("Id", activityInstance.getActivityId());
            map.put("name", activityInstance.getActivityName());
            map.put("assignee", activityInstance.getAssignee());
            map.put("startTime", DateUtil.formatDateTime(activityInstance.getStartTime()));
            map.put("endTime", DateUtil.formatDateTime(activityInstance.getEndTime()));
            res.add(map);
        }
        return Result.ok();
    }
}
