package com.writeframe.设计模式.责任链模式.demo1;

public class ConcreteHandlerB extends Handler {
    @Override
    public void handleRequest(String request) {
        if("requestB".equals(request)){
            System.out.println(this.getClass().getSimpleName() + " deal with request:" + request);
            return;
        }
        if(this.nextHandle != null){
            this.nextHandle.handleRequest(request);
        }
    }
}
