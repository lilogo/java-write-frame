package com.writeframe.设计模式.状态模式.demo;

/**
 * 状态模式【State Pattern】,也称为状态机模式【State Machine Pattern】,是允许对象在内部状态发生改变时
 * 改变它的行为，对象看起来好像修改了它的子类。
 *它的核心，是将状态与行为进行绑定，不同状态对应不同行为。
 *状态模式在生活中的应用
 * 1.网购中订单状态变化
 * 2.电梯状态的变化
 *
 *状态模式的适用场景
 * 1.行为随状态改变而改变
 * 2.一个操作中含有庞大的多分支结构，并且这些分支取决于对象的状态。
 *
 *
 *
 *
 */
public class StateModel {
    public static void main(String[] args) {
        Context context = new Context();
        context.setState(new ConcreteStateB());
        context.handle();
    }
}
