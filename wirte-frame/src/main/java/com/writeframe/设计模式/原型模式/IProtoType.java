package com.writeframe.设计模式.原型模式;

/**
 * 原型实例的顶层接口
 * @param <T>
 */
public interface IProtoType<T> {
    T clone();
}
