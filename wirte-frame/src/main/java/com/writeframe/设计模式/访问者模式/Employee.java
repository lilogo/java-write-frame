package com.writeframe.设计模式.访问者模式;

import java.util.Random;

/**
 * @ClassName Employee
 * @Description 员工，元素抽象
 */
public abstract class Employee {
    private String name;
    private int kpi;

    public Employee(String name) {
        this.name = name;
        this.kpi = new Random().nextInt(10);
    }

    public abstract void accept(IVisitor visitor);

    public String getName() {
        return name;
    }

    public int getKpi() {
        return kpi;
    }
}
