package com.rocketmq.service.config;

import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.core.MessagePostProcessor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collection;
import java.util.Map;

/**
 * @Author zhw
 * @Description rocketMQ 封装API
 * @Date 14:53 2022/9/7
 **/
@Component
public class RocketMqBuilder {
    /**
     * 日志
     */
    private static final Logger LOG = LoggerFactory.getLogger(RocketMqBuilder.class);

    /**
     * rocketmq模板注入
     */
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @PostConstruct
    public void init() {
        LOG.info("---RocketMq助手初始化---");
    }
    //==================同步消息开始===========================//

    /**
     * 发送普通同步消息-Object
     *
     * @param topic   主题
     * @param message 消息
     */
    public SendResult syncSendMessage(String topic, Object message) {
        return rocketMQTemplate.syncSend(topic, message);
    }


    /**
     * 发送普通同步消息-Message
     *
     * @param topic   主题
     * @param message 消息
     */
    public SendResult syncSendMessage(String topic, Message<?> message) {
        return rocketMQTemplate.syncSend(topic, message);
    }

    /**
     * 发送普通同步消息-Object，并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param timeout 超时时间
     */
    public SendResult syncSendMessageTimeOut(String topic, Object message, long timeout) {
        return rocketMQTemplate.syncSend(topic, message, timeout);
    }

    /**
     * 发送普通同步消息-Message，并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param timeout 超时时间
     */
    public SendResult syncSendMessageTimeOut(String topic, Message<?> message, long timeout) {
        return rocketMQTemplate.syncSend(topic, message, timeout);
    }

    /**
     * 发送普通同步延迟消息，并设置超时
     *
     * @param topic      主题
     * @param message    消息
     * @param timeout    超时时间
     * @param delayLevel 延迟级别
     */
    public SendResult syncSendMessageTimeOut(String topic, Message<?> message, long timeout, int delayLevel) {
        return rocketMQTemplate.syncSend(topic, message, timeout, delayLevel);
    }

    /**
     * 发送批量普通同步消息
     *
     * @param topic    主题
     * @param messages 多个消息集合
     */
    public SendResult syncBtachSendMessage(String topic, Collection<?> messages) {
        return rocketMQTemplate.syncSend(topic, messages);
    }

    /**
     * 发送批量普通同步消息，并设置发送超时时间
     *
     * @param topic    主题
     * @param messages 多个消息集合
     */
    public SendResult syncBtachSendMessage(String topic, Collection<?> messages, long timeout) {
        return rocketMQTemplate.syncSend(topic, messages, timeout);
    }

    /**
     * 发送同步 顺序消息-Object
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     */
    public SendResult syncSendMessage(String topic, Object message, String hashKey) {
        return rocketMQTemplate.syncSendOrderly(topic, message, hashKey);
    }

    /**
     * 发送同步 顺序消息-Object 并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     */
    public SendResult syncSendMessage(String topic, Object message, String hashKey, long timeout) {
        return rocketMQTemplate.syncSendOrderly(topic, message, hashKey, timeout);
    }

    /**
     * 发送同步 顺序消息-Message 并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     */
    public SendResult syncSendMessage(String topic, Message<?> message, String hashKey) {
        return rocketMQTemplate.syncSendOrderly(topic, message, hashKey);
    }

    /**
     * 发送同步 顺序消息-Message 并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     * @param timeout 超时时间
     */
    public SendResult syncSendMessage(String topic, Message<?> message, String hashKey, long timeout) {
        return rocketMQTemplate.syncSendOrderly(topic, message, hashKey, timeout);
    }


    //==================同步消息结束===========================//


    //==================异步消息开始===========================//

    /**
     * 发送普通异步消息-Object
     *
     * @param topic   主题
     * @param message 消息
     */
    public void asyncSendMessage(String topic, Object message, SendCallback sendCallback) {
        rocketMQTemplate.asyncSend(topic, message, sendCallback != null ? sendCallback : getDefaultSendCallBack());
    }

    /**
     * 发送普通异步消息-Message
     *
     * @param topic   主题
     * @param message 消息
     */
    public void asyncSendMessage(String topic, Message<?> message, SendCallback sendCallback) {
        rocketMQTemplate.asyncSend(topic, message, sendCallback != null ? sendCallback : getDefaultSendCallBack());
    }


    /**
     * 发送普通异步消息-Object，并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param timeout 超时时间
     */
    public void asyncSendMessage(String topic, Object message, SendCallback sendCallback, long timeout) {
        rocketMQTemplate.asyncSend(topic, message, sendCallback != null ? sendCallback : getDefaultSendCallBack(), timeout);
    }

    /**
     * 发送普通异步消息-Message，并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     */
    public void asyncSendMessage(String topic, Message<?> message, SendCallback sendCallback, long timeout) {
        rocketMQTemplate.asyncSend(topic, message, sendCallback, timeout);
    }

    /**
     * 发送普通异步消息-Message，并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     */
    public void asyncSendMessage(String topic, Message<?> message, long timeout) {
        rocketMQTemplate.asyncSend(topic, message, getDefaultSendCallBack(), timeout);
    }

    /**
     * 发送异步 顺序消息-Object
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     */
    public void asyncSendMessage(String topic, Object message, String hashKey, SendCallback sendCallback) {
        rocketMQTemplate.asyncSendOrderly(topic, message, hashKey, sendCallback != null ? sendCallback : getDefaultSendCallBack());
    }

    /**
     * 发送异步 顺序消息-Object 并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     * @param timeout 超时时间
     */
    public void asyncSendMessage(String topic, Object message, String hashKey, SendCallback sendCallback, long timeout) {
        rocketMQTemplate.asyncSendOrderly(topic, message, hashKey, sendCallback != null ? sendCallback : getDefaultSendCallBack(), timeout);
    }

    /**
     * 发送异步 顺序消息-Message 并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     */
    public void syncSendMessage(String topic, Message<?> message, String hashKey, SendCallback sendCallback) {
        rocketMQTemplate.asyncSendOrderly(topic, message, hashKey, sendCallback != null ? sendCallback : getDefaultSendCallBack());
    }

    /**
     * 发送异步 顺序消息-Message 并设置发送超时时间
     *
     * @param topic   主题
     * @param message 消息
     * @param hashKey 标识
     * @param timeout 超时时间
     */
    public void syncSendMessage(String topic, Message<?> message, String hashKey, SendCallback sendCallback, long timeout) {
        rocketMQTemplate.asyncSendOrderly(topic, message, hashKey, sendCallback != null ? sendCallback : getDefaultSendCallBack(), timeout);
    }


    //==================异步消息结束===========================//

    //==================单向消息开始===========================//

    /**
     * 单向普通消息-Object
     *
     * @param topic   主题
     * @param message 消息
     */
    public void sendOneWay(String topic, Object message) {
        rocketMQTemplate.sendOneWay(topic, message);
    }

    /**
     * 单向普通消息-Message
     *
     * @param topic   主题
     * @param message 消息
     */
    public void sendOneWay(String topic, Message<?> message) {
        rocketMQTemplate.sendOneWay(topic, message);
    }

    /**
     * 单向 顺序消息-Object
     *
     * @param topic   主题
     * @param message 消息
     */
    public void sendOneWay(String topic, Object message, String hashKey) {
        rocketMQTemplate.sendOneWayOrderly(topic, message, hashKey);
    }

    /**
     * 单向 顺序消息-Message
     *
     * @param topic   主题
     * @param message 消息
     */
    public void sendOneWay(String topic, Message<?> message, String hashKey) {
        rocketMQTemplate.sendOneWayOrderly(topic, message, hashKey);
    }

    //==================单向消息结束===========================//

    /**
     * 开启 事务
     *
     * @param topic   主题
     * @param message 消息
     */
    public TransactionSendResult sendMessageInTransaction(String txProducerGroup, String topic, Message<?> message, Object value) {
        return rocketMQTemplate.sendMessageInTransaction(txProducerGroup, topic, message, value);
    }

    /**
     * 移除 事务
     *
     * @param txProducerGroup 事务生产组
     */
    public void sendMessageInTransaction(String txProducerGroup) {
        rocketMQTemplate.removeTransactionMQProducer(txProducerGroup);
    }

    /**
     * 默认CallBack函数
     *
     * @return
     */
    private SendCallback getDefaultSendCallBack() {
        return new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                LOG.info("---发送MQ成功---");
            }

            @Override
            public void onException(Throwable throwable) {
                LOG.error("---发送MQ失败---" + throwable.getMessage(), throwable.getMessage());
            }
        };
    }

    @PreDestroy
    public void destroy() {
        LOG.info("---RocketMq助手注销---");
    }


    /**
     * @param topic   主题
     * @param message 消息
     */
    public void convertAndSend(String topic, Message<?> message) {
        rocketMQTemplate.convertAndSend(topic, message);
    }


    /**
     * @param topic   主题
     * @param message 消息
     */
    public void convertAndSend(String topic, Message<?> message,Map<String, Object>  headMap) {
        rocketMQTemplate.convertAndSend(topic, message,headMap);
    }


}
