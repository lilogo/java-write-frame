package com.lean.smartdoc.vo;

import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class UserVo {

    /**
     * username.
     */
    private String name;

    /**
     * user's age.
     */
    private int age;

    /**
     * user's address.
     */
    private AddressVo address;
}
