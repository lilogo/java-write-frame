package com.lean.mvc.test.converter;

import com.lean.mvc.core.convert.converter.Converter;

/**
 *
 */
public class StringToIntegerConverter implements Converter<String, Integer> {

    @Override
    public Integer convert(String source) {
        return Integer.valueOf(source);
    }

}
