package com.writeframe.baselock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 *
 * CountDownLatch可以使一个获多个线程等待其他线程各自执行完毕后再执行。
 *
 * CountDownLatch 定义了一个计数器，和一个阻塞队列， 当计数器的值递减为0之前，阻塞队列里面的线程处于挂起状态，
 * 当计数器递减到0时会唤醒阻塞队列所有线程，这里的计数器是一个标志，可以表示一个任务一个线程，也可以表示一个倒计时器，
 * CountDownLatch可以解决那些一个或者多个线程在执行之前必须依赖于某些必要的前提业务先执行的场景。
 *
 * CountDownLatch(int count); //构造方法，创建一个值为count 的计数器。
 *
 * await();//阻塞当前线程，将当前线程加入阻塞队列。
 *
 * await(long timeout, TimeUnit unit);//在timeout的时间之内阻塞当前线程,时间一过则当前线程可以执行，
 *
 * countDown();//对计数器进行递减1操作，当计数器递减至0时，当前线程会去唤醒阻塞队列里的所有线程。
 */
public class CountDownLatchDemo {

    // 用于聚合所有的统计指标
    private static Map map = new HashMap();
    // 创建计数器，这里需要统计4个指标
    private static CountDownLatch countDownLatch = new CountDownLatch(4);

    public static void main(String[] args) {

        // 记录开始时间
        long startTime = System.currentTimeMillis();

        Thread countUserThread = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("正在统计新增用户数量");
                    Thread.sleep(3000);// 任务执行需要3秒
                    map.put("userNumber", 1);// 保存结果值
                    countDownLatch.countDown();// 标记已经完成一个任务
                    System.out.println("统计新增用户数量完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        Thread countOrderThread = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("正在统计订单数量");
                    Thread.sleep(3000);// 任务执行需要3秒
                    map.put("countOrder", 2);// 保存结果值
                    countDownLatch.countDown();// 标记已经完成一个任务
                    System.out.println("统计订单数量完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        Thread countGoodsThread = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("正在商品销量");
                    Thread.sleep(3000);// 任务执行需要3秒
                    map.put("countGoods", 3);// 保存结果值
                    countDownLatch.countDown();// 标记已经完成一个任务
                    System.out.println("统计商品销量完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });

        Thread countmoneyThread = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("正在总销售额");
                    Thread.sleep(3000);// 任务执行需要3秒
                    map.put("countmoney", 4);// 保存结果值
                    countDownLatch.countDown();// 标记已经完成一个任务
                    System.out.println("统计销售额完毕");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        // 启动子线程执行任务
        countUserThread.start();
        countGoodsThread.start();
        countOrderThread.start();
        countmoneyThread.start();

        try {
            // 主线程等待所有统计指标执行完毕
            countDownLatch.await();
            long endTime = System.currentTimeMillis();// 记录结束时间
            System.out.println("------统计指标全部完成--------");
            System.out.println("统计结果为：" + map.toString());
            System.out.println("任务总执行时间为" + (endTime - startTime) / 1000 + "秒");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
