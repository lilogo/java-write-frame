package com.lean.websocketlog.logger;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @ClassName: LoggerMessage
 * @Description: 日志实体类
 * @Author: zhanghongwei
 * @Date: 2022/6/17 13:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoggerMessage {
    private String body;
    private String timestamp;
    private String threadName;
    private String className;
    private String level;
}
