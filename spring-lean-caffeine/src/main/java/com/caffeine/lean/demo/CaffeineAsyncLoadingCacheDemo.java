package com.caffeine.lean.demo;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @description Caffeine 异步加载
 */
public class CaffeineAsyncLoadingCacheDemo {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        AsyncLoadingCache<Object, String> cache = Caffeine.newBuilder()
                // 基于时间失效,写入之后开始计时失效
                .expireAfterWrite(2000, TimeUnit.MILLISECONDS)
                // 缓存容量
                .maximumSize(5)
                // 可以使用java8函数式接口的方式,这里其实是重写CacheLoader的load方法
                .buildAsync(new MyCacheLoader());


        // 获取一个不存在的kay,让它异步去调用CacheLoader的load方法。这时候他会返回一个CompletableFuture
        CompletableFuture<String> future = cache.get("key");
        //验证
        future.thenAccept(s -> System.out.println("当前的时间为:" + System.currentTimeMillis() + " -> 异步加载的值为:" + s));

        // 睡眠2秒让它的key失效
        TimeUnit.SECONDS.sleep(2);

        // 注意:当使用getIfPresent时,也是返回的CompletableFuture
        // 因为getIfPresent从缓存中找不到是不会去运算key既不会调用(CacheLoader.load)方法
        // 所以得到的CompletableFuture可能会为null,如果想从CompletableFuture中取值的话.先判断CompletableFuture是否会为null
        CompletableFuture<String> completableFuture = cache.getIfPresent("key");
        if (Objects.nonNull(completableFuture)) {
            System.out.println(completableFuture.get());
        }
    }
    static  class MyCacheLoader implements CacheLoader{

        @Nullable
        @Override
        public Object load(@NonNull Object key) throws Exception {
            return key + "_" + System.currentTimeMillis();
        }
    }

}