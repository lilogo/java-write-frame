package com.writeframe.设计模式.责任链模式.demo;

/**
 * 将链中每一个节点看作是一个对象，每个节点处理的请求均不同，且内部自动维护一个下一节点的对象。当一个请求从链式的首端发现时，
 * 会沿着链的路径依次传递给每一个节点对象，直至对象处理完这个请求为止。
 * 　　属于行为型模式。
 * 如：
 *  工作流中审批流程
 *  过五关，斩六将
 *
 *
 *
 *
 */
public class ChainPatternModel {

    private static AbstractLogger getChainOfLoggers() {

        AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
        AbstractLogger debugLogger = new DebugLogger(AbstractLogger.DEBUG);
        AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

        errorLogger.setNextLogger(debugLogger);
        debugLogger.setNextLogger(consoleLogger);

        return errorLogger;
    }

    public static void main(String[] args) {
        AbstractLogger loggerChain = getChainOfLoggers();

        loggerChain.logMessage(AbstractLogger.INFO, "This is an information.");

        loggerChain.logMessage(AbstractLogger.DEBUG,"This is a debug level information.");

        loggerChain.logMessage(AbstractLogger.ERROR,"This is an error information.");
    }
}
