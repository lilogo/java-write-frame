package com.lean.pay.controller.alipay.face;

import com.lean.pay.service.AliPayScanningPayService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lean.pay.vo.R;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Map;

/**
 *
 * https://opendocs.alipay.com/open/194/106078?ref=api
 * 1. 商家通过扫描顾客支付宝钱包中的条码、二维码等方式完成支付； 扫描机
 * 2. 顾客通过使用支付宝钱包扫一扫，扫描商家的二维码等方式完成支付。
 * 相关问题：
 *      https://opendocs.alipay.com/support/01ras2
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/ali-scanning-pay")
@Api(tags = "当面付--扫码支付")
@Slf4j
public class AlipayScanningPayController {

    @Autowired
    private AliPayScanningPayService aliPayScanningPayService;

    //====================收银员通过收银台或商户后台调用支付宝接口，生成二维码后，展示给用户，由用户扫描二维码完成订单支付。============================//
    // 创建订单两种方式：1.创建二维码 2.调用接口自动下单

    /**
     * 统一收单线下交易预创建
     *  背景：
     *      收银员通过收银台或商户后台调用支付宝接口，生成二维码后，展示给用户，由用户扫描二维码完成订单支付。
     *  注意：
     *      预下单请求生成的二维码有效时间为2小时
     *  文档：
     *      https://opendocs.alipay.com/open/02ekfg?ref=api&scene=19
     * 1.商家系统调用 alipay.trade.precreate（统一收单线下交易预创建接口），获得该订单的二维码串 qr_code，开发者需要利用二维码生成工具获得最终的订单二维码图片。
     * 2.发起轮询获得支付结果：等待 5 秒后调用 alipay.trade.query（统一收单线下交易查询接口），通过支付时传入的商户订单号（out_trade_no）查询支付结果（返回参数 TRADE_STATUS），如果仍然返回等待用户付款（WAIT_BUYER_PAY），则再次等待 5 秒后继续查询，直到返回确切的支付结果（成功 TRADE_SUCCESS 或 已撤销关闭 TRADE_CLOSED），或是超出轮询时间。在最后一次查询仍然返回等待用户付款的情况下，必须立即调用 alipay.trade.cancel（统一收单交易撤销接口）将这笔交易撤销，避免用户继续支付。
     * 3.除了主动轮询，当订单支付成功时，商家也可以通过设置异步通知（notify_url）来获得支付宝服务端返回的支付结果，详情可查看 扫码异步通知，注意一定要对异步通知验签，确保通知是支付宝发出的。
     */
    /**
     * 生成商家的二维码
     * @param productId
     * @return
     */
    @ApiOperation("统一收单线下交易预创建")
    @PostMapping("/trade/page/paycode/{productId}")
    public R tradePagePayQrCode(@PathVariable Long productId){
        log.info("统一收单线下交易预创建的调用");
        String codeUrl = aliPayScanningPayService.tradeCreateCode(productId);
        return R.ok().data("codeUrl", codeUrl);
    }


    /**
     * 统一收单交易创建接口
     * 背景：
     *      商户通过该接口进行交易的创建下单
     * 文档：
     *     https://opendocs.alipay.com/open/02ekfj?ref=api
     */
    @ApiOperation("统一收单交易创建接口")
    @PostMapping("/trade/page/pay/{productId}")
    public R createTradePagePay(@PathVariable Long productId){
        log.info("统一收单交易创建接口的调用");
        Map<String, Object> resultMap =aliPayScanningPayService.createTradePagePay(productId);
        return R.ok().setData(resultMap);
    }

    /**
     * 统一收单交易查询
     * 背景：
     *      该接口提供所有支付宝支付订单的查询，商户可以通过该接口主动查询订单状态，完成下一步的业务逻辑。
     *      需要调用查询接口的情况：
     *              当商户后台、网络、服务器等出现异常，商户系统最终未接收到支付通知；
     *              调用支付接口后，返回系统错误或未知交易状态情况；
     *              调用alipay.trade.pay，返回INPROCESS的状态；
     *              调用alipay.trade.cancel之前，需确认支付状态；
     * 文档：
     *     https://opendocs.alipay.com/open/02ekfh?ref=api&scene=23
     */
    @ApiOperation("统一收单交易创建接口")
    @PostMapping("/trade/page/query/{orderNo}")
    public R queryOrder(@PathVariable String orderNo){
        log.info("统一收单交易创建接口的调用");
        String result = aliPayScanningPayService.queryOrder(orderNo);
        return R.ok().setMessage("查询成功").data("result", result);
    }

    /**
     * 统一收单交易退款接口
     * 背景：
     *      当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，卖家可以通过退款接口将支付款退还给买家，支付宝将在收到退款请求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。
     *  交易超过约定时间（签约时设置的可退款时间）的订单无法进行退款。
     *  支付宝退款支持单笔交易分多次退款，多次退款需要提交原支付订单的订单号和设置不同的退款请求号。一笔退款失败后重新提交，要保证重试时退款请求号不能变更，防止该笔交易重复退款。
     *  同一笔交易累计提交的退款金额不能超过原始交易总金额。
     *
     * 注意：
     *      1. 同一笔交易的退款至少间隔3s后发起
     *      2. 请严格按照接口文档中的参数进行接入。若在此接口中传入【非当前接口文档中的参数】会造成【退款失败或重复退款】。
     *      3. 该接口不可与其他退款产品混用。若商户侧同一笔退款请求已使用了当前接口退款的情况下，【再使用其他退款产品进行退款】可能会造成【重复退款】。
     *      4. 退款成功判断说明：接口返回fund_change=Y为退款成功，fund_change=N或无此字段值返回时需通过退款查询接口进一步确认退款状态。详见退款成功判断指导。注意，接口中code=10000，仅代表本次退款请求成功，不代表退款成功。
     * 文档：
     *      https://opendocs.alipay.com/open/02ekfk?ref=api
     */
    @ApiOperation("申请退款")
    @PostMapping("/trade/refund/{orderNo}/{reason}")
    public R refunds(@PathVariable String orderNo, @PathVariable String reason){
        log.info("申请退款");
        aliPayScanningPayService.refund(orderNo, reason);
        return R.ok();
    }


    /**
     * 统一收单交易退款查询
     * 背景：
     *      商户可使用该接口查询自已通过alipay.trade.refund提交的退款请求是否执行成功。
     *
     * 注意：
     *      1. 该接口的返回码10000，仅代表本次查询操作成功，不代表退款成功，当接口返回的refund_status值为REFUND_SUCCESS时表示退款成功，否则表示退款没有执行成功。
     *      2. 如果退款未成功，商户可以调用退款接口重试，重试时请务必保证退款请求号和退款金额一致，防止重复退款。
     *      3. 发起退款查询接口的时间不能离退款请求时间太短，建议之间间隔10秒以上。
     * 文档：
     *      https://opendocs.alipay.com/open/02ekfl?ref=api
     */
    @ApiOperation("查询退款")
    @GetMapping("/trade/fastpay/refund/{orderNo}")
    public R queryRefund(@PathVariable String orderNo) {

        log.info("查询退款");
        String result = aliPayScanningPayService.queryRefund(orderNo);
        return R.ok().setMessage("查询成功").data("result", result);
    }



    /**
     * 收单退款冲退完成通知
     *  背景：
     *      退款存在退到银行卡场景时，收单会根据银行回执消息发送退款完成信息。仅当退款发起时（统一收单交易退款接口），在query_options中传入：deposit_back_info时会发送。
     *
     */


    /**
     * 统一收单交易撤销接口
     *
     * 背景：
     *      支付交易返回失败或支付系统超时，调用该接口撤销交易。如果此订单用户支付失败，支付宝系统会将此订单关闭；如果用户支付成功，支付宝系统会将此订单资金退还给用户。
     *      注意：只有发生支付系统超时或者支付结果未知时可调用撤销，其他正常支付的单如需实现相同功能请调用申请退款API。提交支付交易后调用【查询订单API】，没有明确的支付结果再调用【撤销订单API】。
     * 文档：
     *     https://opendocs.alipay.com/open/02ekfi?ref=api
     *
     */
    @ApiOperation("统一收单交易撤销接口")
    @PostMapping("/trade/cancel/{orderNo}")
    public R cancelOrder(@PathVariable String orderNo){

        log.info("撤销订单");
        aliPayScanningPayService.cancelOrder(orderNo);
        return R.ok().setMessage("订单已撤销");
    }


    /**
     * 统一收单交易关闭接口
     * 背景：
     *      用于交易创建后，用户在一定时间内未进行支付，可调用该接口直接将未付款的交易进行关闭。
     *
     */
    @ApiOperation("统一收单交易关闭接口")
    @PostMapping("/trade/close/{orderNo}")
    public R closeOrder(@PathVariable String orderNo){

        log.info("关闭订单");
        aliPayScanningPayService.closeOrder(orderNo);
        return R.ok().setMessage("订单已关闭");
    }

    /**
     * 查询对账单下载地址
     * 背景：
     *      为方便商户快速查账，支持商户通过本接口获取商户离线账单下载地址
     * 文档：
     *      https://opendocs.alipay.com/open/02ekfm?ref=api
     */
    @ApiOperation("获取账单url")
    @GetMapping("/bill/downloadurl/query/{billDate}/{type}")
    public R queryTradeBill(
            @PathVariable String billDate,
            @PathVariable String type)  {
        log.info("获取账单url");
        String downloadUrl = aliPayScanningPayService.queryBill(billDate, type);
        return R.ok().setMessage("获取账单url成功").data("downloadUrl", downloadUrl);
    }
}
