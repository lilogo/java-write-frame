package com.writeframe.设计模式.访问者模式;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName BusinessReport
 * @Description 业务报表，数据结构
 *
 * 访问者模式【visitor Pattern】,是一种将数据结构与数据操作分离设计模式。是指封装一些作用于某种数据结构中的各元素的操作。
 *
 * 特征：
 * 　　可以在不改变数据结构的前提下定义作用于这些元素的新操作。属于行为型模式。
 *说明：
 * 　　访问者模式，被称为最复杂的设计模式。运用并不多。
 *
 *访问者模式在生活中的体现
 * 1.参与KPI考核的人员
 * 　　KPI的考核标准，一般是固定不变的，但参与KPI考核的员工会经常变化。
 * 　　kpi考核打分的人也会经常变化，
 * 2.餐厅就餐人员
 * 　　餐厅吃饭，餐厅的菜单是基本稳定的，就餐人员基本每天都在变化。就餐人员就是一个访问者。
 * 总结：
 * 　　访问者，好像就是变化的元素，与不变的结构【标准，规则】的构成关系处理角色。
 *
 *访问者模式的适用场景
 * 　　访问者模式很少能用到，一旦需要使用，涉及到的系统往往比较复杂。
 * 1.数据结构稳定，作用于数据结构的操作经常变化的场景。
 * 2.需要数据结构与数据操作分离的场景。
 * 3.需要对不同数据类型(元素)　进行操作，而不使用分支判断具体类型的场景。
 *
 */
public class BusinessReport {
    private List<Employee> employeeList = new LinkedList<>();

    public BusinessReport() {
        employeeList.add(new Manager("项目经理A"));
        employeeList.add(new Manager("项目经理B"));
        employeeList.add(new Engineer("程序员A"));
        employeeList.add(new Engineer("程序员B"));
        employeeList.add(new Engineer("程序员C"));
    }

    public void showReport(IVisitor visitor) {
        for (Employee employee : employeeList) {
            employee.accept(visitor);
        }
    }

    public static void main(String[] args) {
        BusinessReport report = new BusinessReport();
        System.out.println("===========CEO看报表===============");
        report.showReport(new CEOVisitor());
        System.out.println("===========CTO看报表===============");
        report.showReport(new CTOVisitor());
    }
}
