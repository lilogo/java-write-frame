package com.writeframe.设计模式.责任链模式.loginDemo;

/**
 * 登录校验，判断用户存在性
 */
public class LoginHandler extends Handler {
    @Override
    public void doHandle(Member member) {
        Member existMember = checkExist(member.getLoginName(), member.getLoginPass());
        if (null == existMember) {
            System.out.println("用户不存在");
            return;
        }
        //用户名存在，表示可登录
        System.out.println("登录成功");

        next.doHandle(existMember);
    }

    private Member checkExist(String loginName, String loginPass) {
        Member member = new Member(loginName, loginPass);
        member.setRoleName("管理员");
        return member;
    }
}
