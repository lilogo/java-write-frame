package com.lean.cassandra.repository;

import com.lean.cassandra.entity.City;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface CityRepository extends CassandraRepository<City, Integer> {

}
