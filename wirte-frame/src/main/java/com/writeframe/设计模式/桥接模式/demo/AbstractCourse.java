package com.writeframe.设计模式.桥接模式.demo;

/**
 * @ClassName AbstractCourse
 * @Description 课程抽象
 * 说明：
 * 　　现在，产品的三个维度是相互独立的，互不相关。
 * 　　想要它们关联起来，因此，使用桥接模式，创建抽象，引用接口实例。、
 * 【4】创建抽象，引用接口实现
 *
 * 说明：
 * 　　因为Course存在不同实现，当提供这样一个抽象之后，原来的课程实现，就需要修改。
 * 　　不是实现接口，而是直接继承这个抽象，修改后如下所示：
 */
public class AbstractCourse implements ICourse {

    private IVideo video;

    private INote note;

    public void setVideo(IVideo video) {
        this.video = video;
    }

    public void setNote(INote note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "AbstractCourse{" +
                "video=" + video +
                ", note=" + note +
                '}';
    }
}
