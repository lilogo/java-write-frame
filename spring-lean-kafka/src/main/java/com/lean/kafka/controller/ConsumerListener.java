package com.lean.kafka.controller;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.listener.ConsumerSeekAware;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 自定义seek参考
 * https://docs.spring.io/spring-kafka/docs/current/reference/html/#seek
 * offset设置方式
 * 如代码所示，实现ConsumerSeekAware接口，设置offset几种方式：
 *
 * 指定 offset，需要自己维护 offset，方便重试。
 * 指定从头开始消费。
 * 指定 offset 为最近可用的 offset (默认)。
 * 根据时间戳获取 offset，设置 offset。
 */
@Component
public class ConsumerListener implements ConsumerSeekAware {
    @KafkaListener(topics = "test-offset")
    public void onMessage(List<ConsumerRecord<String, String>> records, Acknowledgment ack){
        System.out.println(records.size());
        System.out.println(records.toString());
        ack.acknowledge();
    }
    @Override
    public void onPartitionsAssigned(Map<TopicPartition, Long> assignments, ConsumerSeekCallback callback) {
        //按照时间戳设置偏移
        callback.seekToTimestamp(assignments.keySet(),1670233826705L);
        //设置偏移到最近
        callback.seekToEnd(assignments.keySet());
        //设置偏移到最开始
        callback.seekToBeginning(assignments.keySet());
        //指定 offset
        for (TopicPartition topicPartition : assignments.keySet()) {
            callback.seek(topicPartition.topic(),topicPartition.partition(),0L);
        }
    }
}
