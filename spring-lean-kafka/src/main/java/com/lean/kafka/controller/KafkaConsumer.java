package com.lean.kafka.controller;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;

import java.util.List;

@Configuration
public class KafkaConsumer {

    private Logger log = LoggerFactory.getLogger(KafkaConsumer.class);

    /**
     * id：监听器的id
     *
     * groupId：消费组id
     *
     * idIsGroup：是否用id作为groupId，如果置为false，并指定groupId时，消费组ID使用groupId；否则默认为true，会使用监听器的id作为groupId
     *
     * topics：指定要监听哪些topic(与topicPattern、topicPartitions 三选一)
     *
     * topicPattern： 匹配Topic进行监听(与topics、topicPartitions 三选一)
     *
     * topicPartitions： 显式分区分配，可以为监听器配置明确的主题和分区（以及可选的初始偏移量）
     *
     * containerFactory：指定监听器容器工厂
     *
     * errorHandler： 监听异常处理器，配置BeanName
     *
     * beanRef：真实监听容器的BeanName，需要在 BeanName前加 “__”
     *
     * clientIdPrefix：消费者Id前缀
     *
     * concurrency： 覆盖容器工厂containerFactory的并发配置
     *
     * @param record
     * @param ack
     * @param topic
     */
    @KafkaListener(topics = "firstTopic")
    public void consumerTopic(ConsumerRecord<?, ?> record, Acknowledgment ack, @Header(KafkaHeaders.RECEIVED_TOPIC) String topic) {
        log.info("这是消费者在消费消息：" + record.topic() + "----" + record.partition() + "----" + record.value() + "-------" + topic);
        // 手动确认
        ack.acknowledge();
    }

    @KafkaListener(topics = "batch-topic")
    public void batchTopicListener(List<ConsumerRecord<?, ?>> consumerRecordList, Acknowledgment ack) {
        for (ConsumerRecord<?, ?> record : consumerRecordList) {
            log.info("这是消费者在消费消息：" + record.topic() + "----" + record.partition() + "----" + record.value());
            // 手动确认
            ack.acknowledge();
        }
    }
    @KafkaListener(topics = "orderTopic", groupId = "group_id", errorHandler = "myKafkaListenerErrorHandler")
    public void consume(String message, Acknowledgment ack) {
        log.info("## consume message: {}", message);
        try {
            // 用于测试异常处理
            // int i = 1 / 0;
            System.out.println(message);
        } finally {
            // 手动确认
            ack.acknowledge();
        }
    }
}
