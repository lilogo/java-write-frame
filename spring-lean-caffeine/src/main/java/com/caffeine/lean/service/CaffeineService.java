package com.caffeine.lean.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @Cacheable 触发缓存入口（这里一般放在创建和获取的方法上）
 * @CacheEvict 触发缓存的eviction（用于删除的方法上）
 * @CachePut 更新缓存且不影响方法执行（用于修改的方法上，该注解下的方法始终会被执行）
 * @Caching 将多个缓存组合在一个方法上（该注解可以允许一个方法同时设置多个注解）
 * @CacheConfig 在类级别设置一些缓存相关的共同配置（与其它缓存配合使用）
 */
@Service
@CacheConfig(cacheManager = "caffeineCacheManager")
@Slf4j
public class CaffeineService {

    @Cacheable(value = "data", key = "#key")
    public String cacheData(String key) {
        log.info("cacheData()方法执行");
        return getCache(key);
    }

    @CachePut(value = "data", key = "#key")
    public String cachePutData(String key) {
        log.info("cachePutData()方法执行");
        return "cachePutData--" + key;
    }

    private String getCache(String key) {
        try {
            log.info("getCache()方法执行");
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return key;
    }

    /**
     * CacheEvict删除key，会调用cache的evict
     * */
    @CacheEvict(cacheNames = "outLimit",key = "#name")
    public String deleteCaffeineServiceTest(String name){
        String value = name + " nihao";
        log.info("deleteCaffeineServiceTest value = {}",value);
        return value;
    }

    /**
     * condition条件判断是否要走缓存，无法使用方法中出现的值（返回结果等）,条件为true放入缓存
     * unless是方法执行后生效，决定是否放入缓存,返回true的放缓存
     * */
    @Cacheable(cacheNames = "outLimit",key = "#name",condition = "#value != null ")
    public String getCaffeineServiceTest(String name,Integer age){
        String value = name + " nihao "+ age;
        log.info("getCaffeineServiceTest value = {}",value);
        return value;
    }

}
