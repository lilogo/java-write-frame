package com.lean.image.test;


import com.lean.image.util.ImageGrayUtil;
import com.lean.image.util.ImageLosslessUtil;
import com.lean.image.xsexception.ImageTypeException;

import java.awt.image.BufferedImage;

/**
 * Description
 * ProjectName imagetool
 * Created by 小帅丶 on 2022-05-13 12:34.
 * Version 1.0
 */

public class GraySample {
    public static void main(String[] args) throws ImageTypeException, Exception {
        String sourcePath = "原图本地路径";
        String targetPath = "新图保存的本地路径";
        String imageSuffix = "jpeg";//图片保存格式
        //图片灰度处理 推荐
        BufferedImage bufferedImage = ImageGrayUtil.grayImage3ByteBGR(sourcePath);
        //图片灰度处理 纯黑
        //BufferedImage bufferedImage = ImageGrayUtil.grayImageByteGRAY(sourcePath);
        //无压缩保存图片
        ImageLosslessUtil.saveLossLessImageTwo(bufferedImage,targetPath,imageSuffix);
    }
}
