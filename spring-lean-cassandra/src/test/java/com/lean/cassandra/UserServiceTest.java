package com.lean.cassandra;

import com.alibaba.fastjson.JSON;
import com.datastax.oss.driver.api.querybuilder.QueryBuilder;
import com.google.common.collect.Lists;

import com.lean.cassandra.entity.User;
import com.lean.cassandra.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void saveUser() {
        User user = new User();
        user.setId(1);
        user.setName("aa");
        user.setAge(10);
        user.setBooks(Lists.newArrayList());
        userService.saveUser(user);
    }

    @Test
    public void batchSaveUser() {
        List<User> userList = new ArrayList<>();
        for (int i = 2; i < 100; i++) {
            User user = new User();
            user.setId(0);
            user.setName("aa");
            user.setAge(10);
            user.setBooks(Lists.newArrayList());
            userList.add(user);
        }
        userService.batchSaveUser(userList);
    }

    @Test
    public void updateUser() {
        User user = new User();
        user.setId(1);
        user.setName("dd");
        user.setAge(10);
        user.setBooks(Lists.newArrayList());
        userService.updateUser(user);
    }

    @Test
    public void getById() {
        Integer id = 1;
        User user = userService.getById(id);
        System.out.println(JSON.toJSONString(user));
    }

    @Test
    public void getUserDetail() {
        Query query = Query.empty();
        // query = query.withAllowFiltering();
        User user = userService.getObj(query);
        System.out.println(JSON.toJSONString(user));
    }

    @Test
    public void findUserList() {
        Query query = Query.empty();
        // query = query.withAllowFiltering();
        List<User> userList = userService.listObjs(query);
        System.out.println(JSON.toJSONString(userList));
    }

}
