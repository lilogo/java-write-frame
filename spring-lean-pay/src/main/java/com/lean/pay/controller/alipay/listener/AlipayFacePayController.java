package com.lean.pay.controller.alipay.listener;

import com.lean.pay.service.AliPayFaceService;
import com.lean.pay.vo.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 *
 * https://opendocs.alipay.com/open/194/106039?ref=api
 * 1. 商家通过扫描顾客支付宝钱包中的条码、二维码等方式完成支付； 扫描机
 * 2. 顾客通过使用支付宝钱包扫一扫，扫描商家的二维码等方式完成支付。
 * 相关问题：
 *      https://opendocs.alipay.com/support/01ras2
 *
 */
@CrossOrigin
@RestController
@RequestMapping("/api/ali-face-pay")
@Api(tags = "当面付--付款码")
@Slf4j
public class AlipayFacePayController {

    @Autowired
    private AliPayFaceService aliPayFaceService;

    //====================收银员使用扫码设备读取用户手机支付宝“付款码”获取设备（如扫码枪）读取用户手机支付宝的付款码信息后，将二维码或条码信息通过本接口上送至支付宝发起支付。============================//

    /**
     * 统一收单交易支付接口
     * 背景：
     *      1.收银员使用扫码设备读取用户手机支付宝“付款码”获取设备（如扫码枪）读取用户手机支付宝的付款码信息后，将二维码或条码信息通过本接口上送至支付宝发起支付。
     *      2.用户与商户签署周期扣款协议后，商户可通过本接口做后续免密代扣操作
     *      3.用户在商户侧授权冻结并享受服务后，商户使用授权单号通过本接口对用户已授权金额发起扣款
     *
     * 注意：
     * 1. 请根据接入的具体产品参考对应场景描述和示例代码
     * 2. 当面付产品对于未获取到明确支付成功结果的交易请务必调用撤销接口
     * @param productId
     * @param type 1-当面付 2-周期付款 3-预授权
     * @return
     */
    @ApiOperation("统一收单交易支付接口")
    @PostMapping("/trade/page/create")
    public R createPayOrder(Long productId,Integer type){
        log.info("统一收单交易支付接口的调用");
        String result = aliPayFaceService.createPayOrder(productId,type);
        return R.ok().data("result", result);
    }

    /**
     * 统一收单交易查询
     * 背景：
     *      该接口提供所有支付宝支付订单的查询，商户可以通过该接口主动查询订单状态，完成下一步的业务逻辑。 需要调用查询接口的情况：
     *      当商户后台、网络、服务器等出现异常，商户系统最终未接收到支付通知； 调用支付接口后，返回系统错误或未知交易状态情况；
     *      调用alipay.trade.pay，返回INPROCESS的状态；
     *      调用alipay.trade.cancel之前，需确认支付状态
     *
     * @param orderNo
     * @return
     */
    @ApiOperation("统一收单交易查询")
    @PostMapping("/trade/page/query/{orderNo}")
    public R queryPayOrder(@PathVariable String orderNo){
        log.info("统一收单交易支付接口的调用");
        String result = aliPayFaceService.queryPayOrder(orderNo);
        return R.ok().data("result", result);
    }


    /**
     * 统一收单交易退款接口
     *  背景：
     *      当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，卖家可以通过退款接口将支付款退还给买家，支付宝将在收到退款请求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。
     *      交易超过约定时间（签约时设置的可退款时间）的订单无法进行退款。
     *      支付宝退款支持单笔交易分多次退款，多次退款需要提交原支付订单的订单号和设置不同的退款请求号。一笔退款失败后重新提交，要保证重试时退款请求号不能变更，防止该笔交易重复退款。
     *      同一笔交易累计提交的退款金额不能超过原始交易总金额。
     *
     * 注意：
     *      1. 同一笔交易的退款至少间隔3s后发起
     *      2. 请严格按照接口文档中的参数进行接入。若在此接口中传入【非当前接口文档中的参数】会造成【退款失败或重复退款】。
     *      3. 该接口不可与其他退款产品混用。若商户侧同一笔退款请求已使用了当前接口退款的情况下，【再使用其他退款产品进行退款】可能会造成【重复退款】。
     *      4. 退款成功判断说明：接口返回fund_change=Y为退款成功，fund_change=N或无此字段值返回时需通过退款查询接口进一步确认退款状态。详见退款成功判断指导。注意，接口中code=10000，仅代表本次退款请求成功，不代表退款成功。
     *
     * @param orderNo
     * @param reason
     * @return
     */
    @ApiOperation("申请退款")
    @PostMapping("/trade/refund/{orderNo}/{reason}")
    public R refunds(@PathVariable String orderNo, @PathVariable String reason){
        log.info("申请退款");
        String result = aliPayFaceService.refund(orderNo, reason);
        return R.ok().setMessage("查询成功").data("result", result);
    }

    /**
     * 统一收单交易退款查询
     * 背景：
     *      商户可使用该接口查询自已通过alipay.trade.refund提交的退款请求是否执行成功。
     * 注意：
     *      1. 该接口的返回码10000，仅代表本次查询操作成功，不代表退款成功，当接口返回的refund_status值为REFUND_SUCCESS时表示退款成功，否则表示退款没有执行成功。
     *      2. 如果退款未成功，商户可以调用退款接口重试，重试时请务必保证退款请求号和退款金额一致，防止重复退款。
     *      3. 发起退款查询接口的时间不能离退款请求时间太短，建议之间间隔10秒以上。
     *
     * @param orderNo
     * @param outRequestNo 退款请求号
     * @return
     */
    @ApiOperation("查询退款")
    @GetMapping("/trade/fastpay/refund")
    public R queryRefund(String orderNo,String outRequestNo) {

        log.info("查询退款");
        String result = aliPayFaceService.queryRefund(orderNo, outRequestNo);
        return R.ok().setMessage("查询成功").data("result", result);
    }

    /**
     *统一收单交易撤销接口
     * 背景：
     *      支付交易返回失败或支付系统超时，调用该接口撤销交易。如果此订单用户支付失败，支付宝系统会将此订单关闭；如果用户支付成功，支付宝系统会将此订单资金退还给用户。
     * 注意：
     *      只有发生支付系统超时或者支付结果未知时可调用撤销，其他正常支付的单如需实现相同功能请调用申请退款API。提交支付交易后调用【查询订单API】，没有明确的支付结果再调用【撤销订单API】。
     *
     *
     * @param orderNo
     * @return
     */
    @ApiOperation("统一收单交易撤销接口")
    @PostMapping("/trade/cancel/{orderNo}")
    public R cancelOrder(@PathVariable String orderNo){

        log.info("撤销订单");
        aliPayFaceService.cancelOrder(orderNo);
        return R.ok().setMessage("订单已撤销");
    }

    /**
     * 统一收单交易关闭接口
     * 背景：
     *      用于交易创建后，用户在一定时间内未进行支付，可调用该接口直接将未付款的交易进行关闭。
     *
     * @param orderNo
     * @return
     */
    @ApiOperation("统一收单交易关闭接口")
    @PostMapping("/trade/close/{orderNo}")
    public R closeOrder(@PathVariable String orderNo){

        log.info("关闭订单");
        aliPayFaceService.closeOrder(orderNo);
        return R.ok().setMessage("订单已关闭");
    }

    /**
     * 查询对账单下载地址
     * 背景：
     *      为方便商户快速查账，支持商户通过本接口获取商户离线账单下载地址
     *
     * @param billDate
     * @param type
     * @return
     */
    @ApiOperation("查询对账单下载地址")
    @GetMapping("/bill/downloadurl/query/{billDate}/{type}")
    public R queryTradeBill(
            @PathVariable String billDate,
            @PathVariable String type)  {
        log.info("获取账单url");
        String downloadUrl = aliPayFaceService.queryBill(billDate, type);
        return R.ok().setMessage("获取账单url成功").data("downloadUrl", downloadUrl);
    }

}
