/**
  * Copyright 2020 bejson.com
  */
package com.lean.xmindParser.entity;
import lombok.Data;

@Data
public class XmindRoot {
    private String id;
    private String title;
    private TopicText rootTopic;
}
