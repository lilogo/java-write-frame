package com.writeframe.设计模式.适配器模式.classadapter;


/**
 * 【1】适配原角色，220v交流电压
 * @ClassName AC220
 * @Description 交流电压220V
 */
public class AC220 {
    public int outputAC220V() {
        int output = 220;
        System.out.println("输出电压：" + output + " V");
        return output;
    }
}
