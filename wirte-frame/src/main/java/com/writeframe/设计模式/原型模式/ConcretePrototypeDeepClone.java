package com.writeframe.设计模式.原型模式;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ConcretePrototypeDeepClone implements Cloneable, Serializable {
    private int age;
    private String name;

    //增加一个引用类型成员
    private List<String> hobbies = new ArrayList<>();

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(List<String> hobbies) {
        this.hobbies = hobbies;
    }

    /**
     * 基于jdk实现浅克隆
     * @return
     */
    @Override
    public ConcretePrototypeDeepClone clone() {
        try {
            //调用jdk中clone()实现
            return (ConcretePrototypeDeepClone) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 基于序列化与反序列化，实现深克隆
     * 》性能不好
     * 》占用IO
     * @return
     */
    public ConcretePrototypeDeepClone deepClone() {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(this);

            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bis);
            return (ConcretePrototypeDeepClone) ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 基于ArrayList底层实现clone，实现深克隆
     * @return
     */
    public ConcretePrototypeDeepClone deepCloneHobbies() {
        try {

            ConcretePrototypeDeepClone res = (ConcretePrototypeDeepClone) super.clone();
            res.hobbies = (List)((ArrayList)res.hobbies).clone();
            return res;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return "ConcretePrototype{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", hobbies=" + hobbies +
                '}';
    }

    public static void main(String[] args) {
        ConcretePrototypeDeepClone prototype = new ConcretePrototypeDeepClone();
        prototype.setName("test");
        prototype.setAge(18);
        //既爱书法，也爱美术
        List<String> hobbies = new ArrayList<>();
        hobbies.add("书法");
        hobbies.add("美术");
        prototype.setHobbies(hobbies);
        //System.out.println(prototype);    //调整打印顺序

        ConcretePrototypeDeepClone cloneType = prototype.deepClone();
        //还是个技术控
        cloneType.getHobbies().add("技术控");

        System.out.println("原型对象："+prototype);
        System.out.println("克隆对象："+cloneType);
        System.out.println(prototype==cloneType);//预期：返回true,实际为false

        System.out.println("原型对象的爱好属性:"+prototype.getHobbies());
        System.out.println("克隆对象的爱好属性:"+cloneType.getHobbies());
        System.out.println(prototype.getHobbies() == cloneType.getHobbies());
    }
}
