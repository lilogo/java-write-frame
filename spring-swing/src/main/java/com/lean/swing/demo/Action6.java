package com.lean.swing.demo;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * swing标签JLabel
 * 方法名称	说明
 * void setText(Stxing text)	定义 JLabel 将要显示的单行文本
 * void setIcon(Icon image)	定义 JLabel 将要显示的图标
 * void setIconTextGap(int iconTextGap)	如果 JLabel 同时显示图标和文本，则此属性定义它们之间的间隔
 * void setHorizontalTextPosition(int textPosition)	设置 JLabel 的文本相对其图像的水平位置
 * void setHorizontalAlignment(int alignment)	设置标签内容沿 X 轴的对齐方式
 * int getText()	返回 JLabel 所显示的文本字符串
 * Component getLabelFor()	获得将 JLabel 添加到的组件
 * int getIconTextGap()	返回此标签中显示的文本和图标之间的间隔量
 * int getHorizontalTextPosition()	返回 JLabel 的文本相对其图像的水平位置
 * int getHorizontalAlignment()	返回 JLabel 沿 X 轴的对齐方式
 */
public class Action6 extends JFrame{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public Action6() {
        // TODO Auto-generated constructor stub
        setTitle("测试");
        setSize(400,200);
        JPanel jp=new JPanel();    //创建面板
        JLabel label1=new JLabel("1、普通标签");    //创建标签
        JLabel label2=new JLabel();
        label2.setText("2、调用setText()方法");
        //创建既含有文本又含有图标的JLabel对象
        jp.add(label1);//添加标签到面板
        jp.add(label2);
        add(jp);
        setBounds(300,200,400,100);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new Action6();
    }
}
