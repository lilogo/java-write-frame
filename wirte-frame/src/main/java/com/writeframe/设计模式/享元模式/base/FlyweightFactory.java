package com.writeframe.设计模式.享元模式.base;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName FlyweightFactory
 * @Description 享元工厂，因为需要把实例添加到缓存，会创建容器
 *
 * Flyweight Pattern,享元模式。又称轻量级模式，是对象池的一种实现，类似于线程池，线程池可以避免不停
 * 地创建和销毁多个对象，消耗性能。通过减少对象数量从而改善应用所需的对象结构的方式。
 *
 *共享细粒度对象，将多个对同一对象的访问集中起来。来达到资源的重复利用。
 * 应用场景
 * 常常应用于系统底层的开发，以便解决系统性能问题。
 * 系统有大量相似对象，需要缓冲池的场景。
 *
 * 享元模式，提出两个要求。一是细粒度，一是共享。
 * 　　细粒度对象，不可避免就会产生很多对象。
 * 　　内部状态：是享元状态共享出来的东西，它存在享元对象内部，不会跟随环境改变而改变。
 * 　　外部状态：是指对象得以依赖的一个标记，会随着环境变化而改变。是不可共享的状态。
 *
 *优点：
 * 　　减少对象的创建，降低内存中对象的数量，降低系统内存消耗，提高效率。
 * 　　减少内存之外的其他资源占用。
 * 缺点:
 * 　　关注内部 ，外部状态，关注线程安全问题
 * 　　使系统，程序的逻辑复杂化。
 */
public class FlyweightFactory {

    //享元对象是多例的
    private static Map<String, IFlyweight> pool = new HashMap<String, IFlyweight>();

    //因为内部状态具备不变性，因此，作为缓存key
    public static IFlyweight getFlyweight(String intrinsicState) {
        if (!pool.containsKey(intrinsicState)) {
            IFlyweight flyweight = new ConcreteFlyweight(intrinsicState);
            pool.put(intrinsicState, flyweight);
        }
        return pool.get(intrinsicState);
    }
}
