package com.lean.flowable.testGateway;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.lean.flowable.config.BaseConfiguation;


/**
 *排他网关--测试
 *排他网关（也称为XOR网关或更专业的基于数据的排他网关）用于对流程中的决策建模。当执行到达此网关时，将按照定义它们的顺序评估所有传出序列流。选择条件评估为真（或没有条件集，在概念上具有在序列流上定义的*'真'*）的第一序列流以继续该过程。
 * 注意，在这种情况下，输出序列流的语义与BPMN 2.0中的一般情况的语义不同。通常，选择条件评估为真的所有序列流以并行方式继续，而在使用排他网关时仅选择一个序列流。如果多个序列流具有计算结果为true的条件，则选择XML中定义的第一个（并且只有那个！）以继续该过程。如果不能选择序列流，则会抛出异常
 */
public class ExclusiveGatewayTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("exclusiveGateway")
													.name("exclusiveGateway")
													.addClasspathResource("process/gateway/排他网关.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *SELECT * FROM flowable.ACT_RU_EXECUTION;
	 * SELECT * FROM flowable.ACT_RU_TASK;
	 */
	@Test
	public void start() {
		String processDefinitionKey = "exclusiveGateway";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("input", 3);
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "130007";
		taskService.complete(taskId);
	}

}
