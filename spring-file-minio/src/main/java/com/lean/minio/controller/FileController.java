package com.lean.minio.controller;

import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.lean.minio.config.MinioConfig;
import com.lean.minio.utils.MinioUtil;
import com.lean.minio.model.Result;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileController {

    @Autowired
    private MinioUtil minioUtil;

    @Autowired
    private MinioConfig minioConfig;

    /**
     * 上传一个文件
     * @param uploadfile
     * @param bucketName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
    @ResponseBody
    public Result fileupload(@RequestParam(name = "file") MultipartFile uploadfile,
                             @RequestParam(name = "bucketName") String bucketName) throws Exception {
        minioUtil.createBucket(bucketName);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String objectName = sdf.format(new Date()) + "/" + uploadfile.getOriginalFilename();
        minioUtil.uploadFile(uploadfile.getInputStream(), bucketName, objectName);
        String fileUrl = minioConfig.getEndpoint() + "/" + bucketName + "/" + objectName;
        return Result.success(fileUrl);
    }

    /**
     * 列出所有的桶
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listBuckets", method = RequestMethod.GET)
    @ResponseBody
    public Result listBuckets() throws Exception {
        return Result.success(minioUtil.listBuckets());
    }

    /**
     * 递归列出一个桶中的所有文件和目录
     * @param bucket
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listFiles", method = RequestMethod.GET)
    @ResponseBody
    public Result listFiles(@RequestParam String bucket) throws Exception {
        return Result.success("200", minioUtil.listFiles(bucket));
    }

    /**
     * 下载一个文件
     * @param bucket
     * @param objectName
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
    @ResponseBody
    public void downloadFile(@RequestParam String bucket,
                             @RequestParam String objectName,
                             HttpServletResponse response) throws Exception {
        InputStream stream = minioUtil.download(bucket, objectName);
        ServletOutputStream output = response.getOutputStream();
        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(objectName.substring(objectName.lastIndexOf("/") + 1), "UTF-8"));
        response.setContentType("application/octet-stream");
        response.setCharacterEncoding("UTF-8");
        IOUtils.copy(stream, output);
    }


    /**
     * 删除一个文件
     * @param bucket
     * @param objectName
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleteFile", method = RequestMethod.GET)
    @ResponseBody
    public Result deleteFile(@RequestParam String bucket,
                             @RequestParam String objectName) throws Exception {
        minioUtil.deleteObject(bucket, objectName);
        return Result.success();
    }

    /**
     * 删除一个桶
     * @param bucket
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/deleteBucket", method = RequestMethod.GET)
    @ResponseBody
    public Result deleteBucket(@RequestParam String bucket) throws Exception {
        minioUtil.deleteBucket(bucket);
        return Result.success();
    }
}