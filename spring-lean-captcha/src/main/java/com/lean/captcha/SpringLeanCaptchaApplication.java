package com.lean.captcha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanCaptchaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanCaptchaApplication.class, args);
    }

}
