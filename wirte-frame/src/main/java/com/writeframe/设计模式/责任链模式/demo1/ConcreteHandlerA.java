package com.writeframe.设计模式.责任链模式.demo1;

public class ConcreteHandlerA extends Handler {
    @Override
    public void handleRequest(String request) {
        if ("requestA".equals(request)) {
            System.out.println(this.getClass().getSimpleName() + " deal with request:" + request);
            return;
        }
        if (this.nextHandle != null) {
            this.nextHandle.handleRequest(request);
        }
    }
}
