package com.rocketmq.service.controller;

import com.rocketmq.service.config.RocketMqBuilder;
import org.apache.rocketmq.client.producer.*;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RequestMapping("/common")
@RestController
public class CommonController {

    private static final Logger log = LoggerFactory.getLogger("CommonController");

    @Autowired
    private RocketMqBuilder rocketMqUtils;

    private static final String topic = "common_topic";

    private static final String asyncTopic = "common_async_topic";

    private static final String delayTopic = "common_delay_topic";

    private static final String topicOrder = "topic_orderly";

    private static final String syncTopicOrder = "sync_test_topic_orderly";

    private static final String oneTopicOrder = "one_test_topic_orderly";

    /*事务 消费主题 消费组 */
    private static final String txTopic = "common-topic-tx";

    private static final String txGroup = "common-tx-group";

    /**
     * 发送普通批量消息
     *
     * @return
     */
    @RequestMapping("/sendBatchMessage")
    public SendStatus sendBatchMessage() {
        List<Message> msgs = new ArrayList<Message>();
        for (int i = 0; i < 10; i++) {
            msgs.add(MessageBuilder.withPayload("Hello RocketMQ Batch Msg#" + i).setHeader(RocketMQHeaders.KEYS, "KEY_" + i).build());
        }
        SendResult sendResult = rocketMqUtils.syncSendMessage(topic, msgs);
        return sendResult.getSendStatus();
    }

    /**
     * 发送普通消息
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendCommonMessage")
    public SendStatus sendCommonMessage(String message) {
        SendResult sendResult = rocketMqUtils.syncSendMessage(topic, message);
        return sendResult.getSendStatus();
    }

    /**
     * 发送普通消息
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendCommonMessageOne")
    public SendStatus sendCommonMessageOne(String message) {
        SendResult sendResult = rocketMqUtils.syncSendMessage(topic, MessageBuilder.withPayload(message).build());
        return sendResult.getSendStatus();
    }

    /**
     * 发送普通消息
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendCommonMessageTwo")
    public SendStatus sendCommonMessageTwo(String message) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 1);
        map.put("time", new Date());
        map.put("msg", message);
        SendResult sendResult = rocketMqUtils.syncSendMessage(topic, MessageBuilder.withPayload(map).build());
        return sendResult.getSendStatus();
    }


    /**
     * 发送异步消息
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendAsyncCommonMessage")
    public String sendAsyncCommonMessage(String message) {
        rocketMqUtils.asyncSendMessage(asyncTopic, message, new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        return "ok";
    }

    /**
     * 发送异步消息
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendAsyncCommonMessageOne")
    public String sendAsyncCommonMessageOne(String message) {
        rocketMqUtils.asyncSendMessage(asyncTopic, MessageBuilder.withPayload(message).build(), new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        return "ok";
    }


    /**
     * 发送普通消息-带tag
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendCommonMessageByTag")
    public SendStatus sendCommonMessage(String message, String tag) {
        SendResult sendResult = rocketMqUtils.syncSendMessage(topic + ":" + tag, message);
        return sendResult.getSendStatus();
    }


    /**
     * 发送普通消息-带sql
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendCommonMessageBySql")
    public String sendCommonMessageBySql(String message) {
        Map<String, Object> headers = new HashMap<>();
        headers.put("type", "user");
        headers.put("a", 6);
        rocketMqUtils.convertAndSend(topic, MessageBuilder.withPayload(message).build(), headers);
        return "ok";
    }

    /**
     * 顺序信息的三种方式：同步
     *
     * @return
     */
    @RequestMapping("/sendSyncOrderMessage")
    public String sendSyncOrderMessage() {
        //参数一：topic   如果想添加tag,可以使用"topic:tag"的写法
        //参数二：消息内容
        //参数三：hashKey 用来计算决定消息发送到哪个消息队列， 一般是订单ID，产品ID等  顺序消费通过hashKey来确定他们在哪个queue
        String message = "orderly message: ";
        for (int i = 0; i < 10; i++) {
            // 模拟有序发送消息
            rocketMqUtils.syncSendMessage(topicOrder, message + i, "select_queue_key");
        }
        return "ok";
    }

    /**
     * 顺序信息的三种方式：异步
     *
     * @return
     */
    @RequestMapping("/sendASyncOrderMessage")
    public String sendASyncOrderMessage() {
        //参数一：topic   如果想添加tag,可以使用"topic:tag"的写法
        //参数二：消息内容
        //参数三：hashKey 用来计算决定消息发送到哪个消息队列， 一般是订单ID，产品ID等
        rocketMqUtils.asyncSendMessage(syncTopicOrder, "aaaa创建", "aaaa", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        rocketMqUtils.asyncSendMessage(syncTopicOrder, "aaaa支付", "aaaa", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        rocketMqUtils.asyncSendMessage(syncTopicOrder, "aaaa完成", "aaaa", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        rocketMqUtils.asyncSendMessage(syncTopicOrder, "bbbb创建", "bbbb", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        rocketMqUtils.asyncSendMessage(syncTopicOrder, "bbbb支付", "bbbb", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        rocketMqUtils.asyncSendMessage(syncTopicOrder, "bbbb完成", "bbbb", new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("异步消息发送成功:{}", sendResult);
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("异步消息发送失败:{}", throwable.getMessage());
            }
        });
        return "ok";
    }

    /**
     * 顺序信息的三种方式：单向
     *
     * @return
     */
    @RequestMapping("/sendOneWayMessage")
    public String sendOneWayMessage() {
        //参数一：topic   如果想添加tag,可以使用"topic:tag"的写法
        //参数二：消息内容
        //参数三：hashKey 用来计算决定消息发送到哪个消息队列
        String message = "one orderly message: ";
        for (int i = 0; i < 10; i++) {
            // 模拟有序发送消息
            rocketMqUtils.sendOneWay(oneTopicOrder, message + i, "select_queue_key");
        }
        return "ok";
    }


    /**
     * 同步延迟消息
     * rocketMQ的延迟消息发送其实是已发送就已经到broker端了，然后消费端会延迟收到消息。
     * RocketMQ 目前只支持固定精度的定时消息。
     * 固定等级：1到18分别对应1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
     * 延迟的底层方法是用定时任务实现的。
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendSyncDelayMessage")
    public SendStatus sendSyncDelayMessage(String message) {
        Message<String> messageData = MessageBuilder.withPayload(message + new Date()).build();
        /**
         * @param destination formats: `topicName:tags`
         * @param message 消息体
         * @param timeout 发送超时时间
         * @param delayLevel 延迟级别  1到18
         * @return {@link SendResult}
         */
        SendResult sendResult = rocketMqUtils.syncSendMessageTimeOut(delayTopic, messageData, 3000, 3);
        return sendResult.getSendStatus();
    }

    /**
     * 异步延迟消息
     *
     * @param message
     * @return
     */
    @RequestMapping("/sendASyncDelayMessage")
    public String sendASyncDelayMessage(String message) {
        Message<String> messageData = MessageBuilder.withPayload(message + new Date()).build();
        SendCallback sc = new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                log.info("发送异步延时消息成功");
            }

            @Override
            public void onException(Throwable throwable) {
                log.info("发送异步延时消息失败:{}", throwable.getMessage());
            }
        };
        /**
         * @param destination formats: `topicName:tags`
         * @param message 消息体
         * @param timeout 发送超时时间
         * @param delayLevel 延迟级别  1到18
         * @return {@link SendResult}
         */
        rocketMqUtils.asyncSendMessage(delayTopic, messageData, sc, 3000);
        return "ok";
    }

    /**
     * 事务消息
     *
     * @return
     */
    @RequestMapping("/sendTxMessage")
    public String sendTxMessage() {
        String[] tags = {"a", "b", "c"};
        for (int i = 0; i < 3; i++) {
            Message<String> message = MessageBuilder.withPayload("事务消息===>" + i).setHeader("rocketmq_tags", tags[i]).build();
            //发送半事务消息
            TransactionSendResult res = rocketMqUtils.sendMessageInTransaction(txGroup, txTopic + ":" + tags[i], message, i + 1);
            if (res.getLocalTransactionState().equals(LocalTransactionState.COMMIT_MESSAGE) && res.getSendStatus().equals(SendStatus.SEND_OK)) {
                log.info("事物消息发送成功");
            }
            log.info("事物消息发送结果:{}", res);
        }
        return "ok";
    }

}
