package com.lean.flowable.testGateway;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

/**
 * 基于事件的网关--测试
 * 基于事件的网关（event-based gateway）提供了根据事件做选择的方式。
 * 网关的每一条出口顺序流都需要连接至一个中间捕获事件。当流程执行到达基于事件的网关时，与等待状态类似，网关会暂停执行，并且为每一条出口顺序流创建一个事件订阅。
 * 请注意：基于事件的网关的出口顺序流与一般的顺序流不同。这些顺序流从不实际执行。相反，它们用于告知流程引擎：当执行到达一个基于事件的网关时，需要订阅什么事件。
 * 有以下限制： 一个基于事件的网关，必须有两条或更多的出口顺序流。
 * 基于事件的网关，只能连接至intermediateCatchEvent（捕获中间事件）类型的元素（Flowable不支持在基于事件的网关之后连接“接收任务 Receive Task”）。
 * 连接至基于事件的网关的intermediateCatchEvent，必须只有一个入口顺序流。
 */
public class EventbasedGatewayTest extends BaseConfiguation {

    /**
     * 部署
     */
    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder =
            repositoryService.createDeployment().category("eventbasedGateway").name("eventbasedGateway").addClasspathResource("process/gateway/基于事件的网关.bpmn20.xml");
        Deployment deploy = deploymentBuilder.deploy();

        System.out.println("流程ID: " + deploy.getId());
    }

    /**
     * 启动流程实例 当执行到达基于事件的网关时，流程执行暂停。
     * 流程实例订阅alert信号事件，并创建一个10分钟后触发的定时器。流程引擎会等待10分钟，
     * 并同时等待信号事件。 如果信号在10分钟内触发，则会取消定时器，流程沿着信号继续执行，激活Handle
     * alert用户任务。如果10分钟内没有触发信号，则会继续执行，并取消信号订阅。
     */
    @Test
    public void start() {
        String processDefinitionKey = "eventbasedGateway";
        runtimeService.startProcessInstanceByKey(processDefinitionKey);
    }

    //触发信号事件 铺货alert 信号
    @Test
    public void testSign() {
        runtimeService.signalEventReceived("alert");
    }

    /**
     * 完成任务
     */
    @Test
    public void complete() {
        String taskId = "17503";
        taskService.complete(taskId);
    }

    /**
     * 20分钟
     *
     * @throws Exception
     */
    @Test
    public void sleep() throws Exception {
        Long millis = 1200000l;
        Thread.sleep(millis);;
    }
}
