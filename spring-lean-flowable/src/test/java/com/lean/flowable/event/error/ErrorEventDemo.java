package com.lean.flowable.event.error;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.DynamicBpmnService;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ManagementService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.runtime.ProcessInstance;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
/**
 *错误事件:
 *  错误启动事件（error start event），可用于触发事件子流程（Event Sub-Process）。错误启动事件不能用于启动流程实例。
 *
 * 错误启动事件总是中断。
 */
public class ErrorEventDemo extends BaseConfiguation {

    /**
     * classpath方式部署 涉及三张表：ACT_RE_PROCDEF,ACT_RE_DEPLOYMENT,ACT_GE_BYTEARRAY
     */
    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment().category("errorprocessevent").key("errorprocessevent").name("错误事件处理")
            .addClasspathResource("/process/event/错误事件处理.bpmn20.xml");
        Deployment deploy = deploymentBuilder.deploy();

        System.out.println("流程部署ID: " + deploy.getId());
    }

    /**
     * 启动流程实例
     */
    @Test
    public void startProcessInstanceByKey() {
        String processDefinitionKey = "errorprocessevent";
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionKey);

        System.out.println("流程ID: " + processInstance.getId());
    }

    /**
     * 完成任务
     */
    @Test
    public void complete() {
        String taskId = "135010";
        taskService.complete(taskId);

    }

}
