package com.lean.sms.properties;

import lombok.Data;

@Data
public class HuaweiSmsProperties {

    private String senderId;
    /**
     * 短信模板
     */
    private String templateId;
    /**
     * 短信签名
     */
    private String signName;
    /**
     * 接入地址，如：华为云
     */
    private String url;
}
