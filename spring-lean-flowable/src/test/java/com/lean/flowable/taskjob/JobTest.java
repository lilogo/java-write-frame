package com.lean.flowable.taskjob;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

//事件（event）通常用于为流程生命周期中发生的事情建模。事件总是图形化为圆圈。在BPMN 2.0中，有两种主要的事件分类：*捕获（catching）与抛出（throwing）*事件。
//捕获:当流程执行到达这个事件时，会等待直到触发器动作。触发器的类型由其中的图标，或者说XML中的类型声明而定义。捕获事件与抛出事件显示上的区别，是其内部的图标没有填充（即是白色的）。
//抛出:当流程执行到达这个事件时，会触发一个触发器。触发器的类型，由其中的图标，或者说XML中的类型声明而定义。抛出事件与捕获事件显示上的区别，是其内部的图标填充为黑色。
public class JobTest extends BaseConfiguation {

    /**
     * 定时事件
     *      定时触发的相关事件，包括定时器启动事件，定时器捕获中间件事件，定时器边界事件
     *
     * 定时器启动事件
     *      定时器启动事件（timer start event）在指定时间创建流程实例。在流程只需要启动一次，或者流程需要在特定的时间间隔重复启动时，都可以使用。
     *
     * *请注意：*子流程不能有定时器启动事件。
     *
     * *请注意：*定时器启动事件，在流程部署的同时就开始计时。不需要调用startProcessInstanceByXXX就会在时间启动。调用startProcessInstanceByXXX时会在定时启动之外额外启动一个流程。
     *
     * *请注意：*当部署带有定时器启动事件的流程的更新版本时，上一版本的定时器作业会被移除。这是因为通常并不希望旧版本的流程仍然自动启动新的流程实例。
     *
     * 定时器启动事件，用其中有一个钟表图标的圆圈来表示。
     *
     * 固定时间：
     *
     *       <timerEventDefinition>
     *         <timeDate>2022-03-27T23:25:14</timeDate>
     *       </timerEventDefinition>
     *
     *循环时间：
     *
     *
     *持续时间
     *
     */


    @Test
    public void test() throws Exception{
        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/taskjob/定时器启动事件.bpmn20.xml")
                .name("定时器启动事件")
                .deploy();
        System.out.println("-----");
    }


    /**
     * 启动流程实例
     *
     */
    @Test
    public void startProcessInstanceByKey()  throws Exception{

        processEngine.getRuntimeService()
                .startProcessInstanceById("Test04:1:325edb10-ae95-11ec-a77f-c03c59ad2248");
        // 需要在此阻塞比等待长的时间
        TimeUnit.MINUTES.sleep(3);
    }


    @Test
    public void test01() throws Exception{
        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/taskjob/定时器捕获中间事件.bpmn20.xml")
                .name("定时器捕获中间事件...")
                .deploy();
        System.out.println("-----");
    }


    /**
     * 启动流程实例
     *
     */
    @Test
    public void startProcessInstanceByKey1()  throws Exception{

        processEngine.getRuntimeService()
                .startProcessInstanceById("Test04:1:325edb10-ae95-11ec-a77f-c03c59ad2248");
        // 需要在此阻塞比等待长的时间
        TimeUnit.MINUTES.sleep(3);
    }



    @Test
    public void test02() throws Exception{
        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/taskjob/定时器边界事件.bpmn20.xml")
                .name("定时器边界事件...")
                .deploy();
        System.out.println("-----");
    }

    /**
     * 启动流程实例
     *
     */
    @Test
    public void startProcessInstanceByKey2()  throws Exception{

        processEngine.getRuntimeService()
                .startProcessInstanceById("test05:1:c46f83bf-ae97-11ec-b055-c03c59ad2248");
        System.out.println("开始启动的时间：" + LocalDateTime.now().toString());
        // 需要在此阻塞比等待长的时间
        TimeUnit.MINUTES.sleep(3);
    }

    //小结：timeDuration在三种定时器的事件中
    //定时器启动事件：等待指定时间后启动流程实例
    //定时器中间事件：AB任务中间有个定时器中间事件，A任务处理后需要等待对应的时间才能流转到B处
    //定时器边界事件：任务A绑定了定时器边界事件后，如果在等待时间以内A没有处理任务，那么就会触发对应的边界事件


}
