package com.lean.flowable.testTask.test;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.FormService;
import org.flowable.engine.form.StartFormData;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.flowable.engine.repository.ProcessDefinition;
import org.flowable.engine.repository.ProcessDefinitionQuery;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 表单：
 * act_fo_form_definition
 * act_fo_form_deployment
 * act_fo_form_instance
 * act_fo_form_resource
 */
public class FormDmnEmailTest extends BaseConfiguation {

    /**
     * 部署流程
     * act_re_deployment: 流程定义部署表，每部署一次就增加一条记录
     * act_ge_bytearray ：流程资源表，流程部署的 bpmn文件和png图片会保存在该表中
     *流程表：act_re_procdef
     *注意：
     * 业务流程定义数据表。此表和ACT_RE_DEPLOYMENT是多对一的关系，即，一个部署的bar包里可能包含多个流程定义文件，每个流程定义文件都会有一条记录在ACT_REPROCDEF表内，
     * 每个流程定义的数据，都会对于ACT_GE_BYTEARRAY表内的一个资源文件和PNG图片文件。和ACT_GE_BYTEARRAY的关联是通过程序用ACT_GE_BYTEARRAY.NAME与ACT_RE_PROCDEF.NAME_完成的
     */
    @Test
    public void deploy() {

        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery()
                .processDefinitionKey("form-decison-email").list();
        if(!CollectionUtils.isEmpty(list)){
            list.stream().forEach(processDefinition -> {
                repositoryService.deleteDeployment(processDefinition.getDeploymentId(), true);
            });
        }
        DeploymentBuilder deploymentBuilder = repositoryService
                .createDeployment()
                .category("form-decison-email")
                .name("form-decison-email")
                .addClasspathResource("process/task/表单决策邮件测试.bpmn20.xml")
                //引入外部表单
                .addClasspathResource("process/task/测试决策.dmn")
                //外置决策表
                .addClasspathResource("process/task/test-form.form");
        Deployment deploy = deploymentBuilder.deploy();

        System.out.println("流程ID: " + deploy.getId());
    }
    /**
     * 启动流程实例
     *
     */
    @Test
    public void start() {
        System.out.println("-------------第一步： 查询出流程定义--------------------------");
        String processDefinitionKey = "form-decison-email";
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        ProcessDefinition processDefinition = processDefinitionQuery.processDefinitionKey(processDefinitionKey).singleResult();
        FormService formService = null;
        // 判断是否存在流程开始表单
        if (processDefinition.hasStartFormKey()) {
            // 获取开始流程表单
            formService = processEngine.getFormService();
            StartFormData form = formService.getStartFormData(processDefinition.getId());
            System.out.println("1.------==----"+form.getFormProperties());
            System.out.println("2.------==----"+form.toString());
        };

        System.out.println("-------------第二步： 加载表单或决策表中的数据--------------------------");

        Map<String, Object> vars = new HashMap<String, Object>();
        //表单 使用runtimeService.getStartFormModel获取开始表单
        //因为表单数据实际上是存在流程variables里的，所以使用execution.getVariable()
        //流程中的表单有两种：流程开始表单和流程中表单
        //查看是否有表单
                // 流程开始表单   ProcessDefinition.hasStartFormKey();
                // 流程中表单     Task.getFormKey();
        //获取开始流程表单
            //ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionKey(code).latestVersion().singleResult();
            //StartFormData form = formService.getStartFormData(pd.getId());

            //FormInfo info = formRepositoryService.getFormModelByKey(form.getFormKey());
            //info.getFormModel();

        //获取流程中用户需要填写的表单

            //TaskFormData form = formService.getTaskFormData(taskId);

        //填写表单
            //runtimeService.startProcessInstanceWithForm   和  formService.submitStartFormData都能填写完成表单并开始流程
        //也就是说其实表单填写的数据都是放在variables里的

        //1.开始表单

        /**
         * ProcessDefinition pd = repositoryService.createProcessDefinitionQuery().processDefinitionKey("formRequest").latestVersion().singleResult();
         *         Map<String, String> properties = new HashMap<>();
         *         properties.put("startTime", "2018-12-14");
         *         properties.put("endTime", "2018-12-20");
         *         properties.put("reason", "回家");
         * //        ProcessInstance pi = runtimeService.startProcessInstanceWithForm(pd.getId(), "sendToParent", properties, null);
         *         ProcessInstance pi = formService.submitStartFormData(pd.getId(), UUID.randomUUID().toString(), properties);
         */

        //2.流程中表单
        /**
         * TaskService.completeTaskWithForm(String taskId, String formDefinitionId, String outcome, Map<String, Object> variables)
         */
        //设置表单参数
        vars.put("name", "张三");
        vars.put("days", "12");
        vars.put("remark", "备注");

        System.out.println("-------------第三步： 启动流程实例--------------------------");

        // 附带表单数据启动流程实例
        //    ProcessInstance RuntimeService.startProcessInstanceWithForm(String processDefinitionId, String outcome, Map<String,Object> properties, String taskName);
        // 附带表单数据完成任务
        //    void TaskService.completeTaskWithForm(String taskId, String formDefinitionId, String outcome, Map<String,Object> properties);

        ProcessInstance holidayRequest = runtimeService.startProcessInstanceWithForm(processDefinition.getId(), UUID.randomUUID().toString(), vars, null);

//        ProcessInstance holidayRequest = formService.submitStartFormData(processDefinition.getId(), UUID.randomUUID().toString(), vars);

        System.out.println("holidayRequest.getProcessDefinitionId() = " + holidayRequest.getProcessDefinitionId());
        System.out.println("holidayRequest.getName() = " + holidayRequest.getName());
        System.out.println("holidayRequest.getActivityId() = " + holidayRequest.getActivityId());
        System.out.println("holidayRequest.getId() = " + holidayRequest.getId());

        //获取表单任务
        Task task = taskService.createTaskQuery().processInstanceId(holidayRequest.getProcessInstanceId()).active().singleResult();

        System.out.println("task.getId() = " + task.getId());
        System.out.println("task.getName() = " + task.getName());

        //完成表单任务 自动执行决策任务 和发送邮件 完成任务

        Map<String, Object> resultMap = new HashMap<String, Object>();
        //决策表中的变量
        resultMap.put("days",9);

        //邮件配置
        String recipient = "731727335@qq.com";
        String recipientName = "JamesYee";
        String subject = "flowable邮件服务测试";

        resultMap.put("recipient", recipient);
        resultMap.put("recipientName", recipientName);
        resultMap.put("subject", subject);

        resultMap.put("gender", "male");
        resultMap.put("html", "<html><body>Hello ${gender == 'male' ? 'Mr' : 'Ms' }. <b>flowable邮件服务测试</b><body></html>");

        taskService.complete(task.getId(),resultMap);


    }

}
