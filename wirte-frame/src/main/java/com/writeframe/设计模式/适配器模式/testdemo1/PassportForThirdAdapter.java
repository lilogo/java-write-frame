package com.writeframe.设计模式.适配器模式.testdemo1;


import com.writeframe.设计模式.适配器模式.ResultMsg;

/**
 * 【2】适配器扩展系统　
 * B.适配器类
 * 这里使用，类适配器实现。
 * @ClassName PassportForThirdAdapter
 * @Description 第三方登录适配器，类适配器
 */
public class PassportForThirdAdapter extends PassportService implements IPassportForThird {
    @Override
    public ResultMsg loginForQQ(String openId) {
        return loginForRegister(openId, null);
    }

    @Override
    public ResultMsg loginForWeChat(String openId) {
        return loginForRegister(openId, null);
    }

    @Override
    public ResultMsg loginForToken(String token) {
        return loginForRegister(token, null);
    }

    @Override
    public ResultMsg loginForTelephone(String phone, String code) {
        return loginForRegister(phone, null);
    }

    /**
     * 提供一个老的登录逻辑，先注册，再登录
     * 当使用第三方登录时，不能够得到密码，处理办法是：如果密码为null,约定一个特殊密码。
     * 后台做一个标识，只要得到这个特殊密码，都表示第三方登录，走特殊流程
     *
     * @param username
     * @param password
     * @return
     */
    private ResultMsg loginForRegister(String username, String password) {
        if (null == password) {
            password = "THIRD_EMPTY";
        }
        super.register(username, password);
        return super.login(username, password);
    }
}
