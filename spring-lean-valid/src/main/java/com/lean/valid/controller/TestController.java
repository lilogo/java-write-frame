package com.lean.valid.controller;

import com.lean.valid.entity.ResponseResult;
import com.lean.valid.entity.User;
import com.lean.valid.enums.ResultEnum;
import com.lean.valid.exception.ParamaErrorException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

/**
 * 1. 分组
 *
 * @Validated：提供了一个分组功能，可以在入参验证时，根据不同的分组采用不同的验证机制。没有添加分组属性时，默认验证没有分组的验证属性。
 * @Valid：作为标准JSR-303规范，还没有吸收分组的功能。
 *
 * 2. 注解地方
 *
 * @Validated：可以用在类型、方法和方法参数上。但是不能用在成员属性（字段）上
 * @Valid：可以用在方法、构造函数、方法参数和成员属性（字段）上
 * 两者是否能用于成员属性（字段）上直接影响能否提供嵌套验证的功能。
 *
 * 3. 嵌套验证
 *
 * @Validated：用在方法入参上无法单独提供嵌套验证功能。不能用在成员属性（字段）上，也无法提示框架进行嵌套验证。能配合嵌套验证注解@Valid进行嵌套验证。
 * @Valid：用在方法入参上无法单独提供嵌套验证功能。能够用在成员属性（字段）上，提示验证框架进行嵌套验证。能配合嵌套验证注解@Valid进行嵌套验证。
 *
 */
//开启校验
@Validated
@RestController
public class TestController {

    /**
     * 获取用户信息
     *
     * @param username 姓名
     * @return ResponseResult
     */
    @Validated
    @GetMapping("/user")
    public ResponseResult findUserInfo(@RequestParam String username) {
        if (username == null || "".equals(username)) {
            throw new ParamaErrorException("username 不能为空");
        }
        return new ResponseResult(ResultEnum.SUCCESS);
    }


    /**
     * 新增用户
     *
     * @param user 用户信息
     * @return ResponseResult
     */
    @PostMapping("/user")
    public ResponseResult addUserInfo(@Valid @RequestBody User user) {
        return new ResponseResult(ResultEnum.SUCCESS);
    }

}
