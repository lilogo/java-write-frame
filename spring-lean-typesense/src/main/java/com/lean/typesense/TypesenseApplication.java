package com.lean.typesense;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 文档：https://typesense.org/docs/0.24.1/api/#what-s-new
 */

@SpringBootApplication
public class TypesenseApplication {

    public static void main(String[] args) {
        SpringApplication.run(TypesenseApplication.class, args);
    }

}
