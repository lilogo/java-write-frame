package com.lean.springmvc.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解
 */
@Target(ElementType.METHOD)//元注解。METHOD表示该这个自定义注解的作用范围是在方法上
@Retention(RetentionPolicy.RUNTIME)//表示运行时这个注解起作用
@Documented//生成文档注释，可以不用加
public @interface RequestMapping {
    String value() default "";//添加service注解的参数value
}
