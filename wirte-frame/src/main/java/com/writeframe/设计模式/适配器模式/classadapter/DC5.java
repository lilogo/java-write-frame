package com.writeframe.设计模式.适配器模式.classadapter;


/**
 * 【2】定义目标对象【实际是接口】
 * @ClassName DC5
 * @Description 直流电压5v
 */
public interface DC5 {
    int output5V();
}
