package com.writeframe.设计模式.装饰器模式.demo;

/**
 * 装饰器模式。也叫包装器模式（Wrapper Pattern）,是指在不改变原有对象的基础上，将功能附加到对象上，
 * 提供了比继承更有弹性的替代方案（扩展原有对象的功能）属于结构型模式。
 * 1.生活中的装饰器模式：
 * 　　煎饼【可以加各种辅助材料，本质不变】
 * 　　蛋糕。
 * 2.适用场景
 * 》用于扩展一个类的功能或给一个类添加附加职责。
 * 》动态给一个对象添加功能，这些功能可以再动态的撤销。
 *
 * 装饰器模式：
 * 　　是一种特殊的静态代理模式。
 * 　　强调自身功能的扩展。透明【用户知道扩展细节】的扩展，可动态定制的扩展。
 * 代理模式：
 * 　　强调代理过程的控制。
 * 3.装饰器模式的优缺点
 * 优点：
 * 　　装饰器是继承的有力补充，比继承更灵活。不改变原有对象的情况下，动态地给一个对象扩展功能，即插即用。
 * 　　通过使用不同的装饰类，以及这些装饰类的排列组合，可实现更为强大的功能。　
 * 　　装饰器完全遵循开闭原则。
 * 缺点：
 * 　　会出现更多的代码，更多的类，增加程序复杂性。
 * 　　动态装饰时，多层装饰会比较复杂。客户端调用复杂。
 *
 *
 */
public class DecoratorModel {
    public static void main(String[] args) {
        AbstractBattercake battercake = new BaseBattercake();
        System.out.println(battercake.getMsg() + "，总价是：" + battercake.getPrice());

        //加鸡蛋
        battercake = new EggDecorator(battercake);
        System.out.println(battercake.getMsg() + "，总价是：" + battercake.getPrice());

        //加2个鸡蛋
        battercake = new EggDecorator(battercake);
        System.out.println(battercake.getMsg() + "，总价是：" + battercake.getPrice());

        //加香肠
        battercake = new SausageDecorator(battercake);
        System.out.println(battercake.getMsg() + "，总价是：" + battercake.getPrice());
    }
}
