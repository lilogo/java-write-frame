package com.lean.mvc.util;

/**
 * Simple strategy interface for resolving a String value.
 * Used by {@link com.lean.mvc.beans.factory.config.ConfigurableBeanFactory}.
 * <p>
 *
 *
 *
 *
 *
 *
 */
public interface StringValueResolver {

    String resolveStringValue(String strVal);

}
