package com.write.mybatis.base;


import java.util.Map;

public class Configuration {

    private String username; //用户名
    private String password;//密码
    private String url;//地址
    private String driver;//驱动

    private Map<String, Mapper> mapperMap; //map 集合 Map<唯一标识，Mapper> 用于保存映射文件中的 sql标识及 sql 语句
    //set、get方法


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public Map<String, Mapper> getMapperMap() {
        return mapperMap;
    }

    public void setMapperMap(Map<String, Mapper> mapperMap) {
        this.mapperMap = mapperMap;
    }
}
