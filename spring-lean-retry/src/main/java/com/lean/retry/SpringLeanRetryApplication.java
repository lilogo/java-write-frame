package com.lean.retry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;


@EnableRetry //
@SpringBootApplication
public class SpringLeanRetryApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanRetryApplication.class, args);
    }

}
