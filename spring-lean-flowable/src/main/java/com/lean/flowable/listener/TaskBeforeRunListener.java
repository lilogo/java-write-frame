package com.lean.flowable.listener;

import java.util.Set;

import org.flowable.engine.delegate.TaskListener;
import org.flowable.task.service.delegate.DelegateTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson2.JSON;
import com.lean.flowable.base.ProcessConstants;

/**
 * 任务监听器
 * create:任务创建后触发
 * assignment:任务分配后触发
 * Delete:任务完成后触发
 * All：所有事件都触发
 */
@Component(value = "taskBeforeRunListener")
public class TaskBeforeRunListener implements TaskListener {

    private static final Logger log = LoggerFactory.getLogger(TaskListener.class);

    /**
     * event（事件）（必填）：触发任务监听器的任务事件类型。可用的事件有：
     *
     * create（创建）：当任务已经创建，并且所有任务参数都已经设置时触发。
     *
     * assignment（指派）：当任务已经指派给某人时触发。请注意：当流程执行到达用户任务时，在触发create事件之前，会首先触发assignment事件。这顺序看起来不太自然，但是有实际原因的：当收到create事件时，我们通常希望能看到任务的所有参数，包括办理人。
     *
     * complete（完成）：当任务已经完成，从运行时数据中删除前触发。
     *
     * delete（删除）：在任务即将被删除前触发。请注意任务由completeTask正常完成时也会触发。
     *
     * @param delegateTask
     */
    @Override
    public void notify(DelegateTask delegateTask) {
        log.warn("进入通用用户任务启动监听器");
        String eventName = delegateTask.getEventName();
        log.info("event:" + eventName);
        // 查询信息
        log.info("任务执行人：" + delegateTask.getAssignee());
        log.info("任务配置ID: " + delegateTask.getTaskDefinitionKey());
        // 查询变量
        Set<String> setNames = delegateTask.getVariableNames();
        if (!CollectionUtils.isEmpty(setNames)) {
            log.info("任务变量:" + setNames.toString());
            for (String varName : setNames) {
                Object varValue = delegateTask.getVariable(varName);
                if (varValue != null) {
                    log.info("变量名:" + varName + " 变量值:" + JSON.toJSONString(varValue));
                }
            }
        }
        String executionId = delegateTask.getExecutionId();
        String taskDefinitionKey = delegateTask.getTaskDefinitionKey();
        String processInstanceId = delegateTask.getProcessInstanceId();
        System.out.println("eventName = " + eventName);
        System.out.println("executionId = " + executionId);
        System.out.println("taskDefinitionKey = " + taskDefinitionKey);
        System.out.println("processInstanceId = " + processInstanceId);

        String name = delegateTask.getName();
        log.info("监听器触发了：" + name);
        String formKey = delegateTask.getFormKey();
        log.info("监听器--formKey：" + formKey);
        // 设置下一个任务的 执行人 执行组
        delegateTask.setAssignee("234");
        // 更改任务拥有者
        // delegateTask.setOwner();
        log.warn("退出通用用户任务启动监听器");
    }
}
