package com.lean.invoice.sysInfo;

import oshi.SystemInfo;
import oshi.hardware.CentralProcessor;
import oshi.hardware.CentralProcessor.TickType;
import oshi.hardware.GlobalMemory;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.software.os.FileSystem;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;
import oshi.util.Util;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * 服务器相关信
 */
public class ServerInfomation {

    private static final int OSHI_WAIT_SECOND = 1000;
    /**
     * CPU相关信息
     */
    private SysCpu sysCpu = new SysCpu();
    /**
     * 內存相关信息
     */
    private SysMem sysMem = new SysMem();
    /**
     * JVM相关信息
     */
    private SysJvm sysJvm = new SysJvm();
    /**
     * 服务器相关信
     */
    private SysBaseInfo sysBaseInfo = new SysBaseInfo();
    /**
     * 磁盘相关信息
     */
    private List<SysFile> sysFiles = new LinkedList<SysFile>();

    private List<MenModel> memNameList=new ArrayList<>();


    public SysCpu getCpu() {
        return sysCpu;
    }

    public void setCpu(SysCpu sysCpu) {
        this.sysCpu = sysCpu;
    }

    public SysMem getMem() {
        return sysMem;
    }

    public void setMem(SysMem mem) {
        this.sysMem = mem;
    }

    public SysJvm getJvm() {
        return sysJvm;
    }

    public void setJvm(SysJvm sysJvm) {
        this.sysJvm = sysJvm;
    }

    public SysBaseInfo getSys() {
        return sysBaseInfo;
    }

    public void setSys(SysBaseInfo sysBaseInfo) {
        this.sysBaseInfo = sysBaseInfo;
    }

    public List<SysFile> getSysFiles() {
        return sysFiles;
    }

    public void setSysFiles(List<SysFile> sysFiles) {
        this.sysFiles = sysFiles;
    }

    public List<MenModel> getMemNameList() {
        return memNameList;
    }

    public void setMemNameList(List<MenModel> memNameList) {
        this.memNameList = memNameList;
    }


    public void getSysInfo() throws Exception {
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        setCpuInfo(hal.getProcessor());
        setMemInfo(hal.getMemory());
        setSysInfo();
        setJvmInfo();
        setSysFiles(si.getOperatingSystem());
        setMenEcharts(getMem());
    }

    private void setMenEcharts(SysMem sysMem) {
        double free = sysMem.getFree();
        double used = sysMem.getUsed();
        double total = sysMem.getTotal();
        MenModel menModel=new MenModel();
        menModel.setName("总内存(G)");
        menModel.setValue(total+"");
        MenModel menModel1=new MenModel();
        menModel1.setName("已用内存(G)");
        menModel1.setValue(used+"");
        MenModel menModel2=new MenModel();
        menModel2.setName("剩余内存(G)");
        menModel2.setValue(free+"");

        memNameList.add(menModel);
        memNameList.add(menModel1);
        memNameList.add(menModel2);
    }

    /**
     * 设置CPU信息
     */
    private void setCpuInfo(CentralProcessor processor) {
        // CPU信息
        long[] prevTicks = processor.getSystemCpuLoadTicks();
        Util.sleep(OSHI_WAIT_SECOND);
        long[] ticks = processor.getSystemCpuLoadTicks();
        long nice = ticks[TickType.NICE.getIndex()] - prevTicks[TickType.NICE.getIndex()];
        long irq = ticks[TickType.IRQ.getIndex()] - prevTicks[TickType.IRQ.getIndex()];
        long softirq = ticks[TickType.SOFTIRQ.getIndex()] - prevTicks[TickType.SOFTIRQ.getIndex()];
        long steal = ticks[TickType.STEAL.getIndex()] - prevTicks[TickType.STEAL.getIndex()];
        long cSys = ticks[TickType.SYSTEM.getIndex()] - prevTicks[TickType.SYSTEM.getIndex()];
        long user = ticks[TickType.USER.getIndex()] - prevTicks[TickType.USER.getIndex()];
        long iowait = ticks[TickType.IOWAIT.getIndex()] - prevTicks[TickType.IOWAIT.getIndex()];
        long idle = ticks[TickType.IDLE.getIndex()] - prevTicks[TickType.IDLE.getIndex()];
        long totalCpu = user + nice + cSys + idle + iowait + irq + softirq + steal;
        sysCpu.setCpuNum(processor.getLogicalProcessorCount());
        sysCpu.setTotal(totalCpu);
        sysCpu.setSys(cSys);
        sysCpu.setUsed(user);
        sysCpu.setWait(iowait);
        sysCpu.setFree(idle);


    }

    /**
     * 设置内存信息
     */
    private void setMemInfo(GlobalMemory memory) {
        sysMem.setTotal(memory.getTotal());
        sysMem.setUsed(memory.getTotal() - memory.getAvailable());
        sysMem.setFree(memory.getAvailable());
    }

    /**
     * 设置服务器信
     */
    private void setSysInfo() {
        Properties props = System.getProperties();
        sysBaseInfo.setComputerName(IpUtils.getHostName());
        sysBaseInfo.setComputerIp(IpUtils.getHostIp());
        sysBaseInfo.setOsName(props.getProperty("os.name"));
        sysBaseInfo.setOsArch(props.getProperty("os.arch"));
        sysBaseInfo.setUserDir(props.getProperty("user.dir"));
    }

    /**
     * 设置Java虚拟
     */
    private void setJvmInfo() throws UnknownHostException {
        Properties props = System.getProperties();
        sysJvm.setTotal(Runtime.getRuntime().totalMemory());
        sysJvm.setMax(Runtime.getRuntime().maxMemory());
        sysJvm.setFree(Runtime.getRuntime().freeMemory());
        sysJvm.setVersion(props.getProperty("java.version"));
        sysJvm.setHome(props.getProperty("java.home"));
    }

    /**
     * 设置磁盘信息
     */
    private void setSysFiles(OperatingSystem os) {
        FileSystem fileSystem = os.getFileSystem();
        List<OSFileStore> fileStores = fileSystem.getFileStores();
        for (OSFileStore fs : fileStores) {
            long free = fs.getUsableSpace();
            long total = fs.getTotalSpace();
            long used = total - free;
            SysFile sysFile = new SysFile();
            sysFile.setDirName(fs.getMount());
            sysFile.setSysTypeName(fs.getType());
            sysFile.setTypeName(fs.getName());
            sysFile.setTotal(convertFileSize(total));
            sysFile.setFree(convertFileSize(free));
            sysFile.setUsed(convertFileSize(used));
            sysFile.setUsage(ArithUtils.mul(ArithUtils.div(used, total, 4), 100));
            sysFiles.add(sysFile);
        }
    }

    /**
     * 字节转换
     *
     * @param size 字节大小
     * @return 转换后
     */
    public String convertFileSize(long size) {
        long kb = 1024;
        long mb = kb * 1024;
        long gb = mb * 1024;
        if (size >= gb) {
            return String.format("%.1f GB", (float) size / gb);
        } else if (size >= mb) {
            float f = (float) size / mb;
            return String.format(f > 100 ? "%.0f MB" : "%.1f MB", f);
        } else if (size >= kb) {
            float f = (float) size / kb;
            return String.format(f > 100 ? "%.0f KB" : "%.1f KB", f);
        } else {
            return String.format("%d B", size);
        }
    }
}
