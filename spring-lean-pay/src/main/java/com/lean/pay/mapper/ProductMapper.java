package com.lean.pay.mapper;

import com.lean.pay.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface ProductMapper extends BaseMapper<Product> {

}
