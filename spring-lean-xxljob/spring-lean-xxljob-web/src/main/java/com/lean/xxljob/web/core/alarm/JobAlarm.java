package com.lean.xxljob.web.core.alarm;

import com.lean.xxljob.web.core.model.XxlJobInfo;
import com.lean.xxljob.web.core.model.XxlJobLog;

/**
 * @author xuxueli 2020-01-19
 */
public interface JobAlarm {

    /**
     * job alarm
     *
     * @param info
     * @param jobLog
     * @return
     */
    public boolean doAlarm(XxlJobInfo info, XxlJobLog jobLog);

}
