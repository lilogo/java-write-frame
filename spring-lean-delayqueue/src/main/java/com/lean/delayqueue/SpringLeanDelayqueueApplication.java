package com.lean.delayqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanDelayqueueApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanDelayqueueApplication.class, args);
    }

}
