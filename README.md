# java-write-frame

#### 介绍
learning-pdf-word：pdf、svg 、png、项目监控、图片换白底、提取视频封面、图片压缩等demo

spring-monitoring：监控

spring-secrity-learn：SpringSecrity学习

spring-mongodb： mongodb学习

wirte-frame：手写框架学习总结

spring-druidMonitor：druid监控

ssoserver：单点登录

websocket-infomation：监控用户状态，信息等

websocket-log：实时监控日志

spring-JSqlParser：SQL语句解析

spring-flyway：数据库版本管理

spring-execl：execl导入导出

spring-mybatis-velocity：自动生成前后端代码的工具

spring-file-minio：minio-对象存储服务

spring-lean-caffeine：caffeine本地缓存

spring-lean-guava-cache：guava cache本地缓存

spring-generate-layui-code：springBoot 自动生成一个完整的后台项目，前端页面是layui

spring-generate-vue：springBoot vue自动生成

spring-lean-redis: redis 监控功能

spring-lean-quartz：定时任务动态添加修改启动等

spring-lean-sms：集成各大平台短信服务

spring-lena-upload：集成各大平台文件存储服务

spring-lean-xmindparser：解析xmind文件

spring-lean-process： 模拟进度条

spring-lean-pay： 支付（支付宝、微信）

spring-lean-mybatis： mybatis插件-数据权限

spring-lean-auth： SpringSecurity 权限包含数据权限

spring-lean-canal： 阿里巴巴 MySQL binlog 增量订阅&消费组件

spring-lean-retry： Spring自带重试注解

spring-lean-redis： redis key过期监听、redis监控

spring-lean-shortLink： 短链接生成

spring-lean-rocketmq： rocketMQ 学习整理

spring-amazon： 产品订单整理

spring-lean-captcha： 验证码

spring-lean-look： 在线预览

spring-lean-image： 图片处理

spring-lean-event: spring 事件处理

spring-lean-shardingsphere: 分库分表

spring-lean-word: word 编写导出

spring-lean-xxljob: 分布式定时器

spring-lean-neo4j: 图数据库

spring-lean-kafka: kafka

spring-lean-itextpdf: pdf生成、二次处理

spring-lean-influxdb: 时间序列库

spring-lean-flowable: flowable 流程梳理

spring-lean-delayqueue: 图数据库

spring-lean-cassandra: 列 分布式Nosql数据库

spring-lean-activity: activity 流程梳理

spring-lean-valid: 参数验证






