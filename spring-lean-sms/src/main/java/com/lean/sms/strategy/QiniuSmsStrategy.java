package com.lean.sms.strategy;

import com.lean.sms.properties.SmsProperties;
import com.qiniu.http.Response;
import com.qiniu.sms.SmsManager;
import com.qiniu.util.Auth;
import java.util.Map;

/**
 * 七牛云短信
 *https://developer.qiniu.com/sms/5993/sms-sdk
 * @author 
 */
public class QiniuSmsStrategy extends SmsStrategy {

    private final SmsManager smsManager;

    public QiniuSmsStrategy(SmsProperties smsProperties) {
        this.smsProperties = smsProperties;
        Auth auth = Auth.create(smsProperties.getConfig().getAccessKey(), smsProperties.getConfig().getSecretKey());
        smsManager = new SmsManager(auth);
    }

    @Override
    public void send(String mobile, Map<String, String> params) {
        try {
            Response response = smsManager.sendSingleMessage(smsProperties.getQiniu().getTemplateId(), mobile, params);
            // 是否发送成功
            if (!response.isOK()) {
                throw new RuntimeException(response.error);
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
