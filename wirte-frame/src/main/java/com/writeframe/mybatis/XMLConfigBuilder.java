package com.writeframe.mybatis;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * xml解析器，sql执行器，以及创建数据原的类 用于解析配置文件
 */
public class XMLConfigBuilder {

    /**
     * 解析主配置文件，把里面的内容填充到DefaultSqlSession所需要的地方 使用的技术： dom4j+xpath
     */
    public static Configuration loadConfiguration(InputStream config) {
        try {
            // 定义封装连接信息的配置对象（mybatis的配置对象）
            Configuration cfg = new Configuration();

            // 1.获取SAXReader对象
            SAXReader reader = new SAXReader();
            // 2.根据字节输入流获取Document对象
            Document document = reader.read(config);
            // 3.获取根节点
            Element root = document.getRootElement();
            // 4.使用xpath中选择指定节点的方式，获取所有property节点
            List<Element> propertyElements = root.selectNodes("//property");
            // 5.遍历节点
            for (Element propertyElement : propertyElements) {
                // 判断节点是连接数据库的哪部分信息
                // 取出name属性的值
                String name = propertyElement.attributeValue("name");
                if ("driver".equals(name)) {
                    // 表示驱动
                    // 获取property标签value属性的值
                    String driver = propertyElement.attributeValue("value");
                    cfg.setDriver(driver);
                }
                if ("url".equals(name)) {
                    // 表示连接字符串
                    // 获取property标签value属性的值
                    String url = propertyElement.attributeValue("value");
                    cfg.setUrl(url);
                }
                if ("username".equals(name)) {
                    // 表示用户名
                    // 获取property标签value属性的值
                    String username = propertyElement.attributeValue("value");
                    cfg.setUsername(username);
                }
                if ("password".equals(name)) {
                    // 表示密码
                    // 获取property标签value属性的值
                    String password = propertyElement.attributeValue("value");
                    cfg.setPassword(password);
                }
            }
            // 取出mappers中的所有mapper标签，判断他们使用了resource还是class属性
            List<Element> mapperElements = root.selectNodes("//mappers/mapper");
            // 遍历集合
            for (Element mapperElement : mapperElements) {
                // 判断mapperElement使用的是哪个属性
                Attribute attribute = mapperElement.attribute("resource");
                if (attribute != null) {
                    System.out.println("使用的是XML配置");
                    // 表示有resource属性，用的是XML
                    // 取出属性的值
                    String mapperPath = attribute.getValue();// 获取映射文件位置："cn/blogsx/dao/IUserDao.xml"
                    // 把 映射配置文件 的内容获取出来，封装成一个map
                    Map<String, Mapper> mappers = loadMapperConfiguration(mapperPath);
                    // 给configuration中的mappers赋值
                    cfg.setMappers(mappers);
                } else {
                    System.out.println("使用的是注解配置");
                    // 表示没有resource属性，用的是注解
                    // 获取class属性的值
                    String daoClassPath = mapperElement.attributeValue("class");
                    // 根据daoClassPath获取封装的必要信息
                    Map<String, Mapper> mappers = loadMapperAnnotation(daoClassPath);
                    // 给configuration中的mappers赋值
                    cfg.setMappers(mappers);
                }
            }
            // 返回Configuration
            return cfg;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                config.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 根据传入的参数，解析XML，并且封装到Map中
     *
     * @param mapperPath 映射配置文件的位置
     * @return map中包含了获取的唯一标识（key是由dao的全限定类名和方法名组成） 以及执行所需的必要信息（value是一个Mapper对象，里面存放的是执行的SQL语句和要封装的实体类全限定类名）
     */
    private static Map<String, Mapper> loadMapperConfiguration(String mapperPath) {
        InputStream in = null;
        try {
            // 定义返回值对象
            Map<String, Mapper> mappers = new HashMap();
            // 根据路径获取字节输入流
            in = Resources.getResourceAsStream(mapperPath);
            // 根据字节输入流获取 Document 对象
            SAXReader reader = new SAXReader();
            Document document = reader.read(in);
            // 获取根节点,获取根节点的 namespace 属性取值
            Element root = document.getRootElement();
            String namespace = root.attributeValue("namespace");// 是组成 map 中 key 的部分
            // 获取所有的 select 节点
            List<Element> selectElements = root.selectNodes("//select");
            List<Element> insertElements = root.selectNodes("//insert");
            List<Element> updateElements = root.selectNodes("//update");
            List<Element> deleteElements = root.selectNodes("//delete");
            List<Element> allElements = new ArrayList<>();
            allElements.addAll(selectElements);
            allElements.addAll(insertElements);
            allElements.addAll(updateElements);
            allElements.addAll(deleteElements);
            // 遍历 select 节点集合
            String paramType = null;
            for (Element selectElement : allElements) {
                String type = selectElement.getName();// 获取到标签的名字，也就是SQL语句的类型
                String id = selectElement.attributeValue("id");
                String useGeneratedKeys = selectElement.attributeValue("useGeneratedKeys");
                String keyProperty = selectElement.attributeValue("keyProperty");
                String keyColumn = selectElement.attributeValue("keyColumn");
                String resultType = selectElement.attributeValue("resultType");
                if (selectElement.attribute("parameterType") != null) {
                    paramType = selectElement.attribute("parameterType").getValue();// 获取返回值类型
                }
                // 取出文本内容 组成 map 中的SQL语句
                String querySql = selectElement.getText();
                // 创建 Key
                String key = namespace + "." + id;
                if (mappers.containsKey(key)) {
                    throw new RuntimeException("xml存在同一个key");
                }
                // 创建 Value
                Mapper mapper = new Mapper();
                mapper.setType(type);
                mapper.setParamType(paramType);
                mapper.setQueryString(querySql.trim());
                mapper.setResultType(resultType);
                mapper.setUseGeneratedKeys(useGeneratedKeys);
                mapper.setKeyColumn(keyColumn);
                mapper.setKeyProperty(keyProperty);
                // 把 key 和 value 存入 mappers 中
                mappers.put(key, mapper);
            }
            return mappers;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 根据传入的参数，得到dao中所有被select注解标注的方法。 根据方法名称和类名，以及方法上注解value属性的值，组成Mapper的必要信息
     *
     * @param daoClassPath
     * @return
     */
    private static Map<String, Mapper> loadMapperAnnotation(String daoClassPath) throws ClassNotFoundException {
        // 定义返回值对象
        Map<String, Mapper> mappers = new HashMap<String, Mapper>();
        // 得到 dao 接口的字节码对象
        Class daoClass = Class.forName(daoClassPath);
        // 得到 dao 接口中的方法数组
        Method[] methods = daoClass.getMethods();
        // 遍历 Method 数组
        for (Method method : methods) {
            // 取出每一个方法，判断是否有 select 注解
            if (method.isAnnotationPresent(Select.class)) {
                // 创建 Mapper 对象
                Mapper mapper = new Mapper();
                // 取出注解的 value 属性值
                Select selectAnno = method.getAnnotation(Select.class);
                String queryString = selectAnno.value();
                mapper.setQueryString(queryString);
                // 获取当前方法的返回值，还要求必须带有泛型信息
                Type type = method.getGenericReturnType();// List<User>
                // 判断 type 是不是参数化的类型
                if (type instanceof ParameterizedType) {
                    // 强转
                    ParameterizedType ptype = (ParameterizedType)type;
                    // 得到参数化类型中的实际类型参数
                    Type[] types = ptype.getActualTypeArguments();
                    // 取出第一个
                    Class domainClass = (Class)types[0];
                    // 获取 domainClass 的类名
                    String resultType = domainClass.getName();
                    // 给 Mapper 赋值
                    mapper.setResultType(resultType);
                }
                // 组装 key 的信息
                // 获取方法的名称
                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();
                String key = className + "." + methodName;
                // 给 map 赋值
                mappers.put(key, mapper);
            }
        }
        return mappers;
    }

}
