package com.writeframe.设计模式.命令模式.demo1;


import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Controller
 * @Description 遥控器, 发出命令
 * @Author wf
 * @Date 2020/6/19 14:35
 * @Version 1.0
 */
public class Controller {
    private List<IAction> actions = new ArrayList<IAction>();

    public void addAction(IAction action) {
        actions.add(action);
    }

    public void batchExecute() {
        for (IAction action : actions) {
            action.execute();
        }
        actions.clear();
    }

    public void execute(IAction action) {
        action.execute();
    }
}
