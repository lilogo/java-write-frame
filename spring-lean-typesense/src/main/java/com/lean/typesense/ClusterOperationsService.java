package com.lean.typesense;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;
import org.typesense.api.Client;

import java.util.HashMap;
import java.util.Map;

/**
 * 集群操作
 */
@Service
public class ClusterOperationsService {

    @Autowired
    private Client client;

    /**
     * 创建快照（用于备份）
     *
     * @return
     * @throws Exception
     */
    public Map<String, String> create(String path) throws Exception {
        HashMap<String, String> query = new HashMap<>();
        query.put("snapshot_path", path);
        return client.operations.perform("snapshot", query);
    }

    /**
     * 压缩磁盘数据库
     *
     * @return
     * @throws Exception
     */
    public Map<String, String> db() throws Exception {
        return client.operations.perform("db/compact");
    }

    /**
     * 重新选举领导人
     *
     * @return
     * @throws Exception
     */
    public Map<String, String> vote() throws Exception {
        return client.operations.perform("vote");
    }

    /**
     * Get current RAM, CPU, Disk & Network usage metrics.
     * @return
     * @throws Exception
     */
    public Map<String, String> metrics() throws Exception {
        return client.metrics.retrieve();
    }

    /**
     * Toggle 慢速请求日志
     *
     * @return
     * @throws Exception
     */
    public HttpResponse toggleSlowRequestLog() {
        return HttpRequest.post("https://xxxxxxx.net/config")
                .header("X-TYPESENSE-API-KEY", "Qum8OjyuRZsikLXUr6QoNjnQLQBeyQbq")
                .body("{\"log-slow-requests-time-ms\":2000}")
                .timeout(5 * 60 * 1000)
                .execute();
    }
    /**
     * API 统计
     *
     * @return
     * @throws Exception
     */
    public HttpResponse apiStats() {
        HttpResponse response =HttpRequest.get("https://xxxxxxx.net/stats.json")
                  .header("X-TYPESENSE-API-KEY", "Qum8OjyuRZsikLXUr6QoNjnQLQBeyQbq")
                  .timeout(5 * 60 * 1000)
                  .execute();
        return response;
    }

    /**
     * 健康
     *
     * @return
     * @throws Exception
     */
    public Map<String, Object> health() throws Exception {
        return client.health.retrieve();

    }

}
