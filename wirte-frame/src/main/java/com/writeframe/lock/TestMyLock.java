package com.writeframe.lock;


import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName: TestMyLock
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/5 11:25
 */
public class TestMyLock {
    MyLock myLock = new MyLock();
    AtomicInteger atomicInteger = new AtomicInteger(0);
    public void incr() {
        myLock.lock();
        atomicInteger.incrementAndGet();
        myLock.unlock();
    }


    public static void main(String[] args) {
        TestMyLock testMyLock = new TestMyLock();
        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                for (int j = 0; j < 10000; j++) {
                    testMyLock.incr();
                }
            }).start();
        }
        try {
            Thread.sleep(2000);
            System.out.println("i=" + testMyLock.atomicInteger.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}
