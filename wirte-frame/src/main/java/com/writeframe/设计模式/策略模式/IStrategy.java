package com.writeframe.设计模式.策略模式;

/**
 *  顶层策略接口
 */
public interface IStrategy {
    /**
     * 算法接口
     */
    void algorithm();
}
