package com.lean.springsecurity.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 *
 * comment:
 */
@SpringBootApplication
@RestController
@EnableSwagger2
//如果引入的是app项目，则打开如下注解
//@ComponentScan(basePackages = {"com.lean.springsecurity.app","com.lean.springsecurity.demo"
//,"com.lean.springsecurity.core"})
//@EnableWebSecurity
//如果引入的是browser项目，则打开如下注解
@ComponentScan(basePackages = {"com.lean.springsecurity.browser","com.lean.springsecurity.demo"
,"com.lean.springsecurity.core"})
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 60*10,redisNamespace = "self#security#session")
public class SpringSecurityDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityDemoApplication.class);
    }

    @GetMapping("/hello")
    public String hello(){
        return "hello spring springsecurity";
    }

}
