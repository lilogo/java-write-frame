package com.write.mybatis.base;

import java.io.InputStream;

public class SqlSessionFactoryBuilder {

    /**
     * 根据传入的流，实现对 SqlSessionFactory 的创建
     *
     * @param inputStream
     * @return
     */
    public SqlSessionFactory build(InputStream inputStream) {
        DefaultSqlSessionFactory factory = new DefaultSqlSessionFactory();
        factory.setInput(inputStream);
        return factory;
    }
}
