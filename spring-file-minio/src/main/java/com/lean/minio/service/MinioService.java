package com.lean.minio.service;

import io.minio.BucketExistsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.UploadObjectArgs;
import io.minio.errors.MinioException;
import org.springframework.stereotype.Service;

/**
 * @Author zhw
 **/
@Service
public class MinioService {
    public static void main(String[] args) throws Exception {
        try {
            MinioClient minioClient = MinioClient.builder().endpoint("http://192.168.16.79:9000")
                    .credentials("kI6vjh97ftvUfqTU", "ns5yNXEsXeGO0CGedlXpPcyByiJrQ8L9").build();
            boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("mall").build());
            if (!found) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("abc").build());
            } else {
                System.out.println("Bucket已存在");
            }
			minioClient.uploadObject(UploadObjectArgs.builder().bucket("mall").object("1111.pdf").filename("e:\\1111.pdf").build());
        } catch (MinioException e) {
            System.out.println("Error occurred: " + e);
            System.out.println("HTTP trace: " + e.httpTrace());
        }
    }
}
