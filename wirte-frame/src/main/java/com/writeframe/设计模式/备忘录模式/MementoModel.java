package com.writeframe.设计模式.备忘录模式;

/**
 * 备忘录模式【Memento Pattern】，也称快照模式【Snapshot Pattern】,或令牌模式【Token Pattern],是指在不破坏封装的前提下，
 * 捕获一个对象内部状态，并在对象之外保存这个状态，这样以后就可将该对象恢复到原先保存的状态。
 * 　　特征："后悔药"
 *备忘录模式在生活中体现
 *      代码版本管理中的撤销和恢复
 *      游戏存档
 *备忘录模式的适用场景
 * 1.需要保存历史快照的场景
 * 2.希望在对象之外保存状态，且除了自己，其他类对象无法访问状态保存的具体内容。
 *
 *优点：
 *      1.简化发起人实体类【Originator】的职责,隔离状态存储与获取。实现信息封装，客户端无须关心状态的保存细节。
 *      2.提供状态回滚的功能。
 * 缺点：
 * 　　  1.消耗资源：如果需要存储的状态过多时，每一次保存都会消耗很多内存。
 *
 */
public class MementoModel {

    /**
     * 在网页编辑中，常常会用到富文本编辑器。会提供一个草稿箱【相当于剪贴板】，支持撤销，和恢复功能。
     * @param args
     */
    public static void main(String[] args) {
        DraftsBox box = new DraftsBox();
        Editor editor = new Editor("手写spring", "文章节选至《。。》", "xxx.png");
        ArticleMemento articleMemento = editor.saveToMemento();
        box.addMemento(articleMemento);

        System.out.println("标题：" + editor.getTitle() +
                "\n内容：" + editor.getContent() +
                "\n图片：" + editor.getImages() +
                "暂存成功");

        System.out.println("完整信息：" + editor);

        System.out.println("==========第一次修改文章==========");
        editor.setTitle("【Tom原创】，我是这样手写spring中，麻雀虽小五脏俱全");
        editor.setContent("文章节选至《。。》,tom著");

        System.out.println("===============首次完成文章修改==============");
        System.out.println("完整信息：" + editor);
        articleMemento = editor.saveToMemento();
        box.addMemento(articleMemento);
        System.out.println("===============保存到草稿箱==============");

        System.out.println("==========第二次修改文章==========");
        editor.setTitle("手写spring核心原理功能实现");
        System.out.println("完整信息：" + editor);

        System.out.println("===============第二次完成文章修改==============");
        System.out.println("==========第一次撤销回退==========");
        ArticleMemento memento = box.getMemento();
        editor.undoFromMemento(memento);
        System.out.println("完整信息：" + editor);
        System.out.println("===============第1次撤销完成==============");

        System.out.println("==========第二次撤销回退==========");
        memento = box.getMemento();
        editor.undoFromMemento(memento);
        System.out.println("完整信息：" + editor);
        System.out.println("===============第2次撤销完成==============");
    }
}
