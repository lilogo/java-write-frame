package com.lean.auth.service;


import com.lean.auth.entity.SysRoleEntity;
import com.lean.auth.page.PageResult;
import com.lean.auth.query.SysRoleQuery;
import com.lean.auth.vo.SysRoleDataScopeVO;
import com.lean.auth.vo.SysRoleVO;

import java.util.List;

/**
 * 角色
 *
 * 
 */
public interface SysRoleService extends BaseService<SysRoleEntity> {

	PageResult<SysRoleVO> page(SysRoleQuery query);

	List<SysRoleVO> getList(SysRoleQuery query);

	void save(SysRoleVO vo);

	void update(SysRoleVO vo);

	void dataScope(SysRoleDataScopeVO vo);

	void delete(List<Long> idList);
}
