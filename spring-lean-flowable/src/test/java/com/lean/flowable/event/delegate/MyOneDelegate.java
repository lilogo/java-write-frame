package com.lean.flowable.event.delegate;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

import java.time.LocalDateTime;

//补偿自动任务
public class MyOneDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {

        System.out.println("完成自动审批任务-----》MyOneDelegate" + LocalDateTime.now().toString());
    }
}
