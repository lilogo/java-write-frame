package com.lean.acitivity.common;

import lombok.Data;
import org.springframework.http.HttpStatus;


@Data
public class Result<T>{
	private static final long serialVersionUID = 1L;

	private T data;

	private Integer code;

	private String message;


	public static Result error() {
		return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "未知异常，请联系管理员");
	}

	public static Result error(String msg) {
		return error(HttpStatus.NOT_IMPLEMENTED.value(), msg);
	}

	public static Result error(int code, String msg) {
		Result r = new Result();
		r.setCode(code);
		r.setMessage(msg);
		return r;
	}
	public static Result ok(Object object) {
		Result r = new Result();
		r.setData(object);
		return r;
	}

	public static Result ok() {
		return new Result();
	}

}
