package com.lean.wxchat.person.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WxchatPersonApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxchatPersonApiApplication.class, args);
    }

}
