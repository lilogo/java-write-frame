package com.guava.cache.lean.service;

import com.guava.cache.lean.entity.UserInfo;
import java.util.ArrayList;
import java.util.List;
public class UserDataFactory {
    private UserDataFactory() {
    }
    private static List<UserInfo> userDtoList;
    static {
        // 初始化集合
        userDtoList = new ArrayList<>();
        UserInfo user = null;
        for (int i = 0; i < 5; i++) {
            user = new UserInfo();
            user.setName("user" + i);
            user.setAge(i);
            userDtoList.add(user);
        }
    }
    public static List<UserInfo> getUserDaoList() {
        return userDtoList;
    }
}
