package com.lean.mvc.jdbc.core;

/**
 * @author zhangdd on 2022/2/9
 */
public interface SqlProvider {

    String getSql();
}
