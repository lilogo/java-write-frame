package com.writeframe.设计模式.解释器模式;

/**
 * @ClassName Interpreter
 * @Description 操作数解释器实现
 */
public abstract class Interpreter implements IArithmeticInterpreter {
    protected IArithmeticInterpreter left;
    protected IArithmeticInterpreter right;

    public Interpreter(IArithmeticInterpreter left, IArithmeticInterpreter right) {
        this.left = left;
        this.right = right;
    }
    //这里是一个空方法，具体逻辑在子类实现
    @Override
    public int interpret() {
        return 0;
    }
}
