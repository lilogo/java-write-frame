package com.lean.websocketlog.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

/**
 * @ClassName: WebSocketConfig
 * @Description: 配置WebSocket消息代理端点，即stomp服务端
 * @Author: zhanghongwei
 * @Date: 2022/6/17 16:28
 */
@Configuration
@EnableWebSocketMessageBroker
@EnableScheduling
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Bean(value = "serverEndpointExporter")
    public ServerEndpointExporter serverEndpointExporter() {
        return new ServerEndpointExporter();
    }
    /**
     * 注册stomp端点。起到的作用就是添加一个服务端点，来接收客户端的连接，
     * registry.addEndpoint("/websocket")
     * 表示添加了一个 /websocket 端点，客户端可以通过这个端点来进行连接。
     * withSockJS() 的作用是开启 SockJS 访问支持，即可通过http://IP:PORT/websocket 来和服务端 websocket 连接。
     */
    @Override
    public void registerStompEndpoints(final StompEndpointRegistry registry) {
        registry.addEndpoint("/websocket").setAllowedOrigins("http://localhost:8087")
                .withSockJS();
    }

}
