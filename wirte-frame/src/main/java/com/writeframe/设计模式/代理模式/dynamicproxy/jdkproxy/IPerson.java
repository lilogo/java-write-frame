package com.writeframe.设计模式.代理模式.dynamicproxy.jdkproxy;

/**
 * @ClassName IPerson
 * @Description 程序员顶层抽象接口
 */
public interface IPerson {
    void findLove();
}
