package com.lean.flowable.model;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.bpmn.model.*;
import org.flowable.bpmn.model.Process;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import java.util.Arrays;

/**
 * DeploymentBuilder addBpmnModel(String resourceName, BpmnModel bpmnModel);
 * 部署流程文档
 */
public class BpmnModelTest extends BaseConfiguation {

    @Test
    public void deploy() {
        BpmnModel model = createModel();
        DeploymentBuilder deploymentBuilder =
            repositoryService.createDeployment().
                    category("bpmnmodeltest")
                    .key("bpmnmodeltest")
                    .name("自定义bpmnModel")
                    .addBpmnModel("test-bpmnmodel",model);
        Deployment deploy = deploymentBuilder.deploy();
        System.out.println("流程ID: " + deploy.getId());
    }

    public BpmnModel createModel() {
        SequenceFlow flow1 = new SequenceFlow();
        flow1.setId("flow1");
        flow1.setName("开始节点->任务节点1");
        flow1.setSourceRef("start1");
        flow1.setTargetRef("userTask1");
        // 任务节点1->任务节点2
        SequenceFlow flow2 = new SequenceFlow();
        flow2.setId("flow2");
        flow2.setName("任务节点1->任务节点2");
        flow2.setSourceRef("userTask1");
        flow2.setTargetRef("userTask2");

        // 任务节点1->任务节点2
        SequenceFlow flow3 = new SequenceFlow();
        flow3.setId("flow3");
        flow3.setName("任务节点2->结束节点");
        flow3.setSourceRef("userTask2");
        flow3.setTargetRef("endEvent");

        String resource = "shareniu_addBpmnModel";
        // 声明BpmnModel对象
        BpmnModel bpmnModel = new BpmnModel();
        // 声明Process对象 一个BpmnModel可以包含多个Process对象
        Process process = new Process();
        process.setId("process1");
        // 开始节点的封装
        StartEvent start = new StartEvent();
        start.setName("开始节点");
        start.setId("start1");
        start.setOutgoingFlows(Arrays.asList(flow1));
        // 任务节点1
        UserTask userTask1 = new UserTask();
        userTask1.setName("任务节点1");
        userTask1.setId("userTask1");
        userTask1.setIncomingFlows(Arrays.asList(flow1));
        userTask1.setOutgoingFlows(Arrays.asList(flow2));
        // 任务节点2
        UserTask userTask2 = new UserTask();
        userTask2.setName("任务节点2");
        userTask2.setId("userTask2");
        userTask2.setIncomingFlows(Arrays.asList(flow2));
        userTask2.setOutgoingFlows(Arrays.asList(flow3));

        // 结束节点
        EndEvent endEvent = new EndEvent();
        endEvent.setName("结束节点");
        endEvent.setId("endEvent");
        endEvent.setIncomingFlows(Arrays.asList(flow3));
        // 将所有的FlowElement添加到process中
        process.addFlowElement(start);
        process.addFlowElement(flow1);
        process.addFlowElement(userTask1);
        process.addFlowElement(flow2);
        process.addFlowElement(userTask2);
        process.addFlowElement(flow3);
        process.addFlowElement(endEvent);
        bpmnModel.addProcess(process);
        return bpmnModel;
    }

}
