package com.lean.flowable.demo.delegate;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

/**
 * 服务任务
 *
 *
 */
public class SignalstartServiceTaskJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {
		System.out.println("==================消息事件====================");
		System.out.println("当前流程定义id: " + execution.getProcessDefinitionId());
		System.out.println("当前流程实例id: " + execution.getProcessInstanceId());
		//获取当前节点
		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点名称: " + flowElement.getName());

	}

}
