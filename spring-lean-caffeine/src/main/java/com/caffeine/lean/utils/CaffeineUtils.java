package com.caffeine.lean.utils;

import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Caffeine缓存工具类
 * Cache 可以有的操作
 * V getIfPresent(K key) ：如果缓存中 key 存在，则获取 value，否则返回 null。
 * void put( K key, V value)：存入一对数据 <key, value>。
 * Map<K, V> getAllPresent(Iterable<?> var1) ：参数是一个迭代器，表示可以批量查询缓存。
 * void putAll( Map<? extends K, ? extends V> var1); 批量存入缓存。
 * void invalidate(K var1)：删除某个 key 对应的数据。
 * void invalidateAll(Iterable<?> var1)：批量删除数据。
 * void invalidateAll()：清空缓存。
 * long estimatedSize()：返回缓存中数据的个数。
 * CacheStats stats()：返回缓存当前的状态指标集。
 * ConcurrentMap<K, V> asMap()：将缓存中所有的数据构成一个 map。
 * void cleanUp()：会对缓存进行整体的清理，比如有一些数据过期了，但是并不会立马被清除，所以执行一次 cleanUp 方法，会对缓存进行一次检查，清除那些应该清除的数据。
 * V get( K var1, Function<? super K, ? extends V> var2)：第一个参数是想要获取的 key，第二个参数是函数
 **/
@Component
public class CaffeineUtils {

    @Autowired
    Cache<String, Object> caffeineCache;

    /**
     * 添加或更新缓存
     *
     * @param key
     * @param value
     */
    public void putAndUpdateCache(String key, Object value) {
        caffeineCache.put(key, value);
    }


    /**
     * 获取对象缓存
     *
     * @param key
     * @return
     */
    public <T> T getObjCacheByKey(String key, Class<T> t) {
        caffeineCache.getIfPresent(key);
        return (T) caffeineCache.asMap().get(key);
    }

    /**
     * 根据key删除缓存
     *
     * @param key
     */
    public void removeCacheByKey(String key) {
        // 从缓存中删除
        caffeineCache.asMap().remove(key);
    }
}


