package com.lean.flowable.taskjob;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class IntermediateTimerServiceTaskJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {

		System.out.println("＝＝＝＝＝＝＝＝＝＝＝中间抛出事件触发＝＝＝＝＝＝＝＝＝＝＝");

		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点: " + flowElement.getName());

	}

}
