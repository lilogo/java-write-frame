package com.lean.flowable.taskjob;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

import java.time.LocalDateTime;

public class SignalStartOnedelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("-------触发了-111-------->"+ LocalDateTime.now().toString());
    }
}
