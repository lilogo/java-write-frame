package com.writeframe.设计模式.状态模式.demo;

/**
 * @ClassName ConcreteStateB
 * @Description 具体状态实现子B
 */
public class ConcreteStateB implements IState {
    @Override
    public void handle() {
        System.out.println("StateB do action");
    }
}
