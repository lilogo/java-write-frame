package com.writeframe.设计模式.桥接模式.base;

/**
 * @ClassName ConcreteImplementorB
 * @Description 接口实现B
 */
public class ConcreteImplementorB implements IImplementor {
    @Override
    public void operationImpl() {
        System.out.println("I'm ConcreteImplementorB");
    }
}
