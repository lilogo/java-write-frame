package com.lean.springsecurity.example.validate;

import com.lean.springsecurity.example.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * comment: 自定义校验逻辑
 */
public class SelfValidatorImpl implements ConstraintValidator<SelfValidator, Object> {

    @Autowired
    private HelloService helloService;

    @Override
    public void initialize(SelfValidator constraintAnnotation) {
        System.out.println("my validator init");
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
//        helloService.greeting("tom");
        //TODO:这里处理校验逻辑，如果校验通过，则返回true，否则返回false
        System.out.println(value);
        return false;
    }
}
