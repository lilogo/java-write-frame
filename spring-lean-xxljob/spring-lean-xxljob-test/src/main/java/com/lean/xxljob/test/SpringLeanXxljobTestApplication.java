package com.lean.xxljob.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanXxljobTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanXxljobTestApplication.class, args);
    }

}
