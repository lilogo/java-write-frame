package com.rocketmq.service.tx;


import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

/**
 * 生产者消息监听器：
 * 用于监听本地事务执行的状态和检查本地事务状态。
 * 异步
 */
@RocketMQTransactionListener(txProducerGroup = "tx-group")
public class TransactionMsgListener implements RocketMQLocalTransactionListener {

    private static final Logger log = LoggerFactory.getLogger("TransactionMsgListener");


    @Autowired
    private UserService userService;


    /**
     * 执行本地事务（在发送消息成功时执行）
     * executeLocalTransaction方法，当我们处理完业务后，可以根据业务处理情况，返回事务执行状态，有rollback, commit or unknown三种，
     * 分别是回滚事务，提交事务和未知；根据事务消息执行流程，如果返回rollback，则直接丢弃消息；如果是返回commit，则消费消息；如果是unknow，
     * 则继续等待，然后调用checkLocalTransaction方法，最多重试15次，超过了默认丢弃此消息；
     * @param message
     * @param obj
     * @return commit or rollback or unknown
     */
    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object obj) {
        // 获取消息体里参数
        MessageHeaders messageHeaders = message.getHeaders();
        String transactionId = (String) messageHeaders.get(RocketMQHeaders.TRANSACTION_ID);
        log.info("【执行本地事务】消息体参数：transactionId={}", transactionId);
        // 执行带有事务注解的本地方法：增加用户余额+保存mq日志
        try {
            UserModel userModel = (UserModel) obj;
            int total = userService.addBalance(userModel, transactionId);
            if (total != 0) {
                //让去check本地事务状态 进行事务补偿
                //如果本地事务返回UNKNOWN，会进行事务补偿，自动执行下面的checkLocalTransaction方法
                return RocketMQLocalTransactionState.UNKNOWN;
            }
            return RocketMQLocalTransactionState.COMMIT; // 正常：向MQ Server发送commit消息
        } catch (Exception e) {
            log.error("【执行本地事务】发生异常，消息将被回滚", e);
            return RocketMQLocalTransactionState.ROLLBACK; // 异常：向MQ Server发送rollback消息
        }


    }

    /**
     * 检查本地事务的状态
     * 回查方法不一定会执行，但是得有，回查就是根据我们之前生成穿过来的那个事务id（transactionId）来查询事务日志表，这样的好处是业务牵涉的表再多无所谓，我这个日志表也与你本地事务绑定，
     * 我只需查询这一张事务表就够了，能找到就代表本地事务执行成功了
     *
     * @param message
     * @return
     */
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        MessageHeaders headers = message.getHeaders();
        String transactionId = headers.get(RocketMQHeaders.TRANSACTION_ID, String.class);
        log.info("【回查本地事务】transactionId={}", transactionId);
        try {
            // 根据事务id查询事务日志表
            MQTransactionLog mqTransactionLog = userService.getMqTransactionLogById(transactionId);
            if (null == mqTransactionLog) {
                // 没查到表明本地事务执行失败,通知回滚
                return RocketMQLocalTransactionState.ROLLBACK;
            }
            //事务补偿提交 查到表明本地事务执行成功，提交
            return RocketMQLocalTransactionState.COMMIT;
        } catch (Exception e) {
            //发生异常 重试
            //如果事务补偿过程还是UNKNOWN 就会一直进行事务补偿，60s一次
            return RocketMQLocalTransactionState.UNKNOWN;
        }
    }
}


