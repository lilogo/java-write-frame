package com.lean.geneate.layui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGenerateLayuiCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringGenerateLayuiCodeApplication.class, args);
    }

}
