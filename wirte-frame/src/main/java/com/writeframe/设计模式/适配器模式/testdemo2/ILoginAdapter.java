package com.writeframe.设计模式.适配器模式.testdemo2;


import com.writeframe.设计模式.适配器模式.ResultMsg;

/**
 * 【3】顶层适配器接口
 * @ClassName ILoginAdapter
 * @Description 适配器公共接口
 */
public interface ILoginAdapter {
    /**
     * 匹配适配器子类实现
     * @param object
     * @return
     */
    boolean support(Object object);

    /**
     * 登录逻辑
     * @param id
     * @param adapter
     * @return
     */
    ResultMsg login(String id, Object adapter);
}
