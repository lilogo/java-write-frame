package com.writeframe.设计模式.备忘录模式;

/**
 * @ClassName Editor
 * @Description 编辑器，备忘录发起者
 */
public class Editor {
    private String title;
    private String content;
    private String images;

    public Editor(String title, String content, String images) {
        this.title = title;
        this.content = content;
        this.images = images;
    }

    public ArticleMemento saveToMemento(){
        ArticleMemento articleMemento = new ArticleMemento(title,content,images);
        return articleMemento;
    }
    public void undoFromMemento( ArticleMemento articleMemento){
        //撤销，还原编辑器内容，为快照内容
        this.title = articleMemento.getTitle();
        this.content = articleMemento.getContent();
        this.images = articleMemento.getImages();
    }

    @Override
    public String toString() {
        return "Editor{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", images='" + images + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getImages() {
        return images;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setImages(String images) {
        this.images = images;
    }
}
