package com.lean.flowable.event.delegate;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

import java.time.LocalDateTime;

/**
 * 取消事件结束
 */
public class MyThreeDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("MyThreeDelegate---->执行了" + LocalDateTime.now().toString());
    }
}
