package com.writeframe.baselock;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 在多线程程序设计中，经常会遇到一个线程等待一个或多个线程的场景，遇到这样的场景应该如何解决？
 *
 * 如果是一个线程等待一个线程，则可以通过await()和notify()来实现；
 *
 * 如果是一个线程等待多个线程，则就可以使用CountDownLatch和CyclicBarrier来实现比较好的控制。
 *
 * 有四个游戏玩家玩游戏，游戏有三个关卡，每个关卡必须要所有玩家都到达后才能允许通关。
 *
 * 其实这个场景里的玩家中如果有玩家A先到了关卡1，他必须等待其他所有玩家都到达关卡1时才能通过，也就是说线程之间需要互相等待， 这和CountDownLatch的应用场景有区别，CountDownLatch里的线程是到了运行的目标后继续干自己的其他事情， 而这里的线程需要等待其他线程后才能继续完成下面的工作。
 *
 * 1. CyclicBarrier(int parties)
 *
 * 这里的parties也是一个计数器，例如，初始化时parties里的计数是3，于是拥有该CyclicBarrier对象的线程当parties的计数为3时就唤醒， 注：这里parties里的计数在运行时当调用CyclicBarrier:await()时,计数就加1，一直加到初始的值
 *
 * 2. CyclicBarrier(int parties, Runnable barrierAction)
 *
 * 这里的parties与上一个构造方法的解释是一样的，这里需要解释的是第二个入参（Runnable barrierAction）, 这个参数是一个实现Runnable接口的类的对象，也就是说当parties加到初始值时就出发barrierAction的内容。
 *
 *
 * 适用场景：满了才开始进行下一步操作
 *
 */
public class CyclicBarrierDemo {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(4, new Runnable() {

            @Override
            public void run() {
                System.out.println("所有玩家进入第二关！");
            }
        });

        for (int i = 0; i < 4; i++) {
            int finalI = i;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        System.out.println("玩家" + finalI + "正在玩第一关...");
                        cyclicBarrier.await();
                        System.out.println("玩家" + finalI + "进入第二关...");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
