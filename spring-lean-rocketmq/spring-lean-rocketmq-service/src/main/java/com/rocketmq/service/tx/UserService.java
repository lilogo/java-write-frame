package com.rocketmq.service.tx;

import com.alibaba.fastjson.JSON;
import com.rocketmq.service.config.Caffeines;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

@Service
public class UserService {

    @Autowired
    private Caffeines caffeineUtils;

    /**
     * 本地事务 处理业务 然后将本地事务id 追加到事务表中。
     * @param userModel
     * @param transactionId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public int addBalance(UserModel userModel, String transactionId) {
        int random=new Random().nextInt(10);
        if(random%2==0){
            // 1. 增加余额
            caffeineUtils.putAndUpdateCache("user-key"+transactionId,JSON.toJSONString(userModel));
            // 2. 写入mq事务日志
            MQTransactionLog transactionLog = new MQTransactionLog();
            transactionLog.setTransactionId(transactionId);
            transactionLog.setLog(JSON.toJSONString(userModel));
            caffeineUtils.putAndUpdateCache("txLog"+transactionId,JSON.toJSONString(transactionLog));
            return 1;
        }else {
            return 0;
        }

    }

    /**
     * 查询事务表，确定本地事务是否执行成功
     * @param transactionId
     * @return
     */
    public MQTransactionLog getMqTransactionLogById(String transactionId) {
        String cache = caffeineUtils.getObjCacheByKey("txLog" + transactionId, String.class);
        return JSON.parseObject(cache, MQTransactionLog.class);
    }

    /**
     * 模拟消费者接受成功,处理后续业务
     * @param userId
     * @param chargeAmount
     * @return
     */
    public int addNumber(String userId, Integer chargeAmount) {
        int random=new Random().nextInt(10);
        if(random%2==0){
            caffeineUtils.putAndUpdateCache("user-score"+userId,chargeAmount);
            return 1;
        }
        return 0;
    }
}
