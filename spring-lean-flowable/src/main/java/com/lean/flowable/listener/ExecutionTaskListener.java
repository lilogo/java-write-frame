package com.lean.flowable.listener;

import java.util.Map;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.lean.flowable.base.ProcessConstants;

/**
 * 执行监听器 流程 开始 进行 结束
 */
@Component(value = "executionTaskListener")
public class ExecutionTaskListener implements ExecutionListener {

    private static final Logger log = LoggerFactory.getLogger("StartListener");

    @Override
    public void notify(DelegateExecution execution) {

        String eventName = execution.getEventName();
        log.info("eventName : " + eventName);
        if ("start".equals(eventName)) {
            // 流程开始
            log.info("start......");
        } else if ("end".equals(eventName)) {
            // 流程结束
            log.info("end......");
        } else if ("take".equals(eventName)) {
            // 连线监听器
            log.info("take......");
        }
        String currentActivityId = execution.getCurrentActivityId();
        log.info("currentActivityId : " + currentActivityId);
        String processInstanceId = execution.getProcessInstanceId();
        log.info("processInstanceId : " + processInstanceId);
        String processDefinitionId = execution.getProcessDefinitionId();
        log.info("processDefinitionId : " + processDefinitionId);
        String processInstanceBusinessKey = execution.getProcessInstanceBusinessKey();
        log.info("processInstanceBusinessKey : " + processInstanceBusinessKey);
        Map<String, Object> transientVariables = execution.getTransientVariables();
        if (!CollectionUtils.isEmpty(transientVariables)) {
            transientVariables.forEach((s, o) -> {
                log.info("key : " + s + "value: " + o);
            });
        }

        // 执行之前设置变量
        execution.setVariable("roleIdList", ProcessConstants.USERGROUPLIST);
        execution.setVariable("assigneeList", ProcessConstants.USERIDLIST);

    }
}
