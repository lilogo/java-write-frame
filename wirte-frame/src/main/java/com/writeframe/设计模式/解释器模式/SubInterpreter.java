package com.writeframe.设计模式.解释器模式;

/**
 * @ClassName SubInterpreter
 * @Description 减法解释器
 */
public class SubInterpreter extends Interpreter{
    public SubInterpreter(IArithmeticInterpreter left, IArithmeticInterpreter right) {
        super(left, right);
    }

    @Override
    public int interpret() {
        return this.left.interpret() - this.right.interpret();
    }
}
