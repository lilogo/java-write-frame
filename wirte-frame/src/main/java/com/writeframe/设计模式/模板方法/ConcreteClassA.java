package com.writeframe.设计模式.模板方法;

public class ConcreteClassA extends AbstractClass {
    @Override
    public void PrimitiveOperation1() {
        System.out.println("具体类A方法1实现");
    }

    @Override
    public void PrimitiveOperation2() {
        System.out.println("具体类A方法2实现");
    }
}
