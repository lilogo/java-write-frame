package com.writeframe.设计模式.桥接模式.base;

/**
 * @ClassName RefinedAbstraction
 * @Description 修正抽象
 */
public class RefinedAbstraction extends Abstraction {

    public RefinedAbstraction(IImplementor mImplementor) {
        super(mImplementor);
    }

    @Override
    public void operation() {
        super.operation();
        System.out.println("refined operation");
    }
}
