package com.writeframe.设计模式.备忘录模式;

/**
 * @ClassName ArticleMemento
 * @Description 文章备忘录
 */
public class ArticleMemento {
    private String title;
    private String content;
    private String images;

    public ArticleMemento(String title, String content, String images) {
        this.title = title;
        this.content = content;
        this.images = images;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getImages() {
        return images;
    }
}
