package com.lean.swing;

import org.springframework.beans.BeansException;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringSwingApplication {

    public static void main(String[] args) {
        try {
            SpringApplicationBuilder builder = new SpringApplicationBuilder(SpringSwingApplication.class);
            ApplicationContext context = builder.headless(false).web(WebApplicationType.NONE).run(args);
            SwingApp swingApp = context.getBean(SwingApp.class);
            swingApp.createUI();
        } catch (BeansException e) {
            e.printStackTrace();
        }
    }

}
