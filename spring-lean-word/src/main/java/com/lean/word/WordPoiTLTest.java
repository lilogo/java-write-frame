package com.lean.word;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.data.*;
import com.deepoove.poi.data.style.PictureStyle;
import com.deepoove.poi.plugin.markdown.MarkdownRenderData;
import com.deepoove.poi.plugin.markdown.MarkdownRenderPolicy;
import com.deepoove.poi.plugin.markdown.MarkdownStyle;
import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Data;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * 导出基于template的word
 *
 *
 */
public class WordPoiTLTest {

    public static void main(String[] args) throws Exception{

//        generateWordXWPFTemplate("./doc/12.docx");

        generateWordXWPFTemplateMD("./doc/13.docx");
    }

    /**
     * 生成word 普通格式
     * @param filePath
     * @throws IOException
     */
    public static void generateWordXWPFTemplate(String filePath) throws IOException {
        Map<String, Object> content = new HashMap<>();
        content.put("title", "Springboot 生成Word文档");
        content.put("author", "zhw");
        content.put("site", new HyperlinkTextRenderData("http://deepoove.com/poi-tl/", "http://deepoove.com/poi-tl/"));

        content.put("poiText",
            "poi-tl是一个基于Apache POI的Word模板引擎，也是一个免费开源的Java类库，你可以非常方便的加入到你的项目中，并且拥有着让人喜悦的特性。");

        content.put("poiTextTwo", "条形图（3D条形图）、柱形图（3D柱形图）、面积图（3D面积图）、折线图（3D折线图）、雷达图、饼图（3D饼图）、散点图等图表渲染");
        content.put("poiTlList", Numberings.create("将标签渲染为文本", "将标签渲染为表格","将标签渲染为列表"));

        RowRenderData headRow = Rows.of("标题一", "标题二", "标题三", "标题四", "标题五").textColor("FFFFFF").bgColor("4472C4").center().create();
        TableRenderData table = Tables.create(headRow);

        List<Integer> dataList=new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            dataList.add(i);
        }
        List<List<Integer>> subList = Lists.partition(dataList, 5);
        for (List<Integer> list : subList) {
            for (Integer value : list) {
                table.addRow(Rows.create(value+"", value+"", value+"", value + "", value+""));
            }

        }
        //填充数据
        content.put("poiTable", table);

        //图片
        Resource resource = new ClassPathResource("a.jpeg");
        content.put("poiImage", Pictures.ofStream(new FileInputStream(resource.getFile())).create());

        //读取模板
        XWPFTemplate render = XWPFTemplate.compile(new ClassPathResource("poi-tl-template.docx").getFile()).render(content);
        //渲染模板
        render.write(new FileOutputStream(filePath));
    }

    /**
     * 导出markdown为word
     *
     * @param filePath
     * @throws IOException
     */
    public static void generateWordXWPFTemplateMD(String filePath) throws IOException {
        MarkdownRenderData code = new MarkdownRenderData();

        Resource resource = new ClassPathResource("test.md");
        code.setMarkdown(new String(Files.readAllBytes(resource.getFile().toPath())));
        code.setStyle(MarkdownStyle.newStyle());

        Map<String, Object> data = new HashMap<>();
        data.put("md", code);

        Configure config = Configure.builder().bind("md", new MarkdownRenderPolicy()).build();
        XWPFTemplate render = XWPFTemplate.compile(new ClassPathResource("markdown_template.docx").getFile(), config).render(data);
        render.write(new FileOutputStream(filePath));
    }

}
