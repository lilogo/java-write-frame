package com.writeframe.设计模式.命令模式.demo1;


/**
 * @ClassName StopAction
 * @Description 停止播放命令
 */
public class StopAction implements IAction {
    private Player player;

    public StopAction(Player player) {
        this.player = player;
    }

    @Override
    public void execute() {
        player.stop();
    }
}


