package com.write.mybatis.test;

import java.util.List;

public interface UserDao {
    // @Select("select * from t_users")
    List<User> findAll();
    int saveUser(User user);
    int deleteUser(Integer id);
    int updateUserName(String username,Integer id);
}
