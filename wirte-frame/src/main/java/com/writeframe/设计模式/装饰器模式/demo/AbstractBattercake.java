package com.writeframe.设计模式.装饰器模式.demo;

/**
 * @ClassName AbstractBattercake
 * @Description 煎饼类，抽象顶层组件
 */
public abstract class AbstractBattercake {
    protected abstract String getMsg();

    public abstract int getPrice();
}
