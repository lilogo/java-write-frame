package com.caffeine.lean.config;

import com.github.benmanes.caffeine.cache.*;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * 本地caffeine缓存配置
 */
@EnableCaching
@Configuration
public class CaffeineCacheConfig {

    @Bean
    public Cache<String, Object> caffeineCache() {
        return Caffeine.newBuilder()
                // 设置最后一次写入或访问后经过固定时间过期
                .expireAfterWrite(6000, TimeUnit.SECONDS)
                // 初始的缓存空间大小
                .initialCapacity(100)
                // 缓存的最大条数
                .maximumSize(1000)
                .build();
    }

    /**
     * 在 springboot 中使用 CaffeineCacheManager 管理器管理 Caffeine 类型的缓存，Caffeine 类似 Cache 缓存的工厂，
     * 可以生产很多个 Cache 实例，Caffeine 可以设置各种缓存属性，这些 Cache 实例都共享 Caffeine 的缓存属性。
     * @return
     */
    @Bean(name = "caffeineCacheManager")
    public CacheManager oneHourCacheManager(){
        Caffeine caffeine = Caffeine.newBuilder()
                .initialCapacity(10) //初始大小
                .maximumSize(11)  //最大大小
                //写入/更新之后1小时过期
                .expireAfterWrite(1, TimeUnit.HOURS);

        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
        caffeineCacheManager.setAllowNullValues(true);
        caffeineCacheManager.setCaffeine(caffeine);
        return caffeineCacheManager;
    }
}

