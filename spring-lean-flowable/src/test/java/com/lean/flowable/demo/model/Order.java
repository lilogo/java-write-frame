package com.lean.flowable.demo.model;

import java.io.Serializable;

/**
 * 订单信息
 *
 */
public class Order implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;

	private String name;

	private int price;

	public Order() {

	}

	public Order(Integer id, String name, int price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}


}
