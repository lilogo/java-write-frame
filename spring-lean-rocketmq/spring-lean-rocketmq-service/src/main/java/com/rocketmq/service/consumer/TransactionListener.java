package com.rocketmq.service.consumer;

import com.alibaba.druid.util.StringUtils;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 生产者消息监听器：
 *    用于监听本地事务执行的状态和检查本地事务状态。
 */
@Component
@RocketMQTransactionListener(txProducerGroup = "common-tx-group")
public class TransactionListener implements RocketMQLocalTransactionListener {

    private static final Logger log = LoggerFactory.getLogger("TransactionListener");

    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message msg, Object arg) {
        // 执行本地事务
        String tag = String.valueOf(msg.getHeaders().get("rocketmq_tags"));
        if (StringUtils.equals("a", tag)){
            //这里只讲TAGA消息提交，状态为可执行
            return RocketMQLocalTransactionState.COMMIT;
        }else if (StringUtils.equals("b", tag)) {
            return RocketMQLocalTransactionState.ROLLBACK;
        } else if (StringUtils.equals("c",tag)) {
            return RocketMQLocalTransactionState.UNKNOWN;
        }
        return RocketMQLocalTransactionState.UNKNOWN;
    }

    //mq回调检查本地事务执行情况
    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message msg) {
        log.info("checkLocalTransaction===>{}",msg);
        return RocketMQLocalTransactionState.COMMIT;
    }
}

