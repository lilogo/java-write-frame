package com.lean.springsecurity.core.social.qq.api;

import java.io.IOException;

/**
 *
 * comment:
 */
public interface QQLoginInterface {

    QQUserInfo getQQUserInfo() ;

}
