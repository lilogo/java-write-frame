package com.lean.retry.service;

import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.time.LocalTime;


/**
 * 重试机制要素如下：
 * 限制重试次数
 * 每次重试的时间间隔
 * 最终失败结果的报警或事物回滚
 * 在特定失败异常事件情况下选择重试
 */
@Service
public class RetryService {
    /**
     * @return
     * @Recover的 第一个参数  必须是Throwable ,最好与@Retryable方法 捕捉的异常相同返回值,必须与@Retryable方法相同,否则不执行
     * value：表示遇到何种异常情况下需要重试
     * maxAttempts：表示最大重试次数
     * delay：表示重试的延迟时间（毫秒计数）
     * multiplier：表示上一次延迟时间为本次的倍数
     */
    @Retryable(value = Exception.class, maxAttempts = 3, backoff = @Backoff(delay = 2000,multiplier = 1.5))
    public String testRetry() throws Exception {
        System.out.println("开始执行代码："+ LocalTime.now());
        int code = 0;
        // 模拟一直失败
        if(code == 0){
            // 这里可以使自定义异常，@Retryable中value需与其一致
            throw new Exception("代码执行异常");
        }
        System.out.println("代码执行成功");
        return "success";
    }

    /**
     * 最终重试失败处理
     * @param e
     * @return
     */
    @Recover
    public String recover(Exception e){
        System.out.println("代码执行重试后依旧失败");
        return "fail";
    }

}
