package com.lean.websocketlog.logger;

import com.lmax.disruptor.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * @ClassName: LoggerEventHandler
 * @Description: 进程日志事件处理器作用是监听本地环形队列中的消息，如果有消息，就会将这些消息推送到 Socket 管道中
 * @Author: zhanghongwei
 * @Date: 2022/6/17 13:45
 */
@Component
public class LoggerEventHandler implements EventHandler<LoggerEvent> {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    //推送日志到 /topic/pullLogger
    @Override
    public void onEvent(LoggerEvent stringEvent, long l, boolean b) {
        messagingTemplate.convertAndSend("/topic/pullLogger", stringEvent.getLog());
    }
}
