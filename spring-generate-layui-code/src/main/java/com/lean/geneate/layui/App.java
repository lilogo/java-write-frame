package com.lean.geneate.layui;

import com.lean.geneate.layui.engine.AbstractEngine;

//代码生成器运行类
public class App {

    /***
     * 执行 - 构建项目
     */
    public static void main(String[] args){
        AbstractEngine engine = AbstractEngine.init();
        engine.execute();
    }
}
