package com.lean.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLenaUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLenaUploadApplication.class, args);
    }

}
