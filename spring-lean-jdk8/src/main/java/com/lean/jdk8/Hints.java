package com.lean.jdk8;

public @interface Hints {
    Hint[] value();
}
