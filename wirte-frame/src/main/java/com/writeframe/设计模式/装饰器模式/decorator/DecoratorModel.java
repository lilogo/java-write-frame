package com.writeframe.设计模式.装饰器模式.decorator;

public class DecoratorModel {
    public static void main(String[] args) {
        Component component = new ConcreteComponent();
        Decorator decoratorA = new ConcreteDecoratorA(component);
        decoratorA.operation();
        System.out.println("----------------------------------");
        Decorator decoratorB = new ConcreteDecoratorB(component);
        decoratorB.operation();
        System.out.println("----------------------------------");
        Decorator decoratorBandA = new ConcreteDecoratorB(decoratorA);
        decoratorBandA.operation();
    }
}
