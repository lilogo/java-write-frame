package com.lean.quartz.controller;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lean.quartz.base.PageResult;
import com.lean.quartz.base.Result;
import com.lean.quartz.convert.ScheduleJobLogConvert;
import com.lean.quartz.entity.ScheduleJobLogEntity;
import com.lean.quartz.query.ScheduleJobLogQuery;
import com.lean.quartz.service.ScheduleJobLogService;
import com.lean.quartz.vo.ScheduleJobLogVO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;

/**
 * 定时任务日志
 *
 * @author
 */
@RestController
@RequestMapping("schedule/log")
@Tag(name = "定时任务日志")
@AllArgsConstructor
public class ScheduleJobLogController {
    private final ScheduleJobLogService scheduleJobLogService;

    //http://localhost:8082/schedule/log/page?page=1&limit=10
    @GetMapping("page")
    @Operation(summary = "分页")
    public Result<PageResult<ScheduleJobLogVO>> page(@Valid ScheduleJobLogQuery query) {
        PageResult<ScheduleJobLogVO> page = scheduleJobLogService.page(query);

        return Result.ok(page);
    }

    //http://localhost:8082/schedule/log/1
    @GetMapping("{id}")
    @Operation(summary = "信息")
    public Result<ScheduleJobLogVO> get(@PathVariable("id") Long id) {
        ScheduleJobLogEntity entity = scheduleJobLogService.getById(id);

        return Result.ok(ScheduleJobLogConvert.INSTANCE.convert(entity));
    }

}
