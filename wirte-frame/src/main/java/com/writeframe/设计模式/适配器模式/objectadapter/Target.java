package com.writeframe.设计模式.适配器模式.objectadapter;

/**
 * @ClassName Target
 * @Description 目标对象 B.目标对象【实现是接口】
 */
public interface Target {
    int request();
}
