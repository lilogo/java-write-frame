-- 删除所有的节点和关系
MATCH(n) OPTIONAL MATCH (n)-[r]-() DELETE n,r

CREATE (n:Java {name:'系统C',firstDeploy:'2022-1-1',leader:'A'});
CREATE (n:Java {name:'系统B',firstDeploy:'2022-5-1',leader:'B'});
CREATE (n:App {name:'系统D',firstDeploy:'2022-6-1',leader:'C'});
CREATE (n:MinPro {name:'系统A',firstDeploy:'2022-8-1',leader:'D'});
CREATE (n:Web {name:'系统管理平台',firstDeploy:'2022-9-1',leader:'E'});
CREATE (n:MQ {name:'系统F',firstDeploy:'2022-9-1',leader:'F'});
CREATE (n:Public {name:'系统E',firstDeploy:'2022-4-25',leader:'G'});

MATCH (n) RETURN n

-- 删除节点
      MATCH (n) WHERE n.name='系统A' DELETE n;
-- 等价于
      MATCH (n:MinPro{name:'系统A'}) DELETE n;

-- 删除节点的属性
MATCH (n) WHERE n.name='系统A' REMOVE n.leader;
-- 等价于
MATCH (n:MinPro{name:'系统A'}) REMOVE n.leader;

-- 查询节点
-- 根据节点的属性进行查询（更接近SQL语法，推荐）
      MATCH (n) WHERE n.name='系统A' RETURN n;
-- 等价查询语句如下，增加了节点的类型MinPro，查询结果更加准确
     MATCH (n:MinPro{name:'系统A'}) RETURN n;

-- 修改/新增节点属性
    MATCH (n) WHERE n.name='系统A' SET n.leader='steve';


-- 新增/修改关系（属性）
MATCH (a),(b) where a.name='系统A' AND b.name='系统B' MERGE (a)-[:invoke]->(b) RETURN a,b;
MATCH (a:MinPro{name:'系统A'}),(b:Java{name:'系统C'}) MERGE (a)-[:invoke]->(b) RETURN a,b;
MATCH (a),(b) where a.name='系统D' AND b.name='系统B' CREATE (a)-[:invoke]->(b) RETURN a,b;
MATCH (a),(b) where a.name='系统D' AND b.name='系统C' CREATE (a)-[:invoke]->(b) RETURN a,b;
MATCH (a),(b) where a.name='系统B' AND b.name='系统F' CREATE (a)-[:produce]->(b) RETURN a,b;
MATCH (a),(b) where a.name='系统E' AND b.name='系统F' CREATE (a)-[:consume]->(b) RETURN a,b;
-- 也可以对关系增加属性
-- MERGE语句会覆盖现有的关系，达到更新关系及其属性的目的
MATCH (a),(b) where a.name='商家管理平台' AND b.name='系统C' MERGE (a)-[:invoke{since:2021-1-1}]->(b) RETURN a,b;

MATCH (n) RETURN n


-- 删除关系
MATCH (a)-[r:consume]->(b) WHERE a.name='系统E' AND b.name='系统F' DELETE r;
-- 删除关系属性
MATCH (a)-[r:invoke]->(b) WHERE a.name='商家管理平台' AND b.name='系统C' REMOVE r.since;
-- 增加/更新关系属性
MATCH (a)-[r:invoke]->(b) WHERE a.name='系统A' AND b.name='系统B' set r.since=2021;
-- 查询关系
-- 查询所有调用系统B的关联系统
MATCH (n)-[r:invoke]->(b) WHERE b.name='系统B' RETURN n;
-- 查询所有调用系统B的关联系统及其调用系统B的关系
MATCH (n)-[r:invoke]->(b) WHERE b.name='系统B' RETURN n,r,b;
-- 查询所有和系统B有关联的系统及其和系统B的关系
MATCH (n)-[r]-(b) WHERE b.name='系统B' RETURN n,r,b;