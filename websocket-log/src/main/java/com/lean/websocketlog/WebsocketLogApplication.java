package com.lean.websocketlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebsocketLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketLogApplication.class, args);
    }

}
