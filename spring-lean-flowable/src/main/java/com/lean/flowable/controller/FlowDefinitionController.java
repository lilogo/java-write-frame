package com.lean.flowable.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.lean.flowable.entity.FlowProcModel;
import liquibase.pro.packaged.S;
import org.flowable.engine.repository.Deployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.service.FlowDefinitionService;
import org.springframework.web.multipart.MultipartFile;

/**
 * 流程定义 https://www.cnblogs.com/m13002622490/archive/2022/03/23/16044650.html
 * 项目中如果讲文件放到processes下就会导致多份文件使用同一个部署模型 他的deploymentId是同一个。
 * 如果是手动上传，上传一份文件，部署一个模型。 建议： 手动上传
 * act_re_deployment
 *
 * 一共六种方式部署流程文档
 * DeploymentBuilder addInputStream(String resourceName, InputStreaminputStream);
 *
 * DeploymentBuilder addClasspathResource(String resource);
 *
 * DeploymentBuilder addString(String resourceName, String text);
 *
 * DeploymentBuilder addBytes(String resourceName, byte[] bytes);
 *
 * DeploymentBuilder addZipInputStream(ZipInputStream zipInputStream);
 *
 * DeploymentBuilder addBpmnModel(String resourceName, BpmnModel bpmnModel);
 *
 */
@RestController
@RequestMapping(value = "/flowable-define")
public class FlowDefinitionController {
    public static final Logger log = LoggerFactory.getLogger(FlowDefinitionController.class);

    @Autowired
    private FlowDefinitionService flowDefinitionService;

    /**
     * 部署信息 http://localhost:8834/flowable-define/deploy-list?name=SpringBootAutoDeployment&pageNum=1&pageSize=10
     * @param name 模板名称（模板ID）
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deploy-list")
    public ResponseResult deployList(@RequestParam(value = "name", required = false) String name, @RequestParam Integer pageNum,
        @RequestParam Integer pageSize) {
        return ResponseResult.success(flowDefinitionService.deployListByName(name, pageNum, pageSize));
    }

    /**
     * 部署压缩包形式的模板(.zip .bar)，主子流程定义部署 可用于一次性部署多个资源文件（.bpmn .drl .form等）
     * @param name 主模板名称（模板ID）
     * @param category 模板类别
     * @param tenantId 系统标识
     * @param file 模板文件
     * @return
     */
    @RequestMapping(value = "/deploy-zip", method = RequestMethod.POST, headers = "content-type=multipart/form-data")
    @ResponseBody
    public ResponseResult deployByZip(String name, String category, String tenantId, MultipartFile file) {
        Deployment deployment = flowDefinitionService.deployByZip(name, category, tenantId, file);
        return ResponseResult.success(deployment);
    }

    /**
     * 导入流程文件 上传bpmn20的xml文件
     * @param name
     * @param category
     * @param file
     * @return
     */
    @PostMapping("/import")
    public ResponseResult importFile(@RequestParam String name, @RequestParam(required = false) String category, MultipartFile file) {
        InputStream in = null;
        try {
            in = file.getInputStream();
            flowDefinitionService.importFile(name, category, in);
        } catch (Exception e) {
            log.error("导入失败:", e);
            return ResponseResult.success(e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                log.error("关闭输入流出错", e);
            }
        }
        return ResponseResult.success("导入成功");
    }

    /**
     * 读取xml文件  deployId processDefinitionId 可以保证唯一性
     * http://localhost:8834/flowable-define/readXml?deployId=7971305e-57a3-11ed-9d73-da22ef844221&processDefinitionId=approve_process%3A1%3A7a17f6bd-57a3-11ed-9d73-da22ef844221
     * @param deployId 流程模型id
     * @param processDefinitionId 流程定义id
     * @return
     */
    @GetMapping("/readXml")
    public ResponseResult readXmlByDeploymentId(String deployId, String processDefinitionId) {
        try {
            return flowDefinitionService.readXml(deployId,processDefinitionId);
        } catch (Exception e) {
            return ResponseResult.fail("加载xml文件异常");
        }
    }

    /**
     * 查询流程定义列表, 涉及到 act_re_procdef表，部署成功会新增记录
     * @param pageNum
     * @param pageSize
     * @param name 流程名称
     * @return
     */
    @GetMapping("/findProcessList")
    @ResponseBody
    public ResponseResult findProcessList(@RequestParam Integer pageNum, @RequestParam Integer pageSize, @RequestParam(required = false) String name) {
        List<FlowProcModel> processList = flowDefinitionService.findProcessList(name, pageNum, pageSize);
        return ResponseResult.success(processList);
    }

    /**
     * 获取流程定义具体信息 http://localhost:8834/flowable-define/findProcessDetailById?processDefinitionId=leaveApproval:1:d9410bb0-56a3-11ed-8dda-0a0027000004
     * @param processDefinitionId 流程定义id
     * @return
     */
    @GetMapping("/findProcessDetailById")
    @ResponseBody
    public ResponseResult findProcessDetailById(String processDefinitionId) {
        FlowProcModel flowProcModel = flowDefinitionService.findProcessDetailById(processDefinitionId);
        return ResponseResult.success(flowProcModel);
    }

    /**
     * 激活或挂起流程定义
     * @param state 1:激活,2:挂起
     * @param deploymentId 流程部署ID
     * @return
     */
    @PutMapping(value = "/updateProcessState")
    public ResponseResult updateProcessState(@RequestParam Integer state, @RequestParam String deploymentId) {
        flowDefinitionService.updateProcessState(state, deploymentId);
        return ResponseResult.success("ok");
    }

    /**
     * 删除流程 http://localhost:8834/process/deleteDeployment?deploymentId=0f69ac8e-5666-11ed-8b7a-0a0027000004
     * @param deploymentId 流程部署ID act_ge_bytearray 表中 deployment_id值
     */
    @GetMapping("/deleteDeployment")
    @ResponseBody
    public ResponseResult deleteDeployment(String deploymentId) {
        flowDefinitionService.deleteDeployment(deploymentId);
        return ResponseResult.success("ok");
    }



}
