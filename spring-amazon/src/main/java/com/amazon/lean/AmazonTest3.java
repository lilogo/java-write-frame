package com.amazon.lean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * 亚马逊中国站通过ASIN获取商品信息:
 *
 * 两种方式介绍
 * 通过ASIN获取商品信息至少有两种方式，第一种是进入商品详情页，第二种是通过搜索得到商品信息。
 *
 * 进入商品详情页
 * 拼接访问链接：https://www.amazon.cn/dp/ + asin编码，进入的页面就是商品详情页面。通过不同的标签获取到商品信息。
 * 该方法有个小问题，亚马逊不同的商品信息详情页是不相同的，需要匹配多种情况。优点是详细信息都有，所有数据都可以拿到。
 *
 * 搜索ASIN搜索
 * 拼接搜索结果链接：https://www.amazon.cn/s?k= + asin编码，查询出来的结果就是对应asin编码的商品信息，和商品列表中获取到的信息基本上一致。优点是格式统一，方便获取数据；缺点是信息有限，部分数据拿不到，只能拿到列表上有的信息。
 */
public class AmazonTest3 {

    public static void main(String[] args) throws Exception {
        printInfo1("B07X43GJ1L");

        System.out.println("========================");

        printInfo2("B07X43GJ1L");
    }

    static void printInfo1(String asin) throws Exception {
        Document doc = Jsoup.connect("https://www.amazon.cn/dp/" + asin)
                .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36")
                .get();
        String title = doc.getElementById("productTitle").text();
        System.out.println("名称：" + title);

        String price = doc.getElementById("priceblock_ourprice").text();
        System.out.println("价格：" + price);
    }

    static void printInfo2(String asin) throws Exception {
        Document doc = Jsoup.connect("https://www.amazon.cn/s?k=" + asin)
                .header("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36")
                .get();

        Element goodsEle = doc.getElementsByClass("sg-col-4-of-12 s-result-item s-asin sg-col-4-of-16 sg-col sg-col-4-of-20").first();

        String detailUrl = "https://www.amazon.cn" + goodsEle.getElementsByTag("a").first().attr("href");
        System.out.println("商品详情：" + detailUrl);

        String asins = goodsEle.attr("data-asin");
        System.out.println("ASIN：" + asins);

        String uuid = goodsEle.attr("data-uuid");
        System.out.println("UUID：" + uuid);

        String img = goodsEle.getElementsByTag("img").first().attr("src");
        System.out.println("封面图片：" + img);

        String subTitle = goodsEle.getElementsByTag("h2").first().text();
        System.out.println("名称：" + subTitle);

        Element starEle = goodsEle.getElementsByClass("a-icon-alt").first();
        if(starEle != null) {
            String star = starEle.text();
            System.out.println("评分：" + star);

            String count = goodsEle.getElementsByClass("a-section a-spacing-none a-spacing-top-micro").first().getElementsByClass("a-size-base").first().text().replaceAll(",", "");
            System.out.println("评价人数：" + count);
        } else {
            System.out.println("暂无评分");
            System.out.println("评价人数：0");
        }

        String price = goodsEle.getElementsByClass("a-offscreen").first().text().replaceAll(",", "");
        System.out.println("价格：" + price);

    }


}
