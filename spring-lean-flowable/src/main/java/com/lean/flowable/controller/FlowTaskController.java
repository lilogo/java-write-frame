package com.lean.flowable.controller;

import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.entity.FlowTaskDModel;
import com.lean.flowable.service.FlowTaskService;
import org.flowable.engine.task.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 任务相关
 */
@RequestMapping(value = "/flow-task")
@RestController
public class FlowTaskController {

    @Autowired
    private FlowTaskService flowTaskService;

    /**
     * 根据候选组查询任务 -- 待签收任务
     *
     * @param userGroup
     * @return
     */
    @RequestMapping(value = "/findTaskListByUserGroup")
    @ResponseBody
    public ResponseResult findTaskListByUserGroup(String userGroup) {
        return flowTaskService.findTaskListByUserGroup(userGroup);
    }

    /**
     * 根据候选人查询任务 -- 待签收任务
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/findTaskListByUser")
    @ResponseBody
    public ResponseResult findTaskListByUser(String userId) {
        return flowTaskService.findTaskListByUser(userId);
    }

    /**
     * 根据处理人查询任务 -- 正在进行的 且 已认领 或 设置了执行人的任务
     *
     * @param assignee
     * @return
     */
    @RequestMapping(value = "/findTaskListByAssignee")
    @ResponseBody
    public ResponseResult findTaskListByAssignee(String assignee) {
        return flowTaskService.findTaskListByAssignee(assignee);
    }

    /**
     * 获取待办列表
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "/todoList")
    public ResponseResult todoList(@RequestParam String userId, @RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        return flowTaskService.todoList(userId, pageNum, pageSize);
    }

    /**
     * 获取已办任务
     *
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    @GetMapping(value = "/finishedList")
    public ResponseResult finishedList(@RequestParam String userId, @RequestParam Integer pageNum, @RequestParam Integer pageSize) {
        return flowTaskService.finishedList(userId, pageNum, pageSize);
    }

    /**
     * 根据流程实例id 获取 正处于运行状态的任务 http://localhost:8834/task/getActiveTask?processInstanceId=de498cfc-5683-11ed-a899-0a0027000004
     *
     * @param processInstanceId
     * @return
     */
    @RequestMapping(value = "/getActiveTask")
    @ResponseBody
    public ResponseResult getActiveTask(String processInstanceId) {
        return flowTaskService.getActiveTask(processInstanceId);
    }

    /**
     * 根据流程实例id 获取 所有taskId任务
     *
     * @param processInstanceId
     * @return
     */
    @RequestMapping(value = "/getTaskList")
    @ResponseBody
    public ResponseResult getTaskList(String processInstanceId) {
        return flowTaskService.getTaskList(processInstanceId);
    }

    /**
     * 为流程任务设置变量。 如果变量尚未存在，则将在任务中创建该变量。
     *
     * @param taskId
     * @param variableName 变量键名
     * @param variableValue 变量键值
     * @return
     */
    @RequestMapping(value = "/setVariableToTask")
    @ResponseBody
    public ResponseResult setVariableToTask(String taskId, String variableName, Object variableValue) {
        flowTaskService.setVariableLocal(taskId, variableName, variableValue);
        return ResponseResult.success("ok");
    }

    /**
     * 为流程任务设置变量。 如果变量尚未存在，则将在任务中创建该变量。
     *
     * @param taskId
     * @param variables 多对变量键值对
     * @return
     */
    @RequestMapping(value = "/setVariablesToTask")
    @ResponseBody
    public ResponseResult setVariablesToTask(String taskId, @RequestBody Map<String, Object> variables) {
        flowTaskService.setVariablesLocal(taskId, variables);
        return ResponseResult.success("ok");
    }

    /**
     * 为任务添加任务处理人
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/addCandidateUser")
    @ResponseBody
    public ResponseResult addCandidateUser(String taskId, String userId) {
        flowTaskService.addCandidateUser(taskId, userId);
        return ResponseResult.success("ok");
    }

    /**
     * 任务移交：将任务的所有权转移给其他用户。
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/setAssignee")
    @ResponseBody
    public ResponseResult setAssignee(String taskId, String userId) {
        flowTaskService.setAssignee(taskId, userId);
        return ResponseResult.success("ok");
    }

    /**
     * 任务委派
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/delegateTask")
    @ResponseBody
    public ResponseResult delegate(String taskId, String userId) {
        flowTaskService.delegate(taskId, userId);
        return ResponseResult.success("ok");
    }

    /**
     * 委派任务完成，归还委派人
     * @param taskId
     * @return
     */
    @RequestMapping(value = "/resolveTask")
    @ResponseBody
    public ResponseResult resolveTask(String taskId) {
        flowTaskService.resolveTask(taskId);
        return ResponseResult.success("ok");
    }

    /**
     * 更改任务拥有者
     * @param taskId
     * @return
     */
    @RequestMapping(value = "/setOwner")
    @ResponseBody
    public ResponseResult setOwner(String taskId, String userId) {
        flowTaskService.setOwner(taskId, userId);
        return ResponseResult.success("ok");
    }

    /**
     * 删除任务
     * @param taskId
     * @return
     */
    @RequestMapping(value = "/deleteTaskById")
    @ResponseBody
    public ResponseResult deleteTask(String taskId) {
        flowTaskService.delete(taskId);
        return ResponseResult.success("ok");
    }

    /**
     * 删除任务，附带删除理由
     *
     * @param taskId
     * @param reason 删除理由
     * @return
     */
    @RequestMapping(value = "/deleteWithReasonById")
    @ResponseBody
    public ResponseResult deleteWithReasonById(String taskId, String reason) {
        flowTaskService.deleteWithReason(taskId, reason);
        return ResponseResult.success("ok");
    }

    /**
     * 申领任务: 同一个用户组中的用户可以看到该任务 并且认领该任务
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/claimTask")
    @ResponseBody
    public ResponseResult claimTask(String taskId, String userId) {
        flowTaskService.claim(taskId, userId);
        return ResponseResult.success("ok");
    }

    /**
     * 任务释放 同一个用户组中的用户可以看到该任务 并且认领该任务后不想执行，可以释放该任务，一边其他组员认领
     *
     * @param taskId
     * @return
     */
    @RequestMapping(value = "/unClaimTask")
    @ResponseBody
    public ResponseResult claimTask(String taskId) {
        flowTaskService.unclaim(taskId);
        return ResponseResult.success("ok");
    }

    /**
     * 执行任务
     *
     * @param taskId
     * @param variables
     * @param localScope
     * @return
     */
    @RequestMapping(value = "/completeTask")
    @ResponseBody
    public ResponseResult completeTask(@RequestParam(value = "taskId") String taskId, @RequestBody(required = false) Map<String, Object> variables,
        @RequestParam(value = "localScope", required = false, defaultValue = "false") boolean localScope) {
        flowTaskService.complete(taskId, variables, localScope);
        return ResponseResult.success();
    }

    /**
     * 获取上一点任务
     *
     * @param taskId
     * @return
     */
    @PostMapping(value = "/getBeforeTaskId")
    public ResponseResult getBeforeTaskId(String taskId) {
        return ResponseResult.success(flowTaskService.getBeforeTaskId(taskId));
    }

    /**
     * 获取所有可回退的节点
     *
     * @param taskId
     * @return
     */
    @PostMapping(value = "/findReturnTaskList")
    public ResponseResult findReturnTaskList(String taskId) {
        return ResponseResult.success(flowTaskService.findReturnTaskList(taskId));
    }

    /**
     * 回退流程 回退只能回退到串行路线上 https://dpb-bobokaoya-sm.blog.csdn.net/article/details/124164129
     *
     * @param currentTaskId
     * @param targetKey 目标节点的key 为act_hi_taskinst 中 TASK_DEF_KEY_
     */
    @GetMapping("rollbackTask")
    @ResponseBody
    public ResponseResult rollbackTask(@RequestParam String currentTaskId, @RequestParam String targetKey) {
        // 1、获取当前任务节点
        // 2、判断是否被签领
        // 3、若被签领，则提示不能撤回。
        // 4、若未被签领，则执行退回任务。
        FlowTaskDModel currentTask = flowTaskService.getCurrentTaskDetail(currentTaskId);
        if (currentTask == null) {
            return ResponseResult.fail("任务不存在");
        }
        if (null != currentTask.getAssigneeName()) {
            return ResponseResult.fail("任务已被签领，无法撤回!");
        }
        // 回退多个节点 进行填充
        List<String> currentTaskKeys = new ArrayList<>();
        currentTaskKeys.add(currentTask.getTaskDefKey());
        flowTaskService.changeActivityState(currentTask.getProcInsId(), targetKey, currentTaskKeys);
        // 5、自动签领任务。
        // 6、返回要跳转的路径
        return ResponseResult.success("撤回成功！请尽快完成任务");
    }

    /**
     * 终止任务
     *
     * @param taskId
     * @return
     */
    @PostMapping(value = "/stopTask")
    public ResponseResult stopTask(@RequestParam String taskId, @RequestParam String comment) {
        return ResponseResult.success(flowTaskService.stopTask(taskId, comment));
    }

    /**
     * 查询与任务相关的注释信息
     *
     * @param taskId
     * @return
     */
    @RequestMapping(value = "/getTaskComments")
    @ResponseBody
    public ResponseResult getTaskComments(String taskId) {
        List<Map> list = new ArrayList<>();
        List<Comment> taskComments = flowTaskService.getTaskComments(taskId);
        if (!CollectionUtils.isEmpty(taskComments)) {
            for (Comment taskComment : taskComments) {
                Map map = new HashMap();
                map.put("id", taskComment.getId());
                map.put("taskId", taskComment.getTaskId());
                map.put("processInstanceId", taskComment.getProcessInstanceId());
                map.put("message", taskComment.getFullMessage());
                map.put("time", taskComment.getTime());
                map.put("type", taskComment.getType());
                map.put("userId", taskComment.getUserId());
                list.add(map);
            }
        }
        return ResponseResult.success(list);
    }

    /**
     * 为流程任务 和/或 流程实例添加注释。
     *
     * @param taskId
     * @param processInstanceId
     * @param message
     * @return
     */
    @RequestMapping(value = "/addComment")
    @ResponseBody
    public ResponseResult addComment(String taskId, String processInstanceId, String message) {
        Comment taskComment = flowTaskService.addComment(taskId, processInstanceId, message);
        Map map = new HashMap();
        if (taskComment != null) {
            map.put("id", taskComment.getId());
            map.put("taskId", taskComment.getTaskId());
            map.put("processInstanceId", taskComment.getProcessInstanceId());
            map.put("message", taskComment.getFullMessage());
            map.put("time", taskComment.getTime());
            map.put("type", taskComment.getType());
            map.put("userId", taskComment.getUserId());
        }
        return ResponseResult.success(map);
    }
}
