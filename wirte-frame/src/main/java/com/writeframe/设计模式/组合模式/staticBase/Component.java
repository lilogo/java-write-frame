package com.writeframe.设计模式.组合模式.staticBase;

/**
 * @ClassName Component
 * @Description 组合对象，顶层抽象组件，根节点
 * A.抽象组件
 * 　　也是一个根节点
 */
public abstract class Component {
    protected String name;

    public Component(String name) {
        this.name = name;
    }

    public abstract String operation();

    public boolean addChild(Component component){
        throw new UnsupportedOperationException("addChild not supported!");
    }
    public boolean removeChild(Component component){
        throw new UnsupportedOperationException("removeChild not supported!");
    }

    public Component getChild(int index){
        throw new UnsupportedOperationException("getChild not supported!");
    }
}
