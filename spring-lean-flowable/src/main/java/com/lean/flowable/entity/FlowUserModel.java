package com.lean.flowable.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class FlowUserModel implements Serializable {

    private String id;
    private String password;
    private String firstName;
    private String lastName;
    private String displayName;
    private String email;
}
