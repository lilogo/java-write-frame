package com.lean.mybatis;

import com.lean.mybatis.dao.RoleMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringLeanMybatisApplicationTests {

    @Autowired
    private RoleMapper roleMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void testAuth() {
        roleMapper.selectByIdAuth(1);
    }
}
