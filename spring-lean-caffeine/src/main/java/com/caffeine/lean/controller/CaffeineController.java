package com.caffeine.lean.controller;

import com.caffeine.lean.service.CaffeineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CaffeineController {

    @Autowired
    private CaffeineService caffeineService;

    @GetMapping("/cache-Data/{key}")
    public String cacheDataL(@PathVariable String key) {

        return caffeineService.cacheData(key);
    }

    @GetMapping("/cache-put-Data/{key}")
    public String cachePutDataL(@PathVariable String key) {

        return caffeineService.cachePutData(key);
    }


    @GetMapping("/cache-delete-Data/{key}")
    public String cacheDelteDataL(@PathVariable String key) {

        return caffeineService.deleteCaffeineServiceTest(key);
    }

    @GetMapping("/getCaffeineServiceTest")
    public String getCaffeineServiceTest(String name,Integer age) {

        return caffeineService.getCaffeineServiceTest(name,age);
    }

}
