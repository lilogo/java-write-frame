package com.lean.image.zoom;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @author 小帅丶
 * @className ZoomUtil
 * @Description 缩放工具类-不完美 谨慎使用
 **/
public class ZoomUtil {
    /**
     * 高
     */
    public  int MAX_HEIGHT;

    /**
     * 宽
     */
    public  int MAX_WIDTH;

    /**
     * @Description ZoomUtil初始化
     * @Author 小帅丶
     * @Date  2021-01-15 17:51
     * @param MAX_WIDTH  宽
     * @param MAX_HEIGHT  高
     * @return
     **/
    public ZoomUtil(int MAX_WIDTH,int MAX_HEIGHT) {
        this.MAX_WIDTH = MAX_WIDTH;
        this.MAX_HEIGHT = MAX_HEIGHT;
    }

    /**
     * 以宽度为基准，等比例放缩图片
     * @param maxWidthLiteScene int 新宽度
     * @param sceneImg 原图
     */
    public BufferedImage resizeByWidth(int maxWidthLiteScene, byte[] sceneImg,String format) throws Exception{
        ByteArrayInputStream in = new ByteArrayInputStream(sceneImg);
        Image image = ImageIO.read(in);
        int h = MAX_HEIGHT * maxWidthLiteScene / MAX_WIDTH;
        return resizePNG(maxWidthLiteScene, h,image,format);
    }
    /**
     * 以宽度为基准，等比例放缩图片
     * @param maxWidthLiteScene int 新宽度
     * @param sceneImg 原图
     */
    public BufferedImage resizeByWidth(int maxWidthLiteScene, byte[] sceneImg) throws Exception{
        ByteArrayInputStream in = new ByteArrayInputStream(sceneImg);
        Image image = ImageIO.read(in);
        int h = MAX_HEIGHT * maxWidthLiteScene / MAX_WIDTH;
        return resize(maxWidthLiteScene, h,image);
    }
    /**
     * 以宽度为基准，等比例放缩图片
     * @param w int 新宽度
     */
    private   void resizeByWidth(int w, String path, String newPath,String formatName) throws IOException {
        Image image = ImageIO.read(new File(path));
        int h = MAX_HEIGHT * w / MAX_WIDTH;
        resize(w, h,newPath, formatName,image);
    }

    /**
     * 以高度为基准，等比例缩放图片
     * @param h int 新高度
     */
    private  void resizeByHeight(int h, String path,String newPath, String formatName) throws IOException {
        Image image = ImageIO.read(new File(path));
        int w = MAX_WIDTH * h / MAX_HEIGHT;
        resize(w, h,newPath, formatName,image);
    }

    /**
     * 强制压缩/放大图片到固定的大小
     * @param w int 新宽度
     * @param h int 新高度
     */
    private static void resize(int w, int h,String newPath, String formatName,Image img) throws IOException {
        //SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
        BufferedImage image = new BufferedImage(w, h, Image.SCALE_SMOOTH);
        // 绘制缩小后的图
        image.getGraphics().drawImage(img, 0, 0, w, h, null);
        File destFile = new File(newPath);
        // 输出到文件流
        FileOutputStream out = new FileOutputStream(destFile);
        ImageIO.write(image, formatName, new File(newPath));
        out.close();
    }

    /**
     * 强制压缩/放大图片到固定的大小
     * @param w int 新宽度
     * @param h int 新高度
     * @param img 要处理的图片
     * @param format 图片格式
     */
    private static BufferedImage resizePNG(int w, int h,Image img,String format) throws IOException {
        //SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
        BufferedImage image = new BufferedImage(w, h, Image.SCALE_SMOOTH);
        Graphics2D g2D = image.createGraphics();
        image = g2D.getDeviceConfiguration().createCompatibleImage(image.getWidth(null), image.getHeight(null), Transparency.TRANSLUCENT);
        // 绘制缩小后的图
        image.getGraphics().drawImage(img, 0, 0, w, h, null);
        // 输出到文件流
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image,format,outputStream);
        return image;
    }

    /**
     * 强制压缩/放大图片到固定的大小
     * @param w int 新宽度
     * @param h int 新高度
     * @param img 要处理的图片
     */
    private static BufferedImage resize(int w, int h,Image img) throws IOException {
        //SCALE_SMOOTH 的缩略算法 生成缩略图片的平滑度的 优先级比速度高 生成的图片质量比较好 但速度慢
        BufferedImage image = new BufferedImage(w, h, Image.SCALE_SMOOTH);
        // 绘制缩小后的图
        image.getGraphics().drawImage(img, 0, 0, w, h, null);
        // 输出到文件流
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(image,"jpg",outputStream);
        return image;
    }

    public static void main(String[] args) throws Exception {
        zipWidthHeightImageFile(new File("F:\\testimg\\hua\\hua.png"), new File("F:\\testimg\\hua\\hua2.png"), 62, 62, 0.8f);
    }

    public static String zipImageFile(File oldFile, File newFile, int width, int height, float quality) throws Exception{
        if (oldFile == null) {
            return null;
        }
        try {
            Image srcFile = ImageIO.read(oldFile);
            int w = srcFile.getWidth(null);
            int h = srcFile.getHeight(null);
            double bili;
            if (width > 0) {
                bili = width / (double) w;
                height = (int) (h * bili);
            } else {
                if (height > 0) {
                    bili = height / (double) h;
                    width = (int) (w * bili);
                }
            }

            String srcImgPath = newFile.getAbsoluteFile().toString();
            String subfix = "jpg";
            subfix = srcImgPath.substring(srcImgPath.lastIndexOf(".") + 1, srcImgPath.length());

            BufferedImage buffImg = null;
            if (subfix.equals("png")) {
                buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            } else {
                buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            }

            Graphics2D graphics = buffImg.createGraphics();
            graphics.setBackground(new Color(255, 255, 255));
            graphics.setColor(new Color(255, 255, 255));
            graphics.fillRect(0, 0, width, height);
            graphics.drawImage(srcFile.getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);

            ImageIO.write(buffImg, subfix, new File(srcImgPath));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return newFile.getAbsolutePath();
    }

    public static boolean zipWidthHeightImageFile(File oldFile, File newFile, int width, int height, float quality) {
        if (oldFile == null) {
            return false;
        }
        Image srcFile = null;
        BufferedImage buffImg = null;
        try {
            srcFile = ImageIO.read(oldFile);

            String srcImgPath = newFile.getAbsoluteFile().toString();
            String subfix = "jpg";
            subfix = srcImgPath.substring(srcImgPath.lastIndexOf(".") + 1, srcImgPath.length());

            if (subfix.equals("png")) {
                buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            } else {
                buffImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            }

            Graphics2D graphics = buffImg.createGraphics();
            graphics.setBackground(new Color(255, 255, 255));
            graphics.setColor(new Color(255, 255, 255));
            graphics.fillRect(0, 0, width, height);
            graphics.drawImage(srcFile.getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);

            ImageIO.write(buffImg, subfix, new File(srcImgPath));
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (srcFile != null) {
                srcFile.flush();
            }
            if (buffImg != null) {
                buffImg.flush();
            }

        }

    }
}
