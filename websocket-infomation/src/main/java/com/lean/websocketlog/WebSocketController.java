package com.lean.websocketlog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @ClassName: WebSocketController
 * @Description: http://localhost:18801/open/socket/onReceive?id=1
 * @Author: zhanghongwei
 * @Date: 2022/6/17 16:06
 */
@RestController
@RequestMapping("/open/socket")
public class WebSocketController {

    @Autowired
    private WebSocketServer webSocketServer;

    /**
     * 手机客户端请求接口
     * @param id    发生异常的设备ID
     * @throws IOException
     */
    @GetMapping(value = "/onReceive")
    public void onReceive(String id) throws IOException {
        webSocketServer.broadCastInfo(id);
    }

}
