package com.lean.flowable.listener;

import java.io.Serializable;

import org.flowable.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Component;

/**
 *会签 自定义处理
 *
 */
@Component(value = "multiInstance")
public class MultiInstanceCompleteTask implements Serializable {

    private static final double scale = 0.5;

    /**
     * 判断是否通过条件
     * @param execution
     * @return
     */
    public boolean accessCondition(DelegateExecution execution) {
        // 否决判断，一票否决
        if (execution.getVariable("reject") != null) {
            int rejectCount = (int)execution.getVariable("reject");
            System.out.println("reject:" + rejectCount);
            if (rejectCount > 0) {
                // 输出方向为拒绝
                execution.setVariable("outcome", "否决");
                // 一票否决其他实例没必要做，结束
                return true;
            }
        }
        // 已完成的实例数
        int completedInstance = (int)execution.getVariable("nrOfCompletedInstances");
        // 获取所有的实例数
        int AllInstance = (int)execution.getVariable("nrOfInstances");
        System.out.println("总实例数目:" + AllInstance);
        int rejectNum = (int)(execution.getVariable("rejectNum")!=null?execution.getVariable("rejectNum"):0);
        // 获取同意人的次数
        int agreeNum = (int)(execution.getVariable("agreeNum")!=null?execution.getVariable("agreeNum"):0);
        // 所有实例任务未全部做完则继续其他实例任务
        if (completedInstance != AllInstance) {
            // 加入不同意的人数大于设置比例*总人数
            if (rejectNum * 1.00 / AllInstance > (1 - scale)) {
                execution.setVariable("outcome", "否决");
                return true;
            }
            if (agreeNum * 1.00 / AllInstance >= scale) {
                execution.setVariable("outcome", "通过");
                return true;
            }
            return false;
        } else {
            // 输出方向为赞同
            execution.setVariable("outcome", "通过");
            // 所有都做完了没被否决，结束
            return true;
        }

    }
}
