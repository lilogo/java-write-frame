package com.lean.springsecurity.core.verify;

import lombok.Data;

import java.awt.image.BufferedImage;
import java.time.LocalDateTime;

/**
 * 
 * 
 * comment: 图形验证码
 */
@Data
public class ImageVerifyCode extends BaseVerifyCode {

    private BufferedImage image;


    public ImageVerifyCode(BufferedImage image, String verifyCode, int expireSecond) {
        super(verifyCode,expireSecond);
        this.image = image;
    }

    public ImageVerifyCode(BufferedImage image, String verifyCode, LocalDateTime expireTime) {
        super(verifyCode,expireTime);
        this.image = image;
    }

}
