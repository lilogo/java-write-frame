package com.lean.typesense;

import cn.hutool.json.JSONUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.typesense.model.*;

import java.util.HashMap;
import java.util.Map;

public class SearcherTest extends TypesenseApplicationTests {

    @Autowired
    private SearchService searchService;

    @Autowired
    private CurationService curationService;

    @Autowired
    private SynonyService synonyService;

    @Test
    public void searchData() throws Exception {
        SearchResult searchResult = searchService.searchData("companies", "*", "company_name",
                "num_employees:>100", "num_employees:desc", "country", 10, null, null);
        System.out.println(searchResult);
    }

    @Test
    public void creatGeosearchCollection() throws Exception {
        CollectionResponse response = searchService.creatGeosearchCollection("places");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    @Test
    public void createGeosearchDocument() throws Exception {
        HashMap<String, Object> document = new HashMap<>();
        float[] location = {48.86093481609114f, 2.33698396872901f};
        document.put("title", "Louvre Museuem");
        document.put("points", 1);
        document.put("location", location);
        Map<String, Object> map = searchService.createGeosearchDocument("places", document);
        System.out.println(JSONUtil.toJsonStr(map));
    }

    @Test
    public void getGeosearch() throws Exception {
        SearchResult searchResult = searchService.getGeosearch("places");
        System.out.println(JSONUtil.toJsonStr(searchResult));
    }

    @Test
    public void getGeosearch2() throws Exception {
        SearchResult searchResult = searchService.getGeosearch2("places");
        System.out.println(JSONUtil.toJsonStr(searchResult));
    }

    //向量

    @Test
    public void createVectorSearchCollection() throws Exception {
        CollectionResponse vectorSearchCollection = searchService.createVectorSearchCollection("vector-docs");
        System.out.println(JSONUtil.toJsonStr(vectorSearchCollection));
    }

    @Test
    public void createVectorSearchDocument() throws Exception {
        Map<String, Object> vectorSearchDocument = searchService.createVectorSearchDocument("vector-docs");
        System.out.println(JSONUtil.toJsonStr(vectorSearchDocument));
    }

    @Test
    public void getVectorSearchDocument() throws Exception {
        MultiSearchResponse searchResult = searchService.getVectorSearchDocument("vector-docs");
        System.out.println(JSONUtil.toJsonStr(searchResult));
    }

    @Test
    public void likeVectorSearchDocument() throws Exception {
        MultiSearchResponse searchResult = searchService.likeVectorSearchDocument("vector-docs");
        System.out.println(JSONUtil.toJsonStr(searchResult));
    }

    @Test
    public void strogeLikeVectorSearchDocument() throws Exception {
        MultiSearchResponse searchResult = searchService.strogeLikeVectorSearchDocument("vector-docs");
        System.out.println(JSONUtil.toJsonStr(searchResult));
    }

    @Test
    public void multiSearches() throws Exception {
        MultiSearchResponse searchResult = searchService.multiSearches("vector-docs", "");
        System.out.println(JSONUtil.toJsonStr(searchResult));
    }

    @Test
    public void includeOrExcludesItem() throws Exception {
        SearchOverride companies = curationService.includeOrExcludesItem("companies");
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void includeOrExcludesItem2() throws Exception {
        SearchOverride companies = curationService.includeOrExcludesItem2("companies");
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void listCuration() throws Exception {
        SearchOverridesResponse companies = curationService.list("companies");
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void getCuration() throws Exception {
        SearchOverride companies = curationService.get("companies");
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void delCuration() throws Exception {
        SearchOverride companies = curationService.del("companies");
        System.out.println(JSONUtil.toJsonStr(companies));
    }

    @Test
    public void createOrUpdateSynony() throws Exception {
        SearchSynonym products = synonyService.createOrUpdateSynony("products");
        System.out.println(JSONUtil.toJsonStr(products));
    }

    @Test
    public void createOrUpdateSimpleSynony() throws Exception {
        SearchSynonym products = synonyService.createOrUpdateSimpleSynony("products");
        System.out.println(JSONUtil.toJsonStr(products));
    }

    @Test
    public void getSynony() throws Exception {
        SearchSynonym products = synonyService.get("products");
        System.out.println(JSONUtil.toJsonStr(products));
    }

    @Test
    public void listSynony() throws Exception {
        SearchSynonymsResponse products = synonyService.list("products");
        System.out.println(JSONUtil.toJsonStr(products));
    }

    @Test
    public void delSynony() throws Exception {
        SearchSynonym products = synonyService.del("products");
        System.out.println(JSONUtil.toJsonStr(products));

    }

}
