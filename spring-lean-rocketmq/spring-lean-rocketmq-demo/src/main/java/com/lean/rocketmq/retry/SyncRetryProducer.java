package com.lean.rocketmq.retry;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

/**
 * Producer对发送失败的消息进行重新发送的机制，称为消息发送重试机制，也称为消息重投机制。
 * 对于消息重投，需要注意以下几点：
     * 生产者在发送消息时，若采用同步或异步发送方式，发送失败会重试，但oneway消息发送方式
     * 发送失败是没有重试机制的
     * 只有普通消息具有发送重试机制，顺序消息是没有的
     * 消息重投机制可以保证消息尽可能发送成功、不丢失，但可能会造成消息重复。消息重复在
     * RocketMQ中是无法避免的问题
     * 消息重复在一般情况下不会发生，当出现消息量大、网络抖动，消息重复就会成为大概率事件
     * producer主动重发、consumer负载变化（发生Rebalance，不会导致消息重复，但可能出现重复
     * 消费）也会导致重复消息
     * 消息重复无法避免，但要避免消息的重复消费。
     * 避免消息重复消费的解决方案是，为消息添加唯一标识（例如消息key），使消费者对消息进行消
     * 费判断来避免重复消费
     * 消息发送重试有三种策略可以选择：同步发送失败策略、异步发送失败策略、消息刷盘失败策略
 *
 *同步发送失败策略:
 *
 *      对于普通消息，消息发送默认采用round-robin策略来选择所发送到的队列。如果发送失败，默认重试2
 *      次。但在重试时是不会选择上次发送失败的Broker，而是选择其它Broker。当然，若只有一个Broker其
 *      也只能发送到该Broker，但其会尽量发送到该Broker上的其它Queue。
 *      同时，Broker还具有失败隔离功能，使Producer尽量选择未发生过发送失败的Broker作为目标
 *      Broker。其可以保证其它消息尽量不发送到问题Broker，为了提升消息发送效率，降低消息发送耗时。
 *      如果超过重试次数，则抛出异常，由Producer去保证消息不丢。当然当生产者出现
 *      RemotingException、MQClientException和MQBrokerException时，Producer会自动重投消息。
 *
 */
public class SyncRetryProducer {
    public static void main(String[] args) throws Exception {
        // 创建一个producer，参数为Producer Group名称
        DefaultMQProducer producer = new DefaultMQProducer("pg");
        // 指定nameServer地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        // 设置同步发送失败时重试发送的次数，默认为2次
        producer.setRetryTimesWhenSendFailed(3);
        // 设置发送超时时限为5s，默认3s
        producer.setSendMsgTimeout(5000);
        // 开启生产者
        producer.start();
        // 生产并发送100条消息
        for (int i = 0; i < 100; i++) {
            byte[] body = ("Hi," + i).getBytes();
            Message msg = new Message("someTopic", "someTag", body);
            // 为消息指定key setKey,做唯一标识
            msg.setKeys("key-" + i);
            // 同步发送消息
            SendResult sendResult = producer.send(msg);
            System.out.println(sendResult);
        }
        // 关闭producer
        producer.shutdown();
    }
}
