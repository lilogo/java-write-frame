package com.writeframe.设计模式.访问者模式;

/**
 * @ClassName CTOVisitor
 * @Description ceo考核者,只有看kpi打分就行
 */
public class CEOVisitor implements IVisitor{

    @Override
    public void visit(Engineer engineer) {
        System.out.println("工程师："+engineer.getName()+" ,KPI："+engineer.getKpi());
    }

    @Override
    public void visit(Manager manager) {
        System.out.println("项目经理："+manager.getName()+" ,KPI："+manager.getKpi());
    }
}
