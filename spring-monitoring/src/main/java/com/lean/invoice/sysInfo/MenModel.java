package com.lean.invoice.sysInfo;

/**
 * @ClassName: MenModel
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/6/1 13:42
 */
public class MenModel {

    private String name;

    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
