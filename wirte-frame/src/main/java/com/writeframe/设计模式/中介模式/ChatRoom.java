package com.writeframe.设计模式.中介模式;

/**
 * @ClassName ChatRoom
 * @Description 聊天室
 * <p>
 * 中介者模式【Mediator Pattern】,又称调解者模式或调停者模式。用一个中介对象封装一系列的对象
 * 交互，中介者使用各对象不需要显式地相互作用，从而使其耦合松散，而且可以独立地改变它们之间的交互。
 * <p>
 * 核心：
 * <p>
 * 　　通过中介者解耦系统各层次对象的直接耦合，层次对象的对外依赖通信统统交由中介者转发。属于行为型模式。
 * <p>
 * 中介者模式在生活场景中应用
 * 1.人际交往圈，通讯网络。如果需要自己逐个去建立关系会很困难且复杂，如果加入中介者，来维护这个网络。中介者就很方便了。
 * 2.朋友圈。
 * 3.数据整合中心
 * 4.rpc通信
 * <p>
 * 中介者模式的使用场景
 * 1.系统中对象之间存在复杂的引用关系，产生相互依赖关系结构混乱且难以理解。
 * 2.交互的公共行为，如果需要改变行为则可以增加新的中介者类。
 * <p>
 * 中介模式应用案例之聊天室
 * 很多人进入一个聊天室，或聊天群里，群或聊天室，都充当中介者的角色。
 * <p>
 * 优点：
 * 　　1.减少类间依赖，将多对多依赖关系，转化成一对多的关系，降低了类间的耦合性。
 * 　　2.类间各司其职，符合迪米特法则。
 * 缺点：
 * 　　中介者模式，将原本多个对象直接的相互依赖关系，变成依赖中介者和多个同事类的依赖关系。
 * 当同事类越来越多时，中介者责任越来越大，且复杂难以维护。
 * <p>
 * 中介者模式和代理模式的区别：
 * 1.都存在中间角色。
 * 2.代理模式，着重在代理【增强功能】
 * 3.中介者模式，只他帮你联系上了，就不管了，后面的事情全权由自己完成。【只管搭桥】
 */
public class ChatRoom {
    public void showMsg(User user, String message) {
        System.out.println("【" + user.getName() + "】，传达信息：" + message);
    }

    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom();
        User tom = new User("Tom", chatRoom);
        User jerry = new User("Jerry", chatRoom);

        tom.sendMessage("大家好，我是" + tom.getName() + "欢迎大家，进入聊天室");
        jerry.sendMessage("大家好，我是" + jerry.getName() + "欢迎大家，进入聊天室");
    }
}
