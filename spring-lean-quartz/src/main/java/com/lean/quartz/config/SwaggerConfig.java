package com.lean.quartz.config;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

/**
 * Swagger配置
 *
 * 
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi userApi() {
        String[] paths = {"/**"};
        String[] packagedToMatch = {"com.xxx"};
        return GroupedOpenApi.builder().group("swagger").pathsToMatch(paths).packagesToScan(packagedToMatch).build();
    }

    @Bean
    public OpenAPI customOpenAPI() {
        Contact contact = new Contact();
        contact.setName("aa ");

        return new OpenAPI().info(new Info().title("swagger").description("swagger").contact(contact).version("1.0")
            .termsOfService("https://127.0.0.1").license(new License().name("aa").url("https://127.0.0.1")));
    }

}