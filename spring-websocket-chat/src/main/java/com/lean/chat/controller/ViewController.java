package com.lean.chat.controller;

import com.lean.chat.entitiy.LoginUser;
import com.lean.chat.service.LoginService;
import com.lean.chat.service.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author zhw
 **/
@Controller
public class ViewController {
    @Autowired
    private LoginService loginservice;

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession httpSession) {
        return "login";
    }

    @RequestMapping("/onlineusers")
    @ResponseBody
    public Set<String> onlineusers(@RequestParam("currentuser") String currentuser) {
        ConcurrentHashMap<String, Session> map = WebSocketServer.getSessionPools();
        Set<String> set = map.keySet();
        Iterator<String> it = set.iterator();
        Set<String> nameset = new HashSet<String>();
        while (it.hasNext()) {
            String entry = it.next();
            if (!entry.equals(currentuser)) {
                nameset.add(entry);
            }
        }
        return nameset;
    }

    @RequestMapping("/loginvalidate")
    public String loginvalidate(@RequestParam("username") String username, @RequestParam("password") String pwd, HttpSession httpSession) {
        if (username == null) {
            return "login";
        }
        String realpwd = loginservice.getpwdbyname(username);
        if (realpwd != null && pwd.equals(realpwd)) {
            Integer id = loginservice.getIdbyname(username);
            httpSession.setAttribute("id", id);
            return "chatroom";
        } else {
            return "fail";
        }
    }

    @RequestMapping(value = "/currentuser", method = RequestMethod.GET)
    @ResponseBody
    public LoginUser currentuser(HttpSession httpSession) {
        Integer id = (Integer) httpSession.getAttribute("id");
        return loginservice.getUserById(id);
    }

}
