package com.writeframe.设计模式.桥接模式.base;

/**
 * @ClassName IImplementor
 * @Description 顶层接口定义
 */
public interface IImplementor {
    void operationImpl();
}
