package com.lean.jsqlparser.utils;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.statement.update.Update;
import net.sf.jsqlparser.util.TablesNamesFinder;
import java.util.List;

/**
 * @ClassName: SqlParserUtil
 * @Description:
 * @Author: zhw
 * @Date: 2022/7/6 15:41
 */
public class SqlParserUtil {

    /**
     * 获取 update 语句的表名
     *
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static String getUpdateTableName(String sql) throws JSQLParserException {
        Update model = (Update) CCJSqlParserUtil.parse(sql);
        Table table = model.getTable();
        String schemaName = StringUtil.isBlank(table.getSchemaName()) ? "" : table.getSchemaName() + ".";
        return schemaName + table.getName();
    }

    /**
     * 获取 select 语句的表名
     *
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static List<String> getSelectTableName(String sql) throws JSQLParserException {
        Select model = (Select) CCJSqlParserUtil.parse(sql);
        TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
        List<String> tableNameList = tablesNamesFinder.getTableList(model);
        return tableNameList;
    }

    /**
     * 获取 delete 语句的表名
     *
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static String getDeleteTableName(String sql) throws JSQLParserException {
        Delete model = (Delete) CCJSqlParserUtil.parse(sql);
        Table item = model.getTable();
        String schemaName = StringUtil.isBlank(item.getSchemaName()) ? "" : item.getSchemaName() + ".";
        return schemaName + item.getName();
    }

    /**
     * 获取insert 语句的表名
     *
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static String getInsertTableName(String sql) throws JSQLParserException {
        Insert model = (Insert) CCJSqlParserUtil.parse(sql);
        Table item = model.getTable();
        String schemaName = StringUtil.isBlank(item.getSchemaName()) ? "" : item.getSchemaName() + ".";
        return schemaName + item.getName();
    }

    /**
     * 获取删表语句的表名
     *
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static String getDropTableName(String sql) throws JSQLParserException {
        Drop model = (Drop) CCJSqlParserUtil.parse(sql);
        Table item = model.getName();
        String schemaName = StringUtil.isBlank(item.getSchemaName()) ? "" : item.getSchemaName() + ".";
        return schemaName + item.getName();
    }

    /**
     * 获取建表语句的表名
     *
     * @param sql
     * @return
     * @throws JSQLParserException
     */
    public static String getCreateTableName(String sql) throws JSQLParserException {
        CreateTable model = (CreateTable) CCJSqlParserUtil.parse(sql);
        Table item = model.getTable();
        String schemaName = StringUtil.isBlank(item.getSchemaName()) ? "" : item.getSchemaName() + ".";
        return schemaName + item.getName();
    }


    static class StringUtil {
        /**
         * 是否为空
         *
         * @param s
         * @return
         */
        public static boolean isBlank(String s) {
            if (null == s || "".equals(s)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static void main(String[] args) throws JSQLParserException {
        System.out.println(getCreateTableName("create table js.cm_template ( id bigint(10) );"));
        System.out.println(getDeleteTableName("delete from hj.cc where 1=1"));
        System.out.println(getDropTableName("drop table jh.xxx"));
        System.out.println(getInsertTableName("insert into jhjc.cm_table (id) values(1);"));
        System.out.println(getSelectTableName("select * from jhjc.cm_group where id = (select id from cm.cm_gg )"));
        System.out.println(getUpdateTableName("update jh.cm_123 set id = 1"));
    }

}
