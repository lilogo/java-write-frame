package com.writeframe.设计模式.门面模式;

/**
 * @ClassName GiftInfo
 * @Description 业务类，礼品信息
 */
public class GiftInfo {
    private String name;

    public GiftInfo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
