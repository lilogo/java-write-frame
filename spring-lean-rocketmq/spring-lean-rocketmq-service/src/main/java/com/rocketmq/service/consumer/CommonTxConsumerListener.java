package com.rocketmq.service.consumer;


import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 消费事务消息
 * 配置RocketMQ监听
 */
@Service
@RocketMQMessageListener(consumerGroup = "common-customer-tx", topic = "common-topic-tx",selectorExpression = "TAGA||TAGB||TAGC",messageModel = MessageModel.CLUSTERING)
public class CommonTxConsumerListener implements RocketMQListener<String> {

    @Override
    public void onMessage(String s) {
        System.out.println("消费消息 事务消息：" + s);
    }
}
