package com.lean.flowable.event.delegate;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

import java.time.LocalDateTime;

//正常结束任务
public class MyTwoDelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("MyTwoDelegate---->执行了" + LocalDateTime.now().toString());
    }
}
