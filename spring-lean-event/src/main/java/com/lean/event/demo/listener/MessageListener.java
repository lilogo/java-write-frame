package com.lean.event.demo.listener;


import com.lean.event.demo.events.MessageEvent;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
/**
 * 同步
 * 监听器：
 * 实现方式：
 *      1.实现ApplicationListener
 *      2.@EventListener
 */
@Slf4j
@Component
public class MessageListener implements ApplicationListener<MessageEvent> {
    @SneakyThrows
    @Override
    public void onApplicationEvent(MessageEvent event) {
        String messageId = event.getMessageId();
        long start = System.currentTimeMillis();
        long end = System.currentTimeMillis();
        log.info("{}：耗时：({})毫秒", messageId, (end - start));
    }
}

