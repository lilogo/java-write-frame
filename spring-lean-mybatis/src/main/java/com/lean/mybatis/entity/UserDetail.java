package com.lean.mybatis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDetail {
    private Long id;
    private Long orgId;
    private String superAdmin;
}
