package com.writeframe.设计模式.适配器模式.interfaceadapter;


/**
 * @ClassName Adaptee
 * @Description 适配原角色
 */
public class Adaptee {
    public int specificRequest() {
        return 220;
    }
}
