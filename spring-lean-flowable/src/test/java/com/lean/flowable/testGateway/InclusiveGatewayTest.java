package com.lean.flowable.testGateway;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.lean.flowable.config.BaseConfiguation;


/**
 *包含网关--测试
 可以把**包容网关（inclusive gateway）**看做排他网关与并行网关的组合。与排他网关一样，可以在包容网关的出口顺序流上定义条件，
 包容网关会计算条件。然而主要的区别是，包容网关与并行网关一样，可以同时选择多于一条出口顺序流。
 包容网关的功能取决于其入口与出口顺序流：
 **分支：**流程会计算所有出口顺序流的条件。对于每一条计算为true的顺序流，流程都会创建一个并行执行。
 **合并：**所有到达包容网关的并行执行，都会在网关处等待。直到每一条具有流程标志（process token）的入口顺序流，
 * 都有一个执行到达。这是与并行网关的重要区别。换句话说，包容网关只会等待可以被执行的入口顺序流。在合并后，流程穿过合并并行网关继续。
 *
 请注意，如果包容网关同时具有多条入口与出口顺序流，可以同时具有分支与合并的行为。在这种情况下，网关首先合并所有具有流程标志的入口顺序流，然后为每一个条件计算为true的出口顺序流分裂出并行执行路径。

 包容网关的汇聚行为比并行网关更复杂。所有到达包容网关的并行执行，都会在网关等待，直到所有“可以到达”包容网关的执行都“到达”包容网关。 判断方法为：计算当前流程实例中的所有执行，检查从其位置是否有一条到达包容网关的路径（忽略顺序流上的任何条件）。如果存在这样的执行（可到达但尚未到达），则不会触发包容网关的汇聚行为。
 */
public class InclusiveGatewayTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("inclusiveGateway")
													.name("inclusiveGateway")
													.addClasspathResource("process/gateway/包含网关.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *在启动流程后，如果流程变量paymentReceived== false和shipOrder == true，则将创建两个任务。如果这些过程变量中只有一个等于true，
	 * 则只创建一个任务。如果没有条件计算为true，则抛出异常。这可以通过指定默认的传出顺序流来防止。
	 */
	@Test
	public void start() {
		String processDefinitionKey = "inclusiveGateway";

		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("paymentReceived", false);
		variables.put("shipOrder", true);

		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}
	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "30005";
		taskService.complete(taskId);
	}

}
