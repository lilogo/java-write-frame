package com.writeframe.设计模式.装饰器模式.demo;

/**
 * @ClassName EggDecorator
 * @Description 加蛋装饰器
 */
public class EggDecorator extends BattercakeDecorator {
    public EggDecorator(AbstractBattercake battercake) {
        super(battercake);
    }

    @Override
    protected String getMsg() {
        return super.getMsg() +",外加一个鸡蛋";
    }

    @Override
    public int getPrice() {
        return super.getPrice() + 1;
    }
}
