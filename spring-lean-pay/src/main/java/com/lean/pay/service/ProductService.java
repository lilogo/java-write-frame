package com.lean.pay.service;

import com.lean.pay.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;

public interface ProductService extends IService<Product> {

}
