package com.lean.flowable.event.error;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class ErrorEventServiceTaskJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {

		System.out.println("＝＝＝＝＝＝＝＝＝＝＝错误抛出事件触发＝＝＝＝＝＝＝＝＝＝＝");

		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点: " + flowElement.getName());

	}

}
