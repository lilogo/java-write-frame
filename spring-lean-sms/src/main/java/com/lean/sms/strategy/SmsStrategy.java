package com.lean.sms.strategy;

import com.lean.sms.properties.SmsProperties;

import java.util.Map;

/**
 * 短信
 *
 * @author 
 */
public abstract class SmsStrategy {

    public  SmsProperties smsProperties;

    /**
     * 发送短信
     * @param mobile  手机号
     * @param params  参数
     */
   public abstract void send(String mobile, Map<String, String> params);
}
