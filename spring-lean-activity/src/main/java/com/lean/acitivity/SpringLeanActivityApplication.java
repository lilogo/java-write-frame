package com.lean.acitivity;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

//https://zhengjianfeng.cn/?p=162
//https://blog.csdn.net/chinese_cai/article/details/103792322
//https://blog.csdn.net/qq_40925189/category_11009323.html
@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
@MapperScan("com.lean.acitivity.mapper")
public class SpringLeanActivityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanActivityApplication.class, args);
    }

}
