package com.lean.process.common;

import cn.hutool.json.JSONUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Component
public class RedisUtil {

    @Resource
    private RedisTemplate redisTemplate;

    public ProcessMsg getProcessMsg(String key) {
        ProcessMsg pm = get(key, ProcessMsg.class);
        return pm == null ? new ProcessMsg() : pm;
    }

    /**
     * 根据key 获取对象
     *
     * @param key key
     * @return T
     */
    public  <T> T get(String key, Class<T> clazz) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        //redis 获取数据 TODO
        return null;
    }

    public  void del(String uuid) {
        //redis 删除数据
    }

    public  void setProcessMsg(String redisKey, ProcessMsg pm) {
        //设置数据
        if (!StringUtils.isEmpty(redisKey)) {
            redisTemplate.opsForValue().set(redisKey, JSONUtil.toJsonStr(pm),60 * 60 * 24);
        }
    }

    /**
     * 根据key值得到String类型的返回值
     *
     * @param key key
     * @return String
     */
    public  String get(String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        return redisTemplate.opsForValue().get(key).toString();
    }
}
