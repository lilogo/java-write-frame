package com.writeframe.设计模式.享元模式.base;

/**
 * @ClassName ConcreteFlyweight
 * @Description 享元实现,因为该类实例需要反复使用，所以，使用工厂类来创建实例
 */
public class ConcreteFlyweight implements IFlyweight {

    private String intrinsicState;

    public ConcreteFlyweight(String intrinsicState) {
        this.intrinsicState = intrinsicState;
    }

    @Override
    public void operation(String extrinsicState) {
        System.out.println("Object address:"+System.identityHashCode(this));
        System.out.println("IntrinsicState:"+this.intrinsicState);
        System.out.println("ExtrinsicState:"+extrinsicState);
    }
}
