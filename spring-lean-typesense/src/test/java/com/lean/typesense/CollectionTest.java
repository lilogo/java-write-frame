package com.lean.typesense;

import cn.hutool.json.JSONUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.typesense.model.CollectionAlias;
import org.typesense.model.CollectionAliasesResponse;
import org.typesense.model.CollectionResponse;
import org.typesense.model.CollectionUpdateSchema;


public class CollectionTest extends TypesenseApplicationTests{

    @Autowired
    private CollectionService collectionService;

    @Test
    public void createCollectionTest() throws Exception {
        CollectionResponse response = collectionService.createCollection("companies");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    @Test
    public void createAutoCollectionTest() throws Exception {
        CollectionResponse response = collectionService.createAutoCollection("common-companies");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    @Test
    public void getColection() throws Exception {
        CollectionResponse response = collectionService.getColection("companies");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    @Test
    public void listColection() throws Exception {
        CollectionResponse[] collectionResponses = collectionService.listColection();
        System.out.println(JSONUtil.toJsonStr(collectionResponses));
    }

    @Test
    public void delColection() throws Exception {
        CollectionResponse response = collectionService.delColection("places");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    @Test
    public void insertFieldColection() throws Exception {
        CollectionUpdateSchema response = collectionService.insertFieldColection("companies", "employees");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    @Test
    public void updateFiledColection() throws Exception {
        CollectionUpdateSchema response = collectionService.updateFieldColection("companies", "employees");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    @Test
    public void dropFiledColection() throws Exception {
        CollectionUpdateSchema response = collectionService.dropFiledColection("companies", "employees");
        System.out.println(JSONUtil.toJsonStr(response));
    }

    //别名

    @Test
    public void createOrUpdateCollectionAlias() throws Exception {
        CollectionAlias alias = collectionService.createOrUpdateCollectionAlias("companiesAlias", "companies");
        System.out.println(JSONUtil.toJsonStr(alias));
    }

    @Test
    public void getCollectionAlias() throws Exception {
        CollectionAlias alias = collectionService.getCollectionAlias("companiesAlias");
        System.out.println(JSONUtil.toJsonStr(alias));
    }

    @Test
    public void listCollectionAlias() throws Exception {
        CollectionAliasesResponse alias = collectionService.listCollectionAlias();
        System.out.println(JSONUtil.toJsonStr(alias));
    }

    @Test
    public void delCollectionAlias() throws Exception {
        CollectionAlias alias = collectionService.delCollectionAlias("companiesAlias");
        System.out.println(JSONUtil.toJsonStr(alias));
    }
}
