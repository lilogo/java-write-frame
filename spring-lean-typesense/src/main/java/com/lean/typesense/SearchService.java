package com.lean.typesense;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.typesense.api.Client;
import org.typesense.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 搜索
 */
@Service
public class SearchService {

    @Autowired
    private Client client;

    /**
     * 针对一个或多个文本字段的查询以及针对数字或构面字段的过滤器列表组成
     *
     * @param query 要在集合中搜索的查询文本。
     * @param queryParam string / string[]应查询的一个或多个字段。用逗号分隔多个字段：company_name, country.字段的顺序很重要：与列表中较早的字段匹配的记录被认为比与列表中较晚的字段匹配的记录更相关。
     * @param filter 过滤
     * @param sort 排序
     * @param group 分组
     * @param groupLimit 分组后每组数量限制 例如，如果某个查询的搜索结果包含太多同一品牌的文档，您可以执行以下操作group_by=brand&group_limit=3以确保搜索结果中仅返回每个品牌的前 3 个结果。
     * @param page 页码
     * @param pageSize 每页数量
     * @return
     * @throws Exception
     */
    public SearchResult searchData(String collectName, String query, String queryParam, String filter,
                                   String sort, String group, Integer groupLimit,
        Integer page, Integer pageSize) throws Exception {
        SearchParameters searchParameters = new SearchParameters().q(query);
        searchParameters.queryBy(queryParam);
        if (!StringUtils.isEmpty(filter)) {
            searchParameters.filterBy(filter);
        }
        // 未指定配许 默认 根据text_match其计算的相关性分数对结果进行排名。
        if (!StringUtils.isEmpty(sort)) {
            searchParameters.sortBy(sort);
        }
        if (!StringUtils.isEmpty(group)) {
            searchParameters.groupBy(group);
        }
        if (groupLimit != null) {
            searchParameters.groupLimit(groupLimit);
        }
        // 默认情况下，Typesense 返回前 10 个结果 ( page: 1, per_page: 10)。
        if (page != null) {
            searchParameters.page(groupLimit);
        }
        if (pageSize != null) {
            searchParameters.perPage(pageSize);
        }
        return client.collections(collectName).documents().search(searchParameters);
    }

    // 地理搜索 Typesense 支持对包含纬度和经度值的字段进行地理搜索，指定为geopoint或geopoint[] 字段类型

    /**
     * 创建地理搜索集合
     *
     * @param collectionName
     * @return
     * @throws Exception
     */
    public CollectionResponse creatGeosearchCollection(String collectionName) throws Exception {
        CollectionSchema collectionSchema = new CollectionSchema();
        collectionSchema.name(collectionName);
        collectionSchema.addFieldsItem(new Field().name("title").type("string"));
        collectionSchema.addFieldsItem(new Field().name("points").type("int32"));
        collectionSchema.addFieldsItem(new Field().name("location").type("geopoint"));
        collectionSchema.defaultSortingField("points");
        return client.collections().create(collectionSchema);
    }

    /**
     * 创建地理搜索文档
     */
    public Map<String, Object> createGeosearchDocument(String collectionName, Map<String, Object> document) throws Exception {
        Map<String, Object> map = client.collections(collectionName).documents().create(document);
        return map;
    }

    /**
     * 在半径内搜索
     */
    public SearchResult getGeosearch(String collectionName) throws Exception {
        SearchParameters searchParameters = new SearchParameters()
                .q("*")
                .queryBy("title")
                .filterBy("location:(48.90615915923891, 2.3435897727061175, 5.1 km)")
            .sortBy("location(48.853, 2.344):asc");
        return client.collections(collectionName).documents().search(searchParameters);
    }

    /**
     * 在地理多边形内搜索
     */
    public SearchResult getGeosearch2(String collectionName) throws Exception {
        SearchParameters searchParameters = new SearchParameters().q("*").queryBy("title")
            .filterBy("location:(48.8662, 2.3255, 48.8581, 2.3209, 48.8561, 2.3448, 48.8641, 2.3469)")
                .sortBy("location(48.853, 2.344):asc");
        // 排除半径
        // 有时，根据另一个属性（例如 ）对半径内的附近位置进行排序popularity，然后按该半径之外的距离进行排序是很有用的。您可以使用exclude_radius该选项。
        // .sortBy("location(48.853, 2.344, exclude_radius: 2mi):asc, popularity:desc");
        // 精确 使用该参数将所有地理点分组到“组”中precision，以便该组内的所有结果都将具有相同的“地理距离分数”。
        // .sortBy("location(48.853, 2.344, precision: 2mi):asc, popularity:desc");
        return client.collections(collectionName).documents().search(searchParameters);
    }

    // 矢量搜索 Typesense 支持将机器学习模型生成的嵌入添加到每个文档，然后对它们进行最近邻搜索的功能。这使您可以构建相似性搜索、推荐、语义搜索、视觉搜索等功能。

    /**
     *向量创建集合
     * @param collectionName
     * @return
     * @throws Exception
     */
    public CollectionResponse createVectorSearchCollection(String collectionName) throws Exception {
        CollectionSchema collectionSchema = new CollectionSchema();
        collectionSchema.name(collectionName)
                .addFieldsItem(new Field().name("title").type("string"))
                .addFieldsItem(new Field().name("points").type("int32"))
                .addFieldsItem(new Field().name("vec").type("float[]").numDim(4))
                .defaultSortingField("points");
        return client.collections().create(collectionSchema);
    }

    /**
     *向量创建文档
     * @param collectionName
     * @return
     * @throws Exception
     */
    public Map<String, Object> createVectorSearchDocument(String collectionName) throws Exception {
        HashMap<String, Object> document = new HashMap<>();
        float[] vec =  {0.04f, 0.234f, 0.113f, 0.001f};
        document.put("title", "Louvre Museuem");
        document.put("points", 1);
        document.put("vec", vec);
        return client.collections(collectionName).documents().create(document);
    }

    /**
     *最近邻向量搜索
     * @param collectionName
     * @return
     * @throws Exception
     */
    public MultiSearchResponse getVectorSearchDocument(String collectionName) throws Exception {
        MultiSearchSearchesParameter multiSearchParameters = new MultiSearchSearchesParameter();
        List<MultiSearchCollectionParameters> searches = new ArrayList<>();
        MultiSearchCollectionParameters parameters = new MultiSearchCollectionParameters();
        parameters.setCollection(collectionName);
        parameters.setQ("*");
        parameters.setVectorQuery("vec:([0.96826, 0.94, 0.39557, 0.306488], k:100)");
        searches.add(parameters);
        multiSearchParameters.searches(searches);
        HashMap<String,String> commonSearchParams = new HashMap<>();
        commonSearchParams.put("query_by","name");
       return client.multiSearch.perform(multiSearchParameters,commonSearchParams);

    }

    /**
     *查询相似文档
     * @param collectionName
     * @return
     * @throws Exception
     */
    public MultiSearchResponse likeVectorSearchDocument(String collectionName) throws Exception {
        MultiSearchSearchesParameter multiSearchParameters = new MultiSearchSearchesParameter();
        List<MultiSearchCollectionParameters> searches = new ArrayList<>();
        MultiSearchCollectionParameters parameters = new MultiSearchCollectionParameters();
        parameters.setCollection(collectionName);
        parameters.setQ("*");
        parameters.setVectorQuery("vec:([], id: foobar)");
        searches.add(parameters);
        multiSearchParameters.searches(searches);
        HashMap<String,String> commonSearchParams = new HashMap<>();
        commonSearchParams.put("query_by","name");
       return client.multiSearch.perform(multiSearchParameters,commonSearchParams);

    }
    /**
     *暴力搜索
     * @param collectionName
     * @return
     * @throws Exception
     */
    public MultiSearchResponse strogeLikeVectorSearchDocument(String collectionName) throws Exception {
        MultiSearchSearchesParameter multiSearchParameters = new MultiSearchSearchesParameter();
        List<MultiSearchCollectionParameters> searches = new ArrayList<>();
        MultiSearchCollectionParameters parameters = new MultiSearchCollectionParameters();
        parameters.setCollection(collectionName);
        parameters.setQ("*");
        parameters.setFilterBy("category:shoes");
        parameters.setVectorQuery("vec:([0.96826, 0.94, 0.39557, 0.306488], k:100, flat_search_cutoff: 20)");
        searches.add(parameters);
        multiSearchParameters.searches(searches);
        HashMap<String,String> commonSearchParams = new HashMap<>();
        commonSearchParams.put("query_by","name");
       return client.multiSearch.perform(multiSearchParameters,commonSearchParams);

    }


    //联合 、多重搜索
    //limit_multi_searches:多搜索请求中可以发送的最大搜索请求数
    //x-typesense-api-key:可以为 multi_search 请求中的每个搜索嵌入单独的搜索 API 密钥。
    public MultiSearchResponse multiSearches(String collectionName,String collectionName2) throws Exception {
        MultiSearchSearchesParameter multiSearchParameters = new MultiSearchSearchesParameter();
        List<MultiSearchCollectionParameters> searches = new ArrayList<>();
        MultiSearchCollectionParameters parameters = new MultiSearchCollectionParameters();
        parameters.setCollection(collectionName);
        parameters.setQ("shoe");
        parameters.setFilterBy("price:=[50..120]");
        parameters.setVectorQuery("vec:([0.96826, 0.94, 0.39557, 0.306488], k:100, flat_search_cutoff: 20)");
        searches.add(parameters);

        MultiSearchCollectionParameters parameters2 = new MultiSearchCollectionParameters();
        parameters2.setCollection(collectionName2);
        parameters2.setQ("Nike");
        searches.add(parameters2);

        multiSearchParameters.searches(searches);
        HashMap<String,String> commonSearchParams = new HashMap<>();
        commonSearchParams.put("query_by","name");
        return client.multiSearch.perform(multiSearchParameters, commonSearchParams);
    }

}
