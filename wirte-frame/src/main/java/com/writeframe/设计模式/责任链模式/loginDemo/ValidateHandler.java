package com.writeframe.设计模式.责任链模式.loginDemo;

import cn.hutool.core.util.StrUtil;

/**
 * 入参判空处理
 */
public class ValidateHandler extends Handler {
    @Override
    public void doHandle(Member member) {
        //入参判空
        if (StrUtil.isEmpty(member.getLoginName()) || StrUtil.isEmpty(member.getLoginPass())) {
            System.out.println("用户名和密码为空");
            return;
        }
        System.out.println("用户名和密码不为空，可以往下执行了");
        //向下传递
        next.doHandle(member);
    }
}
