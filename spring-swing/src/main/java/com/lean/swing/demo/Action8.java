package com.lean.swing.demo;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * swing的表单操作（文本框、文本域、单选、复选、下拉）
 * 文本框JTextField：
 *      方法名称	说明
 *      Dimension getPreferredSize()	获得文本框的首选大小
 *      void scrollRectToVisible(Rectangle r)	向左或向右滚动文本框中的内容
 *      void setColumns(int columns)	设置文本框最多可显示内容的列数
 *      void setFont(Font f)	设置文本框的字体
 *      void setScrollOffset(int scrollOffset)	设置文本框的滚动偏移量（以像素为单位）
 *      void setHorizontalAlignment(int alignment)	设置文本框内容的水平对齐方式
 *
 * 文本域JTextArea：
 *      方法名称	说明
 *      void append(String str)	将字符串 str 添加到文本域的最后位置
 *      void setColumns(int columns)	设置文本域的行数
 *      void setRows(int rows)	设置文本域的列数
 *      int getColumns()	获取文本域的行数
 *      void setLineWrap(boolean wrap)	设置文本域的换行策略
 *      int getRows()	获取文本域的列数
 *      void insert(String str,int position)	插入指定的字符串到文本域的指定位置
 *      void replaceRange(String str,int start,int end)	将指定的开始位 start 与结束位 end 之间的字符串用指定的字符串 str 取代
 *
 *下拉JComboBox:
 * 方法名称	说明
 * void addItem(Object anObject)	将指定的对象作为选项添加到下拉列表框中
 * void insertItemAt(Object anObject,int index)	在下拉列表框中的指定索引处插入项
 * void removeltem(0bject anObject)	在下拉列表框中删除指定的对象项
 * void removeItemAt(int anlndex)	在下拉列表框中删除指定位置的对象项
 * void removeAllItems()	从下拉列表框中删除所有项
 * int getItemCount()	返回下拉列表框中的项数
 * Object getItemAt(int index)	获取指定索引的列表项，索引从 0 开始
 * int getSelectedIndex()	获取当前选择的索引
 * Object getSelectedltem()	获取当前选择的项
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
public class Action8 extends JFrame{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public Action8() {
        // TODO Auto-generated constructor stub
        setTitle("文本输入框");
        setSize(600,300);
        JPanel jp=new JPanel();    //创建面板

        //文本框
        JTextField txtfield1=new JTextField();
        txtfield1.setText("普通文本框");    //设置文本框的内容
        JTextField txtfield2=new JTextField(28);
        txtfield2.setFont(new Font("楷体",Font.BOLD,16));    //修改字体样式
        txtfield2.setText("指定长度和字体的文本框");
        JTextField txtfield3=new JTextField(30);
        txtfield3.setText("居中对齐");
        txtfield3.setHorizontalAlignment(JTextField.CENTER);    //居中对齐
        jp.add(txtfield1);
        jp.add(txtfield2);
        jp.add(txtfield3);

        //文本域
        JTextArea jta=new JTextArea("请输入内容",7,30);
        jta.setLineWrap(true);    //设置文本域中的文本为自动换行
        jta.setForeground(Color.BLACK);    //设置组件的背景色
        jta.setFont(new Font("楷体",Font.BOLD,16));    //修改字体样式
        jta.setBackground(Color.YELLOW);    //设置按钮背景色
        JScrollPane jsp=new JScrollPane(jta);    //将文本域放入滚动窗口
        jp.add(jsp);



        //单选框JRadioButton
        JLabel label1=new JLabel("今日翻牌子：");
        JRadioButton rb1=new JRadioButton("海棠");    //创建JRadioButton对象
        JRadioButton rb2=new JRadioButton("夏竹");    //创建JRadioButton对象
        JRadioButton rb3=new JRadioButton("秋香",true);    //创建JRadioButton对象
        JRadioButton rb4=new JRadioButton("冬梅");    //创建JRadioButton对象
        label1.setFont(new Font("华文行楷",Font.BOLD,26));    //修改字体样式
        ButtonGroup group=new ButtonGroup();
        //添加JRadioButton到ButtonGroup中
        group.add(rb1);
        group.add(rb2);
        jp.add(label1);
        jp.add(rb1);
        jp.add(rb2);
        jp.add(rb3);
        jp.add(rb4);


        //复选框JCheckBox
        JLabel label=new JLabel("嵩山秘术·初子决：");
        label.setFont(new Font("华文行楷",Font.BOLD,26));    //修改字体样式
        JCheckBox chkbox1=new JCheckBox("拙石", true);    //创建指定文本和状态的复选框
        JCheckBox chkbox2=new JCheckBox("璞玉");    //创建指定文本的复选框
        JCheckBox chkbox3=new JCheckBox("破梦", true);
        JCheckBox chkbox4=new JCheckBox("灭神");
        JCheckBox chkbox5=new JCheckBox("诸佛", true);
        JCheckBox chkbox6=new JCheckBox("戳魔");
        jp.add(label);
        jp.add(chkbox1);
        jp.add(chkbox2);
        jp.add(chkbox3);
        jp.add(chkbox4);
        jp.add(chkbox5);
        jp.add(chkbox6);

        //下拉JComboBox
        JLabel label2=new JLabel("幸福模式：");    //创建标签
        JComboBox cmb=new JComboBox();    //创建JComboBox
        cmb.addItem("--请选择--");    //向下拉列表中添加一项
        cmb.addItem("长命百岁");
        cmb.addItem("幸福安康");
        cmb.addItem("颐养天年");
        jp.add(label2);
        jp.add(cmb);
        add(jp);

        setBackground(Color.pink);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    public static void main(String[] args) {
        new Action8();
    }
}
