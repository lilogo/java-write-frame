package com.writeframe.设计模式.模板方法;

/**
 * 模板方法模式可以初步的理解，模板就是一种规范，也可以说是一种固定的格式；方法就是为了达成某个意图所使用的方式或者办法。
 * 模板方法模式：定义一个操作中的算法的估价，而将一些步骤延迟到子类中。模板方法使得子类可以不改变一个算法的结构即可重定义该算法的某些特定步骤。
 * 优点：
 * 把重复不变的代码，放到父类，让子类去继承父类，降低了子类代码的冗余。
 * 缺点:
 * 如果算法过于复杂，将导致子类过多，使得程序会变得更加复杂。
 */
public class TemplateModel {
    public static void main(String[] args) {
        AbstractClass abstractClass=new ConcreteClassA();
        abstractClass.TemplateMethod();
    }
}
