package com.lean.mybatis.entity;

import lombok.Data;

import java.util.Date;
@Data
public class Role {
    private Long id;
    private String roleName;
    private String roleRemark;
    private Date createDate;
    private Date updateDate;
    private Integer isValid;
}
