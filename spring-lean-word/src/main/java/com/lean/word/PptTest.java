package com.lean.word;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.sl.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.IOUtils;
import org.apache.poi.util.Units;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xslf.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.xmlbeans.XmlObject;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;
import org.openxmlformats.schemas.drawingml.x2006.main.CTRegularTextRun;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextBody;
import org.openxmlformats.schemas.drawingml.x2006.main.CTTextParagraph;
import org.openxmlformats.schemas.presentationml.x2006.main.CTGroupShape;
import org.openxmlformats.schemas.presentationml.x2006.main.CTShape;
import org.openxmlformats.schemas.presentationml.x2006.main.CTSlide;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * ppt 生成
 */
public class PptTest {

    private static final int COLUMN_LANGUAGES = 0;
    private static final int COLUMN_COUNTRIES = 1;
    private static final int COLUMN_SPEAKERS = 2;

    public static void main(String[] args) throws Exception {
//         createBaseXSLFPPT();
        createPPTByTemplate();

        // String[] file = new String[] {"./doc/3.pptx","./doc/2.pptx"};
        // mergePptx(file,"./doc/4.pptx");
        // pptToPng("./doc/3.pptx",2);
    }

    private static void createPPTByTemplate() throws Exception {
        // 读取模板文件
        ClassPathResource resource = new ClassPathResource("ppt-template.pptx");
        // 根据模板，创建一个新的ppt文档
        XMLSlideShow ppt = new XMLSlideShow(resource.getInputStream());
        // 替换模板内容
        // 得到每页ppt
        List<XSLFSlide> slides = ppt.getSlides();
        // 遍历ppt，填充模板
        for (int i = 0; i < slides.size(); i++) {
            // 遍历每页ppt中待填充的形状组件
            String slideName = slides.get(i).getSlideName();
            System.out.println(slideName);
            for (XSLFShape shape : slides.get(i).getShapes()) {
                if (shape instanceof TextShape) {
                    // 替换文本
                    TextShape textShape = (TextShape) shape;
                    TextRun textRun;
                    String text = textShape.getText().trim();
                    switch (text) {
                        case "username":
                            textRun = textShape.setText("张三");
                            textRun.setFontFamily("宋体(正文)");
                            textRun.setFontSize(18.0);
                            break;
                        case "dates":
                            textRun = textShape.setText("2022-10-30");
                            textRun.setFontFamily("宋体(正文)");
                            textRun.setFontSize(18.0);
                            break;
                        case "desc":
                            textRun = textShape.setText("描述");
                            textRun.setFontFamily("宋体(正文)");
                            textRun.setFontSize(18.0);
                            textRun.setFontColor(Color.green);
                            break;
                        case "prec":
                            textRun = textShape.setText("23%");
                            textRun.setFontFamily("宋体(正文)");
                            textRun.setFontSize(18.0);
                            textRun.setFontColor(Color.red);
                            break;
                    }
                } else if (shape instanceof PictureShape) {
                    // 替换图片
                    PictureData pictureData = ((PictureShape) shape).getPictureData();
                    byte[] bytes = IOUtils.toByteArray(new FileInputStream("./doc/3.png"));
                    pictureData.setData(bytes);
                } else if (shape instanceof XSLFTable) {
                    // 表格
                    XSLFTable xslfTable = ((XSLFTable) shape);
                    List<XSLFTableRow> rowList = xslfTable.getRows();
                    //每一行
                    for (int i1 = 0; i1 < rowList.size(); i1++) {
                        //每一列
                        rowList.get(i1).setHeight(12);
                        List<XSLFTableCell> cells = rowList.get(i1).getCells();
                        for (int a1 = 0; a1 < cells.size(); a1++) {
                            cells.get(a1).setText("cells" + a1);
                        }
                    }
                    //XSLFGraphicFrame
                }else if(shape instanceof XSLFGraphicFrame){
                    XSLFGraphicFrame xslfGraphicFrame=(XSLFGraphicFrame)shape;
                    System.out.println("图形");
                }
            }
        }
        // 将新的ppt写入到指定的文件中
        FileOutputStream outputStream = new FileOutputStream("./doc/33.pptx");
        ppt.write(outputStream);
        outputStream.close();

    }

    public static void createBaseXSLFPPT() throws Exception {
        // 创建一个新的空幻灯片
        XMLSlideShow ppt = new XMLSlideShow();
        // 检索页面大小。坐标以点表示 (72 dpi)
        // Dimension pgsize = ppt.getPageSize();
        // int pgx = pgsize.width; //以磅为单位的滑动宽度
        // System.out.println(pgx);
        // int pgy = pgsize.height; //以磅为单位的滑动高度
        // System.out.println(pgy);
        // //设置新页面大小
        // ppt.setPageSize(new java.awt.Dimension(1024, 768));

        // 空白幻灯片 设置标题
        XSLFSlide titleslide = ppt.createSlide();
        XSLFTextShape titleShape = titleslide.createTextBox();
        titleShape.setPlaceholder(Placeholder.TITLE);
        titleShape.setText("This is a slide title");
        titleShape.setAnchor(new Rectangle(50, 50, 400, 100));

        // 获取布局模板 默认 BLANK布局
        XSLFSlideMaster defaultMaster = ppt.getSlideMasters().get(0);
        // 标题幻灯片布局
        XSLFSlideLayout titleLayout = defaultMaster.getLayout(SlideLayout.TITLE);
        // 添加第一张幻灯片
        XSLFSlide firstSlide = ppt.createSlide(titleLayout);
        // 添加标题
        XSLFTextShape title = firstSlide.getPlaceholder(0);
        title.setText("标题");

        XSLFTextBox shape1 = firstSlide.createTextBox();
        // initial height of the text box is 100 pt but
        Rectangle anchor = new Rectangle(10, 100, 300, 100);
        shape1.setAnchor(anchor);

        XSLFTextParagraph p1 = shape1.addNewTextParagraph();
        XSLFTextRun textRun = p1.addNewTextRun();
        textRun.setText("org.apache.poi.xslf.usermodel.XSLFTextParagraph");
        textRun.setFontSize(24d);
        textRun.setFontColor(new Color(85, 142, 213));

        XSLFTextParagraph p2 = shape1.addNewTextParagraph();

        p2.setSpaceBefore(-20d);
        p2.setSpaceAfter(300d);
        XSLFTextRun textRun1 = p2.addNewTextRun();
        textRun1.setText("com/lean/word/PptTest.java:157");
        textRun1.setFontSize(16d);

        XSLFTextParagraph p3 = shape1.addNewTextParagraph();

        XSLFTextRun textRun2 = p3.addNewTextRun();
        textRun2.setText("com/lean/word/PptTest.java:163");
        textRun2.setFontSize(24d);
        textRun2.setFontColor(new Color(85, 142, 213));

        XSLFTextParagraph p4 = shape1.addNewTextParagraph();
        // 设置距离之前的间隔
        p4.setSpaceBefore(-20d);
        // 设置距离之后的间隔
        p4.setSpaceAfter(300d);
        XSLFTextRun textRun3 = p4.addNewTextRun();
        // 字体大小
        textRun3.setFontSize(16d);
        textRun3.setText("V2.returns.get_return_list API: 可获取一个店铺的退货退款申请列表，" + "每个申请都将会返回一个return_sn作为唯一标识，买家针对同一个订单可能会提交多个return_sn。"
                + "返回参数中包含order_sn即为此退货退款申请关联的订单号。另外，接口支持筛选不同类型的退货退款申请，包括退货状态（status）、"
                + "谈判状态（negotiation_status）、证据上传状态（seller_proof_status）、卖家赔偿状态（seller_compensation_status）。");
        // 调整形状大小以适合文本
        shape1.resizeToFitText();

        // 标题和内容 第三个布局
        XSLFSlideLayout titleBodyLayout = defaultMaster.getLayout(SlideLayout.TITLE_AND_CONTENT);
        XSLFSlide slide2 = ppt.createSlide(titleBodyLayout);
        XSLFTextShape title2 = slide2.getPlaceholder(0);
        title2.setText("第二个标题");
        XSLFTextShape body2 = slide2.getPlaceholder(1);
        body2.clearText(); // 取消设置任何现有的文本
        body2.addNewTextParagraph().addNewTextRun().setText("第一段");
        body2.addNewTextParagraph().addNewTextRun().setText("第二段");
        body2.addNewTextParagraph().addNewTextRun().setText("第三段");

        // 设置层级
        XSLFTextParagraph xslfTextRuns1 = body2.addNewTextParagraph();
        xslfTextRuns1.setIndentLevel(0);
        xslfTextRuns1.addNewTextRun().setText("第一段");
        XSLFTextParagraph xslfTextRuns2 = body2.addNewTextParagraph();
        xslfTextRuns1.setIndentLevel(1);
        xslfTextRuns2.addNewTextRun().setText("第二段");
        XSLFTextParagraph xslfTextRuns3 = body2.addNewTextParagraph();
        xslfTextRuns3.setIndentLevel(3);
        xslfTextRuns3.addNewTextRun().setText("第三段");

        // 布局页面
        XSLFSlide slide3 = ppt.createSlide();
        // 构建一个文本框
        XSLFTextBox shape = slide3.createTextBox();
        // 设置文本
        shape.setText("com/lean/word/PptTest.java:210");
        shape.setAnchor(new Rectangle2D.Double(100, 100, 500, 350));

        XSLFTextParagraph p = shape.addNewTextParagraph();
        XSLFTextRun r1 = p.addNewTextRun();
        r1.setText("blue ");
        r1.setFontColor(Color.blue);
        r1.setFontSize(24.);
        XSLFTextRun r2 = p.addNewTextRun();
        r2.setText(" red");
        r2.setFontColor(Color.red);
        r2.setBold(true);
        XSLFTextRun r3 = p.addNewTextRun();
        r3.setFontColor(Color.black);
        r3.setText(" black");
        r3.setFontSize(12.);
        r3.setItalic(true);
        r3.setStrikethrough(true);
        XSLFTextRun r4 = p.addNewTextRun();
        r4.setFontColor(Color.yellow);
        r4.setText(" yellow");
        r4.setUnderlined(true);
        // 创建超链接
        XSLFTextRun r5 = p.addNewTextRun();
        r5.setText("超链接");
        XSLFHyperlink link = r5.createHyperlink();
        link.setAddress("https://poi.apache.org");
        r5.setText("https://poi.apache.org"); // visible text

        // 创建文本框
        XSLFTextBox shape2 = slide3.createTextBox();
        // 定位
        shape2.setAnchor(new Rectangle(300, 50, 200, 50));
        XSLFTextRun textRun4 = shape2.addNewTextParagraph().addNewTextRun();
        XSLFHyperlink link2 = textRun4.createHyperlink();
        textRun4.setText("Go to top");
        // 超链接定位到第三个幻灯片
        link2.linkToSlide(slide2);

        // 创建第四个幻灯片 表格
        XSLFSlide slide4 = ppt.createSlide();
        // 将图像添加到幻灯片
        byte[] pictureData = IOUtils.toByteArray(new FileInputStream("./doc/3.png"));
        XSLFPictureData idx = ppt.addPicture(pictureData, PictureData.PictureType.PNG);
        // 插入图片
        XSLFPictureShape pic = slide4.createPicture(idx);
        pic.setAnchor(new Rectangle2D.Double(100, 100, 500, 350));

        // 插入表格
        createTextList(ppt);

        // 插入表格
        createTable(ppt);

        // 插入表格
        createTableOne(ppt);

        String chartTitle = "10 languages with most speakers as first language";
        String[] seriesdata = new String[]{"countries", "speakers", "language"};

        String[] categories = new String[]{"العربية", "বাংলা", "中文", "English", "हिन्दी", "日本語", "português", "ਪੰਜਾਬੀ", "Русский язык", "español"};
        Double[] values1 = new Double[]{58.0, 4.0, 38.0, 118.0, 4.0, 2.0, 15.0, 6.0, 18.0, 31.0};
        Double[] values2 = new Double[]{315.0, 243.0, 1299.0, 378.0, 260.0, 128.0, 223.0, 119.0, 154.0, 442.0};

        /**
         * 柱状图
         */
        createSlideWithChart(ppt, chartTitle, seriesdata, categories, values1, values2);
        /**
         * 扇形图
         */
        createSlideWithChartOne(ppt, chartTitle, seriesdata, categories, values2, COLUMN_SPEAKERS);

        // 柱状图
        createSlideWithChartTwo(ppt);

        // 输出ppt文件
        ppt.write(new FileOutputStream("./doc/2.pptx"));

        //
        readPpt("./doc/2.pptx");

    }

    private static void createSlideWithChartTwo(XMLSlideShow ppt) {
        // 创建了一个幻灯片 这就是个空白的幻灯片
        XSLFSlide slide6 = ppt.createSlide();
        // 创建一个图表
        XSLFChart chart = ppt.createChart();
        // 创建一个工作簿
        XSSFWorkbook workbook = new XSSFWorkbook();
        // 写入数据
        XSSFSheet sheet = workbook.createSheet();
        XSSFRow row0 = sheet.createRow(0);
        row0.createCell(1).setCellValue("A");
        row0.createCell(2).setCellValue("B");
        for (int i = 0; i < 4; i++) {
            // 设置每一行的字段标题和数据
            XSSFRow row = sheet.createRow(i + 1);
            row.createCell(0).setCellValue((i + 1) + "队");
            row.createCell(1).setCellValue(3);
            row.createCell(2).setCellValue(4);
        }
        // 把工作簿放到图表里，这样可以方便文件更新
        chart.setWorkbook(workbook);
        // 图表头
        chart.setTitleText("测试文本title");
        // 这个是生成图表底部的示例的
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.BOTTOM);
        // x坐标轴 底部
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);
        // y轴 左侧
        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        // 创建图表数据，第一个指定是什么图表 柱状图或者饼图，折线图都ok，
        XDDFChartData data = chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
        // 底部类别的数据源，可以从数组读，也可以从指定一个excel范围
        XDDFCategoryDataSource xddfCategoryDataSource = XDDFDataSourcesFactory.fromStringCellRange(sheet, new CellRangeAddress(1, 4, 0, 0));
        // 这是第一个柱状图的数据源
        XDDFNumericalDataSource<Double> doubleXDDFNumericalDataSource = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(1, 4, 1, 1));
        // 这是第二个柱状图的数据源
        XDDFNumericalDataSource<Double> doubleXDDFNumericalDataSource2 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(1, 4, 2, 2));
        // 把第一组柱状图添加到图表数据里 返回一个系列数据
        XDDFChartData.Series series = data.addSeries(xddfCategoryDataSource, doubleXDDFNumericalDataSource);
        // 第二组
        XDDFChartData.Series series1 = data.addSeries(xddfCategoryDataSource, doubleXDDFNumericalDataSource2);
        // 设置第一个系列的名称 上面那个生成图表底部的示例的就是这里 ,第一个指定名称，第二个可以给一个单元格。两个参数可以有一个为null
        series.setTitle("A", new CellReference(sheet.getRow(0).getCell(1)));
        series1.setTitle("B", new CellReference(sheet.getRow(0).getCell(2)));
        // 数据源转为barchart
        XDDFBarChartData bar = (XDDFBarChartData) data;
        // 这一句是y轴的一个操作，也没懂什么意思，但是没有这个，画的图表会超出画布范围
        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);
        // 是否设置不同的颜色 false就行
        bar.setVaryColors(false);
        // 柱状图的方向
        bar.setBarDirection(BarDirection.COL);
        // 柱状图的类型
        bar.setBarGrouping(BarGrouping.STANDARD);
        // 可以设置间隙宽度
        // bar.setGapWidth(200);
        // 开始画图
        chart.plot(data);
        Rectangle2D.Double rect = new Rectangle2D.Double(700000, 500000, 7000000, 5000000);
        // 把柱状图加到幻灯片里，指定画布
        slide6.addChart(chart, rect);
    }

    private static void createSlideWithChart(XMLSlideShow ppt, String chartTitle, String[] series, String[] categories, Double[] values1, Double[] values2) {
        XSLFSlide slide = ppt.createSlide();
        XSLFChart chart = ppt.createChart();
        Rectangle2D rect2D = new java.awt.Rectangle(fromCM(1.5), fromCM(4), fromCM(22), fromCM(14));
        // 把柱状图加到幻灯片里，指定画布
        slide.addChart(chart, rect2D);
        setBarData(chart, chartTitle, series, categories, values1, values2);
    }

    private static void createSlideWithChartOne(XMLSlideShow ppt, String chartTitle, String[] series, String[] categories, Double[] values, int valuesColumn) {
        XSLFSlide slide = ppt.createSlide();
        XSLFChart chart = ppt.createChart();
        Rectangle2D rect2D = new java.awt.Rectangle(fromCM(1.5), fromCM(4), fromCM(22), fromCM(14));
        slide.addChart(chart, rect2D);
        setDoughnutData(chart, chartTitle, series, categories, values, valuesColumn);
    }
    private static void createSlideWithChartOne2(XSLFSlide slide,XSLFChart chart, String chartTitle, String[] series, String[] categories, Double[] values, int valuesColumn) {
        Rectangle2D rect2D = new java.awt.Rectangle(fromCM(1.5), fromCM(4), fromCM(22), fromCM(14));
        slide.addChart(chart, rect2D);
        setDoughnutData(chart, chartTitle, series, categories, values, valuesColumn);
    }

    private static int fromCM(double cm) {
        return (int) (Math.rint(cm * Units.EMU_PER_CENTIMETER));
    }

    private static void setBarData(XSLFChart chart, String chartTitle, String[] series, String[] categories, Double[] values1, Double[] values2) {

        XDDFChartAxis bottomAxis = chart.createCategoryAxis(AxisPosition.BOTTOM);

        bottomAxis.setTitle(series[2]);

        XDDFValueAxis leftAxis = chart.createValueAxis(AxisPosition.LEFT);
        leftAxis.setTitle(series[0] + "," + series[1]);
        leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);
        leftAxis.setMajorTickMark(AxisTickMark.OUT);
        leftAxis.setCrossBetween(AxisCrossBetween.BETWEEN);

        final int numOfPoints = categories.length;

        final String categoryDataRange = chart.formatRange(new CellRangeAddress(1, numOfPoints, COLUMN_LANGUAGES, COLUMN_LANGUAGES));

        final String valuesDataRange = chart.formatRange(new CellRangeAddress(1, numOfPoints, COLUMN_COUNTRIES, COLUMN_COUNTRIES));

        final String valuesDataRange2 = chart.formatRange(new CellRangeAddress(1, numOfPoints, COLUMN_SPEAKERS, COLUMN_SPEAKERS));

        // 底部类别的数据源
        final XDDFDataSource<?> categoriesData = XDDFDataSourcesFactory.fromArray(categories, categoryDataRange, COLUMN_LANGUAGES);

        // 这是第一个柱状图的数据源
        final XDDFNumericalDataSource<? extends Number> valuesData = XDDFDataSourcesFactory.fromArray(values1, valuesDataRange, COLUMN_COUNTRIES);
        valuesData.setFormatCode("General");

        values1[6] = 16.0;
        // 这是第二个柱状图的数据源
        final XDDFNumericalDataSource<? extends Number> valuesData2 = XDDFDataSourcesFactory.fromArray(values2, valuesDataRange2, COLUMN_SPEAKERS);
        valuesData2.setFormatCode("General");

        // 创建图表数据，第一个指定是什么图表 柱状图或者饼图，折线图都ok，
        XDDFBarChartData bar = (XDDFBarChartData) chart.createData(ChartTypes.BAR, bottomAxis, leftAxis);
        // 柱状图的类型
        bar.setBarGrouping(BarGrouping.CLUSTERED);

        // 把第一组柱状图添加到图表数据里 返回一个系列数据
        // 设置第一个系列的名称 上面那个生成图表底部的示例的就是这里 ,第一个指定名称，第二个可以给一个单元格。两个参数可以有一个为null
        XDDFBarChartData.Series series1 = (XDDFBarChartData.Series) bar.addSeries(categoriesData, valuesData);
        series1.setTitle(series[0], chart.setSheetTitle(series[COLUMN_COUNTRIES - 1], COLUMN_COUNTRIES));
        // 第二组
        XDDFBarChartData.Series series2 = (XDDFBarChartData.Series) bar.addSeries(categoriesData, valuesData2);
        series2.setTitle(series[1], chart.setSheetTitle(series[COLUMN_SPEAKERS - 1], COLUMN_SPEAKERS));
        // 是否设置不同的颜色 false就行
        bar.setVaryColors(true);
        // 柱状图的方向
        bar.setBarDirection(BarDirection.COL);
        // 可以设置间隙宽度
        // bar.setGapWidth(200);
        // 开始画图
        chart.plot(bar);

        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.LEFT);
        legend.setOverlay(false);

        chart.setTitleText(chartTitle);
        chart.setTitleOverlay(false);
        chart.setAutoTitleDeleted(false);
    }

    private static void setDoughnutData(XSLFChart chart, String chartTitle, String[] series, String[] categories, Double[] values, int valuesColumn) {
        final int numOfPoints = categories.length;
        final String categoryDataRange = chart.formatRange(new CellRangeAddress(1, numOfPoints, COLUMN_LANGUAGES, COLUMN_LANGUAGES));
        final String valuesDataRange = chart.formatRange(new CellRangeAddress(1, numOfPoints, valuesColumn, valuesColumn));
        final XDDFDataSource<?> categoriesData = XDDFDataSourcesFactory.fromArray(categories, categoryDataRange, COLUMN_LANGUAGES);
        final XDDFNumericalDataSource<? extends Number> valuesData = XDDFDataSourcesFactory.fromArray(values, valuesDataRange, valuesColumn);
        valuesData.setFormatCode("General");

        XDDFDoughnutChartData data = (XDDFDoughnutChartData) chart.createData(ChartTypes.DOUGHNUT, null, null);
        XDDFDoughnutChartData.Series series1 = (XDDFDoughnutChartData.Series) data.addSeries(categoriesData, valuesData);
        series1.setTitle(series[0], chart.setSheetTitle(series[valuesColumn - 1], valuesColumn));

        data.setVaryColors(true);
        // data.setHoleSize(42);
        // data.setFirstSliceAngle(90);
        chart.plot(data);

        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(LegendPosition.LEFT);
        legend.setOverlay(false);

        chart.setTitleText(chartTitle);
        chart.setTitleOverlay(false);
        chart.setAutoTitleDeleted(false);
    }

    private static void createTextList(XMLSlideShow ppt) {
        XSLFSlide slide = ppt.createSlide();
        XSLFTextBox shape = slide.createTextBox();
        shape.setAnchor(new Rectangle(50, 50, 400, 200));

        XSLFTextParagraph p1 = shape.addNewTextParagraph();
        p1.setIndentLevel(0);
        p1.setBullet(true);
        XSLFTextRun r1 = p1.addNewTextRun();
        r1.setText("r1");

        XSLFTextParagraph p2 = shape.addNewTextParagraph();
        // 文本前缩进
        p2.setLeftMargin(60d);
        p2.setIndent(-40d);
        p2.setBullet(true);
        // 自定义颜色
        p2.setBulletFontColor(Color.red);
        p2.setBulletFont("Wingdings");
        p2.setBulletCharacter("\u0075");
        p2.setIndentLevel(1);
        XSLFTextRun r2 = p2.addNewTextRun();
        r2.setText("Bullet2");

        XSLFTextParagraph p3 = shape.addNewTextParagraph();
        p3.setBulletAutoNumber(AutoNumberingScheme.alphaLcParenRight, 1);
        p3.setIndentLevel(2);
        XSLFTextRun r3 = p3.addNewTextRun();
        r3.setText("Numbered List Item - 1");

        XSLFTextParagraph p4 = shape.addNewTextParagraph();
        p4.setBulletAutoNumber(AutoNumberingScheme.alphaLcParenRight, 2);
        p4.setIndentLevel(2);
        XSLFTextRun r4 = p4.addNewTextRun();
        r4.setText("Numbered List Item - 2");

        XSLFTextParagraph p5 = shape.addNewTextParagraph();
        p5.setBulletAutoNumber(AutoNumberingScheme.alphaLcParenRight, 3);
        p5.setIndentLevel(2);
        XSLFTextRun r5 = p5.addNewTextRun();
        r5.setText("Numbered List Item - 3");

        shape.resizeToFitText();
    }

    public static void createTableOne(XMLSlideShow ppt) {
        XSLFSlide slide = ppt.createSlide();
        XSLFTable tbl = slide.createTable();
        tbl.setAnchor(new Rectangle(50, 50, 450, 300));
        int numColumns = 3;
        int numRows = 5;
        XSLFTableRow headerRow = tbl.addRow();
        headerRow.setHeight(50);
        // header
        for (int i = 0; i < numColumns; i++) {
            XSLFTableCell th = headerRow.addCell();
            XSLFTextParagraph p = th.addNewTextParagraph();
            p.setTextAlign(TextParagraph.TextAlign.CENTER);
            XSLFTextRun r = p.addNewTextRun();
            r.setText("Header " + (i + 1));
            r.setBold(true);
            r.setFontColor(Color.white);
            th.setFillColor(new Color(79, 129, 189));
            th.setBorderWidth(TableCell.BorderEdge.bottom, 2.0);
            th.setBorderColor(TableCell.BorderEdge.bottom, Color.white);

            tbl.setColumnWidth(i, 150); // all columns are equally sized
        }
        // rows
        for (int rownum = 0; rownum < numRows; rownum++) {
            XSLFTableRow tr = tbl.addRow();
            tr.setHeight(50);
            // header
            for (int i = 0; i < numColumns; i++) {
                XSLFTableCell cell = tr.addCell();
                XSLFTextParagraph p = cell.addNewTextParagraph();
                XSLFTextRun r = p.addNewTextRun();

                r.setText("Cell " + (i + 1));
                if (rownum % 2 == 0) {
                    cell.setFillColor(new Color(208, 216, 232));
                } else {
                    cell.setFillColor(new Color(233, 247, 244));
                }
            }
        }
    }

    private static void createTable(XMLSlideShow ppt) {
        XSLFSlide slide5 = ppt.createSlide();
        // 创建文本框
        XSLFTextBox textBox4 = slide5.createTextBox();
        // x y设置距离 w h 设置大小
        textBox4.setAnchor(new Rectangle2D.Double(300, 50, 100, 50));
        textBox4.addNewTextParagraph().addNewTextRun().setText("表格测试");
        // 渲染的基础数据
        Object[][] datas = {{"aaaaa", "", ""}, {"bb", "ccc", "dddd"}, {"A", 1, 2}, {"B", 3, 4}};
        // 创建表格
        XSLFTable table = slide5.createTable();
        // 定位 如果不通过setAnchor()方法指定坐标，则幻灯片中不会显示该文本元素。
        table.setAnchor(new Rectangle2D.Double(10, 50, 0, 0));
        for (int i = 0; i < datas.length; i++) {
            // 创建表格行
            XSLFTableRow tableRow = table.addRow();
            for (int j = 0; j < datas[i].length; j++) {
                // 创建表格单元格
                XSLFTableCell tableCell = tableRow.addCell();
                XSLFTextParagraph xslfTextRuns = tableCell.addNewTextParagraph();
                XSLFTextRun tr = xslfTextRuns.addNewTextRun();
                tr.setText(String.valueOf(datas[i][j]));

                tableCell.setFillColor(Color.getColor("0xdd7e6b"));
                xslfTextRuns.setTextAlign(TextParagraph.TextAlign.CENTER);
                tableCell.setVerticalAlignment(VerticalAlignment.MIDDLE);

                if (i == datas.length - 1 && j == 3 - 1) {
                    tr.setFontSize(16D);
                    tr.setBold(true);
                    tr.setItalic(true);
                    tr.setUnderlined(true);
                    tr.setFontFamily("\u5b8b\u4f53");
                    tr.setFontColor(Color.RED);
                }
                // 表格宽度 和颜色
                tableCell.setBorderWidth(TableCell.BorderEdge.bottom, 1);
                tableCell.setBorderWidth(TableCell.BorderEdge.left, 1);
                tableCell.setBorderWidth(TableCell.BorderEdge.top, 1);
                tableCell.setBorderWidth(TableCell.BorderEdge.right, 1);

                tableCell.setBorderColor(TableCell.BorderEdge.bottom, Color.black);
                tableCell.setBorderColor(TableCell.BorderEdge.left, Color.black);
                tableCell.setBorderColor(TableCell.BorderEdge.top, Color.black);
                tableCell.setBorderColor(TableCell.BorderEdge.right, Color.black);
            }
            // 每行高度
            tableRow.setHeight(30);
        }
        // 设置列宽
        table.setColumnWidth(0, 150);
        table.setColumnWidth(1, 150);
        table.setColumnWidth(2, 250);
        // 合并单元格
        table.mergeCells(0, 0, 0, 2);
    }

    /**
     * 合并 pdf
     *
     * @param files
     * @param outPaths
     * @throws Exception
     */
    public static void mergePptx(String[] files, String outPaths) throws Exception {
        XMLSlideShow ppt = new XMLSlideShow();
        for (String arg : files) {
            FileInputStream fileInputStream = new FileInputStream(arg);
            XMLSlideShow src = new XMLSlideShow(fileInputStream);
            for (XSLFSlide srcSlide : src.getSlides()) {
                ppt.createSlide().importContent(srcSlide);
            }
        }
        FileOutputStream out = new FileOutputStream(outPaths);
        ppt.write(out);
        System.out.println("Merging done successfully");
        out.close();
    }

    /**
     * ppt 转成图片
     *
     * @param filePath
     * @param times    生成图片放大的倍数,倍数越高，清晰度越高
     * @throws Exception
     */
    public static void pptToPng(String filePath, Integer times) throws Exception {
        File file = new File(filePath);
        XMLSlideShow ppt = new XMLSlideShow(new FileInputStream(file));

        Dimension pgsize = ppt.getPageSize();
        List<XSLFSlide> slideList = ppt.getSlides();
        for (XSLFSlide xslfShapes : slideList) {
            // 解决乱码问题
            XSLFShape[] shapes = xslfShapes.getShapes().toArray(new XSLFShape[0]);
            for (XSLFShape shape : shapes) {
                if (shape instanceof XSLFTextShape) {
                    XSLFTextShape sh = (XSLFTextShape) shape;
                    List<XSLFTextParagraph> textParagraphs = sh.getTextParagraphs();
                    for (XSLFTextParagraph xslfTextParagraph : textParagraphs) {
                        List<XSLFTextRun> textRuns = xslfTextParagraph.getTextRuns();
                        for (XSLFTextRun xslfTextRun : textRuns) {
                            xslfTextRun.setFontFamily("宋体");
                        }
                    }
                }
            }
            // 创建BufferedImage对象，图像的尺寸为原来的每页的尺寸*倍数times
            BufferedImage img = new BufferedImage(pgsize.width * times, pgsize.height * times, BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics = img.createGraphics();
            graphics.setPaint(Color.white);
            graphics.scale(times, times);// 将图片放大times倍
            graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width, pgsize.height));
            // 最核心的代码
            xslfShapes.draw(graphics);
            // 这里设置图片的存放路径和图片的格式(jpeg,png,bmp等等),注意生成文件路径
            FileOutputStream out = new FileOutputStream("./doc/ppt_image_" + System.currentTimeMillis() + ".png");
            // 写入到图片中去
            ImageIO.write(img, "png", out);
            out.close();
        }
        System.out.println("Image successfully created");
    }

    /**
     * 读取ppt
     */
    public static void readPpt(String filePath) {
        File file = new File(filePath);
        StringBuffer content = new StringBuffer();
        try {
            InputStream is = new FileInputStream(file);
            XMLSlideShow xmlSlideShow = new XMLSlideShow(is);
            // 获取ppt中所有幻灯片
            List<XSLFSlide> slides = xmlSlideShow.getSlides();
            for (XSLFSlide slide : slides) {
                List<XSLFShape> shapes = slide.getShapes();
                for (XSLFShape shape : shapes) {
                    System.out.println(shape.getShapeName());
                }
            }
            // 获取所有的图表
            List<XSLFChart> charts = xmlSlideShow.getCharts();
            for (XSLFChart chart : charts) {
                // 获取图表标题
                String text = chart.getTitleShape().getText();
                System.out.println(text);
            }
            xmlSlideShow.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(content.toString());
    }

}
