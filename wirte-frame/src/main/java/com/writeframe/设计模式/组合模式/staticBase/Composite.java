package com.writeframe.设计模式.组合模式.staticBase;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Composite
 * @Description 抽象组合，透明实现，实现的父类的空方法,树节点
 *
 * B.树枝节点，抽象组件实现
 */
public class Composite  extends Component{
    List<Component> mComponents;
    public Composite(String name) {
        super(name);
        mComponents = new ArrayList<Component>();
    }

    @Override
    public String operation() {
        StringBuilder builder = new StringBuilder(this.name);
        for(Component component: this.mComponents){
            builder.append("\n");
            builder.append(component.operation());
        }
        return builder.toString();
    }
    @Override
    public boolean addChild(Component component) {
        return this.mComponents.add(component);
    }

    @Override
    public boolean removeChild(Component component) {
        return this.mComponents.remove(component);
    }

    @Override
    public Component getChild(int index) {
        return this.mComponents.get(index);
    }
}
