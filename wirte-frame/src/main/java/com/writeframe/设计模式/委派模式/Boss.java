package com.writeframe.设计模式.委派模式;

/**
 * @ClassName Boss
 * @Description 老板
 *
 * 委托模式：
 *      基本作用就是负责任务的调度和任务分配，将任务的分配和执行分离开来。可以看成是一种特殊情况下的静态代理的全权代理。
 *使用场景
 * 　　1.委派对象本身不知道如何处理一个任务（或请求），把请求交给其他对象来处理。
 * 　　2.实现程序的解耦。
 * 委派模式在生活中应用
 * 　　1.老板给员工下达任务，安排工作
 * 　　2.授权委托书
 *适用场景：
 *  在老板，和员工的关系中，老板一般是向各部门主管下达任务，再由主管向下面的员工安排工作。
 *
 * 优点：
 * 　　通过任务委派，能够将一个大型的任务细化，然后通过统一管理这些子任务的完成情况实现任务的跟进，能够
 * 加快任务执行的效率。
 * 缺点：
 * 　　委派模式，需要根据任务的复杂程序进行不同程度的改变，在任务比较复杂的情况下可能需要进行多重委派，容易生成紊乱。
 */
public class Boss {
    /**
     * 直接给主管分配任务
     * @param task
     * @param leader
     */
    public void command(String task, Leader leader) {
        leader.doWork(task);
    }

    public static void main(String[] args) {
        new Boss().command("海报图", new Leader());
        new Boss().command("爬虫", new Leader());
        new Boss().command("卖手机", new Leader());
    }
}
