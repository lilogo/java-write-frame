package com.writeframe.设计模式.建造者模式.demo;

import com.writeframe.设计模式.建造者模式.IBuilder;

/**
 * 建造者模式：对象构建，步骤可定制。
 * 适用场景
 * 　　》创建对象需要进行很多步设置，但每一步之间顺序不固定【与顺序无关】
 * 　　》如果一个对象有非常复杂的内部结构【很多属性】
 * 　　》把复杂对象的创建和使用分离。
 */
public class ConcreteBuilder implements IBuilder<Product> {
    private Product product = new Product();

    @Override
    public Product build() {
        return product;
    }

    public static void main(String[] args) {
        IBuilder builder = new ConcreteBuilder();
        System.out.println(builder.build());
    }
}
