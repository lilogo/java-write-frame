package com.lean.flowable.event.sign;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 信号事件
 */
public class SignTest extends BaseConfiguation {
    // 1.开始事件

    /**
     * Deploy
     */
    @Test
    void testDeploy() throws Exception {
        Deployment deploy = repositoryService.createDeployment().addClasspathResource("/process/sign/信号启动事件.bpmn20.xml").name("信号启动事件").deploy();
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
        System.out.println("部署开始的时间：" + new Date());
        // TimeUnit.MINUTES.sleep(3);
    }
    // 流程是一个信息启动事件，所以我们需要释放对应的信号来触发流程的启动

    /**
     * 通过信号发送来触发信号启动事件的执行
     * 全局的信息
     */
    @Test
    void signalReceived() throws Exception {
        runtimeService.signalEventReceived("firstSignal");
        // 我们得保证容器的运行，所以需要阻塞
        TimeUnit.MINUTES.sleep(1);
    }

    //可以把信息的作用域由原来的golbal全局的调整为processInstance，测试后发现还是执行了，说明在启动事件信息的作用域其实是不起作用的。
    //<signal id="signal01" name="firstSignal" flowable:scope="processInstance"></signal>


    //2.中间捕获事件

    //案例如下：当我们启动事件后，会阻塞在这个消息获取中间事件处，等待相关信号后才会继续流转。

    /**
     * Deploy
     */
    @Test
    void test01() throws Exception {
        //RepositoryService repositoryService = processEngine.getRepositoryService();
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("/process/sign/信号中间捕获事件.bpmn20.xml")
                .name("信号中间捕获事件")
                .deploy();
        System.out.println("deploy.getId() = " + deploy.getId());
        System.out.println("deploy.getName() = " + deploy.getName());
        System.out.println("部署开始的时间：" + new Date());
        //TimeUnit.MINUTES.sleep(3);
    }

    @Test
    void start01() throws Exception{
        runtimeService.startProcessInstanceById("event2002:1:adc5b8f8-afcf-11ec-959a-c03c59ad2248");
        System.out.println("启动时间：" + new Date());
    }

    /**
     * 通过信号发送来触发信号启动事件的执行
     * 全局的信息
     */
    @Test
    void signalGolbal() throws Exception {
        runtimeService.signalEventReceived("secondSingal");
        // 我们得保证容器的运行，所以需要阻塞
        TimeUnit.MINUTES.sleep(1);
    }

    //可以把信息的作用域由原来的golbal全局的调整为processInstance，测试后发现不执行了，
    // 说明发送global信号是不会被捕获的。则说明processInstance的信息我们需要在流程实例内部抛出信号。
    //<signal id="singnal02" name="secondSingal" flowable:scope="processInstance"></signal>

    //3.中间抛出事件

    //信号中间抛出事件也就是在流程执行中的某个节点抛出了对应的信号，然后对应的信号中间捕获事件就会触发

    @Test
    public void test02() throws Exception{

        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/sign/信号中间抛出事件.bpmn20.xml")
                .name("信号中间抛出事件")
                .deploy();
        System.out.println("-----");
    }

    /**
     * 启动流程实例
     *
     */
    @Test
    public void startProcessInstanceByKey()  throws Exception{
        processEngine.getRuntimeService()
                .startProcessInstanceById("event2003:1:665b1533-b020-11ec-877d-c03c59ad2248");
        System.out.println("开始启动的时间：" + LocalDateTime.now().toString());
        // 需要在此阻塞比等待长的时间
        TimeUnit.MINUTES.sleep(3);
    }

    //4.边界事件
    //定义的信号为：
    //<signal id="signal2" name="signal2" flowable:scope="global"></signal>

    @Test
    public void test03() throws Exception{

        Deployment deployment = processEngine.getRepositoryService().createDeployment()
                .addClasspathResource("/process/sign/信号边界事件.bpmn20.xml")
                .name("信号边界事件")
                .deploy();
        System.out.println("-----");
    }

    @Test
    public void start03()  throws Exception{

        processEngine.getRuntimeService()
                .startProcessInstanceById("event2004:1:e8b5c39f-b024-11ec-bdac-c03c59ad2248");
        System.out.println("开始启动的时间：" + LocalDateTime.now().toString());
        // 需要在此阻塞比等待长的时间
        TimeUnit.MINUTES.sleep(3);
    }

    //
    @Test
    public void signalGlobal03()  throws Exception{
        String signal = "signal2";
        Map<String, Object> variables = new HashMap();
        processEngine.getRuntimeService().signalEventReceived(signal,variables);
    }

}
