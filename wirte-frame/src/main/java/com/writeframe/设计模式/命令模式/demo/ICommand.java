package com.writeframe.设计模式.命令模式.demo;

/**
 * @ClassName ICommand
 * @Description 命令顶层接口
 */
public interface ICommand {
    void execute();
}
