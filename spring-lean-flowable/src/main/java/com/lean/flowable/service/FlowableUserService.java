package com.lean.flowable.service;

import com.lean.flowable.base.ResponseResult;
import com.lean.flowable.entity.FlowGroupModel;
import com.lean.flowable.entity.FlowUserModel;
import org.apache.commons.lang3.StringUtils;
import org.flowable.common.engine.api.management.TableMetaData;
import org.flowable.idm.api.Group;
import org.flowable.idm.api.GroupQuery;
import org.flowable.idm.api.User;
import org.flowable.idm.api.UserQuery;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntity;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户相关表（4个，IdentityService接口操作的表）
 * act_id_group：用户组信息表，对应节点选定候选组信息；
 * act_id_info：用户扩展信息表，存储用户扩展信息；
 * act_id_membership：用户与用户组关系表；
 * act_id_user：用户信息表，对应节点选定办理人或候选人信息；
 *
 * 用于身份信息获取和保存，这里主要是获取身份信息。 identityService.createUserQuery().userId(userId).singleResult(); 获取审批用户的具体信息
 * identityService.createGroupQuery().groupId(groupId).singleResult(); 获取审批组的具体信息
 *
 * 添加系统本地用户的时候，也往 flowable 中添加/更新用户。
 *
 */
@Service
public class FlowableUserService extends BaseService {

    private static final Logger log = LoggerFactory.getLogger(FlowableUserService.class);

    /**
     * 查看用户表详情
     *
     * @return
     */
    public ResponseResult getTableBaseData() {
        // 获取系统信息，实际上是读取 ACT_ID_PROPERTY 表的信息
        Map<String, String> properties = idmManagementService.getProperties();
        log.info("properties = " + properties);
        // 获取表的详细信息
        TableMetaData tableMetaData = idmManagementService.getTableMetaData(idmManagementService.getTableName(User.class));
        // 获取表名
        log.info("tableMetaData.getTableName() = " + tableMetaData.getTableName());
        // 获取列名
        log.info("tableMetaData.getColumnNames() = " + tableMetaData.getColumnNames());
        // 获取列的类型
        log.info("tableMetaData.getColumnTypes() = " + tableMetaData.getColumnTypes());
        return ResponseResult.success(tableMetaData);
    }


    /**
     * 添加用户
     *
     * @return
     */
    public ResponseResult insertUser(FlowUserModel flowUserModel) {
        User userInfo = identityService.createUserQuery().userId(flowUserModel.getId()).singleResult();
        if (userInfo != null) {
            return ResponseResult.fail("用户已存在");
        }
        UserEntityImpl user = new UserEntityImpl();
        BeanUtils.copyProperties(flowUserModel,user);
        user.setRevision(0);
        identityService.saveUser(user);
        return ResponseResult.success("ok");
    }

    /**
     * 修改用户密码 可以修改其他用户属性
     *
     * @return
     */
    public ResponseResult updateUserPassword(String userId, String password) {
        User user = identityService.createUserQuery().userId(userId).singleResult();
        if (user == null) {
            return ResponseResult.fail("用户不存在");
        }
        user.setPassword(password);
        identityService.updateUserPassword(user);
        return ResponseResult.success("ok");
    }

    /**
     * 删除用户
     *
     * @return
     */
    public ResponseResult deleteUser(String userId) {
        User user = identityService.createUserQuery().userId(userId).singleResult();
        if (user == null) {
            return ResponseResult.fail("用户不存在");
        }
        identityService.deleteUser(userId);
        return ResponseResult.success("ok");
    }

    /**
     * 查询用户基本信息
     *
     * @return
     */
    public ResponseResult getUserDetail(String userId) {
        User user = identityService.createUserQuery().userId(userId).singleResult();
        if (user == null) {
            return ResponseResult.fail("用户不存在");
        }
        FlowUserModel flowUserModel = new FlowUserModel();
        BeanUtils.copyProperties(user, flowUserModel);
        return ResponseResult.success(flowUserModel);
    }

    /**
     * 查询用户列表
     *
     * @return
     */
    public ResponseResult findUserList(String firstName, String lastName, String displayName, Integer pageNum, Integer pageSize) {
        Map<String, Object> resultMap = new HashMap<>();
        UserQuery userQuery = identityService.createUserQuery();
        if (StringUtils.isNotEmpty(firstName)) {
            userQuery.userFirstNameLike(firstName);
        }
        if (StringUtils.isNotEmpty(lastName)) {
            userQuery.userLastNameLike(lastName);
        }
        if (StringUtils.isNotEmpty(displayName)) {
            userQuery.userDisplayNameLike(displayName);
        }
        if (StringUtils.isNotEmpty(displayName)) {
            userQuery.userDisplayNameLike(displayName);
        }
        resultMap.put("total", userQuery.count());
        List<User> list = null;
        List<FlowUserModel> flowUserModelList = new ArrayList<>();
        if (pageNum == null || pageSize == null) {
            list = userQuery.orderByUserId().asc().list();
        } else {
            list = userQuery.orderByUserId().asc().listPage(pageSize * (1 - pageNum), pageSize);
        }
        if (!CollectionUtils.isEmpty(list)) {
            list.stream().forEach(user -> {
                FlowUserModel flowUserModel = new FlowUserModel();
                BeanUtils.copyProperties(user, flowUserModel);
                flowUserModelList.add(flowUserModel);
            });
        }
        resultMap.put("dataList", flowUserModelList);
        return ResponseResult.success(resultMap);
    }

    /**
     * 添加组
     *
     * @return
     */
    public ResponseResult insertGroup(String groupId, String groupName) {
        GroupEntityImpl g = new GroupEntityImpl();
        g.setName(groupName);
        g.setId(groupId);
        g.setRevision(0);
        identityService.saveGroup(g);
        return ResponseResult.success("ok");
    }

    /**
     * 组添加用户
     *
     * @return
     */
    public ResponseResult addGroupUser(String groupId, String userId) {
        identityService.createMembership(userId, groupId);
        return ResponseResult.success("ok");
    }

    /**
     * 删除组中的成员
     *
     * @return
     */
    public ResponseResult deleteMembership(String groupId, String userId) {
        identityService.deleteMembership(userId, groupId);
        return ResponseResult.success("ok");
    }

    /**
     * 删除组
     *
     * @return
     */
    public ResponseResult deleteGroup(String groupId) {
        Group group = identityService.createGroupQuery().groupId(groupId).singleResult();
        if (group == null) {
            return ResponseResult.fail("组不存在");
        }
        identityService.deleteGroup(groupId);
        return ResponseResult.success("ok");
    }

    /**
     * 根据 id 查询组信息
     *
     * @return
     */
    public ResponseResult getGroupDetailById(String groupId) {
        Group group = identityService.createGroupQuery().groupId(groupId).singleResult();
        if (group == null) {
            return ResponseResult.fail("组不存在");
        }
        FlowGroupModel flowGroupModel = new FlowGroupModel();
        BeanUtils.copyProperties(group, flowGroupModel);
        return ResponseResult.success(flowGroupModel);
    }

    /**
     * 根据 name 查询组信息
     *
     * @return
     */
    public ResponseResult getGroupDetailByGroupName(String groupName) {
        Group group = identityService.createGroupQuery().groupName(groupName).singleResult();
        if (group == null) {
            return ResponseResult.fail("组不存在");
        }
        FlowGroupModel flowGroupModel = new FlowGroupModel();
        BeanUtils.copyProperties(group, flowGroupModel);
        return ResponseResult.success(flowGroupModel);
    }

    /**
     * 根据 成员 查询组信息
     *
     * @return
     */
    public ResponseResult getGroupListByUserName(String userName, Integer pageNum, Integer pageSize) {
        GroupQuery groupQuery = identityService.createGroupQuery();
        List<Group> list = null;
        List<FlowGroupModel> flowGroupModelList = new ArrayList<>();
        if (pageNum == null || pageSize == null) {
            list = groupQuery.groupMember(userName).list();
        } else {
            list = groupQuery.groupMember(userName).listPage(pageSize * (1 - pageNum), pageSize);
        }
        if (CollectionUtils.isEmpty(list)) {
            return ResponseResult.fail("组不存在");
        }
        list.stream().forEach(group -> {
            FlowGroupModel flowGroupModel = new FlowGroupModel();
            BeanUtils.copyProperties(group, flowGroupModel);
            flowGroupModelList.add(flowGroupModel);
        });
        return ResponseResult.success(flowGroupModelList);
    }


}
