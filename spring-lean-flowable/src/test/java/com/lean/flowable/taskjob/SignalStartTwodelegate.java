package com.lean.flowable.taskjob;

import java.time.LocalDateTime;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class SignalStartTwodelegate implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) {
        System.out.println("-------触发了-222-------->"+ LocalDateTime.now().toString());
    }
}
