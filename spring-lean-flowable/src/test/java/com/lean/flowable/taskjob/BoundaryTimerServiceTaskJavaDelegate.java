package com.lean.flowable.taskjob;

import org.flowable.bpmn.model.FlowElement;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;

public class BoundaryTimerServiceTaskJavaDelegate implements JavaDelegate {

	public void execute(DelegateExecution execution) {

		System.out.println("＝＝＝＝＝＝＝＝＝＝＝定时边界事件触发＝＝＝＝＝＝＝＝＝＝＝");

		FlowElement flowElement = execution.getCurrentFlowElement();
		System.out.println("当前节点: " + flowElement.getName());

	}

}
