package com.writeframe.设计模式.委派模式;

/**
 * @ClassName IEmployee
 * @Description 员工顶层接口
 */
public interface IEmployee {
    void doWork(String task);
}
