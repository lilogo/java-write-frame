package com.rocketmq.service.consumer;


import com.alibaba.fastjson.JSONObject;
import com.rocketmq.service.tx.UserModel;
import com.rocketmq.service.tx.UserService;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 消费事务消息的消费者
 */
@Service
@RocketMQMessageListener(consumerGroup = "customer-group-tx",topic = "topic-tx",selectorExpression = "transTag",messageModel = MessageModel.CLUSTERING)
public class RocketMQTransationListener implements RocketMQListener<UserModel> {

    private static final Logger log= LoggerFactory.getLogger("RocketMQTransationListener");

    @Autowired
    private UserService userService;


    @Override
    public void onMessage(UserModel userModel) {
        // 一般真实环境这里消费前，得做幂等性判断，防止重复消费
        // 方法一：如果你的业务中有某个字段是唯一的，有标识性，如订单号，那就可以用此字段来判断
        // 方法二：新建一张消费记录表t_mq_consumer_log，字段consumer_key是唯一性，能插入则表明该消息还未消费，往下走，否则停止消费
        // 建议用方法二，根据你的项目业务来定义key，这里我就不做幂等判断了，因为此案例只是模拟，重在分布式事务

        // 给用户增加积分
        int i = userService.addNumber(userModel.getUserId(), userModel.getChargeAmount());
        if (1 == i) {
            log.info("【MQ消费】用户增加积分成功，userCharge={}", JSONObject.toJSONString(userModel));
        } else {
            log.error("【MQ消费】用户充值增加积分消费失败，userCharge={}", JSONObject.toJSONString(userModel));
        }
    }
}
