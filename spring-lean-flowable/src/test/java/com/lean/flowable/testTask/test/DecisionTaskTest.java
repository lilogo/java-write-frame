package com.lean.flowable.testTask.test;

import com.lean.flowable.config.BaseConfiguation;
import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 决策任务
 * act_dmn_decision_table
 * act_dmn_deployment
 * act_dmn_deployment_resource
 * act_dmn_hi_decision_execution
 */
public class DecisionTaskTest extends BaseConfiguation {

    /**
     * 部署
     */
    @Test
    public void deploy() {
        DeploymentBuilder deploymentBuilder =
            repositoryService.createDeployment().category("juece-key").name("juece-key")
                    .addClasspathResource("process/task/决策任务.bpmn20.xml")
                    // 外置决策表
                    .addClasspathResource("process/task/请假决策表.dmn");
        Deployment deploy = deploymentBuilder.deploy();

        System.out.println("流程ID: " + deploy.getId());
    }

    /**
     * 启动流程实例
     *
     */
    @Test
    public void start() {
        String processDefinitionKey = "juece-key";
        Map<String, Object> variables = new HashMap<String, Object>();
        // 决策表中的变量
        variables.put("days", 12);
        runtimeService.startProcessInstanceByKey(processDefinitionKey, variables);
    }

    /**
     * 完成任务
     */
    @Test
    public void complete() {
        String taskId = "2510";
        taskService.complete(taskId);
    }

}
