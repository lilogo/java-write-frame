package com.lean.influxdb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanInfluxdbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanInfluxdbApplication.class, args);
    }

}
