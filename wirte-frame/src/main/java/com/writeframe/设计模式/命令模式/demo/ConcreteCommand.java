package com.writeframe.设计模式.命令模式.demo;

/**
 * @ClassName ConcreteCommand
 * @Description 具体命令实现
 */
public class ConcreteCommand implements ICommand {
    private Receiver receiver = new Receiver();


    @Override
    public void execute() {
        receiver.action();
    }
}
