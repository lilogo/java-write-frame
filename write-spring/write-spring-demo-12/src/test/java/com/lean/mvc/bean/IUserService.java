package com.lean.mvc.test.bean;

public interface IUserService {

    String queryUserInfo();

    String register(String userName);
}
