package com.lean.springsecurity.core.social.support;

import lombok.Data;
import lombok.ToString;

/**
 *
 * comment: 第三方用户信息的实体
 */
@Data
@ToString
public class SocialUserInfo {

    private String providerId;

    private String providerUserId;

    private String nickName;

    private String headImg;

}
