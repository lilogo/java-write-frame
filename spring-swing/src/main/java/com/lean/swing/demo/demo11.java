package com.lean.swing.demo;

import javax.swing.JOptionPane;

/**
 * swing对话框JOptionPane
 */
public class demo11 {
    public demo11() {
        // TODO Auto-generated constructor stub
        JOptionPane.showMessageDialog(null, "用户名或密码错误！", "错误 ", 0);
//        JOptionPane.showMessageDialog(null, "请注册或登录...", "提示", 1);
//        JOptionPane.showMessageDialog(null, "普通会员无权执行删除操作！", "警告", 2);
//        JOptionPane.showMessageDialog(null, "你是哪一位？请输入用户名", "问题", 3);
//        JOptionPane.showMessageDialog(null, "扫描完毕，没有发现病毒！", "提示", JOptionPane.PLAIN_MESSAGE);
    }

    public static void main(String[] args) {
        new demo11();
    }
}
