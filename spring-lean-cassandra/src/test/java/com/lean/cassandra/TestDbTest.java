package com.lean.cassandra;


import com.alibaba.fastjson.JSON;
import com.lean.cassandra.entity.TestDb;
import com.lean.cassandra.service.TestDbService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
@RunWith(SpringRunner.class)
@SpringBootTest
public class TestDbTest {

    @Autowired
    private TestDbService testDbService;

    @Test
    public void save() {
        for (int i = 0; i < 100; i++) {
            TestDb testDb=new TestDb();
            if(i%2==0){
                testDb.setKeyOne("1");
            }else {
                testDb.setKeyOne("2");
            }
            testDb.setKeyTwo((i+2)+"");
            testDb.setValue(1.0D);
            testDbService.save(testDb);
        }
    }

    @Test
    public void query() {
        TestDb testDb=new TestDb();
        testDb.setKeyOne("1");
        List<TestDb> query = testDbService.query(testDb);
        System.out.println(JSON.toJSONString(query));
    }

    @Test
    public void delete() {
        TestDb testDb=new TestDb();
        testDb.setKeyOne("1");
        testDb.setKeyTwo("2");
        testDbService.delete(testDb);
    }
}
