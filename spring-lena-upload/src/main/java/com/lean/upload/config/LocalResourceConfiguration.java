package com.lean.upload.config;

import javax.annotation.Resource;

import com.lean.upload.enums.StorageTypeEnum;
import com.lean.upload.properties.LocalStorageProperties;
import com.lean.upload.properties.StorageProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * 本地资源映射配置
 *
 * @author 
 */
@Configuration
@ConditionalOnProperty(prefix = "upload", value = "enabled")
public class LocalResourceConfiguration implements WebMvcConfigurer {
    @Resource
    private StorageProperties properties;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 如果不是本地存储，则返回
        if(properties.getConfig().getType() != StorageTypeEnum.LOCAL){
            return;
        }
        LocalStorageProperties local = properties.getLocal();
        registry.addResourceHandler("/" + local.getUrl() + "/**")
                .addResourceLocations("file:" + local.getPath() + "/");
    }
}