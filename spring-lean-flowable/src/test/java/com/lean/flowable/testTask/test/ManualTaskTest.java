package com.lean.flowable.testTask.test;

import java.util.HashMap;
import java.util.Map;

import org.flowable.engine.repository.Deployment;
import org.flowable.engine.repository.DeploymentBuilder;
import org.junit.Test;

import com.lean.flowable.config.BaseConfiguation;


/**
 *手动任务--测试
 *手动任务将按**直接穿过活动**处理，在流程执行到达手动任务时，自动继续执行流程。
 */
public class ManualTaskTest extends BaseConfiguation{

	/**
	 * 部署
	 */
	@Test
	public void deploy() {
		DeploymentBuilder deploymentBuilder = repositoryService
												.createDeployment()
													.category("manualtaskprocess")
													.name("manualtaskprocess")
													.addClasspathResource("process/task/手动任务.bpmn20.xml");
		Deployment deploy = deploymentBuilder.deploy();

		System.out.println("流程ID: " + deploy.getId());
	}
	/**
	 * 启动流程实例
	 *
	 */
	@Test
	public void start() {
		String processDefinitionKey = "manualtaskprocess";
		Map<String, Object> variables = new HashMap<String, Object>();
		runtimeService.startProcessInstanceByKey(processDefinitionKey,variables);
	}

	/**
	 * 完成任务
	 */
	@Test
	public void complete() {
		String taskId = "167506";
		taskService.complete(taskId);
	}
}
