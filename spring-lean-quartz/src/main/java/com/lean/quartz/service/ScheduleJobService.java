package com.lean.quartz.service;

import java.util.List;

import com.lean.quartz.base.PageResult;
import com.lean.quartz.entity.ScheduleJobEntity;
import com.lean.quartz.query.ScheduleJobQuery;
import com.lean.quartz.vo.ScheduleJobVO;

/**
 * 定时任务
 *
 * @author
 */
public interface ScheduleJobService extends BaseService<ScheduleJobEntity> {

    PageResult<ScheduleJobVO> page(ScheduleJobQuery query);

    void save(ScheduleJobVO vo);

    void update(ScheduleJobVO vo);

    void delete(List<Long> idList);

    void run(String id);

    void changeStatus(String id,Integer status);
}