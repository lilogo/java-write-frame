package com.lean.auth.service;

import com.lean.auth.page.PageResult;
import com.lean.auth.entity.SysUserEntity;
import com.lean.auth.query.SysRoleUserQuery;
import com.lean.auth.query.SysUserQuery;
import com.lean.auth.vo.SysUserVO;

import java.util.List;

/**
 * 用户管理
 *
 * 
 */
public interface SysUserService extends BaseService<SysUserEntity> {

    PageResult<SysUserVO> page(SysUserQuery query);

    void save(SysUserVO vo);

    void update(SysUserVO vo);

    void delete(List<Long> idList);

    SysUserVO getByMobile(String mobile);

    /**
     * 修改密码
     *
     * @param id          用户ID
     * @param newPassword 新密码
     */
    void updatePassword(Long id, String newPassword);

    /**
     * 分配角色，用户列表
     */
    PageResult<SysUserVO> roleUserPage(SysRoleUserQuery query);

}
