package com.rocketmq.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanRocketmqServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanRocketmqServiceApplication.class, args);
    }

}
