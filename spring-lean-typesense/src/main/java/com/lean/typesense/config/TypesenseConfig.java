package com.lean.typesense.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.typesense.api.Client;
import org.typesense.resources.Node;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class TypesenseConfig {

    @Bean
    public Client client() {
        List<Node> nodes = new ArrayList<>();
        nodes.add(new Node("https", "xxxxxxx.net", "443"));
        org.typesense.api.Configuration configuration = new org.typesense.api.Configuration(nodes, Duration.ofSeconds(3), "Qum8OjyuRZsikLXUr6QoNjnQLQBeyQbq");
        return new Client(configuration);
    }
}
