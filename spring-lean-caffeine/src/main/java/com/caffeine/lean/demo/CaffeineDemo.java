package com.caffeine.lean.demo;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
/**
 * Caffeine 手动加载
 */
public class CaffeineDemo {

    public static void main(String[] args) throws InterruptedException {
        Cache<String, String> cache = Caffeine.newBuilder()
                // 基于时间失效,写入之后开始计时失效
                .expireAfterWrite(2000, TimeUnit.MILLISECONDS)
                // 缓存容量
                .maximumSize(5)
                .build();

        // 使用java8 Lambda表达式声明一个方法,get不到缓存中的值调用这个方法运算、缓存、返回
        String value = cache.get("key", key -> key + "_" + System.currentTimeMillis());
        System.out.println(value);

        //让缓存到期
        Thread.sleep(2001);
        // 存在就取,不存在就返回空
        System.out.println(cache.getIfPresent("key"));
        // 重新存值
        cache.put("key", "value");
        String key = cache.get("key", keyOne -> keyOne + "_" + System.currentTimeMillis());
        System.out.println(key);
        // 获取所有值打印出来
        ConcurrentMap<String, String> concurrentMap = cache.asMap();
        System.out.println(concurrentMap);
        // 删除key
        cache.invalidate("key");
        // 获取所有值打印出来
        System.out.println(cache.asMap());
    }
}