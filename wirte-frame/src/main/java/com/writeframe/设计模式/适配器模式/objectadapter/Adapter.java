package com.writeframe.设计模式.适配器模式.objectadapter;

/**
 * @ClassName Adapter
 * @Description 对象适配器, 组合引用原角色，也得实现目标对象的功能
 */
public class Adapter implements Target {
    //组合引用
    private Adaptee adaptee;

    public Adapter(Adaptee adaptee) {
        this.adaptee = adaptee;
    }

    @Override
    public int request() {
        return adaptee.specificRequest() / 10;
    }

    /**
     * 说明：
     * 　　客户调用时，使用多态的方式。如此，就只会出现接口的功能，而不会出现原角色的功能调用。
     * @param args
     */
    public static void main(String[] args) {
        Target adapter = new Adapter(new Adaptee());
        int result = adapter.request();
        System.out.println("最终结果：" + result);
    }
}
