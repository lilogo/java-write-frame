package com.write.mybatis.base;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.util.Map;

public class MapperProxyFactory implements InvocationHandler {

    private Connection connection;
    private Map<String, Mapper> mappers;

    public MapperProxyFactory(Connection connection, Map<String, Mapper> mappers) {
        this.connection = connection;
        this.mappers = mappers;
    }

    /**
     * Map 中获取 Value（Mapper),使用工具类 Executor 的 selectList 方法
     *
     * @param proxy
     * @param method
     * @param args
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        String className = method.getDeclaringClass().getName();
        // 拼接key
        String key = className + "." + methodName;
        // ，获取对应mapper
        Mapper mapper = mappers.get(key);
        // 如果mapper为空
        if (mapper == null) {
            throw new IllegalArgumentException(key + "is not exit");
        }
        Executor executor = new Executor();
        // 调用executor的selectList获取所有方法
        if ("insert".equals(mapper.getType())) {
            return executor.insert(mapper, connection, args);
        } else if ("delete".equals(mapper.getType())) {
            return executor.delete(mapper, connection, args);
        } else if ("update".equals(mapper.getType())) {
            return executor.update(mapper, connection, args);
        } else if ("select".equals(mapper.getType())) {
            return executor.selectList(mapper, connection, args);
        }
        return null;
    }
}
