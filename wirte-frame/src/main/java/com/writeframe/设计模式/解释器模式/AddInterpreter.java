package com.writeframe.设计模式.解释器模式;


/**
 * @ClassName AddInterpreter
 * @Description +运算符解释器
 */
public class AddInterpreter extends Interpreter{
    public AddInterpreter(IArithmeticInterpreter left, IArithmeticInterpreter right) {
        super(left, right);
    }

    @Override
    public int interpret() {
        return this.left.interpret() + this.right.interpret();
    }
}




