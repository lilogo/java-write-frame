package com.lean.sms.properties;

import lombok.Data;

@Data
public class AliyunSmsProperties {
    /**
     * 短信签名
     */
    private String signName;
    /**
     * 短信模板
     */
    private String templateId;
}
