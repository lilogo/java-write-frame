package com.lean.flowable;

import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.ProcessDefinition;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class SpringLeanFlowableApplicationTests {

    @Test
    void contextLoads() {}

    @Autowired
    private RepositoryService repositoryService;

    /**
     * 查询流程定义列表, 涉及到 act_re_procdef表，部署成功会新增记录
     */
    @Test
    public void testProcessDefinition() {
        List<ProcessDefinition> processList = repositoryService.createProcessDefinitionQuery().list();
        for (ProcessDefinition processDefinition : processList) {
            System.out.println(processDefinition.getName() + "===========" + processDefinition.getDeploymentId());
        }
    }
}
