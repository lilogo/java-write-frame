package com.writeframe.设计模式.适配器模式.testdemo2;



/**
 * @ClassName LoginForQQAdapter
 * @Description 适配器
 */
public class LoginForQQAdapter extends AbstractAdapter {
    @Override
    public boolean support(Object adapter) {
        return adapter instanceof LoginForQQAdapter;
    }

}






