package com.lean.flowable.entity;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 动态人员、组
 */
@Data
public class FlowNextModel implements Serializable {

    private String type;

    private String vars;

    private List<String> userList;

    private List<String> roleList;
}
