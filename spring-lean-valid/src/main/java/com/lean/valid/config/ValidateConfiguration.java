package com.lean.valid.config;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * @AssertTrue 用于boolean字段，该字段只能为true
 * @AssertFalse 用于boolean字段，该字段只能为false
 * @CreditCardNumber 对信用卡号进行一个大致的验证
 * DecimalMax	只能小于或等于该值
 * @DecimalMin 只能大于或等于该值
 * @Email 检查是否是一个有效的email地址
 * @Future 检查该字段的日期是否是属于将来的日期
 * @Length(min=,max=) 检查所属的字段的长度是否在min和max之间, 只能用于字符串
 * @Max 该字段的值只能小于或等于该值
 * @Min 该字段的值只能大于或等于该值
 * @NotNull 不能为null
 * @NotBlank 不能为空，检查时会将空格忽略
 * @NotEmpty 不能为空，这里的空是指空字符串
 * @Pattern(regex=) 被注释的元素必须符合指定的正则表达式
 * @URL(protocol=,host,port) 检查是否是一个有效的URL，如果提供了protocol,host等，则该URL还需满足提供的条件
 */
@Configuration
public class ValidateConfiguration {
    @Bean
    public Validator validator(){
        ValidatorFactory validFactory = Validation.byProvider(HibernateValidator.class)
                .configure()
                //设置快速失败返回模式
                .addProperty("hibernate.validator.fail_fast","true")
                .buildValidatorFactory();
        return validFactory.getValidator();
    }

    //开启快速返回，参数校验有异常直接抛出不进入controller中，使用全局异常拦截
    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor(){
        MethodValidationPostProcessor postProcessor = new MethodValidationPostProcessor();
        postProcessor.setValidator(validator());
        return postProcessor;
    }
}


