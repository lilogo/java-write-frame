package com.lean.quartz.utils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson2.JSON;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.BeanUtils;

import com.lean.quartz.base.ExceptionUtils;
import com.lean.quartz.entity.ScheduleJobEntity;
import com.lean.quartz.entity.ScheduleJobLogEntity;
import com.lean.quartz.enums.ScheduleStatusEnum;
import com.lean.quartz.service.ScheduleJobLogService;

import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

@Slf4j
public abstract class AbstractScheduleJob implements Job {
    private static final ThreadLocal<Date> threadLocal = new ThreadLocal<>();

    @Override
    public void execute(JobExecutionContext context) {
        Object value = context.getMergedJobDataMap().get(ScheduleUtils.JOB_PARAM_KEY);
        ScheduleJobEntity scheduleJob = JSON.parseObject((String) value, ScheduleJobEntity.class);
        try {
            threadLocal.set(new Date());
            doExecute(scheduleJob);
            saveLog(scheduleJob, null);
        } catch (Exception e) {
            log.error("任务执行失败，任务ID：{}", scheduleJob.getId(), e);
            saveLog(scheduleJob, e);
        }
    }

    /**
     * 执行spring bean方法
     */
    protected void doExecute(ScheduleJobEntity scheduleJob) throws Exception {
        log.info("准备执行任务，任务ID：{}", scheduleJob.getId());

        Object bean = SpringUtil.getBean(scheduleJob.getBeanName());
        List<Object[]> methodParams = getMethodParams(scheduleJob.getParams());
        if (CollectionUtils.isEmpty(methodParams)) {
            Method method = bean.getClass().getDeclaredMethod(scheduleJob.getMethod());
            method.invoke(bean);
        } else {
            Method method = bean.getClass().getDeclaredMethod(scheduleJob.getMethod(), getMethodParamsType(methodParams));
            method.invoke(bean, getMethodParamsValue(methodParams));
        }
        log.info("任务执行完毕，任务ID：{}", scheduleJob.getId());
    }

    /**
     * 保存 log
     */
    protected void saveLog(ScheduleJobEntity scheduleJob, Exception e) {
        Date startTime = threadLocal.get();
        threadLocal.remove();

        // 执行总时长
        long times = System.currentTimeMillis() - startTime.getTime();

        // 保存执行记录
        ScheduleJobLogEntity log = new ScheduleJobLogEntity();
        log.setJobId(scheduleJob.getId());
        log.setJobName(scheduleJob.getJobName());
        log.setJobGroup(scheduleJob.getJobGroup());
        log.setBeanName(scheduleJob.getBeanName());
        log.setMethod(scheduleJob.getMethod());
        log.setParams(scheduleJob.getParams());
        log.setTimes(times);
        log.setCreateTime(new Date());
        if (e != null) {
            log.setStatus(ScheduleStatusEnum.PAUSE.getValue());
            String error = StringUtils.substring(ExceptionUtils.getExceptionMessage(e), 0, 2000);
            log.setError(error);
        } else {
            log.setStatus(ScheduleStatusEnum.NORMAL.getValue());
        }

        // 保存日志
        SpringUtil.getBean(ScheduleJobLogService.class).save(log);
    }

    /**
     * 获取参数类型
     *
     * @param methodParams 参数相关列表
     * @return 参数类型列表
     */
    public static Class<?>[] getMethodParamsType(List<Object[]> methodParams) {
        Class<?>[] classs = new Class<?>[methodParams.size()];
        int index = 0;
        for (Object[] os : methodParams) {
            classs[index] = (Class<?>) os[1];
            index++;
        }
        return classs;
    }

    /**
     * 获取参数值
     *
     * @param methodParams 参数相关列表
     * @return 参数值列表
     */
    public static Object[] getMethodParamsValue(List<Object[]> methodParams) {
        Object[] classs = new Object[methodParams.size()];
        int index = 0;
        for (Object[] os : methodParams) {
            classs[index] = (Object) os[0];
            index++;
        }
        return classs;
    }


    /**
     * 获取method方法参数相关列表
     *
     * @param invokeTarget 目标字符串 多个参数
     * @return method方法相关参数列表
     */
    public static List<Object[]> getMethodParams(String invokeTarget) {
        Map<String, Object> paramMap = JSON.parseObject(invokeTarget, Map.class);
        if (paramMap == null) {
            return null;
        }
        List<Object> paramValues = paramMap.values().stream().collect(Collectors.toList());
        List<Object[]> classs = new LinkedList<>();
        for (int i = 0; i < paramValues.size(); i++) {
            if (paramValues.get(i) instanceof String) {
                classs.add(new Object[]{paramValues.get(i), String.class});
            }
            if (paramValues.get(i) instanceof Boolean) {
                classs.add(new Object[]{paramValues.get(i), Boolean.class});
            }
            if (paramValues.get(i) instanceof Long) {
                classs.add(new Object[]{paramValues.get(i), Long.class});
            }
            if (paramValues.get(i) instanceof Double) {
                classs.add(new Object[]{paramValues.get(i), Double.class});
            }
            if (paramValues.get(i) instanceof Integer) {
                classs.add(new Object[]{paramValues.get(i), Integer.class});
            }
        }
        return classs;
    }
}
