package com.writeframe.设计模式.原型模式;

/**
 * 原型模式其实在开发中很常见。比如：我们对一个对象设值时，通常的做法：
 * 先new 一个实例，然后调用set方法，传参另一个对象【原型】，调用get方法进行设值
 *这里copy方法，就用到的原型模式。【原来的对象，生成另一个对象】
 * 但是，它有一些问题：
 * 　　》代码使用大量get,set转换。重复度很高。
 * 　　》代码不够优雅。
 * 　　》其实是一种硬编码形式，当增加一个字段时，就必须得修改copy方法。
 *
 */
public class ExamPaper {
    private Long id;
    private String name;

    public ExamPaper copy (){
        ExamPaper examPaper = new ExamPaper();
        examPaper.setId(this.getId());
        examPaper.setName(this.getName());
        //....
        return examPaper;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
