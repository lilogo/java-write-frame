package com.writeframe.设计模式.适配器模式.interfaceadapter;


/**
 * @ClassName Target
 * @Description 目标对象
 * 说明：
 * 　　可以看到，新增目标对象需要很多方法，并且都与适配原角色有关联。
 *
 * 　　因此，把这些方法，统一放到一个目标接口中，而不是定义多个目标类。
 *
 */
public interface Target {
    int request0();
    int request1();
    int request2();
    int request3();
    int request4();
}
