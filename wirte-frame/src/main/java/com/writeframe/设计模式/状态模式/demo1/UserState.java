package com.writeframe.设计模式.状态模式.demo1;

/**
 * @ClassName UserState
 * @Description 用户状态
 */
public abstract class UserState {
    //抽象类引入context
    protected AppContext context;

    public void setContext(AppContext context) {
        this.context = context;
    }

    //收藏功能
    public abstract void favorite();
    //评论
    public abstract void comment(String comment);

}
