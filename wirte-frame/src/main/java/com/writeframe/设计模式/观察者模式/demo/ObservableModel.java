package com.writeframe.设计模式.观察者模式.demo;

/**
 * 观察者模式【Observer Pattern】,又叫发布-订阅【Publish/Subcribe】模式，模型-视图【Model/View】模式、
 * 源监听器【Source/Listener】模式，从属者【Dependents】模式。
 * 　　定义一种一对多的关系，一个主题对象可被多个观察者对象同时监听，使得每当主题对象状态变化时，所有
 * 依赖它的对象都会得到通知并被自动更新。属于行为型模式。
 *
 * 观察者模式在生活场景中的应用
 * 　　1.App角标通知
 * 　　2.起床闹钟设置
 *
 *观察者模式适用场景
 * 1.当一个抽象模型包含两个方面内容，其中一个方面依赖于另一个方面。
 * 2.其他一个或多个对象的变化依赖于另一个对象的变化。
 * 3.实现类似广播机制的功能，无需知道具体收听者，只需分发广播，系统中感兴趣的对象会自动接收该广播。
 * 4.多层嵌套使用，形成一种链式触发机制，使得事件具备跨域（跨越两种观察者类型）通知。
 *
 */
public class ObservableModel {
    public static void main(String[] args) {
        GPer gper = GPer.getInstance();
        Teacher tom = new Teacher("tom");
        Teacher jerry = new Teacher("Jerry");

        gper.addObserver(tom);
        gper.addObserver(jerry);

        //用户行为
        Question question = new Question();
        question.setUserName("张三");
        question.setContent("观察者模式适用于哪些场景？");
        gper.publishQuestion(question);
    }
}
