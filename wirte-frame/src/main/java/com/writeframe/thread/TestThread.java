package com.writeframe.thread;

/**
 * @ClassName: TestThread
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/4/29 13:15
 */
public class TestThread {
    public static void main(String[] args) {
        ThreadPoolExector myPool = new ThreadPoolExector(3, 50);
        for (int i = 0; i < 50; i++) {
            myPool.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    System.out.println("task :" + Thread.currentThread().getName() + " end...");
                }
            });
        }
    }
}
