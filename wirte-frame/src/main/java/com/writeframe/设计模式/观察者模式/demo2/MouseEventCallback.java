package com.writeframe.设计模式.观察者模式.demo2;

/**
 * @ClassName MouseCallback
 * @Description 事件响应，回调，观察者
 */
public class MouseEventCallback {
    public void onClick(Event event){
        System.out.println("=============触发鼠标单击事件========\n"+event);
    }

    public void onMove(Event event){
        System.out.println("触发鼠标双击事件");
    }

}
