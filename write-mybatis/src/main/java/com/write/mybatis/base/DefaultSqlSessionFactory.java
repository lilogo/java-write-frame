package com.write.mybatis.base;


import java.io.InputStream;

public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private InputStream input;

    public void setInput(InputStream input) {
        this.input = input;
    }

    /**
     * 创建一个新的SqlSession对象
     * @return
     */
    @Override
    public SqlSession openSession() {
        DefaultSqlSession sqlSession = new DefaultSqlSession();
        //调用 xml解析器解析.xml文件
        XMLConfigBuilder.loadConfiguration(sqlSession, input);
        return sqlSession;
    }
}
