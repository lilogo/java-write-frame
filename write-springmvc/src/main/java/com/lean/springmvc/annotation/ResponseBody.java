package com.lean.springmvc.annotation;

import java.lang.annotation.*;


@Target(ElementType.METHOD)//元注解目标位置：方法
@Retention(RetentionPolicy.RUNTIME)//元注解作用范围：运行时
@Documented
public @interface ResponseBody {
    String value() default "";
}
