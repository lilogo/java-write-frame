package com.writeframe.设计模式.适配器模式.testdemo2;

/**
 * @ClassName loginForWeChatAdapter
 * @Description 第三方登录，微信登录
 */
public class LoginForWeChatAdapter extends AbstractAdapter {
    @Override
    public boolean support(Object adapter) {
        return adapter instanceof LoginForWeChatAdapter;
    }
}
