package com.writeframe.tomcat.nio;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: ServletMappingConfig
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/5 15:39
 */
public class ServletMappingConfig {

    public static List<ServletMapping> servletMappingList = new ArrayList<>();

    static {
        servletMappingList.add(new ServletMapping("helloWorld","/world","com.writeframe.tomcat.nio.HelloWorldServlet"));
    }

}
