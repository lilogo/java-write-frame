package com.writeframe.设计模式.解释器模式;

/**
 * @ClassName DivInterpreter
 * @Description 除法解释器
 */
public class DivInterpreter extends Interpreter{
    public DivInterpreter(IArithmeticInterpreter left, IArithmeticInterpreter right) {
        super(left, right);
    }

    @Override
    public int interpret() {
        return this.left.interpret() / this.right.interpret();
    }
}
