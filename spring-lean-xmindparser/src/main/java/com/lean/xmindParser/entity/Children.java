package com.lean.xmindParser.entity;
import lombok.Data;
import java.util.List;
@Data
public class Children {
    private List<TopicText> attached;
}
