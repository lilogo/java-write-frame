<#assign base = request.contextPath />
<!DOCTYPE html>
<html>
<head>
    <!-- 页面meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ONLINE_MALL</title>
    <meta name="description" content="ONLINE_MALL">
    <meta name="keywords" content="ONLINE_MALL">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport">
    <!-- 引入样式 -->
    <link rel="stylesheet" href="/plugins/elementui/index.css">
    <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body class="hold-transition">
<div id="app">
    <div class="content-header">
        <h1>SYS_INFO
            <small>平台运行情况</small>
        </h1>

    </div>
    <div class="filter-container">
        <el-button type="primary" @click="serverInfomation()" round>获取最新数据</el-button>
    </div>

    <template>
        <div class="app-container">
            <el-row>
                <el-col :span="12" class="card-box">
                    <el-card>
                        <div slot="header"><span>CPU</span></div>
                        <div class="el-table el-table--enable-row-hover el-table--medium">
                            <table cellspacing="0" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th class="is-leaf">
                                        <div class="cell">属性</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">值</div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="cell">核心数</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.cpu">{{ serverData.cpu.cpuNum }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">用户使用率</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.cpu">{{ serverData.cpu.used }}%</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">系统使用率</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.cpu">{{ serverData.cpu.sys }}%</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">当前空闲率</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.cpu">{{ serverData.cpu.free }}%</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </el-card>
                </el-col>

                <el-col :span="12" class="card-box">
                    <el-card>
                        <div slot="header"><span>内存</span></div>
                        <div class="el-table el-table--enable-row-hover el-table--medium">
                            <table cellspacing="0" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th class="is-leaf">
                                        <div class="cell">属性</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">内存</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">JVM</div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="cell">总内存</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.mem">{{ serverData.mem.total }}G</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.total }}M</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">已用内存</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.mem">{{ serverData.mem.used}}G</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.used}}M</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">剩余内存</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.mem">{{ serverData.mem.free }}G</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.free }}M</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">使用率</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.mem"
                                             :class="{'text-danger': serverData.mem.usage > 80}">{{ serverData.mem.usage
                                            }}%
                                        </div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm"
                                             :class="{'text-danger': serverData.jvm.usage > 80}">{{ serverData.jvm.usage
                                            }}%
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </el-card>
                </el-col>

                <el-col :span="24" class="card-box">
                    <el-card>
                        <div slot="header">
                            <span>服务器信息</span>
                        </div>
                        <div class="el-table el-table--enable-row-hover el-table--medium">
                            <table cellspacing="0" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="cell">服务器名称</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.sys">{{ serverData.sys.computerName }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">操作系统</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.sys">{{ serverData.sys.osName }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">服务器IP</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.sys">{{ serverData.sys.computerIp }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">系统架构</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.sys">{{ serverData.sys.osArch }}</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </el-card>
                </el-col>

                <el-col :span="24" class="card-box">
                    <el-card>
                        <div slot="header">
                            <span>Java虚拟机信息</span>
                        </div>
                        <div class="el-table el-table--enable-row-hover el-table--medium">
                            <table cellspacing="0" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="cell">Java名称</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.name }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">Java版本</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.version }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="cell">启动时间</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.startTime }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">运行时长</div>
                                    </td>
                                    <td>
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.runTime }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">
                                        <div class="cell">安装路径</div>
                                    </td>
                                    <td colspan="3">
                                        <div class="cell" v-if="serverData.jvm">{{ serverData.jvm.home }}</div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1">
                                        <div class="cell">项目路径</div>
                                    </td>
                                    <td colspan="3">
                                        <div class="cell" v-if="serverData.sys">{{ serverData.sys.userDir }}</div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </el-card>
                </el-col>

                <el-col :span="24" class="card-box">
                    <el-card>
                        <div slot="header">
                            <span>磁盘状态</span>
                        </div>
                        <div class="el-table el-table--enable-row-hover el-table--medium">
                            <table cellspacing="0" style="width: 100%;">
                                <thead>
                                <tr>
                                    <th class="is-leaf">
                                        <div class="cell">盘符路径</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">文件系统</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">盘符类型</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">总大小</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">可用大小</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">已用大小</div>
                                    </th>
                                    <th class="is-leaf">
                                        <div class="cell">已用百分比</div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody v-if="serverData.sysFiles">
                                <tr v-for="sysFile in serverData.sysFiles">
                                    <td>
                                        <div class="cell">{{ sysFile.dirName }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">{{ sysFile.sysTypeName }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">{{ sysFile.typeName }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">{{ sysFile.total }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">{{ sysFile.free }}</div>
                                    </td>
                                    <td>
                                        <div class="cell">{{ sysFile.used }}</div>
                                    </td>
                                    <td>
                                        <div class="cell" :class="{'text-danger': sysFile.usage > 80}">{{ sysFile.usage
                                            }}%
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </el-card>
                </el-col>
            </el-row>
        </div>
    </template>
</div>
<!-- 为ECharts准备一个具备大小（宽高）的Dom -->
<div id="main" style="width: 600px;height:400px;"></div>
</body>
<!-- 引入组件库 -->
<script type="text/javascript" src="/js/vue.js"></script>
<script type="text/javascript" src="/plugins/elementui/index.js"></script>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/axios-0.18.0.js"></script>
<script type="text/javascript" src="${base}/plugins/echarts/echarts.js"></script>
<script>
    var vue = new Vue({
        el: '#app',
        data: {
            serverData: {}
        },
        // 加载层信息
        loading: [],
        // 服务器信息
        server1: [],
        name: "sysInfo",
        //钩子函数，VUE对象初始化完成后自动执行
        created() {
            this.serverInfomation();
        },
        methods: {
            serverInfomation() {
                const loading = this.$loading({
                    lock: true,
                    text: 'Loading',
                    spinner: 'el-icon-loading',
                    background: 'rgba(0, 0, 0, 0.7)'
                });
                axios.get("/system/server").then((response) => {
                    if (response.data.flag) {
                        this.$message.success(response.data.message);
                        this.serverData = response.data.data;
                        this.echartsData(this.serverData.memNameList);
                    } else {
                        this.$message.error(response.data.message);
                    }
                    loading.close();
                })
            },
            echartsData(menData) {
                console.log(menData)
                var charts = echarts.init(document.getElementById('main'))
                charts.setOption({
                    tooltip: {
                        trigger: 'item'
                    },
                    legend: {
                        top: '5%',
                        left: 'center'
                    },
                    series: [
                        {
                            name: 'total',
                            type: 'pie',
                            radius: ['40%', '70%'],
                            avoidLabelOverlap: false,
                            itemStyle: {
                                borderRadius: 10,
                                borderColor: '#fff',
                                borderWidth: 2
                            },
                            label: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                label: {
                                    show: true,
                                    fontSize: '40',
                                    fontWeight: 'bold'
                                }
                            },
                            labelLine: {
                                show: false
                            },
                            data: menData
                        }
                    ]
                })
            }
        }
    });
</script>
</body>
</html>

</html>
