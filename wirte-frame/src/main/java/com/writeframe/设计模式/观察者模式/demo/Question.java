package com.writeframe.设计模式.观察者模式.demo;

/**
 * @ClassName Question
 * @Description 问题
 */
public class Question {
    //问题发布者
    private String userName;
    //内容
    private String content;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserName() {
        return userName;
    }

    public String getContent() {
        return content;
    }
}
