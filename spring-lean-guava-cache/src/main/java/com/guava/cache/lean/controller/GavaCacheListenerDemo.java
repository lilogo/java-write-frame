package com.guava.cache.lean.controller;


import com.google.common.cache.*;

import java.util.concurrent.Executors;

public class GavaCacheListenerDemo {

    public static void main(String[] args) {
        CacheLoader<String, String> cacheLoader = CacheLoader.from(String::toUpperCase);
        RemovalListener<String, String> listener = (notification) -> {
            if (notification.wasEvicted()) {
                RemovalCause cause = notification.getCause();
                System.out.println("remove cacase is:" + cause.toString());
                System.out.println("key:" + notification.getKey() + "value:" + notification.getValue());
            }
        };
        //同步监听
        LoadingCache<String, String> loadingCache = CacheBuilder.newBuilder()
                .maximumSize(4)
                //并发设置
                .concurrencyLevel(Runtime.getRuntime().availableProcessors())
                //同步监听
//                .removalListener(listener)
                //异步监控
                .removalListener(RemovalListeners.asynchronous(listener, Executors.newSingleThreadExecutor()))
                .build(cacheLoader);

        loadingCache.getUnchecked("a");
        loadingCache.getUnchecked("b");
        loadingCache.getUnchecked("c");
        loadingCache.getUnchecked("d");//容量是4 吵了自动清除，监听程序清除是单线程。
        loadingCache.getUnchecked("e");
    }
}
