package com.lean.sms.strategy;

import cn.hutool.core.map.MapUtil;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.lean.sms.properties.SmsProperties;
import com.lean.sms.utils.Constant;
import com.lean.sms.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 阿里云短信
 *https://help.aliyun.com/document_detail/71160.htm?spm=a2c4g.11186623.0.0.51d99373cLZqvl
 * @author 
 */
@Slf4j
public class AliyunSmsStrategy extends SmsStrategy {

    private final Client client;

    public AliyunSmsStrategy(SmsProperties smsProperties) {
        this.smsProperties = smsProperties;

        try {
            Config config = new Config();
            // 您的AccessKey ID
            config.setAccessKeyId(smsProperties.getConfig().getAccessKey());
            // 您的AccessKey Secret
            config.setAccessKeySecret(smsProperties.getConfig().getSecretKey());
            config.endpoint = "dysmsapi.aliyuncs.com";

            this.client = new Client(config);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void send(String mobile, Map<String, String> params) {
        SendSmsRequest request = new SendSmsRequest();
        request.setSignName(smsProperties.getAliyun().getSignName());
        request.setTemplateCode(smsProperties.getAliyun().getTemplateId());
        request.setPhoneNumbers(mobile);
        //设置模板参数
        if (MapUtil.isNotEmpty(params)) {
            request.setTemplateParam(JsonUtils.toJsonString(params));
        }
        try {
            // 发送短信
            SendSmsResponse response = client.sendSms(request);
            // 发送失败
            if (!Constant.OK.equalsIgnoreCase(response.getBody().getCode())) {
                throw new RuntimeException(response.getBody().getMessage());
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
