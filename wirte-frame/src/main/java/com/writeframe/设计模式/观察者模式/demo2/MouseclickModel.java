package com.writeframe.设计模式.观察者模式.demo2;

public class MouseclickModel {

    public static void main(String[] args) {
        MouseEventCallback callback = new MouseEventCallback();
        Mouse mouse = new Mouse();
        mouse.addListener(MouseEventType.ON_CLICK,callback);

        mouse.click();
    }

}
