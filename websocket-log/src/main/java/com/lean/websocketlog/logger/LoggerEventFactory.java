package com.lean.websocketlog.logger;

import com.lmax.disruptor.EventFactory;

/**
 * @ClassName: LoggerEventFactory
 * @Description: 进程日志事件工厂类
 * @Author: zhanghongwei
 * @Date: 2022/6/17 13:45
 */
public class LoggerEventFactory implements EventFactory<LoggerEvent> {
    @Override
    public LoggerEvent newInstance() {
        return new LoggerEvent();
    }
}
