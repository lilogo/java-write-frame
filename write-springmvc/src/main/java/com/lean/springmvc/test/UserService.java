package com.lean.springmvc.test;

import com.lean.springmvc.annotation.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    public List<User> findUsers(String name) {
        System.out.println("查询的参数是:" + name);
        // 模拟一些数据
        List<User> users = new ArrayList<User>();
        users.add(new User(1, "a", "123"));
        users.add(new User(2, "b", "111"));
        return users;
    }
    public String getUserMessage(String name) {
        return "我是getUserMassage方法:  " + name;
    }
}
