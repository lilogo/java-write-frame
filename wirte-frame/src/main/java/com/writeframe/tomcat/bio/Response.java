package com.writeframe.tomcat.bio;

import java.io.OutputStream;

/**
 * @ClassName: Response
 * @Description:
 * @Author: zhanghongwei
 * @Date: 2022/5/5 15:55
 */
public class Response {

    OutputStream outputStream;

    public Response(OutputStream outputStream) {
        this.outputStream=outputStream;
    }



    public static String responseData(String content){
        StringBuffer httpResponse = new StringBuffer();
        httpResponse.append("HTTP/1.1 200 OK\n")
                .append("Content-type:text/html\n")
                .append("\r\n")
                .append("<html><body>")
                .append(content)
                .append("</body></html>");
        return httpResponse.toString();
    }

}
