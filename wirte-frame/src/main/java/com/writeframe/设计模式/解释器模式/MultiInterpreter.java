package com.writeframe.设计模式.解释器模式;

/**
 * @ClassName MultiInterpreter
 * @Description 乘法解释器
 */
public class MultiInterpreter extends Interpreter {
    public MultiInterpreter(IArithmeticInterpreter left, IArithmeticInterpreter right) {
        super(left, right);
    }

    @Override
    public int interpret() {
        return this.left.interpret() * this.right.interpret();
    }
}
