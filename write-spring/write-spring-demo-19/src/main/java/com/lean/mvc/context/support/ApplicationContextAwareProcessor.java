package com.lean.mvc.context.support;

import com.lean.mvc.beans.BeansException;
import com.lean.mvc.beans.factory.config.BeanPostProcessor;
import com.lean.mvc.context.ApplicationContext;
import com.lean.mvc.context.ApplicationContextAware;

/**
 *
 *
 *
 *
 *
 *
 */
public class ApplicationContextAwareProcessor implements BeanPostProcessor {

    private final ApplicationContext applicationContext;

    public ApplicationContextAwareProcessor(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof ApplicationContextAware){
            ((ApplicationContextAware) bean).setApplicationContext(applicationContext);
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

}
