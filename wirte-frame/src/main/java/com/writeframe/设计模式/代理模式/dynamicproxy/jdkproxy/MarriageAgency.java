package com.writeframe.设计模式.代理模式.dynamicproxy.jdkproxy;

import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @ClassName MarriageAgency
 * @Description 婚介所 代理类
 */
public class MarriageAgency implements InvocationHandler {

    //jdk动态代理 实现一个抽象的接口【接口可以定义任意功能】

    //加强目标对象功能，需要引入目标对象
    private IPerson person;

    public IPerson getProxy(IPerson person) {
        this.person = person;
        //根据目标对象，得到代理对象
        //jdk底层是基于字节码，需要传参代理类的类加载器，并且代理对象需要实现接口功能【传参接口，实例this】
        Class<? extends IPerson> personClass = person.getClass();
        return (IPerson) Proxy.newProxyInstance(personClass.getClassLoader(), personClass.getInterfaces(), this);
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //加强目标对象的方法
        doBefore();
        Object result = method.invoke(this.person, args);
        doAfter();
        return result;
    }

    private void doAfter() {
        System.out.println("是不是确认关系，开始交往");
    }

    private void doBefore() {
        System.out.println("开始物色对象");
    }


    public static void main(String[] args) {

//        new MarriageAgency().getProxy(new Person()).findLove();
        new MarriageAgency().getProxy(new Hjgs()).findLove();
        //使用工具，获取代理类的代理对象内部实现
        byte[] bytes = ProxyGenerator.generateProxyClass("$Proxy0", new Class[]{IPerson.class});
        try {
            FileOutputStream fos = new FileOutputStream("proxy0.class");
            fos.write(bytes);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
