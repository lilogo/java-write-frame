package com.writeframe.设计模式.适配器模式.testdemo2;

/**
 * @ClassName LoginForTelephoneAdapter
 * @Description TODO
 */
public class LoginForTelephoneAdapter extends AbstractAdapter {
    @Override
    public boolean support(Object adapter) {
        return adapter instanceof LoginForTelephoneAdapter;
    }
}
