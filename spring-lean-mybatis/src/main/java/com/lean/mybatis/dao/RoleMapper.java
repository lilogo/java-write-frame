package com.lean.mybatis.dao;

import com.lean.mybatis.datascope.DataAuthSelect;
import com.lean.mybatis.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.io.Serializable;

@Mapper
public interface RoleMapper {

    @Select(value = "SELECT * FROM t_role WHERE id = #{id} and is_valid = 0")
    @DataAuthSelect
    Role selectByIdAuth(Serializable id);
}
