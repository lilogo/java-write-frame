package com.writeframe.设计模式.责任链模式.loginDemo;

/**
 * 权限校验处理器
 */
public class AuthHandler extends Handler {
    @Override
    public void doHandle(Member member) {
        if (!"管理员".equals(member.getRoleName())) {
            System.out.println("您不是管理员，没有操作权限");
            return;
        }
        System.out.println("允许操作");
    }
}
