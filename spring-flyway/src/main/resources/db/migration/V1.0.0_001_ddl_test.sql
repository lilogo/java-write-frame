UNLOCK TABLES;

CREATE TABLE IF NOT EXISTS `test` (
                                      `id` int NOT NULL AUTO_INCREMENT COMMENT '自增id主键',
                                      `name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '姓名',
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;

UNLOCK TABLES;
