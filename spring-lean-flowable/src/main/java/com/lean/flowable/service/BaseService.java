package com.lean.flowable.service;

import org.flowable.engine.*;
import org.flowable.idm.api.IdmManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BaseService {

    @Autowired
    protected RepositoryService repositoryService;

    @Autowired
    protected RuntimeService runtimeService;

    @Autowired
    protected IdentityService identityService;

    @Autowired
    protected TaskService taskService;

    @Autowired
    protected FormService formService;

    @Autowired
    protected HistoryService historyService;

    @Autowired
    protected ManagementService managementService;

    @Autowired
    protected IdmManagementService idmManagementService;

    @Qualifier("processEngine")
    @Autowired
    protected ProcessEngine processEngine;

    public static final String BPMN_FILE_SUFFIX = ".bpmn";
}
