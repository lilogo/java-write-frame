package com.lean.mongodb.service;


import com.lean.mongodb.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

/*
 * dao层写法二
 * 写法二需要进行实现
 * */
@Service
public class StudentDaoTypeTwo {
    //    使用MongoTemplate模板类实现数据库操作
    @Autowired
    private MongoTemplate mongoTemplate;

    //    增加一位学生
    public void addOneStudent(Student student) {
        mongoTemplate.save(student);

    }

    //    根据id删除一位学生
    public void deleteOneStudentByStudentId(String studentId) {
        Student student = mongoTemplate.findById(studentId, Student.class);
        if (student != null) {
            mongoTemplate.remove(student);
        }

    }

    //    修改一位学生的信息
    public void updateOneStudent(Student student) {
        mongoTemplate.save(student);
    }

    //    根据主键id获取一名学生
    public Student getOneStudentByStudentId(String studentId) {
        return mongoTemplate.findById(studentId, Student.class);
    }

    //    获取全部学生
    public List<Student> getAllStudent() {
        return mongoTemplate.findAll(Student.class);
    }

    /**
     * 查询一个精确匹配
     *
     * @param studentId
     * @return
     */
    public Student getStudentById(String studentId) {
        Query query = new Query(Criteria.where("_id").is(studentId));
        Student one = mongoTemplate.findOne(query, Student.class);
        return one;
    }

    /**
     * 查询集合-模糊匹配
     */
    public List<Student> findlike(String userName) {
        Pattern pattern = Pattern.compile("^.*" + userName.trim() + ".*$", Pattern.CASE_INSENSITIVE);
        Query query = new Query(Criteria.where("username").regex(pattern));
        List<Student> studentList = mongoTemplate.find(query, Student.class);
        return studentList;
    }

    /**
     * 查询集合-精确匹配
     */
    public List<Student> findmore(String userName) {
        Query query = new Query(Criteria.where("username").is(userName));
        List<Student> students = mongoTemplate.find(query, Student.class);
        return students;
    }

    /**
     * 倒叙排列查询
     */
    public List<Student> findtime() {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, "timer"));
        List<Student> students = mongoTemplate.find(query, Student.class);
        return students;
    }

    /**
     * MongoDB分页查询
     */
    public List<Student> findtimeByPage() {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, "timer"));
        Integer pageNum = 1;
        Integer pageSize = 1;
        query.skip((pageNum - 1) * pageSize).limit(pageSize);
        List<Student> students = mongoTemplate.find(query, Student.class);
        return students;
    }


}



