/*
 Navicat Premium Data Transfer

 Source Server         : 81.69.24.201
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : 81.69.24.201:3306
 Source Schema         : operate-cms

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 23/12/2022 15:47:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for op_adver
-- ----------------------------
DROP TABLE IF EXISTS `op_adver`;
CREATE TABLE `op_adver`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `adver_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `adver_click_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `article_id` int(32) NULL DEFAULT NULL,
  `tag_id` int(32) NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `start_time` datetime NULL DEFAULT NULL,
  `end_time` datetime NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_adver
-- ----------------------------

-- ----------------------------
-- Table structure for op_article
-- ----------------------------
DROP TABLE IF EXISTS `op_article`;
CREATE TABLE `op_article`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `article_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `article_category_id` int(32) NULL DEFAULT NULL,
  `article_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `article_content` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `num` int(32) NULL DEFAULT NULL,
  `article_desc` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `article_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `article_publish_time` datetime NULL DEFAULT NULL,
  `is_like` int(1) NULL DEFAULT 0,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT 0,
  `is_top` int(1) NULL DEFAULT 0,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_article
-- ----------------------------

-- ----------------------------
-- Table structure for op_article_tags
-- ----------------------------
DROP TABLE IF EXISTS `op_article_tags`;
CREATE TABLE `op_article_tags`  (
  `article_id` int(32) NOT NULL,
  `tag_id` int(32) NULL DEFAULT NULL,
  PRIMARY KEY (`article_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_article_tags
-- ----------------------------

-- ----------------------------
-- Table structure for op_category
-- ----------------------------
DROP TABLE IF EXISTS `op_category`;
CREATE TABLE `op_category`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `parent_id` int(32) NULL DEFAULT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `category_pinyin` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `category_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `category_desc` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `is_recomend` int(1) NULL DEFAULT 0,
  `sort_num` int(1) NULL DEFAULT NULL,
  `icon_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `click_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_category
-- ----------------------------

-- ----------------------------
-- Table structure for op_friendship_links
-- ----------------------------
DROP TABLE IF EXISTS `op_friendship_links`;
CREATE TABLE `op_friendship_links`  (
  `id` int(32) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `friend_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '友情链接名称',
  `friend_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_german2_ci NULL DEFAULT NULL COMMENT '点击路径',
  `status` int(1) NULL DEFAULT -1 COMMENT '1:上架,-1:下架',
  `sort` int(3) NULL DEFAULT 0 COMMENT '排序',
  `is_apply` int(1) NULL DEFAULT 0 COMMENT '0:待审核,1:审核通过 ,2:驳回',
  `create_time` datetime NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT 0 COMMENT '删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_german2_ci COMMENT = '友情链接' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_friendship_links
-- ----------------------------
INSERT INTO `op_friendship_links` VALUES (1, '易优CMS', 'https://www.eyoucms.com/', 1, 0, 1, NULL, 0);
INSERT INTO `op_friendship_links` VALUES (2, '小程序', 'http://www.yiyongtong.com/', 1, 0, 1, NULL, 0);
INSERT INTO `op_friendship_links` VALUES (3, '123', '123', 1, 0, 1, NULL, 0);

-- ----------------------------
-- Table structure for op_person
-- ----------------------------
DROP TABLE IF EXISTS `op_person`;
CREATE TABLE `op_person`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `user_address` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `work_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `qq` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `wx_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `desc_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_person
-- ----------------------------

-- ----------------------------
-- Table structure for op_recommend
-- ----------------------------
DROP TABLE IF EXISTS `op_recommend`;
CREATE TABLE `op_recommend`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `article_id` int(32) NULL DEFAULT NULL,
  `common_id` int(32) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_recommend
-- ----------------------------

-- ----------------------------
-- Table structure for op_region
-- ----------------------------
DROP TABLE IF EXISTS `op_region`;
CREATE TABLE `op_region`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `parent_id` int(32) NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_like` int(1) NULL DEFAULT NULL,
  `type` int(1) NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_region
-- ----------------------------

-- ----------------------------
-- Table structure for op_tags
-- ----------------------------
DROP TABLE IF EXISTS `op_tags`;
CREATE TABLE `op_tags`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_tags
-- ----------------------------

-- ----------------------------
-- Table structure for op_tags_category
-- ----------------------------
DROP TABLE IF EXISTS `op_tags_category`;
CREATE TABLE `op_tags_category`  (
  `op_tags_id` int(32) NOT NULL,
  `op_category_id` int(32) NULL DEFAULT NULL,
  PRIMARY KEY (`op_tags_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_tags_category
-- ----------------------------

-- ----------------------------
-- Table structure for op_web_sites
-- ----------------------------
DROP TABLE IF EXISTS `op_web_sites`;
CREATE TABLE `op_web_sites`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `article_num` int(32) NULL DEFAULT NULL,
  `article_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `wx_desc` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `wx_img` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of op_web_sites
-- ----------------------------

-- ----------------------------
-- Table structure for tb_head_line
-- ----------------------------
DROP TABLE IF EXISTS `tb_head_line`;
CREATE TABLE `tb_head_line`  (
  `head_line_id` int(32) NOT NULL AUTO_INCREMENT,
  `head_line_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `head_line_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `head_line_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `priority` int(1) NULL DEFAULT NULL,
  `type_num` int(32) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT 0,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`head_line_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_head_line
-- ----------------------------
INSERT INTO `tb_head_line` VALUES (1, 'xx', 'http://demoall.zancms.com/26053/a/yunying/140.html', 'http://demoall.zancms.com/26053/uploads/191120/1-191120160252229.jpg', 1, 1, 1, '2020-10-17 01:24:22', '2020-10-17 01:25:24');

-- ----------------------------
-- Table structure for tb_resources
-- ----------------------------
DROP TABLE IF EXISTS `tb_resources`;
CREATE TABLE `tb_resources`  (
  `resources_id` int(32) NOT NULL AUTO_INCREMENT,
  `resources_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `resources_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `resources_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`resources_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_resources
-- ----------------------------

-- ----------------------------
-- Table structure for web_desc
-- ----------------------------
DROP TABLE IF EXISTS `web_desc`;
CREATE TABLE `web_desc`  (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `title_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `desc_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `article_num` int(2) NULL DEFAULT NULL,
  `tag_num` int(2) NULL DEFAULT NULL,
  `resource_num` int(2) NULL DEFAULT NULL,
  `remark_num` int(2) NULL DEFAULT NULL,
  `is_del` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of web_desc
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
