package com.lean.mybatis.service;

import cn.hutool.core.util.StrUtil;
import com.lean.mybatis.datascope.DataScope;
import com.lean.mybatis.entity.UserDetail;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuService {

    /**
     * 获取数据权限部门id
     *
     * @param userId 用户id
     * @param orgId  部门id
     * @return
     */
    private List<Long> getDataScopeList(Long userId, Long orgId) {
        //根据用户ID，获取用户最大的数据范围
        //select max(t1.data_scope) from sys_role t1, sys_user_role t2 where t1.id = t2.role_id and t2.user_id = #{userId} and t1.is_del = 0
        Integer dataScope = 1;
        if (dataScope == null) {
            return new ArrayList<>();
        }
        //数据范围  0：全部数据  1：本部门及子部门数据  2：本部门数据  3：本人数据  4：自定义数据
        if (dataScope == 0) {
            // 全部数据权限，则返回null
            return null;
        } else if (dataScope == 1) {
            // 本部门及子部门数据
            List<Long> dataScopeList = new ArrayList<>();
            dataScopeList.add(2L);
            dataScopeList.add(21L);
            return dataScopeList;
        } else if (dataScope == 2) {
            // 本部门数据
            List<Long> dataScopeList = new ArrayList<>();
            dataScopeList.add(orgId);
            return dataScopeList;
        } else if (dataScope == 4) {
            // 自定义数据权限范围
            //select t2.org_id from sys_user_role t1, sys_role_data_scope t2 where t1.user_id = #{userId} and t1.role_id = t2.role_id and t1.is_del = 0
            List<Long> dataScopeList = new ArrayList<>();
            dataScopeList.add(23L);
            return dataScopeList;
        }
        return new ArrayList<>();
    }


    /**
     * 原生SQL 数据权限
     *
     * @param tableAlias 表别名，多表关联时，需要填写表别名
     * @param orgIdAlias 机构ID别名，null：表示org_id
     * @return 返回数据权限
     */
    public DataScope getDataScope(String tableAlias, String orgIdAlias) {
        //模拟当前用户 TODO
        UserDetail user = new UserDetail(10L, 2L, "2");
        // 如果是超级管理员，则不进行数据过滤
        if (user.getSuperAdmin().equals("1")) {
            return null;
        }
        // 如果为null，则设置成空字符串
        if (tableAlias == null) {
            tableAlias = "";
        }
        // 获取表的别名
        if (!StringUtils.isEmpty(tableAlias)) {
            tableAlias += ".";
        }
        StringBuilder sqlFilter = new StringBuilder();
        sqlFilter.append(" (");
        // 数据权限范围
        List<Long> dataScopeList = getDataScopeList(user.getId(), user.getOrgId());
        // 全部数据权限
        if (dataScopeList == null) {
            return null;
        }
        // 数据过滤
        if (dataScopeList.size() > 0) {
            if (StringUtils.isEmpty(orgIdAlias)) {
                orgIdAlias = "org_id";
            }
            sqlFilter.append(tableAlias).append(orgIdAlias);
            sqlFilter.append(" in(").append(StrUtil.join(",", dataScopeList)).append(")");
            sqlFilter.append(" or ");
        }
        // 查询本人数据
        sqlFilter.append(tableAlias).append("create_id").append("=").append(user.getId());
        sqlFilter.append(")");
        return new DataScope(sqlFilter.toString());
    }

}
