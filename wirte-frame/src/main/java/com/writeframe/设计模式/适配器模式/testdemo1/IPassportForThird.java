package com.writeframe.设计模式.适配器模式.testdemo1;


import com.writeframe.设计模式.适配器模式.ResultMsg;

/**
 * 【2】适配器扩展系统　
 * A.目标类【定义为接口】
 * 这里需要实现第三方登录，如：qq登录。就需要使用腾训提供open api【需要传参openId】,如下所示：
 * @ClassName IPassortForThird
 * @Description 第三方登录，目标类
 */
public interface IPassportForThird {
    /**
     * qq登录
     * @param openId
     * @return
     */
    ResultMsg loginForQQ(String openId);

    /**
     * 微信号登录
     * @param openId
     * @return
     */
    ResultMsg loginForWeChat(String openId);

    /**
     * 内部员工token登录
     * @param token
     * @return
     */
    ResultMsg loginForToken(String token);

    /**
     * 手机号+验证码登录
     * @param phone
     * @param code
     * @return
     */
    ResultMsg loginForTelephone(String phone, String code);
}
