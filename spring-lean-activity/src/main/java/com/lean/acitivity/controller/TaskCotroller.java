package com.lean.acitivity.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.lean.acitivity.common.Result;
import lombok.extern.slf4j.Slf4j;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 任务管理
 */
@Slf4j
@RestController
@RequestMapping("/task")
public class TaskCotroller {

    @Autowired
    private TaskService taskService;//任务管理
    @Autowired
    private RuntimeService runtimeService;//执行管理，包括启动、推进、删除流程实例等操作
    @Autowired
    private HistoryService historyService;//历史管理(执行完的数据的管理)

    /**
     * 查询用户待办任务列表
     *
     * @param assignee
     * @return
     */
    @GetMapping("/taskQuery")
    public Result taskQuery(String assignee) {
        //根据流程定义的key,负责人assignee来实现当前用户的任务列表查询
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("myProcess_1")
                .taskAssignee(assignee)
                .orderByTaskCreateTime().desc()
                .list();

        List<Map<String, Object>> res = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (Task task : list) {
                Map<String, Object> map1 = new HashMap<>();
                map1.put("任务ID:", task.getId());
                map1.put("任务名称:", task.getName());
                map1.put("任务的创建时间:", task.getCreateTime());
                map1.put("任务的办理人:", task.getAssignee());
                map1.put("流程实例ID：", task.getProcessInstanceId());
                map1.put("流程定义ID:", task.getProcessDefinitionId());
                map1.put("执行对象ID:", task.getExecutionId());
                map1.put("getOwner:", task.getOwner());
                map1.put("getCategory:", task.getCategory());
                map1.put("getDescription:", task.getDescription());
                map1.put("getFormKey:", task.getFormKey());

                Map<String, Object> map = task.getProcessVariables();
                for (Map.Entry<String, Object> m : map.entrySet()) {
                    log.info("key:" + m.getKey() + " value:" + m.getValue());
                }
                for (Map.Entry<String, Object> m : task.getTaskLocalVariables().entrySet()) {
                    log.info("key:" + m.getKey() + " value:" + m.getValue());
                }

                res.add(map1);
            }
        }
        return Result.ok(res);
    }

    /**
     * @param assignee
     * @return
     */
    @GetMapping("/taskFinished/{assignee}")
    public Result taskFinished(@PathVariable(value = "assignee") String assignee) {
        List<HistoricTaskInstance> list = historyService.createHistoricTaskInstanceQuery()
                .taskAssignee(assignee)
                .finished()
                .list();
        return Result.ok(list);
    }

    /**
     * 用户任务列表
     */
    @GetMapping("/taskList")
    public Result taskList() {
        //根据流程定义的key,负责人assignee来实现当前用户的任务列表查询
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("myProcess_1")
                .orderByTaskCreateTime().desc()
                .list();

        List<Map<String, Object>> res = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (Task task : list) {
                Map<String, Object> map1 = new HashMap<>();
                map1.put("任务ID:", task.getId());
                map1.put("任务名称:", task.getName());
                map1.put("任务的创建时间:", task.getCreateTime());
                map1.put("任务的办理人:", task.getAssignee());
                map1.put("流程实例ID：", task.getProcessInstanceId());
                map1.put("流程定义ID:", task.getProcessDefinitionId());
                map1.put("执行对象ID:", task.getExecutionId());
                map1.put("getOwner:", task.getOwner());
                map1.put("getCategory:", task.getCategory());
                map1.put("getDescription:", task.getDescription());
                map1.put("getFormKey:", task.getFormKey());
                Map<String, Object> map = task.getProcessVariables();
                for (Map.Entry<String, Object> m : map.entrySet()) {
                    log.info("key:" + m.getKey() + " value:" + m.getValue());
                }
                for (Map.Entry<String, Object> m : task.getTaskLocalVariables().entrySet()) {
                    log.info("key:" + m.getKey() + " value:" + m.getValue());
                }
                res.add(map1);
            }
        }
        return Result.ok(res);
    }
}
