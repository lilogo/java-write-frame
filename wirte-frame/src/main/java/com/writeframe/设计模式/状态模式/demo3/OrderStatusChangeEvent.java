package com.writeframe.设计模式.状态模式.demo3;

/**
 * @ClassName OrderStatusChangeEvent
 * @Description 订单状态变更行为状态
 */
public enum OrderStatusChangeEvent {
    //支付，发货，确认收货
    PAYED,DELIVER,RECEIVED;
}
