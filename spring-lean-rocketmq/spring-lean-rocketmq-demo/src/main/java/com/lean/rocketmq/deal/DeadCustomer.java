package com.lean.rocketmq.deal;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 验证私信队列 消费者
 */
public class DeadCustomer {

    public static void main(String[] args) throws Exception {
        /**
         * 推消息消费
         */
        DefaultMQPushConsumer defaultMQPushConsumer = new DefaultMQPushConsumer("consumer_group_topic_name_dle_01");
        // 指定nameserver的地址
        defaultMQPushConsumer.setNamesrvAddr("192.168.16.79:9876");
        //指定主题
        defaultMQPushConsumer.subscribe("topic_name_dle", "*");
        /**
         * 推送消息 提高消费处理能力
         * 1 提高消费并行度
         * 2 以批量方式进行 消费
         * 3 检测延时情况,跳过非重要消息
         */
        //消费限流 只针对推送来设置,拉取消息自己控制
        // 1 提高消费并行度
        defaultMQPushConsumer.setConsumeThreadMax(10);
        defaultMQPushConsumer.setConsumeThreadMin(1);
        // 2 以批量方式进行 消费
        // 设置消息批处理的一个批次中消息的最大个数
        defaultMQPushConsumer.setConsumeMessageBatchMaxSize(10);
        //设置重试次数 默认16次
        defaultMQPushConsumer.setMaxReconsumeTimes(1);
        // 添加消息监听器，一旦有消息推送过来，就进行消费
        defaultMQPushConsumer.setMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs, ConsumeConcurrentlyContext context) {
                //final MessageQueue messageQueue = context.getMessageQueue();
                for (MessageExt msg : msgs) {
                    System.out.println(msg);
                    try {
                        System.out.println(new String(msg.getBody(), "utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
                // 消息消费成功
                //return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                //null  也表示推送失败,会进行重试
                return null;
                // 消息消费失败
//                return ConsumeConcurrentlyStatus.RECONSUME_LATER;
            }
        });
        // 开启消费者消费
        defaultMQPushConsumer.start();
        System.out.println("Consumer Started");
    }
}
