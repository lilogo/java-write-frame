package com.lean.rocketmq.deal;


import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;

/**
 * 验证私信队列 生产者
 */
public class DeadProducer {
    public static void main(String[] args) throws Exception{
        // 实例化生产者，并指定生产组名称
        DefaultMQProducer producer = new DefaultMQProducer("myproducer_group_topic_name_dle_01");
        //设置实例名称，一个jvm中有多个生产者可以根据实例名区分
        //默认default
        producer.setInstanceName("topic_name_dle");
        // 指定nameserver的地址
        producer.setNamesrvAddr("192.168.16.79:9876");
        //设置同步重试次数
        producer.setRetryTimesWhenSendFailed(2);
        //设置异步发送次数
        //producer.setRetryTimesWhenSendAsyncFailed(2);
        // 初始化生产者
        producer.start();
        for (int i = 0; i <10 ; i++) {
            Message message = new Message("topic_name_dle", ("key=" + i).getBytes("utf-8"));
            // 1 同步发送  如果发送失败会根据重试次数重试
            SendResult send = producer.send(message);
            SendStatus sendStatus = send.getSendStatus();
            System.out.println(sendStatus.toString());
        }
    }

}
