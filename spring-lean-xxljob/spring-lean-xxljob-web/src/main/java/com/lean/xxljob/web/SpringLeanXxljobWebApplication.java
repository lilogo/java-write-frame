package com.lean.xxljob.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringLeanXxljobWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringLeanXxljobWebApplication.class, args);
    }

}
