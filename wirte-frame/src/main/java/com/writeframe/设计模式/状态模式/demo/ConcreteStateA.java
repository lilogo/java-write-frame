package com.writeframe.设计模式.状态模式.demo;

/**
 * @ClassName ConcreteStateA
 * @Description 具体状态实现子类A
 */
public class ConcreteStateA implements IState {
    @Override
    public void handle() {
        System.out.println("StateA do action");
    }
}
