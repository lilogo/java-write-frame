package com.writeframe.设计模式.状态模式.demo3;


/**
 * @ClassName OrderStatus
 * @Description 订单状态枚举
 */
public enum OrderStatus {
    //待支付，待发货，待收货，订单结束
    WAIT_PAYMENT,WAIT_DELIVER,WAIT_RECEIVE,FINISH;
}
