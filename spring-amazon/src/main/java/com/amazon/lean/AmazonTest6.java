package com.amazon.lean;

import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

//亚马逊国际站获取商品列表

/**
 * 获取入口
 * 入口来源于商品分类，可以直接使用商品分类中打印出来的地址，比如分类配件和耗材的： https://www.amazon.com/-/zh/s?bbn=16225009011&rh=i%3Aspecialty-aps%2Cn%3A!16225009011%2Cn%3A281407&ref_=nav_em__nav_desktop_sa_intl_accessories_and_supplies_0_2_5_2
 * 【说明】国际站的获取商品次数过多后会出现验证码，两种方式可以处理。方式1：识别验证码并跳转到对应的网站。 方式2：添加代理，请求部分数量后就切换一下。
 * 验证码方式调整相关代码下篇介绍。
 *
 * 获取下一页网页
 * 因为是分页数据，可以通过标签a-last得到下一页按钮对应的url，每次处理完当页数据后，将下一页的url得到并进行继续处理。
 *
 * 数据解析
 * 每页有24条数据，一般都是显示400页。考虑到太靠后的数据参考意义不大，同时一般50页左右就开始需要验证码，本篇代码只处理了前面的10页。
 * 根据不同的标签获取到对应的元素即可。评价部分稍有不同，可能有的商品还没有人评价，单独处理下。同时部分商品已售罄，加个也会不显示。
 */
public class AmazonTest6 {

    public static void main(String[] args) throws Exception {
        String url = "https://www.amazon.com/-/zh/s?bbn=16225009011&rh=i%3Aspecialty-aps%2Cn%3A%2116225009011%2Cn%3A281407&ref_=nav_em__nav_desktop_sa_intl_accessories_and_supplies_0_2_5_2";
        for (int i = 1; i <= 10; i++) {
            CookieStore store = new BasicCookieStore();
            CloseableHttpClient httpclient = HttpClients.custom().setDefaultCookieStore(store).build();
            System.out.println(url);
            url = printInfo(url, httpclient);
        }

    }

    static int i = 1;

    // 返回的是下一页的url
    static String printInfo(String url, CloseableHttpClient httpclient) throws Exception {
        HttpGet get = new HttpGet(url);
        get.addHeader("accept-language", "zh-CN,zh;q=0.9,en;q=0.8");
        get.addHeader("user-agent",
                "Mozilla/5.0(Macintosh;IntelMacOSX10_13_4)AppleWebKit/537.36(KHTML,likeGecko)Chrome/81.0.4044.138Safari/537.36");
        CloseableHttpResponse rese = httpclient.execute(get);
        String redsa = EntityUtils.toString(rese.getEntity());
        Document doc = Jsoup.parse(redsa);
        Elements goodsEles = doc.getElementsByClass("sg-col-4-of-12 s-result-item s-asin sg-col-4-of-16 sg-col s-widget-spacing-small sg-col-4-of-20");
        for (Element goodsEle : goodsEles) {
            System.out.println("商品" + i++);

            String detailUrl = "https://www.amazon.cn" + goodsEle.getElementsByTag("a").first().attr("href");
            System.out.println("商品详情：" + detailUrl);

            String asin = goodsEle.attr("data-asin");
            System.out.println("ASIN：" + asin);

            String uuid = goodsEle.attr("data-uuid");
            System.out.println("UUID：" + uuid);

            String img = goodsEle.getElementsByTag("img").first().attr("src");
            System.out.println("封面图片：" + img);

            String name = goodsEle.getElementsByTag("h2").first().text();
            System.out.println("名称：" + name);

            Element starEle = goodsEle.getElementsByClass("a-icon-alt").first();
            if (starEle != null) {
                String star = starEle.text();
                System.out.println("评分：" + star);

                String count = goodsEle.getElementsByClass("a-section a-spacing-none a-spacing-top-micro").first()
                        .getElementsByClass("a-size-base").first().text().replaceAll(",", "");
                System.out.println("评价人数：" + count);
            } else {
                System.out.println("暂无评分");
                System.out.println("评价人数：0");
            }

            Element priceEle = goodsEle.getElementsByClass("a-offscreen").first();
            if (priceEle != null) {
                String price = priceEle.text().replaceAll(",", "");
                System.out.println("价格：" + price);
            } else {
                System.out.println("价格：列表未显示价格，可能无货");
            }

            System.out.println("\n===================================\n");
        }

        String nextUrl = "https://www.amazon.com"
                + doc.getElementsByClass("a-last").first().getElementsByTag("a").first().attr("href");
        return nextUrl;
    }

}

