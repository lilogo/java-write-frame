package com.lean.acitivity.entity;

import lombok.Data;

@Data
public class WorkFlowEntity {

    private String assignee;

    private String instanceId;

    private String content;
}
