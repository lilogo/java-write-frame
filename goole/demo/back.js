function backjs() {
    chrome.management.getAll((res) => {
        alert(JSON.stringify(res));
    });
}


chrome.contextMenus.create({
    title: "自动搜索",
    id: "1",
    contexts: ["selection", "editable"],  //右键菜单项将会在这个列表指定的上下文类型中显示
    onclick: function () {
        chrome.tabs.executeScript(null, {
            code:
                "let ipt = document.getElementById('kw');  ipt.value = '靓仔'; let btn = document.getElementById('su'); btn.click()",
        });
    },
    // documentUrlPatterns: ["https://*.baidu.com/*"],  //这使得右键菜单只在匹配此模式的url页面上生效
});
chrome.contextMenus.create({
    title: "自动搜索靓仔",
    parentId: "1",
    type: "radio",  //右键菜单项的类型
    contexts: ["selection", "editable"],
    onclick: function () {
        chrome.tabs.executeScript(null, {
            code:
                "let ipt = document.getElementById('kw');  ipt.value = '靓仔'; let btn = document.getElementById('su'); btn.click()",
        });
    },
    // documentUrlPatterns: ["https://*.baidu.com/*"],
});
chrome.contextMenus.create({
    title: "自动搜索叼毛",
    parentId: "1",
    type: "radio",
    contexts: ["selection", "editable"],
    onclick: function () {
        chrome.tabs.executeScript(null, {
            code:
                "let ipt = document.getElementById('kw');  ipt.value = '叼毛'; let btn = document.getElementById('su'); btn.click()",
        });
    },
    // documentUrlPatterns: ["https://*.baidu.com/*"],
});
chrome.omnibox.onInputChanged.addListener((text, suggest) => {
    //text为输入的文字
    if (!text) {
        return;
    }
    if (text == "靓仔") {
        suggest([
            {content: text + "无语", description: "你要找”靓仔无语“？"},
            {content: text + "语塞", description: "你要找”靓仔语塞“？"},
            {content: text + "过敏", description: "你要找”靓仔过敏“？"},
        ]);
    }
});
chrome.omnibox.onInputEntered.addListener((text) => {
    if (!text) {
        return;
    }
    let href = "";
    //startWith表示以什么关键字开头
    if (text.startsWith("靓仔")) {
        //表示如果text以“靓仔”开头
        href =
            "https://www.baidu.com/s?ie=utf-8&f=3&rsv_bp=1&rsv_idx=1&tn=baidu&wd=" +
            text;
        openUrl(href);
    }
});
//获取当前页id
function getCurrentTabId(callback) {
    chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        if (callback) {
            callback(tabs.length ? tabs[0].id : null);
        }
    });
}
//打开页面
function openUrl(url) {
    getCurrentTabId((tabId) => {
        chrome.tabs.update(tabId, {url: url});
    });
}

function executeScriptToCurrentTab(scriptcode) {
    getCurrentTabId((tabId) => {
        chrome.tabs.executeScript(tabId, {code: scriptcode});
    });
}

let updateCss = document.getElementById("updateCss");
updateCss.addEventListener("click", () => {
    executeScriptToCurrentTab('document.body.style.fontSize="30px";');
    executeScriptToCurrentTab('document.body.style.backgroundColor="blue";');
});
