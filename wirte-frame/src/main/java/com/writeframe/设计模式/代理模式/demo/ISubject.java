package com.writeframe.设计模式.代理模式.demo;

/**
 * @ClassName ISubject
 * @Description 目标对象的抽象接口
 */
public interface ISubject {
    void request();
}
